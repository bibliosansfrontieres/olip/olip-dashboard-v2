#!/bin/sh

JSON_STRING='window.configs = { \
  "VITE_AXIOS_BASEURL":"'"${VITE_AXIOS_BASEURL}"'", \
  "VITE_DEFAULT_LANGUAGE":"'"${VITE_DEFAULT_LANGUAGE}"'" \
}'

sed -i "s@// CONFIGURATIONS_PLACEHOLDER@${JSON_STRING}@" /app/dist/index.html

exec "$@"