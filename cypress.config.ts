import { defineConfig } from "cypress";
import dotenv from "dotenv";
import { existsSync, rmSync } from "fs";

dotenv.config();

export default defineConfig({
  video: true,
  retries: {
    runMode: 1,
    openMode: 0,
  },
  e2e: {
    baseUrl: process.env.CYPRESS_URL,
    experimentalModifyObstructiveThirdPartyCode: true,
    setupNodeEvents(on) {
      on(
        "after:spec",
        (_spec: Cypress.Spec, results: CypressCommandLine.RunResult) => {
          if (results && results.video) {
            // Do we have failures for any retry attempts?
            const failures = results.tests.some((test) =>
              test.attempts.some((attempt) => attempt.state === "failed"),
            );
            if (!failures && results.video) {
              // delete the video if the spec passed and no tests retried
              existsSync(results.video) && rmSync(results.video);
            }
          }
        },
      );
    },
  },
  env: {
    baseUrl: process.env.CYPRESS_URL,
    apiUrl: process.env.CYPRESS_API_URL,
    adminUsername: process.env.CYPRESS_ADMIN_USERNAME,
    adminPassword: process.env.CYPRESS_ADMIN_PASSWORD,
    userPassword: process.env.CYPRESS_USER_PASSWORD,
  },
});
