###############
# BASE #
###############

FROM node:21-alpine3.19 AS base

# Pnpm configuration
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm add express@4 dotenv@16

COPY package.json .

FROM base AS build

COPY pnpm-lock.yaml .

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile

COPY . .

RUN pnpm run build

####################
# PRODUCTION STAGE #
####################

FROM base AS production

COPY --from=build /app/dist ./dist
COPY --from=build /app/public ./public
COPY --from=build /app/server.js .
COPY entrypoint.sh .

ENV NODE_ENV production

ENTRYPOINT ["/app/entrypoint.sh"]

CMD [ "node", "server.js" ]