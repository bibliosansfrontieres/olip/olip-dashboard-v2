import { createSvgIcon } from "@mui/material/utils";

const CheckboxHoveredIcon = createSvgIcon(
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_i_296_16737)">
      <rect width="24" height="24" rx="4" fill="#FFFFFE" />
    </g>
    <rect x="0.5" y="0.5" width="23" height="23" rx="3.5" stroke="#363636" />
    <defs>
      <filter
        id="filter0_i_296_16737"
        x="0"
        y="0"
        width="24"
        height="24"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB">
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="BackgroundImageFix"
          result="shape"
        />
        <feColorMatrix
          in="SourceAlpha"
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          result="hardAlpha"
        />
        <feOffset />
        <feGaussianBlur stdDeviation="2" />
        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
        <feColorMatrix
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6 0"
        />
        <feBlend
          mode="normal"
          in2="shape"
          result="effect1_innerShadow_296_16737"
        />
      </filter>
    </defs>
  </svg>,
  "CheckboxHovered"
);

export default CheckboxHoveredIcon;
