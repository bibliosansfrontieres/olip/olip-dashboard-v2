import { createSvgIcon } from "@mui/material/utils";

const ChevronDownIcon = createSvgIcon(
  <svg
    width="60"
    height="60"
    viewBox="0 0 60 60"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <path
      d="M57.5 17.5L31.2956 44.4475C31.1255 44.6227 30.9236 44.7616 30.7013 44.8564C30.479 44.9512 30.2407 45 30 45C29.7593 45 29.521 44.9512 29.2987 44.8564C29.0764 44.7616 28.8745 44.6227 28.7044 44.4475L2.5 17.5"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>,
  "Chevron-down"
);

export default ChevronDownIcon;
