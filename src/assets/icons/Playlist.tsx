import { createSvgIcon } from "@mui/material/utils";

const PlaylistIcon = createSvgIcon(
  <svg
    width="60"
    height="60"
    viewBox="0 0 60 60"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <g clipPath="url(#clip0_634_36279)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4 0C1.79086 0 0 1.79086 0 4V39.3333C0 41.5425 1.79086 43.3333 4 43.3333H39.3333C41.5425 43.3333 43.3333 41.5425 43.3333 39.3333V4C43.3333 1.79086 41.5425 0 39.3333 0H4ZM13.3333 55.1919L13.3333 50.3535H43.3838C47.2889 50.3535 50.4545 47.1879 50.4545 43.2828V15H55.2929C57.5021 15 59.2929 16.7909 59.2929 19V55.1919C59.2929 57.4011 57.5021 59.1919 55.2929 59.1919H17.3333C15.1242 59.1919 13.3333 57.4011 13.3333 55.1919Z"
        fill="currentColor"
      />
    </g>
    <defs>
      <clipPath id="clip0_634_36279">
        <rect width="60" height="60" fill="white" />
      </clipPath>
    </defs>
  </svg>,
  "Playlist"
);

export default PlaylistIcon;
