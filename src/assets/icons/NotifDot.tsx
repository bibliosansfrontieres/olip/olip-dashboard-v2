import { createSvgIcon } from "@mui/material/utils";

const NotifDotIcon = createSvgIcon(
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <circle cx="10.0003" cy="9.99996" r="5.83333" fill="currentColor" />
  </svg>,
  "NotifDot"
);

export default NotifDotIcon;
