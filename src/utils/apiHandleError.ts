import axios, { AxiosError } from "axios";
import { t } from "i18next";
import { toast } from "react-toastify";

import getEnv from "./getEnv";

/**
 * Handle an error from an API request.
 *
 * If the error is an AxiosError, it returns an object with an errorMessage and
 * a statusCode.
 *
 * If the error is a CanceledError, it returns an object with an errorMessage
 * 'CanceledError' and a statusCode 400.
 *
 * Otherwise, it returns an object with an errorMessage 'Unknown error' and a
 * statusCode 500.
 *
 * @param {any} error The error object
 * @returns {{ errorMessage: string; statusCode: number }} An object with an errorMessage and a statusCode
 */
export const apiHandleError = (
  error: any,
): { errorMessage: string; statusCode: number } => {
  if (error.name === "CanceledError") {
    return { errorMessage: "CanceledError", statusCode: 400 };
  }
  if (axios.isAxiosError(error)) {
    return {
      errorMessage:
        error?.response?.data.message || error?.response?.statusText,
      statusCode: error?.response?.data.statusCode || error?.response?.status,
    };
  }
  return { errorMessage: "Unknown error", statusCode: 500 };
};

/**
 * Display a toast error with the given translation if the error is not a canceled
 * request.
 * @param {unknown} error The error object
 * @param {string} translation The translation key to display in the toast
 */
export function toastError(error: unknown, translation: string) {
  if ((error as AxiosError)?.code !== "ERR_CANCELED") {
    toast.error(translation);
  }
}

/**
 * Logs the given error and context in the console if PUBLIC_DEBUG is set to true
 * @param {unknown} error The error to log
 * @param {string} [context] The context of the error
 */
export default function consoleError(error: unknown, context?: string) {
  if (getEnv("PUBLIC_DEBUG") === "true") {
    console.error(error, "-----", context);
  }
}

/**
 * Handles API errors by logging the error,
 * and displaying a toast error message.
 *
 * @param {unknown} error - The error object to handle.
 * @param {string} [translation] - Optional translation key to display in the toast.
 * @param {string} [context] - Optional context information for logging.
 */
export function handleApiError(
  error: unknown,
  translation?: string,
  context?: string,
) {
  consoleError(error, context);
  if ((error as AxiosError)?.response?.status === 401) {
    toast.error(t("common.error.unauthorized"));
    return;
  }
  if (translation) {
    toast.error(translation);
    return;
  }
  toast.error(t("common.error.generic"));
  return;
}
