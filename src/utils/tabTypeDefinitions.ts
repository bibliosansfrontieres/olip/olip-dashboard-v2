import { TabTypeEnum } from "src/enums/tabType.enum";
import { CategoryBackOfficeListCategory } from "src/types/category/categoryBackOfficeList.type";

export type TabTypeDefinition = {
  predicate: (category: CategoryBackOfficeListCategory) => boolean;
  index: number;
};

export const tabTypeDefinitions: Record<TabTypeEnum, TabTypeDefinition> = {
  [TabTypeEnum.ALL]: {
    predicate: () => true,
    index: 0,
  },
  [TabTypeEnum.BSF]: {
    predicate: (category) => category.users?.length === 0,
    index: 1,
  },
  [TabTypeEnum.LOCAL]: {
    predicate: (category) => category.users?.length !== 0,
    index: 2,
  },
};

export const compareTabTypeByIndex = (a: TabTypeEnum, b: TabTypeEnum) =>
  tabTypeDefinitions[a].index - tabTypeDefinitions[b].index;

export const getSortedTabTypes = () => {
  return Object.values(TabTypeEnum)
    .sort(compareTabTypeByIndex)
    .map(
      (type) =>
        [type, tabTypeDefinitions[type]] as [TabTypeEnum, TabTypeDefinition],
    );
};
