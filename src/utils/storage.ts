export const getStoragePercentageColor = (percentage: number) => {
  if (percentage > 95) return "error";
  if (percentage >= 80) return "warning";
  return "success";
};
