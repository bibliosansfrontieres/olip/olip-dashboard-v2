import i18next from "i18next";
import { APILanguage } from "src/types";

export function findIndexTranslation(
  translations: { language: APILanguage; languageIdentifier?: string }[],
  options?: {
    language?: string;
    strict?: boolean;
  },
): number {
  const index = translations.findIndex(
    (translation) =>
      (translation.languageIdentifier || translation.language.identifier) ===
      (options?.language || i18next.language),
  );
  if (options?.strict) {
    return index;
  }
  return index !== -1 ? index : 0;
}
