import Battery20Icon from "@mui/icons-material/Battery20";
import Battery30Icon from "@mui/icons-material/Battery30";
import Battery50Icon from "@mui/icons-material/Battery50";
import Battery60Icon from "@mui/icons-material/Battery60";
import Battery80Icon from "@mui/icons-material/Battery80";
import Battery90Icon from "@mui/icons-material/Battery90";
import BatteryAlertIcon from "@mui/icons-material/BatteryAlert";
import BatteryCharging20Icon from "@mui/icons-material/BatteryCharging20";
import BatteryCharging30Icon from "@mui/icons-material/BatteryCharging30";
import BatteryCharging50Icon from "@mui/icons-material/BatteryCharging50";
import BatteryCharging60Icon from "@mui/icons-material/BatteryCharging60";
import BatteryCharging80Icon from "@mui/icons-material/BatteryCharging80";
import BatteryCharging90Icon from "@mui/icons-material/BatteryCharging90";
import BatteryChargingFullIcon from "@mui/icons-material/BatteryChargingFull";
import BatteryFullIcon from "@mui/icons-material/BatteryFull";
import { SvgIconTypeMap } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";

class BatteryPercentageDefinition {
  percentage: number;
  DefaultIcon: OverridableComponent<
    SvgIconTypeMap<NonNullable<unknown>, "svg">
  >;
  ChargingIcon: OverridableComponent<
    SvgIconTypeMap<NonNullable<unknown>, "svg">
  >;
  color?: "success" | "warning" | "error";

  constructor(
    percentage: number,
    DefaultIcon: OverridableComponent<
      SvgIconTypeMap<NonNullable<unknown>, "svg">
    >,
    ChargingIcon: OverridableComponent<
      SvgIconTypeMap<NonNullable<unknown>, "svg">
    >,
    color?: "success" | "warning" | "error",
  ) {
    this.percentage = percentage;
    this.DefaultIcon = DefaultIcon;
    this.ChargingIcon = ChargingIcon;
    this.color = color ?? "success";
  }
}

const batteryPercentageDefinitions: BatteryPercentageDefinition[] = [
  new BatteryPercentageDefinition(
    100,
    BatteryFullIcon,
    BatteryChargingFullIcon,
  ),
  new BatteryPercentageDefinition(90, Battery90Icon, BatteryCharging90Icon),
  new BatteryPercentageDefinition(80, Battery80Icon, BatteryCharging80Icon),
  new BatteryPercentageDefinition(60, Battery60Icon, BatteryCharging60Icon),
  new BatteryPercentageDefinition(50, Battery50Icon, BatteryCharging50Icon),
  new BatteryPercentageDefinition(30, Battery30Icon, BatteryCharging30Icon),
  new BatteryPercentageDefinition(
    5,
    Battery20Icon,
    BatteryCharging20Icon,
    "warning",
  ),
  new BatteryPercentageDefinition(
    0,
    BatteryAlertIcon,
    BatteryCharging20Icon,
    "error",
  ),
];

export const getBatteryPercentageDefinition = (percentage: number) =>
  batteryPercentageDefinitions.find(
    (batteryPercentageDefinition) =>
      batteryPercentageDefinition.percentage <= percentage,
  );
