import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";

import { TableCellProps } from "@mui/material";

export type ColumnCheckbox = {
  checked: boolean;
  disabled: boolean;
  onChange: (categoryPlaylist: CategoryPlaylistBackOffice) => void;
};

export type CellCheckbox = {
  checked: (categoryPlaylist: CategoryPlaylistBackOffice) => boolean;
  disabled: boolean;
  onChange: (categoryPlaylist: CategoryPlaylistBackOffice) => void;
};
type Column = {
  width?: string;
  header_key?: string;
  checkbox?: ColumnCheckbox;
  align?: TableCellProps["align"];
};

export function getColumns(checkbox?: ColumnCheckbox): Column[] {
  const columns: Column[] = [
    { width: "10%" },
    {
      header_key:
        "settings.pages.contents.theme.playlistsCard.table.header.name",
      width: `${checkbox ? "50" : "55"}%`,
    },
    {
      header_key:
        "settings.pages.contents.theme.playlistsCard.table.header.application",
      width: "20%",
    },
    {
      header_key:
        "settings.pages.contents.theme.playlistsCard.table.header.size",
      width: "15%",
    },
  ];

  if (checkbox) {
    columns.push({
      checkbox,
      width: "5%",
    });
  }

  return columns;
}
