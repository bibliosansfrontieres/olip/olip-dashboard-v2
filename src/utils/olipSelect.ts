import { SelectChangeEvent } from "@mui/material";

/**
 * Format the value of a SelectChangeEvent to be an empty string if the value contains an empty string : meaning we chose to remove the filter,
 * otherwise return the value as a string.
 * @param event the event to format : an array with all the selected values
 * @returns the formatted value
 */
export const formatValue = (event: SelectChangeEvent<string[] | string>) => {
  if (event.target.value.indexOf("") === -1) {
    if (Array.isArray(event.target.value)) {
      return event.target.value
        .filter((category: string) => category !== "")
        .toString();
    }
    return event.target.value;
  }
  return "";
};
