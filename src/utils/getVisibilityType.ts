import { VisibilityTypeEnum } from "src/enums/visibilityType.enum";
import { Visibility } from "src/types/visibility.type";
import { VisibilityBase } from "src/types/visibility/visibilityBase.type";

export function getVisibilityType(
  visibility: Visibility | VisibilityBase,
): VisibilityTypeEnum {
  if (visibility.users.length) {
    return VisibilityTypeEnum.BY_USER;
  }
  if (visibility.roles.length) {
    return VisibilityTypeEnum.BY_ROLE;
  }
  return VisibilityTypeEnum.PUBLIC;
}

export function getVisibilityTranslation(visibilityType: VisibilityTypeEnum) {
  switch (visibilityType) {
    case VisibilityTypeEnum.BY_USER:
      return "common.visibility.byUser";
    case VisibilityTypeEnum.BY_ROLE:
      return "common.visibility.byRole";
    default:
      return "common.visibility.public";
  }
}

export function getVisibilityTranslationFromVisibility(
  visibility: Visibility | VisibilityBase,
) {
  return getVisibilityTranslation(getVisibilityType(visibility));
}
