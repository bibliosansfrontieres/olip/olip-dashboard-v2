import { ApplicationTabTypeEnum } from "src/enums/applicationTabType.enum";
import { Application } from "src/types/application.type";

export type ApplicationTabTypeDefinition = {
  predicate: (application: Application) => boolean;
  index: number;
};

export const applicationTabTypeDefinitions: Record<
  ApplicationTabTypeEnum,
  ApplicationTabTypeDefinition
> = {
  [ApplicationTabTypeEnum.ALL]: {
    predicate: () => true,
    index: 0,
  },
  [ApplicationTabTypeEnum.INSTALLED]: {
    predicate: (application) => application.isInstalled,
    index: 1,
  },
  [ApplicationTabTypeEnum.NOT_INSTALLED]: {
    predicate: (application) => !application.isInstalled,
    index: 2,
  },
};

export const compareApplicationTabTypeByIndex = (
  a: ApplicationTabTypeEnum,
  b: ApplicationTabTypeEnum,
) =>
  applicationTabTypeDefinitions[a].index -
  applicationTabTypeDefinitions[b].index;

export const getSortedApplicationTabTypes = () => {
  return Object.values(ApplicationTabTypeEnum)
    .sort(compareApplicationTabTypeByIndex)
    .map(
      (type) =>
        [type, applicationTabTypeDefinitions[type]] as [
          ApplicationTabTypeEnum,
          ApplicationTabTypeDefinition,
        ],
    );
};
