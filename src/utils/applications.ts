import { CategoryApplication } from "src/types/application/categoryApplication.type";

export const isApplicationOlip = (
  application: CategoryApplication,
): boolean => {
  return application.id === "olip";
};
