import { PaletteOptions } from "@mui/material";

declare module "@mui/material/Button" {
  interface ButtonPropsVariantOverrides {
    rounded: true;
  }
}

declare module "@mui/material/Chip" {
  interface ChipPropsVariantOverrides {
    filter: true;
    tag: true;
    tagActive: true;
    tagNotClickable: true;
    tagPrimary: true;
    tagPrimaryActive: true;
    tagPrimaryNotClickable: true;
  }
}

declare module "@mui/material/InputBase" {
  interface InputBasePropsColorOverrides {
    dark: true;
  }
}

declare module "@mui/material/styles" {
  interface Palette {
    dark: Palette["primary"];
  }

  interface PaletteOptions {
    dark: PaletteOptions["primary"];
  }

  interface PaletteColorOptions {
    dark?: string;
    light?: string;
    main: string;
  }
}

declare module "@mui/material/TextField" {
  interface TextFieldPropsColorOverrides {
    dark: true;
  }
}

declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    h5: false;
    h6: false;
    body1m: true;
    body1sb: true;
    link: true;
    linkHover: true;
    info: true;
    tab: true;
    breadcrumb: true;
    breadcrumbSelect: true;
  }
}
export const paletteFunction = (mainColor: string) => {
  return {
    primary: {
      light: "#FFFFFF",
      main: `${mainColor}`,
      dark: "#363636",
    },
    success: {
      main: "#039F54",
    },
    error: {
      main: "#e31345",
    },
    info: {
      main: "#0087d3",
    },
    warning: {
      main: "#ea6606",
    },
    text: {
      primary: "#363636",
      secondary: "#FFFFFF",
      disabled: "#707070",
    },
    grey: {
      100: "#FAFAFA",
      200: "#F0F0F0",
      400: "#E1E1E1",
      500: "#BEBEBE",
      700: "#8B8B8B",
      800: "#7F7F7F",
    },
    dark: {
      main: "#363636",
      dark: "#202020",
    },
  };
};
export const themeFunction = (
  paletteOptions: PaletteOptions,
  direction: "ltr" | "rtl",
) => {
  const fontFamily = `"Poppins", "NotoSans", "NotoSansArabic", "NotoSansBengali"`;
  return {
    direction: direction,
    palette: {
      mode: "light",
      ...paletteOptions,
    },
    typography: {
      fontFamily,
      h1: {
        fontSize: "2rem",
        fontWeight: 600,
        lineHeight: "2.625rem",
        "@media (min-width: 600px)": {
          fontSize: "3rem",
          lineHeight: "3.75rem",
        },
        "@media (min-width: 1200px)": {
          fontSize: "3.75rem",
          lineHeight: "4.375rem",
        },
      },
      h2: {
        fontSize: "1.75rem",
        fontWeight: 600,
        lineHeight: "2.375rem",
        "@media (min-width: 600px)": {
          fontSize: "2rem",
          lineHeight: "2.625rem",
        },
        "@media (min-width: 1200px)": {
          fontSize: "2.75rem",
          lineHeight: "3.375rem",
        },
      },
      h3: {
        fontSize: "1.25rem",
        fontWeight: 600,
        lineHeight: "1.875rem",
        "@media (min-width: 600px)": {
          fontSize: "1.5rem",
          lineHeight: "2.125rem",
        },
        "@media (min-width: 1200px)": {
          fontSize: "2rem",
          lineHeight: "2.625rem",
        },
      },
      h4: {
        fontSize: "1.5rem",
        fontWeight: 500,
        lineHeight: "2.125rem",
      },
      body1: {
        fontSize: "1.125rem",
        lineHeight: "1.75rem",
      },
      body1m: {
        fontFamily,
        fontSize: "1.125rem",
        fontWeight: 500,
        lineHeight: "1.75rem",
      },
      body1sb: {
        fontFamily,
        fontSize: "1.125rem",
        fontWeight: 600,
        lineHeight: "1.75rem",
      },
      link: {
        fontFamily,
        fontSize: "1.125rem",
        fontWeight: 500,
        lineHeight: "1.75rem",
        textDecorationLine: "underline",
      },
      linkHover: {
        fontFamily,
        fontSize: "1.125rem",
        fontWeight: 600,
        lineHeight: "1.75rem",
        textDecorationLine: "underline",
      },
      info: {
        fontFamily,
        fontSize: "1rem",
        fontWeight: 400,
        lineHeight: "1.75rem",
      },
      tab: {
        fontFamily,
        fontSize: "1rem",
        fontWeight: 600,
        lineHeight: "1.75rem",
      },
      breadcrumb: {
        fontFamily,
        fontSize: "0.875rem",
        fontWeight: 400,
        lineHeight: "1.375rem",
        textDecorationLine: "underline",
      },
      breadcrumbSelect: {
        color: paletteOptions.text?.primary,
        fontFamily,
        fontSize: "0.875rem",
        fontWeight: 600,
        lineHeight: "1.375rem",
        textDecorationLine: "none",
      },
      caption: {
        color: paletteOptions.grey?.[800],
        fontFamily,
        fontSize: "0.875rem",
        fontStyle: "italic",
        fontWeight: 400,
        lineHeight: "1.375rem",
      },
    },
    components: {
      MuiAlert: {
        styleOverrides: {
          root: {
            alignItems: "flex-start",
            borderRadius: "1rem",
            display: "flex",
            fontSize: "0.875rem",
            fontStyle: "italic",
            lineHeight: "1.375rem",
            padding: "1.25rem",
          },
          standardSuccess: {
            backgroundColor: `${paletteOptions.primary?.light} ${paletteOptions.success?.main}1A`,
            color: paletteOptions.success?.main,
          },
          standardError: {
            backgroundColor: `${paletteOptions.primary?.light} ${paletteOptions.error?.main}1A`,
            color: paletteOptions.error?.main,
          },
          standardWarning: {
            backgroundColor: `${paletteOptions.primary?.light} ${paletteOptions.warning?.main}1A`,
            color: paletteOptions.warning?.main,
          },
          standardInfo: {
            backgroundColor: `${paletteOptions.primary?.light} ${paletteOptions.info?.main}1A`,
            color: paletteOptions.info?.main,
          },
        },
      },
      MuiBreadcrumbs: {
        styleOverrides: {
          root: {
            "& svg": {
              color: paletteOptions.grey?.[800],
            },
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            borderRadius: "0.5rem",
            color: paletteOptions.text?.primary,
            fontSize: "1.125rem",
            fontWeight: 500,
            lineHeight: "1.5rem",
            padding: "1.12rem",
            textTransform: "none",
            "&:hover": {
              backgroundColor: "#FFFFFF00",
            },
          },
          contained: {
            backgroundColor: paletteOptions.primary?.dark,
            color: paletteOptions.text?.secondary,
            "&:hover": {
              backgroundColor: "#222222",
            },
            "&.Mui-disabled": {
              backgroundColor: paletteOptions.grey?.[400],
            },
          },
          containedSecondary: {
            backgroundColor: paletteOptions.primary?.light,
            color: paletteOptions.text?.primary,
            "&:hover": {
              backgroundColor: paletteOptions.dark?.main,
              color: paletteOptions.text?.secondary,
            },
            "&.Mui-disabled": {
              backgroundColor: paletteOptions.grey?.[400],
            },
          },
          outlined: {
            borderColor: paletteOptions.grey?.[500],
            "&:hover": {
              backgroundColor: "#FFFFFF",
              borderColor: paletteOptions.primary?.dark,
            },
          },
          text: {
            textDecorationLine: "underline",
            "&:hover": {
              fontWeight: 600,
              textDecorationLine: "underline",
            },
          },
        },
        variants: [
          {
            props: { variant: "rounded" },
            style: {
              border: `0.0625rem solid ${paletteOptions.primary?.dark}`,
              borderRadius: "6.125rem",
              padding: "0.62rem 1rem",
              "&:hover": {
                color: paletteOptions.text?.secondary,
                backgroundColor: paletteOptions.primary?.dark,
              },
            },
          },
        ],
      },
      MuiButtonBase: {
        styleOverrides: {
          root: {
            "&.MuiSwitch-switchBase.Mui-checked+.MuiSwitch-track": {
              opacity: 1,
            },
          },
        },
      },
      MuiChip: {
        styleOverrides: {
          deleteIcon: {
            color: paletteOptions.text?.primary,
            margin: "0 0 0 1rem",
          },
          icon: {
            marginLeft: "0",
            marginRight: "0.625rem",
          },
          label: {
            padding: 0,
            fontWeight: 500,
          },
        },
        variants: [
          {
            props: { variant: "filter" },
            style: {
              backgroundColor: paletteOptions.primary?.light,
              border: `0.0625rem solid ${paletteOptions.grey?.[500]}`,
              fontSize: "1.125rem",
              fontWeight: 500,
              lineHeight: "1.75rem",
              padding: "0.62rem 1.25rem",
              textDecorationLine: "underline",
              "&:hover": {
                border: `2px solid ${paletteOptions.primary?.dark}`,
                padding: "0.62rem 1.125rem",
                "& span": {
                  fontWeight: 600,
                },
              },
            },
          },
          {
            props: { variant: "tag" },
            style: {
              backgroundColor: paletteOptions.grey?.[200],
              border: "none",
              borderRadius: "0.5rem",
              fontSize: "0.875rem",
              padding: "0.5rem",
              "&:hover": {
                border: `0.125rem solid ${paletteOptions.primary?.dark}`,
                padding: "0.5rem 0.375rem",
              },
            },
          },
          {
            props: { variant: "tagActive" },
            style: {
              backgroundColor: paletteOptions.primary?.dark,
              border: "none",
              borderRadius: "0.5rem",
              color: paletteOptions.text?.secondary,
              fontSize: "0.875rem",
              padding: "0.5rem",
              "&:hover": {
                border: `0.125rem solid ${paletteOptions.primary?.dark}`,
                backgroundColor: paletteOptions.grey?.[200],
                color: paletteOptions.text?.primary,
                padding: "0.5rem 0.375rem",
              },
            },
          },
          {
            props: { variant: "tagNotClickable" },
            style: {
              backgroundColor: paletteOptions.primary?.dark,
              border: "none",
              borderRadius: "0.5rem",
              color: paletteOptions.text?.secondary,
              fontSize: "0.875rem",
              padding: "0.5rem",
            },
          },
          {
            props: { variant: "tagPrimary" },
            style: {
              border: `0.0625rem solid ${paletteOptions.grey?.[200]}`,
              borderRadius: "0.5rem",
              fontSize: "0.875rem",
              padding: "0.4375rem",
              "&:hover": {
                border: `0.125rem solid ${paletteOptions.primary?.main}`,
                padding: "0.5rem 0.375rem",
              },
            },
          },
          {
            props: { variant: "tagPrimaryActive" },
            style: {
              backgroundColor: `${paletteOptions.primary?.main}33`,
              border: `0.0625rem solid ${paletteOptions.primary?.main}`,
              borderRadius: "0.5rem",
              color: paletteOptions.text?.primary,
              fontSize: "0.875rem",
              padding: "0.4375rem",
              "&:hover": {
                border: `0.125rem solid ${paletteOptions.primary?.main}`,
                padding: "0.5rem 0.375rem",
              },
            },
          },
          {
            props: { variant: "tagPrimaryNotClickable" },
            style: {
              backgroundColor: paletteOptions.grey?.[200],
              border: "none",
              borderRadius: "0.5rem",
              color: paletteOptions.text?.secondary,
              fontSize: "0.875rem",
              padding: "0.5rem",
            },
          },
        ],
      },
      MuiFormGroup: {
        styleOverrides: {
          root: {
            fontWeight: 600,
            "& .MuiFormControlLabel-label": {
              fontWeight: 600,
            },
          },
        },
      },
      MuiFormHelperText: {
        styleOverrides: {
          root: {
            margin: "0.5rem 0",
          },
        },
      },
      MuiLinearProgress: {
        styleOverrides: {
          root: {
            backgroundColor: paletteOptions.primary?.light,
            borderRadius: "2.5rem",
            height: "1.875rem",
            border: `0.3125rem solid ${paletteOptions.primary?.light}`,
          },
          bar: {
            borderRadius: "2.5rem",
          },
        },
      },
      MuiLink: {
        styleOverrides: {
          root: {
            color: paletteOptions.text?.primary,
            cursor: "pointer",
            fontFamily,
            fontSize: "1.125rem",
            fontWeight: 500,
            lineHeight: "1.75rem",
            textDecorationColor: paletteOptions.text?.primary,
            textDecorationLine: "underline",
            "&:hover": {
              fontWeight: 600,
            },
          },
        },
        variants: [
          {
            props: { variant: "breadcrumb" },
            style: {
              fontFamily,
              fontSize: "0.875rem",
              fontWeight: 400,
              lineHeight: "1.375rem",
              textDecorationLine: "underline",
            },
          },
          {
            props: { variant: "breadcrumbSelect" },
            style: {
              fontFamily,
              fontSize: "0.875rem",
              fontWeight: 600,
              lineHeight: "1.375rem",
              textDecorationLine: "none",
            },
          },
        ],
      },
      MuiModal: {
        styleOverrides: {
          root: {
            "&:focus": {
              outline: "none",
            },
          },
        },
      },
      MuiOutlinedInput: {
        styleOverrides: {
          root: {
            "& > fieldset": {
              border: `0.0625rem solid ${paletteOptions.grey?.[500]}`,
              borderRadius: "0.5rem",
            },
            "&:hover": {
              "& > fieldset": {
                border: `0.125rem solid ${paletteOptions.primary?.dark}`,
              },
              svg: {
                color: paletteOptions.primary?.dark,
              },
            },
            /* "&.Mui-focused": {
              "& > fieldset": {
                border: `0.125rem solid ${paletteOptions.primary?.dark}`,
              },
              svg: {
                color: paletteOptions.primary?.dark,
              },
            }, */
            "&.Mui-error": {
              "& > fieldset": {
                borderWidth: "0.125rem",
              },
            },
            "&.searchInput": {
              fontSize: "0.875rem",
              // "& > fieldset": { borderRadius: "1rem" },
              "& input::placeholder": {
                color: paletteOptions.text?.primary,
              },
              /* "&:hover .searchButton svg, &.Mui-focused .searchButton svg": {
                color: paletteOptions.primary?.light,
              }, */
            },
          },
        },
      },
      MuiPaginationItem: {
        styleOverrides: {
          root: {
            borderRadius: "0.5rem",
            fontSize: "1.125rem",
            fontWeight: 500,
            height: "3.75rem",
            lineHeight: "1.5rem",
            padding: "1.25rem",
            width: "3.75rem",
            "&:hover": {
              backgroundColor: paletteOptions.grey?.[200],
            },
            "&.Mui-selected": {
              backgroundColor: `${paletteOptions.primary?.main}4D`,
            },
          },
        },
      },
      MuiSwitch: {
        styleOverrides: {
          root: {
            padding: "6px 12px",
            width: "4.25rem",
            "&:hover": {
              opacity: 0.8,
            },
            "& .MuiButtonBase-root:hover": {
              backgroundColor: "#FFFFFF00",
            },
            "& .MuiButtonBase-root .Mui-checked": {
              opacity: 1,
            },
          },
          thumb: {
            boxShadow: "none",
            color: paletteOptions.primary?.light,
            marginLeft: "0.375rem",
            height: "1.125rem",
            width: "1.125rem",
          },
          track: {
            borderRadius: "0.75rem",
            backgroundColor: paletteOptions.primary?.dark,
            opacity: 1,
            height: "1.5rem",
          },
        },
      },
      MuiTab: {
        styleOverrides: {
          root: {
            borderBottom: `0.5rem solid ${paletteOptions.grey?.[200]}`,
            color: paletteOptions.text?.primary,
            fontSize: "1.125rem",
            fontWeight: 400,
            lineHeight: "1.75rem",
            padding: "1rem 1.5rem",
            textTransform: "none",
            "&:hover": {
              fontWeight: 500,
            },
            "&.Mui-selected": {
              borderColor: paletteOptions.primary?.main,
              color: paletteOptions.text?.primary,
              fontWeight: 500,
            },
          },
        },
      },
      MuiTextField: {
        styleOverrides: {
          root: {
            "& textarea": {
              fontWeight: 600,
            },
            "& > fieldset": {
              border: `0.0625rem solid ${paletteOptions.grey?.[500]}`,
              borderRadius: "0.5rem",
            },
            "&:hover": {
              "& > fieldset": {
                border: `0.125rem solid ${paletteOptions.primary?.dark}`,
              },
              svg: {
                color: paletteOptions.primary?.dark,
              },
            },
            /* "&.Mui-focused": {
              "& > fieldset": {
                border: `0.125rem solid ${paletteOptions.primary?.dark}`,
              },
              svg: {
                color: paletteOptions.primary?.dark,
              },
            }, */
            "&.Mui-error": {
              "& > fieldset": {
                borderWidth: "0.125rem",
              },
            },
          },
        },
      },
    },
  };
};
