export type ApiRequestResponse<T> = T | undefined;
