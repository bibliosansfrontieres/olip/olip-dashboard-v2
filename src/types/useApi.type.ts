import { Dispatch, SetStateAction } from "react";

export type UseApiType<T> = {
  result: T | undefined;
  isLoading: boolean;
  hasError: boolean;
  reload: () => void;
  setResult: Dispatch<SetStateAction<T | undefined>>;
};

export type UseApiTypeSimplified<T> = Omit<
  UseApiType<T>,
  "reload" | "setResult"
>;

export const useApiDefaultValueSimplified = {
  result: undefined,
  isLoading: true,
  hasError: false,
};

export const useApiDefaultValue = {
  ...useApiDefaultValueSimplified,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setResult: () => {},
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  reload: () => {},
};
