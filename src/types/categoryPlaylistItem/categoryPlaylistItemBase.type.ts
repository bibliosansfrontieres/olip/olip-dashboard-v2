export type CategoryPlaylistItemBase = {
  id: number;
  isHighlightable: boolean;
  isDisplayed: boolean;
};
