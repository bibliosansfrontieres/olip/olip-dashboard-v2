import { ApplicationBase } from "src/types/application/applicationBase.type";
import { WithApplicationSimplified } from "src/types/application/withApplicationSimplified.type";
import { WithApplicationTranslations } from "src/types/applicationTranslation/withApplicationTranslations.type";
import { CategoryPlaylistItemBase } from "src/types/categoryPlaylistItem/categoryPlaylistItemBase.type";
import { WithDublinCoreItem } from "src/types/dublinCoreItem/withDublinCoreItem.type";
import { WithDublinCoreItemTranslations } from "src/types/dublinCoreItemTranslation/withDublinCoreItemTranslations.type";
import { DublinCoreTypeBase } from "src/types/dublinCoreType/dublinCoreTypeBase.type";
import { WithDublinCoreTypeSimplified } from "src/types/dublinCoreType/withDublinCoreTypeSimplified.type";
import { WithDublinCoreTypeTranslations } from "src/types/dublinCoreTypeTranslation/withDublinCoreTypeTranslations.type";
import { WithItem } from "src/types/item/withItem.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";

export type CategoryPlaylistItemList = {
  applications: (ApplicationBase &
    WithApplicationTranslations<WithLanguageSimplified>)[];
  dublinCoreTypes: (DublinCoreTypeBase &
    WithDublinCoreTypeTranslations<WithLanguageSimplified>)[];
  categoryPlaylistItems: (CategoryPlaylistItemBase &
    WithItem<
      WithDublinCoreItem<
        WithDublinCoreItemTranslations<WithLanguageSimplified> &
          WithDublinCoreTypeSimplified
      > &
        WithApplicationSimplified
    >)[];
};
