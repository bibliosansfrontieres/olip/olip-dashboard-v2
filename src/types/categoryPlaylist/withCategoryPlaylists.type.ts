import { CategoryPlaylistBase } from "./categoryPlaylistBase.type";

export type WithCategoryPlaylists<T = object> = {
  categoryPlaylists: (CategoryPlaylistBase & T)[];
};
