import { WithApplicationSimplified } from "../application/withApplicationSimplified.type";
import { WithLanguageSimplified } from "../language/withLanguageSimplified.type";
import { WithPlaylist } from "../playlist/withPlaylist.type";
import { WithPlaylistTranslations } from "../playlistTranslation/withPlaylistTranslations.type";

import { CategoryPlaylistBase } from "./categoryPlaylistBase.type";
import { WithCategoryPlaylistItemsCount } from "./withCategoryPlaylistItemsCount.type";

export type CategoryPlaylist = CategoryPlaylistBase &
  WithCategoryPlaylistItemsCount &
  WithPlaylist<
    WithPlaylistTranslations<WithLanguageSimplified> & WithApplicationSimplified
  >;
