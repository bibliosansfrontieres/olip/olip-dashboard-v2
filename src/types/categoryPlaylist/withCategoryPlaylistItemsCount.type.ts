export type WithCategoryPlaylistItemsCount = {
  categoryPlaylistItemsCount: number;
};
