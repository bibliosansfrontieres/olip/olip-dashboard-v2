import { RoleSimplified } from "./roleSimplified.type";

export type WithRolesSimplified = {
  roles: RoleSimplified[];
};
