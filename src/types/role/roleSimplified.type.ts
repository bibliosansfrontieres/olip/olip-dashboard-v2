import { RoleEnum } from "src/enums/role.enum";

export type RoleSimplified = {
  id: number;
  name: RoleEnum;
};
