import { APILanguage, BaseAPIResult, Pagination } from "src/types";
import { CategoryPlaylistItem } from "src/types/categoryContent.type";
import { Item } from "src/types/item.type";
import { Playlist } from "src/types/playlist.type";
import { User } from "src/types/user.type";
import { UserSimplified } from "src/types/user/userSimplified.type";
import { Visibility } from "src/types/visibility.type";

export type AddPlaylistToCategoryBody = {
  playlistIds: string[];
};

export type AddPlaylistToCategoryParams = {
  categoryId: number | string;
  playlistIds: string[];
};

export type AddPlaylistToCategoryResult = BaseAPIResult;

export type CategoryBase = {
  id: number;
  pictogram: string;
  isHomepageDisplayed: boolean;
  order: number;
  deletedAt: Date | null;
  isUpdateNeeded: boolean;
  updatedAt: number;
};

type CategoryTranslation = {
  id: number;
  title: string;
  description: string;
  language: APILanguage;
};

export type CategoryWithTranslation = CategoryBase & {
  categoryTranslations: CategoryTranslation[];
};

export type CategoryPlaylistBase = {
  id: number;
  totalItems: number;
  order: number;
};
export type Category = CategoryBase & {
  visibility: Visibility;
  // eslint-disable-next-line no-use-before-define
  categoryPlaylists?: CategoryPlaylist[];
  categoryTranslations: CategoryTranslation[];
  users?: User[];
};

export type CategoryOrder =
  | "popularity"
  | "alphabetical"
  | "count_playlists"
  | "last_update";

export type CategoryPlaylist = {
  id: number;
  category: Category;
  playlist: Playlist;
  totalItems: number;
  order: number;
};

export type FormVisibility = "public" | "byRole" | "byUser";

export type GetCategoriesParams = {
  applicationIds?: string;
  categoryId?: string;
  isVisibilityEnabled?: boolean;
  languageIdentifier?: string;
  order?: CategoryOrder;
  withPlaylists?: string;
};

export type NewGetCategoriesParams = {
  applicationIds?: string;
  categoryIds?: string;
  languageIdentifier?: string;
  sortBy?: CategoryOrder;
};

export type GetCategoriesListParams = {
  languageIdentifier?: string;
};
export type GetCategoriesCatalogParams = {
  languageIdentifier?: string;
};
export type GetCategoryBackOfficeParams = {
  id: string;
  languageIdentifier?: string;
};

export type GetCategoriesParamsAPI = {
  applicationIds?: string;
  categoryId?: string;
  isVisibilityEnabled?: boolean;
  languageIdentifier: string;
  order: CategoryOrder;
  withPlaylists: string;
};

export type GetCategoriesResult = BaseAPIResult & {
  categories?: Category[];
  items?: Item[];
};

export type GetCategoriesListResult = {
  categories: CategoryWithTranslation[];
};

export type GetCategoryParams = {
  id: string;
  dublinCoreTypeId?: string;
  getAllCategoryTranslations?: "true" | "false";
  isVisibilityEnabled?: boolean;
  languageIdentifier?: string;
  page?: number;
  playlistId?: string;
  take?: number;
};

export type GetCategoryParamsAPI = {
  languageIdentifier: string;
  getAllCategoryTranslations?: "true" | "false";
};

export type GetCategoryResult = {
  category: Category;
  items: Item[];
  meta: Pagination;
};

export type GetCategorySelectionParams = {
  categoryId: number;
  languageIdentifier?: string;
};

export type GetCategorySelectionParamsAPI = {
  languageIdentifier: string;
};

export type GetCategorySelectionResult = BaseAPIResult & {
  categoryPlaylistItems?: CategoryPlaylistItem[];
};

export type CreateCategoryTranslationDto = {
  title: string;
  description: string;
  languageIdentifier: string;
};
export type CategoryFormValue = {
  actualLanguage: string;
  languages: Array<string>;
  isHomepageDisplayed: boolean;
  pictogram: string;
  createCategoryTranslationsDto: CreateCategoryTranslationDto[];
  roleIds: number[];
  userIds: number[];
  users: User[] | UserSimplified[];
  visibility: FormVisibility;
  origin?: string;
};

export type CreateCategoryParams = {
  pictogram: string;
  isHomepageDisplayed: boolean;
  userIds: Array<number>;
  roleIds: Array<number>;
  createCategoryTranslationsDto: CreateCategoryTranslationDto[];
};

export type CreateCategoryResult = BaseAPIResult & {
  category?: Category;
};

export type DeleteCategoryResult = BaseAPIResult & {
  category?: Category;
};

export type UpdateCategoryParams = {
  pictogram?: string;
  isHomepageDisplayed?: boolean;
  userIds?: Array<number>;
  roleIds?: Array<number>;
  createCategoryTranslationsDto?: CreateCategoryTranslationDto[];
};

export type UpdateCategoryResult = BaseAPIResult & {
  category?: Category;
};

export type UpdateOrderCategoriesBody = {
  categoryIds: number[];
};

export type UpdateOrderCategoriesResult = BaseAPIResult;
