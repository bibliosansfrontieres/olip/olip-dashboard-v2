import { RoleSimplified } from "src/types/role/roleSimplified.type";
import { UserSimplified } from "src/types/user/userSimplified.type";
import { VisibilityBase } from "src/types/visibility/visibilityBase.type";

export type WithVisibility<
  R extends RoleSimplified = RoleSimplified,
  U extends UserSimplified = UserSimplified,
> = {
  visibility: VisibilityBase<R, U>;
};
