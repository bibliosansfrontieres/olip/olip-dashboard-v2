import { RoleSimplified } from "src/types/role/roleSimplified.type";
import { UserSimplified } from "src/types/user/userSimplified.type";

export type VisibilityBase<
  R extends RoleSimplified = RoleSimplified,
  U extends UserSimplified = UserSimplified,
> = {
  id: number;
  roles: R[];
  users: U[];
};
