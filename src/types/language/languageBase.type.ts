export type LanguageBase = {
  id: number;
  identifier: string;
  label: string;
};
