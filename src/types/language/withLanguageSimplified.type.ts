import { LanguageSimplified } from "./languageSimplified.type";

export type WithLanguageSimplified = {
  language: LanguageSimplified;
};
