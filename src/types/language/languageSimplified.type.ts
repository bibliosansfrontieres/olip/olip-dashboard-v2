import { LanguageBase } from "./languageBase.type";

export type LanguageSimplified = Omit<LanguageBase, "id">;
