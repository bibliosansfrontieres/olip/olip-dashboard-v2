import { APILanguage, BaseAPIResult, Pagination } from ".";
import { Application } from "./application.type";

export type DublinCoreItem = {
  id: number;
  dublinCoreType: DublinCoreType;
  dublinCoreItemTranslations: DublinCoreItemTranslation[];
};

export type DublinCoreItemTranslation = {
  id: number;
  title: string;
  description: string;
  creator: string;
  publisher: string;
  source: string;
  extent: string;
  subject: string;
  dateCreated: string;
  language: APILanguage;
};

export type DublinCoreType = {
  id: number;
  image: string;
  dublinCoreTypeTranslations: DublinCoreTypeTranslation[];
};

export type DublinCoreTypeTranslation = {
  id: number;
  label: string;
  description: string;
  language: APILanguage;
};

export type File = {
  id: number;
  name: string;
  path: string;
  size: string;
  duration: string;
  pages: string;
  extension: string;
  mimeType: string;
};

export type GetItemParams = {
  id: string;
  languageIdentifier?: string;
};

export type GetItemParamsAPI = {
  applicationIds?: string;
  categoryId?: string;
  languageIdentifier: string;
  order?: "ASC" | "DESC";
  page?: number;
  take?: number;
  titleLike?: string;
};

export type GetItemsParams = {
  applicationIds?: string;
  categoryId?: string;
  languageIdentifier?: string;
  order?: "ASC" | "DESC";
  page?: number;
  take?: number;
  titleLike?: string;
};

export type GetItemsResult = BaseAPIResult & {
  items?: Item[];
  meta?: Pagination;
};

export type GetItemsParamsAPI = GetItemsParams;

export type GetItemResult = BaseAPIResult & {
  item?: Item;
};

export type GetSuggestionsItemsParams = {
  id: string;
  languageIdentifier?: string;
  categoryId?: string;
  order?: "ASC" | "DESC";
  page?: number;
  playlistId?: string;
  take?: number;
};

export type GetSuggestionsItemsParamsAPI = {
  languageIdentifier: string;
  categoryId?: string;
  order?: "ASC" | "DESC";
  page?: number;
  playlistId?: string;
  take?: number;
};

export type GetSuggestionsItemsResult = BaseAPIResult & {
  items?: Item[];
  meta?: Pagination;
};

export type Item = {
  application: Application;
  contentId: string;
  createdAt: number;
  createdCountry: string;
  dublinCoreItem: DublinCoreItem;
  file: File;
  id: string;
  isBroken: boolean;
  isHomepageDisplayed: boolean;
  isInstalled: boolean;
  isManuallyInstalled: false;
  isUpdateNeeded: boolean;
  thumbnail: string;
  updatedAt: number;
  version: string;
  url?: string;
};

export type ItemMaestro = {
  application: Application;
  createdAt: string;
  createdCountry: string;
  filename: string;
  id: number;
  imageName: string;
  isValid: boolean;
  itemTranslations: ItemTranslationMaestro[];
  mimeType: string;
  size: number;
  thumbnail: null;
  updatedAt: string;
  url: string;
};

type ItemTranslationMaestro = {
  creator: string;
  dateCreated: string;
  description: string;
  extent: string;
  id: number;
  publisher: string;
  source: string;
  subject: string;
  title: string;
};
