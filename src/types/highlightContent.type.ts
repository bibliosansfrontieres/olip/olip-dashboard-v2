import { BaseAPIResult } from ".";
import { Item } from "./item.type";

export type HighlightContent = {
  id: number;
  isDisplayed: boolean;
  item: Item;
  order: number;
};

export type AddHighlightContentBody = {
  itemId: string;
};

export type AddHighlightContentResult = BaseAPIResult;

export type RemoveHighlightContentResult = BaseAPIResult;

export type UpdateDisplayHighlightContentParams = {
  highlightContentIds: number[];
};

export type UpdateDisplayHighlightContentResult = BaseAPIResult;

export type UpdateOrderHighlightContentBody = {
  highlightContentIds: number[];
};

export type UpdateOrderHighlightContentResult = BaseAPIResult;
