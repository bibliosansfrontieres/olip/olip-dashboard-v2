import { DublinCoreTypeTranslationBase } from "src/types/dublinCoreTypeTranslation/dublinCoreTypeTranslationBase.type";

export type WithDublinCoreTypeTranslations<T = object> = {
  dublinCoreTypeTranslations: (DublinCoreTypeTranslationBase & T)[];
};
