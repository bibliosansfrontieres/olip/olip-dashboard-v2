export type DublinCoreTypeTranslationBase = {
  id: number;
  label: string;
  description: string;
};
