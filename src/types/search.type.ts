import { BaseAPIResult, Pagination } from ".";
import { Application } from "./application.type";
import { Category, CategoryPlaylist } from "./category.type";
import { Item } from "./item.type";
export type GetSearchParams = {
  query?: string;
  languageIdentifier?: string;
  order?: "ASC" | "DESC";
  page?: number;
  take?: number;
  dublinCoreTypeId?: string;
};

export type GetSearchParamsAPI = {
  query?: string;
  languageIdentifier?: string;
  order?: "ASC" | "DESC";
  page?: number;
  take?: number;
};

export type GetSuggestionParams = {
  query: string;
  take?: number;
};

export type ItemsWithMeta = {
  data: Item[];
  meta: { totalItemsCount: number };
};
export type CategoryPlaylistsWithMeta = {
  data: CategoryPlaylist[];
  meta: { totalItemsCount: number };
};
export type CategoriesWithMeta = {
  data: Category[];
  meta: { totalItemsCount: number };
};
export type ApplicationsWithMeta = {
  data: Application[];
  meta: { totalItemsCount: number };
};

export type GetSearchResultWithMeta = {
  items?: ItemsWithMeta;
  playlists?: CategoryPlaylistsWithMeta;
  categories?: CategoriesWithMeta;
  applications?: ApplicationsWithMeta;
};
export type GetSearchResult = BaseAPIResult & {
  items?: ItemsWithMeta;
  playlists?: CategoryPlaylistsWithMeta;
  categories?: CategoriesWithMeta;
  applications?: ApplicationsWithMeta;
};

export type GetSearchItemsResult = BaseAPIResult & {
  items?: {
    data: Item[];
    meta: Pagination;
  };
};

export type GetSearchCategoriesResult = BaseAPIResult & {
  categories?: {
    data: Category[];
    meta: Pagination;
  };
};

export type GetSearchApplicationsResult = BaseAPIResult & {
  applications?: {
    data: Application[];
    meta: Pagination;
  };
};

export type GetSearchPlaylistsResult = BaseAPIResult & {
  categoryPlaylists?: {
    data: CategoryPlaylist[];
    meta: Pagination;
  };
};

export type GetSuggestionsResult = BaseAPIResult & {
  suggestions?: {
    suggestion: string;
    id: string;
  }[];
};
