export type CategoryBase = {
  id: number;
  pictogram: string;
  isHomepageDisplayed: boolean;
  order: number;
  isUpdateNeeded: boolean;
  deletedAt: Date;
};
