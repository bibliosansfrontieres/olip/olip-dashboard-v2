export type WithCategoryPlaylistsCount = {
  categoryPlaylistsCount: number;
};
