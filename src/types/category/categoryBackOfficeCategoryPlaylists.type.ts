import { CategoryApplication } from "src/types/application/categoryApplication.type";
import { WithApplicationSimplified } from "src/types/application/withApplicationSimplified.type";
import { CategoryPlaylistBase } from "src/types/categoryPlaylist/categoryPlaylistBase.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";
import { WithPlaylist } from "src/types/playlist/withPlaylist.type";
import { WithPlaylistTranslations } from "src/types/playlistTranslation/withPlaylistTranslations.type";

export type CategoryBackOfficeCategoryPlaylists = {
  applications: CategoryApplication[];
  categoryPlaylists: (CategoryPlaylistBase &
    WithPlaylist<
      WithPlaylistTranslations<WithLanguageSimplified> &
        WithApplicationSimplified
    >)[];
};

export type CategoryPlaylistBackOffice = CategoryPlaylistBase &
  WithPlaylist<
    WithPlaylistTranslations<WithLanguageSimplified> & WithApplicationSimplified
  >;
