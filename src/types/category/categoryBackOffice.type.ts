import { CategoryBase } from "src/types/category.type";
import { WithCategoryTranslations } from "src/types/categoryTranslation/withCategoryTranslations.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";
import { WithVisibility } from "src/types/visibility/withVisibility.type";

export type CategoryBackOffice = CategoryBase &
  WithCategoryTranslations<WithLanguageSimplified> &
  WithVisibility;
