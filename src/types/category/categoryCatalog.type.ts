import { ApplicationBase } from "../application/applicationBase.type";
import { WithApplicationSimplified } from "../application/withApplicationSimplified.type";
import { WithApplicationTranslations } from "../applicationTranslation/withApplicationTranslations.type";
import { WithApplicationType } from "../applicationType/withApplicationType.type";
import { WithApplicationTypeTranslations } from "../applicationTypeTranslation/withApplicationTypeTranslations.type";
import { WithCategoryPlaylistItemsCount } from "../categoryPlaylist/withCategoryPlaylistItemsCount.type";
import { WithCategoryPlaylists } from "../categoryPlaylist/withCategoryPlaylists.type";
import { WithCategoryTranslations } from "../categoryTranslation/withCategoryTranslations.type";
import { WithLanguageSimplified } from "../language/withLanguageSimplified.type";
import { WithPlaylist } from "../playlist/withPlaylist.type";
import { WithPlaylistTranslations } from "../playlistTranslation/withPlaylistTranslations.type";

import { CategoryBase } from "./categoryBase.type";

export type CategoryCatalog = {
  applications: (ApplicationBase &
    WithApplicationTranslations<WithLanguageSimplified> &
    WithApplicationType<
      WithApplicationTypeTranslations<WithLanguageSimplified>
    >)[];
  categories: (CategoryBase &
    WithCategoryTranslations<WithLanguageSimplified> &
    WithCategoryPlaylists<
      WithCategoryPlaylistItemsCount &
        WithPlaylist<
          WithPlaylistTranslations<WithLanguageSimplified> &
            WithApplicationSimplified
        >
    >)[];
};
