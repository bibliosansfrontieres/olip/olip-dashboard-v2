import { WithApplicationSimplified } from "src/types/application/withApplicationSimplified.type";
import { CategoryBase } from "src/types/category/categoryBase.type";
import { WithCategoryPlaylistItemsCount } from "src/types/categoryPlaylist/withCategoryPlaylistItemsCount.type";
import { WithCategoryPlaylists } from "src/types/categoryPlaylist/withCategoryPlaylists.type";
import { WithCategoryTranslations } from "src/types/categoryTranslation/withCategoryTranslations.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";
import { WithPlaylist } from "src/types/playlist/withPlaylist.type";
import { WithPlaylistTranslations } from "src/types/playlistTranslation/withPlaylistTranslations.type";

export type CategoryCategoriesCatalog = CategoryBase &
  WithCategoryTranslations<WithLanguageSimplified> &
  WithCategoryPlaylists<
    WithCategoryPlaylistItemsCount &
      WithPlaylist<
        WithPlaylistTranslations<WithLanguageSimplified> &
          WithApplicationSimplified
      >
  >;
