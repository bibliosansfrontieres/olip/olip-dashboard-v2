import { CategoryBase } from "src/types/category.type";
import { WithCategoryPlaylistsCount } from "src/types/category/withCategoryPlaylistsCount.type";
import { WithCategoryTranslations } from "src/types/categoryTranslation/withCategoryTranslations.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";
import { WithUsersSimplified } from "src/types/user/withUsersSimplified.type";
import { WithVisibility } from "src/types/visibility/withVisibility.type";

export type CategoryBackOfficeList = {
  categories: (CategoryBase &
    WithUsersSimplified &
    WithCategoryTranslations<WithLanguageSimplified> &
    WithVisibility &
    WithCategoryPlaylistsCount)[];
};

export type CategoryBackOfficeListCategory = CategoryBase &
  WithUsersSimplified &
  WithCategoryTranslations<WithLanguageSimplified> &
  WithVisibility &
  WithCategoryPlaylistsCount;
