import { CategoryTranslationBase } from "./categoryTranslationBase.type";

export type WithCategoryTranslations<T = object> = {
  categoryTranslations: (CategoryTranslationBase & T)[];
};
