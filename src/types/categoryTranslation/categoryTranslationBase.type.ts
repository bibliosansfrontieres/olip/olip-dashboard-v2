export type CategoryTranslationBase = {
  id: number;
  title: string;
  description: string | null;
};
