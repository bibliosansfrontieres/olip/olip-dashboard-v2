export type ApplicationTypeBase = {
  id: string;
  name: string;
};
