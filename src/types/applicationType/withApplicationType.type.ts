import { ApplicationTypeBase } from "./applicationTypeBase.type";

export type WithApplicationType<T = object> = {
  applicationType: ApplicationTypeBase & T;
};
