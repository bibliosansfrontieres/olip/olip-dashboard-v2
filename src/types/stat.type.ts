import { BaseAPIResult } from ".";
import { Category } from "./category.type";

export type CategoryStats = {
  category: Category;
  totalItems: number;
  purcentage: number;
};

export type GetInstalledContentsResult = BaseAPIResult & {
  installedContents?: number;
};
export type GetInstalledContentsByCategoryParamsAPI = {
  languageIdentifier?: string;
};

export type GetInstalledContentsByCategoryResult = BaseAPIResult & {
  data?: {
    totalItems: number;
    categories: CategoryStats[];
  };
};
