import { ApplicationType } from "./application.type";

export type GetApplicationTypesParams = {
  languageIdentifier?: string;
};

export type GetApplicationTypesResult = { applicationTypes: ApplicationType[] };
