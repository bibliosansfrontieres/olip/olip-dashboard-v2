import { DublinCoreItemBase } from "src/types/dublinCoreItem/dublinCoreItemBase.type";

export type WithDublinCoreItem<T = object> = {
  dublinCoreItem: DublinCoreItemBase & T;
};
