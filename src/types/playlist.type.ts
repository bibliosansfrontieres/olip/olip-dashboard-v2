import { Application } from "./application.type";
import { Category } from "./category.type";
import { Item, ItemMaestro } from "./item.type";
import { APILanguage, BaseAPIResult, Pagination } from ".";

export type GetInstalledPlaylistsParams = {
  applicationIds?: string;
  categoryId: string;
  languageIdentifier?: string;
  order?: "ASC" | "DESC";
  page?: number;
  query?: string;
  take?: number;
};

export type GetInstalledPlaylistsParamsAPI = {
  applicationIds?: string;
  categoryId: string;
  languageIdentifier: string;
  order?: "ASC" | "DESC";
  page?: number;
  query: string;
  take?: number;
};

export type GetInstalledPlaylistsResult = BaseAPIResult & {
  playlists?: Playlist[];
  meta?: Pagination;
};

export type GetUninstalledPlaylistsParams = {
  applicationIds?: string;
  categoryId: string;
  languageIdentifier?: string;
  order?: "ASC" | "DESC";
  page?: number;
  query?: string;
  take?: number;
};

export type GetUninstalledPlaylistsParamsAPI = {
  applicationIds?: string;
  categoryId: string;
  languageIdentifier: string;
  order?: "ASC" | "DESC";
  page?: number;
  query: string;
  take?: number;
};

export type GetUninstalledPlaylistsResult = BaseAPIResult & {
  playlists?: Playlist[];
  meta?: Pagination;
};

export type GetUpdateNeededPlaylistsParams = {
  applicationIds?: string;
  categoryId: string;
  languageIdentifier?: string;
  order?: "ASC" | "DESC";
  page?: number;
  query?: string;
  take?: number;
  withItems?: boolean;
};

export type GetUpdateNeededPlaylistsParamsAPI = {
  applicationIds?: string;
  categoryId: string;
  languageIdentifier: string;
  order?: "ASC" | "DESC";
  page?: number;
  query: string;
  take?: number;
  withItems: boolean;
};

export type GetUpdateNeededPlaylistsResult = BaseAPIResult & {
  playlists?: Playlist[];
  meta?: Pagination;
};

export type InstallPlaylistsBody = {
  playlistIds: string[];
};

export type InstallPlaylistsParams = InstallPlaylistsBody;

export type InstallPlaylistsResult = BaseAPIResult;

export type Playlist = {
  application: Application;
  applicationId: string;
  contentId: string;
  id: string;
  image: string;
  isInstalled: boolean;
  isUpdateNeeded: boolean;
  isBroken: boolean;
  isManuallyInstalled: boolean;
  items?: Item[] | ItemMaestro[];
  playlistTranslations: PlaylistTranslation[];
  size: number;
  version: string;
};

type PlaylistTranslation = {
  description: string;
  id: number;
  language: APILanguage;
  languageIdentifier?: string;
  title: string;
};

export type UninstallPlaylistsBody = {
  confirm?: boolean;
  languageIdentifier: string;
  playlistIds: string[];
};

export type UninstallPlaylistsParams = {
  confirm?: boolean;
  languageIdentifier?: string;
  playlistIds: string[];
};

export type LinkedPlaylist = {
  categories: Category[];
  playlist: Playlist;
};
export type UninstallPlaylistsResult = BaseAPIResult & {
  linkedPlaylists?: LinkedPlaylist[];
};

export type UpdatePlaylistsBody = {
  playlistIds: string[];
};

export type UpdatePlaylistsParams = UpdatePlaylistsBody;

export type UpdatePlaylistsResult = BaseAPIResult;
