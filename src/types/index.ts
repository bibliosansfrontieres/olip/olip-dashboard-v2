import { PermissionName } from "./user.type";

export type APILanguage = {
  id: number;
  identifier: string;
  label: string;
  oldIdentifier: string;
};

export type BaseAPIResult = {
  errorMessage?: string | string[];
  statusCode: number;
};

export type BreadcrumbsLinks = {
  link: string;
  text: string;
};

export type Language = { id: string; name: string };

export type Page = { name: string; link: string };

export type Pagination = {
  page: number;
  take: number;
  totalItemsCount: number;
  pageCount: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
};

export type WithProtection = { permission: PermissionName | null };

export type Color = {
  name: string;
  color: string;
};
