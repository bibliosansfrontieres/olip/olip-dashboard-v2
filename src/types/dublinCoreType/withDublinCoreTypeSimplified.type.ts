import { DublinCoreTypeSimplified } from "src/types/dublinCoreType/dublinCoreTypeSimplified.type";

export type WithDublinCoreTypeSimplified = {
  dublinCoreType: DublinCoreTypeSimplified;
};
