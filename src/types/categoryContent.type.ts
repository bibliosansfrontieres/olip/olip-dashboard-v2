import { BaseAPIResult, Pagination } from ".";
import { Item } from "./item.type";
import { Playlist } from "./playlist.type";

export type CategoryContent = {
  categoryPlaylistItems: CategoryPlaylistItem[];
  id: number;
  order: number;
  playlist: Playlist;
};

export type CategoryPlaylistItem = {
  id: number;
  isDisplayed: boolean;
  isHighlightable: boolean;
  item: Item;
};

export type GetCategoryContentParams = {
  id: string;
  languageIdentifier?: string;
  playlistId?: string;
  dublinCoreTypeId?: string;
  titleLike?: string;
  order?: "ASC" | "DESC";
  page?: number;
  take?: number;
};

export type GetCategoryContentParamsAPI = {
  languageIdentifier: string;
  playlistId?: string;
  dublinCoreTypeId?: string;
  titleLike?: string;
  order?: "ASC" | "DESC";
  page?: number;
  take?: number;
};

export type GetCategoryContentResult = BaseAPIResult & {
  contents?: CategoryContent[];
  meta?: Pagination;
};

export type RemoveCategoryPlaylistsBody = {
  ids: number[];
};

export type RemoveCategoryPlaylistsResult = BaseAPIResult;

export type UpdateCategoryPlaylistItemParams = {
  isDisplayed?: boolean;
  isHighlightable?: boolean;
};

export type UpdateCategoryPlaylistItemResult = BaseAPIResult & {
  categoryPlaylistItem?: {
    id: number;
    isDisplayed: boolean;
    isHighlightable: boolean;
  };
};

export type UpdateOrderCategoryPlaylistsBody = {
  categoryId: number;
  categoryPlaylistIds: number[];
};

export type UpdateOrderCategoryPlaylistsResult = BaseAPIResult;
