export type DublinCoreItemTranslationBase = {
  id: number;
  title: string;
  description: string;
  creator: string;
  publisher: string;
  source: string;
  extent: string;
  subject: string;
  dateCreated: string;
};
