import { DublinCoreItemTranslationBase } from "src/types/dublinCoreItemTranslation/dublinCoreItemTranslationBase.type";

export type WithDublinCoreItemTranslations<T = object> = {
  dublinCoreItemTranslations: (DublinCoreItemTranslationBase & T)[];
};
