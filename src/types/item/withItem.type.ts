import { ItemBase } from "src/types/item/itemBase.type";

export type WithItem<T = object> = {
  item: ItemBase & T;
};
