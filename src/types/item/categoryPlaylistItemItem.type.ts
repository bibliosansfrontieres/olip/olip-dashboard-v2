import { WithApplicationSimplified } from "src/types/application/withApplicationSimplified.type";
import { WithDublinCoreItem } from "src/types/dublinCoreItem/withDublinCoreItem.type";
import { WithDublinCoreItemTranslations } from "src/types/dublinCoreItemTranslation/withDublinCoreItemTranslations.type";
import { WithDublinCoreTypeSimplified } from "src/types/dublinCoreType/withDublinCoreTypeSimplified.type";
import { ItemBase } from "src/types/item/itemBase.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";

export type CategoryPlaylistItemItem = ItemBase &
  WithDublinCoreItem<
    WithDublinCoreItemTranslations<WithLanguageSimplified> &
      WithDublinCoreTypeSimplified
  > &
  WithApplicationSimplified;
