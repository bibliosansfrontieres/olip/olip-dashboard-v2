export type ItemBase = {
  id: string;
  thumbnail: string;
  createdCountry: string;
  isInstalled: boolean;
  isDownloading: boolean;
  isUpdateNeeded: boolean;
  isBroken: boolean;
  isManuallyInstalled: boolean;
  version: string | null;
  url: string | null;
  createdAt: number;
  updatedAt: number;
};
