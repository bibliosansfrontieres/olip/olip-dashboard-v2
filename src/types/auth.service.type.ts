export type LoginResult = {
  accessToken?: string;
  errorMessage?: string | string[];
  statusCode: number;
};
