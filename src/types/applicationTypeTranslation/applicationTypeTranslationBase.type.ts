export type ApplicationTypeTranslationBase = {
  id: number;
  label: string;
  description: string;
};
