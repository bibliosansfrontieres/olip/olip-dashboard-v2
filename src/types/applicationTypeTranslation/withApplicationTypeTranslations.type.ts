import { ApplicationTypeTranslationBase } from "./applicationTypeTranslationBase.type";

export type WithApplicationTypeTranslations<T = object> = {
  applicationTypeTranslations: (ApplicationTypeTranslationBase & T)[];
};
