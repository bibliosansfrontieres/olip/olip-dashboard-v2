import { BaseAPIResult, Pagination } from ".";
import { User } from "./user.type";

export type CreateUserBody = {
  username: string;
  password?: string;
  photo?: string;
  language: string;
  roleId?: number;
};

export type CreateUserResult = BaseAPIResult & {
  user?: User;
};

export type DeleteUserResult = BaseAPIResult & {
  user?: User;
};

export type GetMyProfileResult = BaseAPIResult & {
  user?: User;
};

export type GetUsersParams = {
  order: "ASC" | "DESC";
  page: number;
  take: number;
  orderByField?: "username" | "role" | "id";
  usernameLike?: string;
};

export type GetUsersResult = BaseAPIResult & {
  users?: User[];
  meta?: Pagination;
};

export type UpdateUserBody = {
  username?: string;
  password?: string;
  photo?: string;
  language?: string;
  roleId?: number;
};

export type UpdateUserResult = BaseAPIResult & {
  user?: User;
};
