import { BaseAPIResult } from ".";
import { Role } from "./role.type";
import { User } from "./user.type";

export type GetVisibilityParams = {
  id: number;
};

export type GetVisibilityResult = BaseAPIResult & {
  visibility?: Visibility;
};

export type Visibility = {
  id: number;
  roles: Role[];
  users: User[];
};
