import { BaseAPIResult } from ".";

export type SettingKey =
  | "logo"
  | "principal_color"
  | "secondary_color"
  | "catalog_timestamp"
  | "is_edito_displayed"
  | "is_highlight_displayed"
  | "is_category_displayed";

export type GetSettingResult = BaseAPIResult & {
  setting?: Setting;
};

export type Setting = {
  id: number;
  key: SettingKey;
  value: string;
};

export type UpdateSettingBody = {
  value: string;
};

export type UpdateSettingResult = BaseAPIResult & {
  setting?: Setting;
};
