import { PlaylistTranslationBase } from "./playlistTranslationBase.type";

export type WithPlaylistTranslations<T = object> = {
  playlistTranslations: (PlaylistTranslationBase & T)[];
};
