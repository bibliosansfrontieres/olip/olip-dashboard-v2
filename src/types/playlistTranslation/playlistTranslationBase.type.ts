export type PlaylistTranslationBase = {
  id: number;
  title: string;
  description: string;
};
