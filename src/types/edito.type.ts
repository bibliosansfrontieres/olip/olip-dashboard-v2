import { APILanguage, BaseAPIResult } from ".";

export type CreateEditoParagraphsDto = {
  createEditoParagraphTranslationsDto: CreateEditoParagraphTranslationDto[];
};

export type CreateEditoParagraphTranslationDto = {
  languageIdentifier: string;
  text: string;
};

export type CreateEditoResult = BaseAPIResult & {
  edito?: Edito;
};

export type CreateEditoTranslationDto = {
  languageIdentifier: string;
  title: string;
  description: string;
};

export type Edito = {
  id: number;
  image: string;
  createdAt: number;
  updatedAt: number;
  editoTranslations: EditoTranslation[];
  editoParagraphs: EditoParagraph[];
};

export type EditoFormValues = {
  actualLanguage: string;
  languages: string[];
  image: string;
  createEditoTranslationsDto: CreateEditoTranslationDto[];
  createEditoParagraphsDto: CreateEditoParagraphsDto[];
};

export type EditoParagraph = {
  id: number;
  editoParagraphTranslations: EditoParagraphTranslation[];
};

export type EditoParagraphTranslation = {
  id: number;
  text: string;
  language: APILanguage;
};

export type EditoTranslation = {
  id: number;
  title: string;
  description: string;
  language: APILanguage;
};

export type GetEditosResult = BaseAPIResult & {
  editos?: Edito[];
};

export type GetLastEditoParams = {
  languageIdentifier?: string;
};

export type GetLastEditoParamsAPI = {
  languageIdentifier?: string;
};

export type GetLastEditoResult = BaseAPIResult & {
  edito?: Edito;
};
