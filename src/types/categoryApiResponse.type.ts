export type GetCategoriesCatalogResponse = {
  applications: {
    id: string;
    name: string;
    displayName: string;
    applicationTranslations: {
      id: number;
      shortDescription: string;
      longDescription: string;
      language: {
        identifier: string;
        label: string;
      };
    }[];
    applicationType: {
      id: string;
      name: string;
      applicationTypeTranslations: {
        id: number;
        label: string;
        language: {
          identifier: string;
          label: string;
        };
      }[];
    };
  }[];
  categories: {
    id: number;
    sourceId: number;
    pictogram: string;
    isHomepageDisplayed: boolean;
    order: number;
    isUpdateNeeded: boolean;
    deletedAt: string | null;
    categoryTranslations: {
      id: number;
      title: string;
      description: string | null;
      language: {
        identifier: string;
        label: string;
      };
    }[];
    categoryPlaylists: {
      playlist: {
        id: string;
        image: string;
        isInstalled: boolean;
        isUpdateNeeded: boolean;
        isBroken: boolean;
        isManuallyInstalled: boolean;
        version: string;
        size: number;
        createdAt: number;
        updatedAt: number;
        playlistTranslations: {
          id: number;
          title: string;
          description: string;
          language: {
            identifier: string;
            label: string;
          };
        }[];
        application: {
          id: string;
        };
      };
      order: number;
      id: number;
      categoryPlaylistItemsCount: number;
    }[];
  }[];
};
