import { APILanguage, BaseAPIResult } from ".";
import { HighlightContent } from "./highlightContent.type";

export type CreateHighlightResult = BaseAPIResult & {
  highlight?: Highlight;
};

export type CreateHighlightTranslation = {
  languageIdentifier: string;
  title: string;
};

export type GetLastHighlightParams = {
  itemsLanguageIdentifier?: string;
  languageIdentifier?: string;
};

export type GetLastHighlightParamsAPI = {
  itemsLanguageIdentifier: string;
  languageIdentifier?: string;
};

export type GetLastHighlightResult = BaseAPIResult & {
  highlight?: Highlight;
};

export type Highlight = {
  id: number;
  highlightTranslations: HighlightTranslation[];
  highlightContents: HighlightContent[];
};

export type HighlightFormValues = {
  actualLanguage: string;
  languages: string[];
  createHighlightTranslations: CreateHighlightTranslation[];
};

export type HighlightTranslation = {
  id: number;
  title: string;
  language: APILanguage;
};
