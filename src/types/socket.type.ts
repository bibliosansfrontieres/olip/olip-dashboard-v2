import { Application } from "./application.type";

export type InternetAvailableEvent = {
  isConnected: boolean;
};

export type LiveUsersEvent = {
  liveUsers: number;
  liveConnectedUsers: number;
};

type StoragePath = StorageSizes & {
  path: string;
};
type StorageSizes = {
  availableBytes: number;
  totalBytes: number;
};

export type StorageEvent = StorageSizes & {
  paths: StoragePath;
};

export type ApplicationEvent = {
  application: Application;
};

export type BatteryInformationEvent = {
  hasBattery: boolean;
  percentage: number;
  isCharging: boolean;
  isPluggedIn: boolean;
  lowBatteryPercentage: number;
};
