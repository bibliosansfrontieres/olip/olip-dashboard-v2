import { BaseAPIResult } from ".";

export type GetSSIDResult = BaseAPIResult & {
  ssid?: string | null;
};
