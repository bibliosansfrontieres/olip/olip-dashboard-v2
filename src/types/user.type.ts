import { APILanguage } from ".";
import { Role } from "./role.type";

export type MyProfileFormValues = {
  language: string;
  password: string;
  passwordRepeat: string;
  photo?: string;
  username: string;
};

export type Permission = {
  id: number;
  name: PermissionName;
  permissionTranslations?: PermissionTranslation[];
};

export type PermissionName =
  | "create_category"
  | "create_edito"
  | "create_user"
  | "delete_edito"
  | "delete_user"
  | "deploy_process_id"
  | "deploy_status"
  | "install_broken_playlist"
  | "manage_applications"
  | "read_activity"
  | "read_ssid"
  | "read_storage"
  | "read_users"
  | "update_catalog"
  | "update_category"
  | "update_highlight"
  | "update_setting"
  | "update_user"
  | "update_personalization";

export type PermissionTranslation = {
  id: number;
  label: string;
  description: string;
  language: APILanguage;
};

export type User = {
  id: number;
  language: string;
  photo?: string;
  role: Role;
  username: string;
};

export type UserFormValues = {
  mode: "create" | "edit";
  language: string;
  password: string;
  role?: number;
  username: string;
};
