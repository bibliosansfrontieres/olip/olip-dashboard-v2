export type UserSimplified = {
  id: number;
  username: string;
};
