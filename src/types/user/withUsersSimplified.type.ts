import { UserSimplified } from "./userSimplified.type";

export type WithUsersSimplified = {
  users: UserSimplified[];
};
