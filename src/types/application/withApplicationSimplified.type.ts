import { ApplicationSimplified } from "./applicationSimplified.type";

export type WithApplicationSimplified = {
  application: ApplicationSimplified;
};
