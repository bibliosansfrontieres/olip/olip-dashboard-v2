export type ApplicationBase = {
  id: string;
  name: string;
  displayName: string;
  version: string;
  logo: string;
  image: string;
  url: string;
  size: number;
  isInstalled: boolean;
  isDownloading: boolean;
  isInstalling: boolean;
  isUp: boolean;
  isUpdateNeeded: boolean;
  isUpdating: boolean;
  isOlip: boolean;
};
