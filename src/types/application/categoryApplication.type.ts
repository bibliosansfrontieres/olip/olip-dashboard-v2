import { WithApplicationTranslations } from "../applicationTranslation/withApplicationTranslations.type";
import { WithApplicationType } from "../applicationType/withApplicationType.type";
import { WithApplicationTypeTranslations } from "../applicationTypeTranslation/withApplicationTypeTranslations.type";
import { WithLanguageSimplified } from "../language/withLanguageSimplified.type";

import { ApplicationBase } from "./applicationBase.type";

export type CategoryApplication = ApplicationBase &
  WithApplicationTranslations<WithLanguageSimplified> &
  WithApplicationType<WithApplicationTypeTranslations<WithLanguageSimplified>>;
