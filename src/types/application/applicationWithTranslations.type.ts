import { ApplicationBase } from "src/types/application/applicationBase.type";
import { WithApplicationTranslations } from "src/types/applicationTranslation/withApplicationTranslations.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";

export type ApplicationWithTranslations = ApplicationBase &
  WithApplicationTranslations<WithLanguageSimplified>;
