import { User } from "./user.type";
import { Visibility } from "./visibility.type";
import { BaseAPIResult, Pagination } from ".";

export type Application = {
  id: number;
  name: string;
  displayName: string;
  logo: string;
  image: string;
  url: string;
  size: number;
  isInstalled: boolean;
  isInstalling: boolean;
  isDownloading: boolean;
  isUpdating: boolean;
  isOlip: boolean;
  isUpdateNeeded: boolean;
  applicationTranslations: ApplicationTranslation[];
  applicationType: ApplicationType;
  visibility: Visibility;
};

export type ApplicationFormValue = {
  roleIds: number[];
  userIds: number[];
  users: User[];
  origin?: string;
  visibility: FormVisibility;
};

export type ApplicationTranslation = {
  id: number;
  shortDescription: string;
  longDescription: string;
};

export type ApplicationType = {
  id: string;
  name: string;
  md5: string;
  applicationTypeTranslations: ApplicationTypeTranslations[];
};

export type ApplicationTypeTranslations = {
  id: number;
  label: string;
  description: string;
  language: string;
};

export type FormVisibility = "public" | "byRole" | "byUser";

export type GetApplicationsParams = {
  applicationTypeId?: string;
  isVisibilityEnabled?: boolean;
  languageIdentifier?: string;
  page?: number;
  status?: "installed" | "uninstalled" | "update_needed";
  take?: number;
  withOlip?: string;
};

export type GetApplicationsParamsAPI = {
  applicationTypeId?: string;
  isVisibilityEnabled?: boolean;
  languageIdentifier: string;
  page?: number;
  status?: "installed" | "uninstalled" | "update_needed";
  take?: number;
  withOlip: string;
};

export type GetApplicationsResult = BaseAPIResult & {
  applications?: Application[];
  meta?: Pagination;
};

export type InstallApplicationParams = {
  id: string;
};

export type InstallApplicationResult = BaseAPIResult & {
  application?: Application;
};

export type UninstallApplicationParams = InstallApplicationParams;

export type UpdateApplicationParams = InstallApplicationParams;

export type UninstallApplicationResult = BaseAPIResult;

export type UpdateApplicationResult = BaseAPIResult;

export type UpdateApplicationVisibilityParams = {
  userIds?: Array<number>;
  roleIds?: Array<number>;
};

export type UpdateApplicationVisibilityResult = BaseAPIResult & {
  application?: Application;
};

export type ForceCheckUpdateApplicationResult = boolean;
