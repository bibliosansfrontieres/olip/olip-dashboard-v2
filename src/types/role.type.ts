import { APILanguage, BaseAPIResult } from ".";
import { Permission } from "./user.type";

export type GetRolesParams = {
  languageIdentifier?: string;
};

export type GetRolesParamsAPI = {
  languageIdentifier: string;
};

export type GetRolesResult = BaseAPIResult & {
  roles?: Role[];
};

export type Role = {
  id: number;
  name?: "admin" | "animator" | "user" | "user_no_password";
  permissions: Permission[];
  roleTranslations: RoleTranslation[];
};

type RoleTranslation = {
  id: number;
  label: string;
  language: APILanguage;
};
