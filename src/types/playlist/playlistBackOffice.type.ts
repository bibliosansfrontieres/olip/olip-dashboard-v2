import { WithApplicationSimplified } from "src/types/application/withApplicationSimplified.type";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";
import { PlaylistBase } from "src/types/playlist/playlistBase.type";
import { WithPlaylistTranslations } from "src/types/playlistTranslation/withPlaylistTranslations.type";

export type PlaylistBackOffice = PlaylistBase &
  WithPlaylistTranslations<WithLanguageSimplified> &
  WithApplicationSimplified;
