export type PlaylistBase = {
  id: string;
  image: string;
  isInstalled: boolean;
  isUpdateNeeded: boolean;
  isBroken: boolean;
  isManuallyInstalled: boolean;
  version: string | null;
  size: number;
};
