import { PlaylistBase } from "./playlistBase.type";

export type WithPlaylist<T = object> = {
  playlist: PlaylistBase & T;
};
