export type ApplicationTranslationBase = {
  id: number;
  shortDescription: string;
  longDescription: string;
};
