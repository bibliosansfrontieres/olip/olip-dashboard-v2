import { ApplicationTranslationBase } from "./applicationTranslationBase.type";

export type WithApplicationTranslations<T = object> = {
  applicationTranslations: (ApplicationTranslationBase & T)[];
};
