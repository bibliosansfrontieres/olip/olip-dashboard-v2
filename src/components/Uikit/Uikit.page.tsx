import { useState } from "react";
import {
  Alert,
  Box,
  Button,
  Chip,
  Container,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  Link,
  OutlinedInput,
  Pagination,
  RadioGroup,
  Select,
  SvgIcon,
  Switch,
  Tab,
  Tabs,
  TextField,
  Typography,
} from "@mui/material";
import ArrowDownwardRoundedIcon from "@mui/icons-material/ArrowDownwardRounded";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

import OLIPBreadcrumbs from "src/components/common/OLIPBreadcrumbs/OLIPBreadcrumbs";
import SearchInput from "src/components/common/SearchInput/SearchInput";
import SearchIcon from "src/assets/icons/SearchIcon";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import KOIcon from "src/assets/icons/KO";
import OKIcon from "src/assets/icons/OK";
import InfoIcon from "src/assets/icons/Info";
import AlertIcon from "src/assets/icons/Alert";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import OLIPRadio from "src/components/common/OLIPRadio/OLIPRadio";
import OLIPCheckbox from "src/components/common/OLIPCheckbox/OLIPCheckbox";

export default function UikitPage(): JSX.Element {
  const [showPassword, setShowPassword] = useState(false);
  return (
    <Container sx={{ marginY: "3rem" }}>
      <Typography variant="h1" sx={{ textAlign: "center" }}>
        Thème UI KIT
      </Typography>
      <Typography variant="h2" sx={{ marginBottom: "2rem" }}>
        1. Typography
      </Typography>
      <Box component="section" display="flex" justifyContent="center">
        <Box>
          <Typography variant="h1">H1 (60/70)</Typography>
          <Typography variant="h2">H2 (44/54)</Typography>
          <Typography variant="h3">H3 (32/42)</Typography>
          <Typography variant="h4">H4 (24/34)</Typography>
          <Typography variant="body1">Body regular (18/28)</Typography>
          <Typography variant="body1m" component="p">
            Body medium (18/28)
          </Typography>
          <Typography variant="body1sb" component="p">
            Body semibold (18/28)
          </Typography>
          <Typography variant="link" component="p">
            Lien (18/28)
          </Typography>
          <Typography variant="linkHover" component="p">
            Lien survol (18/28)
          </Typography>
          <Typography variant="info" component="p">
            Petite info (16/28)
          </Typography>
          <Typography variant="tab" component="p">
            Onglet (16/28)
          </Typography>
          <Typography variant="breadcrumb" component="p">
            Fil ariane unselect (14/22)
          </Typography>
          <Typography variant="breadcrumbSelect" component="p">
            Fil ariane select (14/22)
          </Typography>
          <Typography variant="caption" component="p">
            Petit message (14/22)
          </Typography>

          <Typography>Lien :</Typography>
          <Link variant="link">Lien dans le site</Link>
        </Box>
      </Box>
      <Typography variant="h2" sx={{ marginY: "2rem" }}>
        2. Boutons
      </Typography>
      <Box
        component="section"
        display="flex"
        justifyContent="center"
        flexDirection="column"
        width="100%"
      >
        <Box width="100%">
          <Typography variant="h4">Bouton primaire</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Button variant="contained">Je suis un bouton</Button>
            <Button variant="contained">
              <ArrowDownwardRoundedIcon />
            </Button>
            <Button
              variant="contained"
              startIcon={<ArrowDownwardRoundedIcon />}
              endIcon={<ArrowDownwardRoundedIcon />}
            >
              Je suis un bouton
            </Button>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Bouton secondaire</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Button variant="outlined">Je suis un bouton</Button>
            <Button variant="outlined">
              <ArrowDownwardRoundedIcon />
            </Button>
            <Button
              variant="outlined"
              startIcon={<ArrowDownwardRoundedIcon />}
              endIcon={<ArrowDownwardRoundedIcon />}
            >
              Je suis un bouton
            </Button>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Bouton lien</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Button variant="text">Je suis un bouton</Button>
            <Button variant="text">
              <ArrowDownwardRoundedIcon />
            </Button>
            <Button
              variant="text"
              startIcon={<ArrowDownwardRoundedIcon />}
              endIcon={<ArrowDownwardRoundedIcon />}
            >
              Je suis un bouton
            </Button>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Bouton disable</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Button variant="contained" disabled>
              Je suis un bouton
            </Button>
            <Button variant="text" disabled>
              <ArrowDownwardRoundedIcon />
            </Button>
            <Button
              variant="text"
              startIcon={<ArrowDownwardRoundedIcon />}
              endIcon={<ArrowDownwardRoundedIcon />}
              disabled
            >
              Je suis un bouton
            </Button>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">
            Bouton Rounded (Retour/Ouvrir app)
          </Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Button variant="rounded">Je suis un bouton</Button>
            <Button variant="rounded">
              <ArrowDownwardRoundedIcon />
            </Button>
            <Button
              variant="rounded"
              startIcon={<ArrowDownwardRoundedIcon />}
              endIcon={<ArrowDownwardRoundedIcon />}
            >
              Je suis un bouton
            </Button>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Filtres activés</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Chip
              variant="filter"
              label="Agriculture"
              onDelete={() => null}
              deleteIcon={<CloseRoundedIcon />}
            />
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Tags</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Chip variant="tag" label="Tag" onClick={() => null} />
            <Chip variant="tagActive" label="Tag" onClick={() => null} />
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Ariane</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <OLIPBreadcrumbs
              links={[
                { link: "#", name: "MUI" },
                { link: "#", name: "Core" },
                { link: "#", name: "Breadcrumbs" },
              ]}
            />
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Pagination</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Pagination count={10} shape="rounded" />
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Tabulation</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <Tabs
              value={0}
              onChange={() => null}
              aria-label="basic tabs example"
            >
              <Tab label="Item One" />
              <Tab label="Item Two" />
              <Tab label="Item Three" />
            </Tabs>
          </Box>
        </Box>
      </Box>
      <Typography variant="h2" sx={{ marginY: "2rem" }}>
        3. Formulaires
      </Typography>
      <Box
        component="section"
        display="flex"
        justifyContent="center"
        flexDirection="column"
        width="100%"
      >
        <Box width="100%">
          <Typography variant="h4">Champs de recherche</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <SearchInput />
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Champs simple</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <FormControl sx={{ m: 1, width: "50ch" }}>
              <OutlinedInput color="dark" type="text" placeholder="Email" />
            </FormControl>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Champs icône</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <FormControl sx={{ m: 1, width: "50ch" }}>
              <OutlinedInput
                color="dark"
                placeholder="Email"
                startAdornment={
                  <InputAdornment position="start">
                    <SvgIcon
                      component={SearchIcon}
                      sx={{
                        paddingRight: "0.5rem",
                      }}
                    />
                  </InputAdornment>
                }
                type="text"
              />
            </FormControl>
            <FormControl sx={{ m: 1, width: "50ch" }}>
              <OutlinedInput
                color="dark"
                placeholder="Password"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton onClick={() => setShowPassword(!showPassword)}>
                      <SvgIcon
                        component={
                          showPassword ? VisibilityOffIcon : VisibilityIcon
                        }
                      />
                    </IconButton>
                  </InputAdornment>
                }
                type={showPassword ? "text" : "password"}
              />
            </FormControl>
          </Box>
          <Box width="100%">
            <Typography variant="h4">Erreur</Typography>
            <Box
              display="flex"
              justifyContent="space-around"
              my={2}
              width="100%"
            >
              <FormControl error sx={{ m: 1, width: "50ch" }}>
                <OutlinedInput
                  color="dark"
                  placeholder="Email"
                  startAdornment={
                    <InputAdornment position="start">
                      <SvgIcon
                        component={SearchIcon}
                        sx={{
                          paddingRight: "0.5rem",
                        }}
                      />
                    </InputAdornment>
                  }
                  type="text"
                />
                <FormHelperText>
                  Le mot de passe ne doit pas contenir de caractère spéciaux
                  tels que ....
                </FormHelperText>
              </FormControl>
              <FormControl color="success" focused sx={{ m: 1, width: "50ch" }}>
                <OutlinedInput
                  placeholder="Email"
                  startAdornment={
                    <InputAdornment position="start">
                      <SvgIcon
                        component={SearchIcon}
                        sx={{
                          paddingRight: "0.5rem",
                        }}
                      />
                    </InputAdornment>
                  }
                  type="text"
                />
                <FormHelperText
                  sx={{ color: (theme) => theme.palette.success.main }}
                >
                  Mot de passe fort
                </FormHelperText>
              </FormControl>
            </Box>
          </Box>
          <Box width="100%">
            <Typography variant="h4">Erreur</Typography>
            <Grid container spacing={4} my={2}>
              <Grid item xs={4}>
                <Alert icon={<KOIcon />} severity="error" color="error">
                  This is an error alert — check it out! Avec un message plus
                  long.
                </Alert>
              </Grid>
              <Grid item xs={4}>
                <Alert icon={<AlertIcon />} severity="warning">
                  This is a warning alert — check it out! Avec un message plus
                  long.
                </Alert>
              </Grid>
              <Grid item xs={4}>
                <Alert icon={<OKIcon />} severity="success">
                  This is a success alert — check it out! Avec un message plus
                  long.
                </Alert>
              </Grid>
              <Grid item xs={12}>
                <Alert icon={<InfoIcon />} severity="info">
                  This is an info alert — check it out! Avec un message plus
                  long.
                </Alert>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Champs descriptif éditable</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <FormControl sx={{ m: 1, width: "50ch" }}>
              <Typography>Description</Typography>
              <TextField color="dark" multiline rows={6}></TextField>
              <FormHelperText>294/300 caractères</FormHelperText>
            </FormControl>
            <FormControl sx={{ m: 1, width: "50ch" }}>
              <Typography>Description</Typography>
              <TextField
                color="dark"
                multiline
                rows={6}
                defaultValue="Lorem ipsum dolor sit amet consectetur. Ut sit pulvinar donec semper. Arcu odio sit ultrices sagittis cum. Et nulla a porta aliquam id. Adipiscing eget vel quam id quis. At sed turpis congue nulla neque."
              ></TextField>
              <FormHelperText>294/300 caractères</FormHelperText>
            </FormControl>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Liste déroulante</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <OLIPSelect
              choices={["Choix 1", "Choix 2", "Choix 3"]}
              onChange={() => null}
              placeholder="Sélectionner"
              values={["Choix 3"]}
            />
            <OLIPSelect
              choices={["Choix 1", "Choix 2", "Choix 3"]}
              onChange={() => null}
              placeholder="Sélectionner"
              values={""}
            />
            <Select placeholder="test" sx={{ width: "20ch" }} />
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Radiobutton</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <FormControl>
              <RadioGroup row>
                <FormControlLabel
                  value="female"
                  control={<OLIPRadio />}
                  label="Female"
                />
                <FormControlLabel
                  value="male"
                  control={<OLIPRadio />}
                  label="Male"
                />
                <FormControlLabel
                  value="other"
                  control={<OLIPRadio />}
                  label="Other"
                />
                <FormControlLabel
                  value="disabled"
                  disabled
                  control={<OLIPRadio />}
                  label="other"
                />
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Checkbox</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <FormControl>
              <RadioGroup row>
                <FormControlLabel
                  value="female"
                  control={<OLIPCheckbox />}
                  label="Female"
                />
                <FormControlLabel
                  value="male"
                  control={<OLIPCheckbox />}
                  label="Male"
                />
                <FormControlLabel
                  value="other"
                  control={<OLIPCheckbox />}
                  label="Other"
                />
                <FormControlLabel
                  value="disabled"
                  disabled
                  control={<OLIPCheckbox />}
                  label="other"
                />
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
        <Box width="100%">
          <Typography variant="h4">Toggle</Typography>
          <Box display="flex" justifyContent="space-around" my={2} width="100%">
            <FormControl>
              <RadioGroup row>
                <FormControlLabel
                  value="female"
                  control={<Switch />}
                  label="Female"
                />
                <FormControlLabel
                  value="male"
                  control={<Switch />}
                  label="Male"
                />
                <FormControlLabel
                  value="other"
                  control={<Switch />}
                  label="Other"
                />
                <FormControlLabel
                  value="disabled"
                  disabled
                  control={<Switch />}
                  label="other"
                />
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
      </Box>
    </Container>
  );
}
