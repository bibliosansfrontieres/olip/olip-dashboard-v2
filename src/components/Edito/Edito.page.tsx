import i18next from "i18next";
import { redirect, useLoaderData } from "react-router-dom";
import { getLastEdito } from "src/services/edito.service";
import { Edito } from "src/types/edito.type";
import NavigationBar from "src/components/common/NavigationBar/NavigationBar";
import { Page } from "src/types";
import { Box, Container, Stack, Typography } from "@mui/material";
import { getSetting } from "src/services/setting.service";
import getEnv from "src/utils/getEnv";

export async function editoLoader() {
  const { setting } = await getSetting("is_edito_displayed");
  if (setting?.value !== "true") {
    return redirect("/");
  }
  const { edito } = await getLastEdito({
    languageIdentifier: i18next.language,
  });

  return {
    edito,
  };
}

export default function EditoPage(): JSX.Element {
  const { edito } = useLoaderData() as { edito: Edito };

  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    { link: "/edito", name: "edito.pageTitle" },
  ];

  return (
    <>
      <NavigationBar links={links} />
      <Container maxWidth="md" sx={{ marginBottom: "5rem" }}>
        <Stack spacing={5}>
          <Typography variant="h2">
            {edito.editoTranslations[0].title}
          </Typography>
          <Box
            sx={{
              backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
                edito?.image
              })`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              borderRadius: "1rem",
              aspectRatio: "4/3",
            }}
          />
          <Typography style={{ whiteSpace: "pre-wrap" }}>
            {edito.editoTranslations[0].description}
          </Typography>
          {edito.editoParagraphs.map((paragraph) => (
            <Typography key={paragraph.id} style={{ whiteSpace: "pre-wrap" }}>
              {paragraph.editoParagraphTranslations[0].text}
            </Typography>
          ))}
        </Stack>
      </Container>
    </>
  );
}
