import { Item } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

import { Box } from "@mui/material";

export default function ItemContentAudio({
  item,
}: {
  item: Item;
}): JSX.Element {
  return (
    <Box display="flex" justifyContent="center" alignItems="center" my={10}>
      <audio
        controls
        src={`${getEnv("VITE_AXIOS_BASEURL")}${item.file.path}`}
      />
    </Box>
  );
}
