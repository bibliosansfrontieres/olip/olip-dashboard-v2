import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import VideoIcon from "src/assets/icons/Video";
import { Item } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

import { Box, Button } from "@mui/material";

export default function ItemContentVideo({
  item,
}: {
  item: Item;
}): JSX.Element {
  const { t } = useTranslation();
  const [launchVideo, setLaunchVideo] = useState<boolean>(false);

  useEffect(() => {
    setLaunchVideo(false);
  }, [setLaunchVideo, item]);

  const startVideo = () => {
    setLaunchVideo(true);
    const videoPlayer = document.getElementById(
      "main-video",
    ) as HTMLVideoElement;
    videoPlayer?.play();
  };

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      position="relative"
      my={5}
    >
      <video
        id="main-video"
        controls={launchVideo}
        loop={false}
        muted={false}
        playsInline
        poster={getEnv("VITE_AXIOS_BASEURL") + item.thumbnail}
        style={{ borderRadius: "0.5rem", width: "100%" }}
        src={getEnv("VITE_AXIOS_BASEURL") + item.file.path}
      />
      {!launchVideo && (
        <Box
          sx={{
            borderRadius: "0.5rem",
            height: "100%",
            position: "absolute",
            width: "100%",
          }}
        >
          <Button
            data-testid="launchVideoButton"
            variant="contained"
            color="secondary"
            onClick={startVideo}
            startIcon={
              <VideoIcon fontSize="small" sx={{ marginRight: "0.5rem" }} />
            }
            sx={{
              boxShadow: "none",
              top: "50%",
              left: "50%",
              transform: "translate(-50%,-50%)",
            }}
          >
            {t("item.video.button")}
          </Button>
        </Box>
      )}
    </Box>
  );
}
