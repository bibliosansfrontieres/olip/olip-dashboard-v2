import { Box } from "@mui/material";
import { Item } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

export default function ItemContentImage({
  item,
}: {
  item: Item;
}): JSX.Element {
  return (
    <Box display="flex" justifyContent="center" alignItems="center" my={5}>
      <img
        src={`${getEnv("VITE_AXIOS_BASEURL")}${item.file.path}`}
        alt={item.dublinCoreItem.dublinCoreItemTranslations[0].title}
        style={{ width: "100%" }}
      />
    </Box>
  );
}
