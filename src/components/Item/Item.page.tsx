import { redirect, useLoaderData } from "react-router-dom";
import NavigationBar from "src/components/common/NavigationBar/NavigationBar";
import DownloadMedia from "src/components/Item/DownloadMedia/DownloadMedia";
import ItemContentAudio from "src/components/Item/ItemContentAudio/ItemContentAudio";
import ItemContentImage from "src/components/Item/ItemContentImage/ItemContentImage";
import ItemContentText from "src/components/Item/ItemContentText/ItemContentText";
import ItemContentVideo from "src/components/Item/ItemContentVideo/ItemContentVideo";
import ItemErrorPage from "src/components/Item/ItemError.page";
import ItemHeader from "src/components/Item/ItemHeader/ItemHeader";
import SeeMoreItems from "src/components/Item/SeeMoreItems/SeeMoreItems";
import { ITEM_TYPE } from "src/constants";
import { getCategory } from "src/services/category.service";
import { getItem, getSuggestionsItems } from "src/services/item.service";
import { Page } from "src/types";
import { Category } from "src/types/category.type";
import { Item } from "src/types/item.type";

import { Box, Container } from "@mui/material";

export async function itemLoader({
  params,
  request,
}: {
  params: any;
  request: any;
}) {
  const { itemId } = params;
  if (!itemId) {
    return redirect("/catalog");
  }
  const url = new URL(request.url);
  const categoryId: string = url.searchParams.get("category") || "1";
  const hasContext = Boolean(url.searchParams.get("category"));

  const { item } = await getItem({
    id: itemId,
  });

  let category: Category | undefined;

  if (hasContext) {
    const result = await getCategory({
      id: categoryId,
      take: 12,
    });
    category = result?.category;
  }

  const { items: seeMoreItems } = await getSuggestionsItems({
    id: itemId,
    categoryId: categoryId,
    take: 12,
  });

  return { category: hasContext ? category : undefined, item, seeMoreItems };
}

export default function ItemPage(): React.ReactNode {
  const { category, item } = useLoaderData() as {
    category: Category;
    item: Item;
  };
  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    ...(category
      ? [
          {
            link: `/category/${category.id}`,
            name: category.categoryTranslations[0].title,
          },
        ]
      : []),
    {
      link: "",
      name: item?.dublinCoreItem.dublinCoreItemTranslations[0].title || "",
    },
  ];

  if (item) {
    return (
      <>
        <NavigationBar links={links} />
        <Container maxWidth="lg" disableGutters sx={{ paddingX: "2.5rem" }}>
          <ItemHeader item={item} />
          <Box mb={15}>
            {item?.dublinCoreItem.dublinCoreType.id === ITEM_TYPE.AUDIO.id && (
              <ItemContentAudio item={item} />
            )}
            {item?.dublinCoreItem.dublinCoreType.id === ITEM_TYPE.TEXT.id && (
              <ItemContentText item={item} />
            )}
            {item?.dublinCoreItem.dublinCoreType.id === ITEM_TYPE.VIDEO.id && (
              <ItemContentVideo item={item} />
            )}
            {item?.dublinCoreItem.dublinCoreType.id === ITEM_TYPE.IMAGE.id && (
              <ItemContentImage item={item} />
            )}
            {item.file && <DownloadMedia file={item.file} />}
          </Box>
          <SeeMoreItems />
        </Container>
      </>
    );
  }
  return <ItemErrorPage />;
}
