import { useTranslation } from "react-i18next";
import { File } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

import { Box, Button } from "@mui/material";

export default function DownloadMedia({ file }: { file: File }): JSX.Element {
  const { t } = useTranslation();

  async function downloadFile(
    url = getEnv("VITE_AXIOS_BASEURL") + file.path,
    name = file.name,
  ) {
    let blobUrl: any;
    try {
      const response = await fetch(url);
      const blob = await response.blob();
      blobUrl = window.URL.createObjectURL(
        new Blob([blob], { type: file.mimeType }),
      );
    } catch (e) {
      console.error("Erreur au téléchargement initial : " + e);
    } finally {
      const anchor = document.createElement("a");
      anchor.href = blobUrl || url;
      anchor.download = name;
      document.body.appendChild(anchor);
      anchor.click();
      anchor.remove();
    }
  }

  return (
    <Box display="flex" justifyContent="center">
      <Button
        onClick={() => downloadFile()}
        variant="contained"
        data-testid="downloadButton"
      >
        {t("item.download.button", {
          size: (Number(file.size) / 1024 / 1024).toFixed(2),
        })}
      </Button>
    </Box>
  );
}
