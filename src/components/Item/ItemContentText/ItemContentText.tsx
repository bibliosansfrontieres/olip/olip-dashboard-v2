import { Box } from "@mui/material";
import { Item } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

export default function ItemContentText({ item }: { item: Item }): JSX.Element {
  return (
    <Box display="flex" justifyContent="center" alignItems="center" my={5}>
      <iframe
        height="800"
        src={`${getEnv("VITE_AXIOS_BASEURL")}${item.file.path}#view=fit`}
        style={{ width: "100%", border: "none" }}
      />
    </Box>
  );
}
