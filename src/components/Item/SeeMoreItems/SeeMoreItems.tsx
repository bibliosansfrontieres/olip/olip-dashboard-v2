import { useTranslation } from "react-i18next";
import { useLoaderData } from "react-router-dom";
import { Item } from "src/types/item.type";

import ArrowBackRounded from "@mui/icons-material/ArrowBackRounded";
import ArrowForwardRounded from "@mui/icons-material/ArrowForwardRounded";
import { Box, IconButton, Stack, Typography } from "@mui/material";

import ItemCard from "../ItemCard/ItemCard";

export default function SeeMoreItems(): JSX.Element {
  const { t } = useTranslation();
  const { seeMoreItems } = useLoaderData() as { seeMoreItems: Item[] };

  const scrollPlaylists = (appendValue: number) => {
    const element = document.getElementById("itemsStack");
    if (element !== null) {
      element.scrollLeft += appendValue;
    }
  };
  if (seeMoreItems.length) {
    return (
      <Box component="section" mb={20} data-testid="seeMoreItems">
        <Box
          display="flex"
          justifyContent="space-between"
          data-testid="seeMoreItemsTitle"
        >
          <Typography variant="h3">{t("item.seeMore.title")}</Typography>
          <Box
            sx={{
              display: { xs: "none", sm: "block" },
              textAlign: "center",
            }}
          >
            <IconButton
              data-testid="seeMoreItemsScrollLeft"
              onClick={() =>
                scrollPlaylists(
                  ((document.getElementById("itemsStack")?.clientWidth || 0) +
                    48 || 0) * -1,
                )
              }
              sx={{
                backgroundColor: (theme) => theme.palette.dark.main,
                color: (theme) => theme.palette.primary.light,
                padding: "1rem",
                "&:hover": {
                  backgroundColor: (theme) => theme.palette.dark.dark,
                },
              }}
            >
              <ArrowBackRounded fontSize="large" />
            </IconButton>
            <IconButton
              data-testid="seeMoreItemsScrollRight"
              onClick={() =>
                scrollPlaylists(
                  (document.getElementById("itemsStack")?.clientWidth || 0) +
                    48 || 0,
                )
              }
              sx={{
                backgroundColor: (theme) => theme.palette.dark.main,
                color: (theme) => theme.palette.primary.light,
                marginLeft: "1rem",
                padding: "1rem",
                "&:hover": {
                  backgroundColor: (theme) => theme.palette.dark.dark,
                },
              }}
            >
              <ArrowForwardRounded fontSize="large" />
            </IconButton>
          </Box>
        </Box>
        <Stack
          id="itemsStack"
          direction="row"
          spacing={6}
          sx={{
            overflowX: { xs: "auto", sm: "hidden" },
            paddingBottom: { xs: "2rem", sm: "0.0625rem" },
            paddingTop: "3rem",
            scrollBehavior: "smooth",
            width: "fit-content(min(100%, max(10000%, 100%)))",
            "@media (min-width: 1536px)": {
              "& > div:first-of-type": {
                marginLeft: "calc((100vw - 1536px - 1.5rem)/2)",
              },
              "& > div:last-of-type": {
                marginRight: "calc((100vw - 1536px - 1.5rem)/2)",
              },
            },
          }}
        >
          {seeMoreItems.map((item) => (
            <ItemCard
              key={item.id}
              item={item}
              sxCard={{
                "--nb-display-card": { xs: "1", sm: "2", md: "3", lg: "4" },
                "--stack-spacing": "3rem",
                "--padding-card": "1rem",
                "--border-size-card": "0.0625rem",
                minWidth:
                  "calc((100% - ((var(--nb-display-card) - 1) * var(--stack-spacing)) - (var(--nb-display-card) * (var(--padding-card) + var(--border-size-card)) * 2)) / var(--nb-display-card))",
              }}
            />
          ))}
        </Stack>
      </Box>
    );
  }
  return <></>;
}
