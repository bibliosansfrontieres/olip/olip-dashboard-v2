import { useTranslation } from "react-i18next";
import AudioIcon from "src/assets/icons/Audio";
import PictureIcon from "src/assets/icons/Picture";
import TextIcon from "src/assets/icons/Text";
import VideoIcon from "src/assets/icons/Video";
import { Item } from "src/types/item.type";

import { Chip, Typography } from "@mui/material";

export default function ItemHeader({ item }: { item: Item }): JSX.Element {
  const { t } = useTranslation();
  let icon;
  switch (item?.dublinCoreItem.dublinCoreType.id) {
    case 1:
      icon = <TextIcon color="inherit" fontSize="small" />;
      break;
    case 2:
      icon = (
        <VideoIcon data-testid="videoIcon" color="inherit" fontSize="small" />
      );
      break;
    case 3:
      icon = <AudioIcon color="inherit" fontSize="small" />;
      break;
    case 4:
      icon = <PictureIcon color="inherit" fontSize="small" />;
      break;
    default:
      break;
  }
  return (
    <>
      <Chip
        variant="tagNotClickable"
        icon={icon}
        label={t(`item.type.${item?.dublinCoreItem.dublinCoreType.id}`)}
      />
      <Typography variant="h2" marginY={3} data-testid="itemHeaderTitle">
        {item?.dublinCoreItem.dublinCoreItemTranslations[0].title}
      </Typography>
      <Typography data-testid="itemHeaderDescription">
        {item?.dublinCoreItem.dublinCoreItemTranslations[0].description}
      </Typography>
    </>
  );
}
