import { useTranslation } from "react-i18next";
import { Link, useParams } from "react-router-dom";
import ApplicationChip from "src/components/common/ApplicationChip/ApplicationChip";
import ContentType from "src/components/common/ContentType/ContentType";
import { Item } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

import ArrowForwardRoundedIcon from "@mui/icons-material/ArrowForwardRounded";
import LaunchIcon from "@mui/icons-material/Launch";
import { Box, Button, SxProps, Typography } from "@mui/material";

export default function ItemCard({
  item,
  sxCard,
}: {
  item: Item;
  sxCard?: SxProps;
}): React.ReactNode {
  const { t } = useTranslation();
  const { categoryId } = useParams();

  const fromExternalApplication = !item.application.isOlip;
  /**
   * href to use for the item card. If the item is from an external application
   * and has a url or its application has a url, it returns the url prefixed with
   * http://. If not, it returns undefined.
   */
  const getItemUrl = () => {
    if (fromExternalApplication && (item.url || item.application.url)) {
      return "http://" + (item.url ?? item.application.url);
    }
    return undefined;
  };

  return (
    <Box
      data-testid="itemsCard"
      component={fromExternalApplication ? "a" : Link}
      to={
        fromExternalApplication
          ? undefined
          : `/item/${item.id}${categoryId ? `?category=${categoryId}` : ""}`
      }
      href={getItemUrl()}
      target={fromExternalApplication ? "_blank" : undefined}
      rel={fromExternalApplication ? "noreferrer" : undefined}
      sx={{
        border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
        borderRadius: "1rem",
        color: "inherit",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        aspectRatio: "8/11",
        padding: "1rem",
        textDecoration: "none",
        maxWidth: "25rem",
        margin: "auto",
        "&:hover": {
          border: (theme) => `0.125rem solid ${theme.palette.dark.main}`,
          padding: "calc(1rem - 0.0625rem)",
          "& button": {
            fontWeight: "600",
          },
        },
        ...sxCard,
      }}
    >
      <Box
        flex={4}
        sx={{
          backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
            item.thumbnail
          })`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderRadius: "0.5rem",
          flex: 4,
          height: "100%",
          position: "relative",
          width: "100%",
        }}
      >
        {item?.dublinCoreItem?.dublinCoreType?.id && (
          <Box
            sx={{
              borderRadius: "0.5rem",
              backgroundColor: (theme) => theme.palette.primary.light,
              width: "fit-content",
              margin: "0.625rem",
              padding: "0.5rem",
            }}
          >
            {fromExternalApplication ? (
              <ApplicationChip application={item.application} />
            ) : (
              <ContentType
                typeId={item.dublinCoreItem.dublinCoreType.id}
                iconProps={{ fontSize: "small" }}
              />
            )}
          </Box>
        )}
      </Box>
      <Typography
        data-testid="itemCardTitle"
        variant="body1m"
        sx={{
          flex: "2",
          marginTop: "1.5rem",
          overflow: "auto",
        }}
      >
        {item.dublinCoreItem.dublinCoreItemTranslations[0].title}
      </Typography>
      <Button
        id={`button-access-${item.id}`}
        endIcon={
          fromExternalApplication ? <LaunchIcon /> : <ArrowForwardRoundedIcon />
        }
        sx={{
          maxWidth: "100%",
          paddingX: 0,
          paddingBottom: "1rem",
        }}
      >
        <Typography
          component="span"
          sx={{
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
        >
          {t(`item.card.link.${item?.dublinCoreItem?.dublinCoreType?.id || 1}`)}
        </Typography>
      </Button>
    </Box>
  );
}
