import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLoaderData, useRevalidator } from "react-router-dom";
import ApplicationsSettingsTabs from "src/components/settings/ApplicationsSettings/ApplicationsSettingsTabs/ApplicationsSettingsTabs";
import { ApplicationTypesContextProvider } from "src/contexts/applicationTypes.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import useInternetConnected from "src/hooks/useInternetConnected";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import {
  forceCheckUpdate,
  getApplications,
} from "src/services/application.service";
import { Application } from "src/types/application.type";
import { ApplicationEvent } from "src/types/socket.type";

import AutorenewIcon from "@mui/icons-material/Autorenew";
import { Box, Button, Typography } from "@mui/material";

export async function applicationsSettingsLoader({
  request,
}: {
  request: any;
}) {
  const url = new URL(request.url);
  const applicationTypeId = url.searchParams.get("applicationTypeId");
  const take = url.searchParams.get("take");
  const page = url.searchParams.get("page");
  const { applications, meta } = await getApplications({
    page: Number(page) || 1,
    take: Number(take) || 12,
    applicationTypeId: applicationTypeId || undefined,
    isVisibilityEnabled: false,
  });
  return { applications, meta };
}

export default function ApplicationsSettingsPage(): JSX.Element {
  const { t } = useTranslation();
  const { applications: loaderApplications } = useLoaderData() as {
    applications: Application[];
  };
  const [applications, setApplications] =
    useState<Application[]>(loaderApplications);

  const { displaySnackbar } = useContext(SnackbarContext);

  const isInternetConnected = useInternetConnected();
  const revalidator = useRevalidator();

  const { event } = useSocketOnEvent<ApplicationEvent>("application");

  const [disabled, setDisabled] = useState(false);

  const onForceCheckUpdate = async () => {
    setDisabled(true);
    const result = await forceCheckUpdate();
    setDisabled(false);
    if (result) {
      displaySnackbar(t("applications.force_check_update.success"), "success");
      revalidator.revalidate();
    } else {
      displaySnackbar(t("applications.force_check_update.error"), "error");
    }
  };

  useEffect(() => {
    setApplications(loaderApplications);
  }, [loaderApplications]);

  useEffect(() => {
    setApplications((prev) => {
      if (event) {
        const applicationIndex = prev.findIndex(
          (app) => app.id === event.application.id,
        );
        prev[applicationIndex] = {
          ...prev[applicationIndex],
          isDownloading: event.application.isDownloading,
          isInstalled: event.application.isInstalled,
          isInstalling: event.application.isInstalling,
          isUpdateNeeded: event.application.isUpdateNeeded,
          isUpdating: event.application.isUpdating,
        };
      }
      return [...prev];
    });
  }, [event]);

  return (
    <Box>
      <Typography
        variant="h2"
        sx={{ marginBottom: "1rem" }}
        data-testid="ApplicationsSettingstitle"
      >
        {t("settings.pages.applications.title")}
      </Typography>
      <Box
        sx={{
          borderRadius: "1rem",
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-end",
          marginBottom: "2.5rem",
          padding: "1.25rem",
        }}
      >
        <Button
          onClick={onForceCheckUpdate}
          variant="contained"
          disabled={!isInternetConnected || disabled}
          startIcon={
            <AutorenewIcon sx={{ "&:nth-of-type(1)": { fontSize: "2rem" } }} />
          }
        >
          {t("settings.pages.applications.forceCheckUpdateButton")}
        </Button>
      </Box>
      <ApplicationTypesContextProvider>
        <ApplicationsSettingsTabs applications={applications} />
      </ApplicationTypesContextProvider>
    </Box>
  );
}
