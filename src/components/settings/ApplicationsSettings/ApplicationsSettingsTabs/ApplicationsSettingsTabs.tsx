import { ReactElement, useState } from "react";
import { useTranslation } from "react-i18next";
import { ApplicationTabTypeEnum } from "src/enums/applicationTabType.enum";
import { Application } from "src/types/application.type";
import {
  ApplicationTabTypeDefinition,
  getSortedApplicationTabTypes,
} from "src/utils/applicationsTabTypeDefinitions";

import { Box, Tab, Tabs } from "@mui/material";

import ApplicationsSettingsList from "./ApplicationsSettingsList/ApplicationsSettingsList";

function CustomTabPanel({
  tabSelected,
  tabTypeDefinition,
  applications,
  ...props
}: {
  tabSelected: number;
  tabTypeDefinition: ApplicationTabTypeDefinition;
  applications: Application[];
}) {
  return (
    <div
      role="tabpanel"
      hidden={tabSelected !== tabTypeDefinition.index}
      aria-labelledby={`simple-tab-${tabTypeDefinition.index}`}
      {...props}
    >
      <Box my={4}>
        <ApplicationsSettingsList
          applications={applications.filter(tabTypeDefinition.predicate)}
        />
      </Box>
    </div>
  );
}

function CustomTab({
  type,
  tabTypeDefinition,
  applications,
  ...props
}: {
  type: ApplicationTabTypeEnum;
  applications: Application[];
  tabTypeDefinition: ApplicationTabTypeDefinition;
  value: number;
}): ReactElement {
  const { t } = useTranslation();

  return (
    <Tab
      data-testid={`${type}ApplicationTab`}
      label={t(`settings.pages.applications.applicationsTabs.${type}`, {
        count: applications.filter(tabTypeDefinition.predicate).length,
      })}
      {...props}
    />
  );
}

export default function ApplicationsSettingsTabs({
  applications = [],
}: {
  applications?: Application[];
}): ReactElement {
  const [tabSelected, setTabSelected] = useState<number>(0);

  return (
    <>
      <Tabs
        variant="fullWidth"
        value={tabSelected}
        onChange={(_event: React.SyntheticEvent, newValue: number) =>
          setTabSelected(newValue)
        }
      >
        {getSortedApplicationTabTypes().map(([type, tabTypeDefinition]) => (
          <CustomTab
            key={type}
            type={type}
            tabTypeDefinition={tabTypeDefinition}
            value={tabTypeDefinition.index}
            applications={applications}
          />
        ))}
      </Tabs>
      {getSortedApplicationTabTypes().map(([type, tabTypeDefinition]) => (
        <CustomTabPanel
          key={type}
          tabTypeDefinition={tabTypeDefinition}
          tabSelected={tabSelected}
          applications={applications}
        />
      ))}
    </>
  );
}
