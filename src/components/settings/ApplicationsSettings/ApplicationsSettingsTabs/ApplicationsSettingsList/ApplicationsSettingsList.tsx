import { Box, Container, Grid, Typography } from "@mui/material";
import { Application } from "src/types/application.type";
import ApplicationsSettingsCard from "./ApplicationsSettingsCard/ApplicationsSettingsCard";
import FilterApplicationsSettingsContent from "./FilterApplicationsSettingsContent/FilterApplicationsSettingsContent";

export default function ApplicationsSettingsList({
  applications = [],
}: {
  applications?: Application[];
}): JSX.Element {

  return (
    <>
      <Container maxWidth="xl">
        <Box
          display="flex"
          mb={4}
          sx={{
            display: "flex",
            flexDirection: { xs: "column", sm: "row" },
            justifyContent: "space-between",
          }}
        >
          <Typography
            variant="h3"
            sx={{ marginBottom: { xs: "1rem", sm: "0" } }}
          >
            <FilterApplicationsSettingsContent />
          </Typography>
        </Box>
        <Grid
          container
          spacing={3.75}
          sx={{ marginBottom: "2rem" }}
          alignItems="stretch"
        >
          {applications.map((application) => (
            <Grid item key={application.id} xs={12} sm={6} md={6} lg={4} xl={4}>
              <ApplicationsSettingsCard application={application} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </>
  );
}
