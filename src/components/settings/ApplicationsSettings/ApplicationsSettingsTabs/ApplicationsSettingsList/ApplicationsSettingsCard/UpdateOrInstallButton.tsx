import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { SnackbarContext } from "src/contexts/snackbar.context";
import {
  installApplicationById,
  updateApplicationById,
} from "src/services/application.service";
import { Application } from "src/types/application.type";

import DownloadIcon from "@mui/icons-material/Download";
import { Box, Button, Typography } from "@mui/material";

export default function UpdateOrInstallButton({
  application,
}: {
  application: Application;
}): JSX.Element {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { displaySnackbar } = useContext(SnackbarContext);

  async function handleInstall() {
    const result = await installApplicationById({
      id: application.id.toString(),
    });
    if (result.statusCode === 200) {
      displaySnackbar(t("snackbar.installApplication.success"));
      navigate(".", { replace: true, state: { noNavigationTrigger: true } });
    } else if (result.statusCode === 406) {
      displaySnackbar(
        t("snackbar.installApplication.errorNotEnoughSpace"),
        "error",
      );
    } else {
      displaySnackbar(t("snackbar.installApplication.error"), "error");
    }
  }

  async function handleUpdate() {
    const result = await updateApplicationById({
      id: application.id.toString(),
    });
    if (result.statusCode === 200) {
      displaySnackbar(t("snackbar.updateApplication.success"));
      navigate(".", { replace: true, state: { noNavigationTrigger: true } });
    } else if (result.statusCode === 406) {
      displaySnackbar(
        t("snackbar.updateApplication.errorNotEnoughSpace"),
        "error",
      );
    } else {
      displaySnackbar(t("snackbar.updateApplication.error"), "error");
    }
  }

  if (
    application.isInstalling ||
    application.isDownloading ||
    application.isUpdating
  ) {
    return (
      <>
        <Box
          sx={{
            backgroundColor: "white",
            borderBottomLeftRadius: "1rem",
            borderBottomRightRadius: "1rem",
          }}
          display="flex"
          justifyContent="center"
          alignItems="center"
          p={1}
        >
          <Typography color={(theme) => theme.palette.grey[500]}>
            {application.isDownloading && (
              <>
                {t(
                  "settings.pages.applications.applicationsCard.isDownloading",
                )}
              </>
            )}
            {application.isInstalling && (
              <>
                {t("settings.pages.applications.applicationsCard.isInstalling")}
              </>
            )}
            {application.isUpdating && (
              <>
                {t("settings.pages.applications.applicationsCard.isUpdating")}
              </>
            )}
          </Typography>
        </Box>
      </>
    );
  }

  if (!application.isInstalled) {
    return (
      <>
        <Box
          sx={{
            backgroundColor: (theme) => theme.palette.dark.main,
            borderBottomLeftRadius: "1rem",
            borderBottomRightRadius: "1rem",
          }}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          p={1}
        >
          <Button
            onClick={handleInstall}
            sx={{
              textDecoration: "none",
              color: (theme) => theme.palette.primary.light,
              display: "flex",
              textAlign: "center",
              width: "100%",
              "&:hover": {
                textDecoration: "none",
              },
            }}
            data-testid="installButton"
            disabled={application.isDownloading || application.isInstalling}
            endIcon={<DownloadIcon />}
          >
            {t("settings.pages.applications.applicationsCard.install")}
          </Button>
        </Box>
      </>
    );
  }

  if (application.isUpdateNeeded) {
    return (
      <>
        {application.isUpdateNeeded && (
          <Box
            sx={{
              backgroundColor: (theme) => theme.palette.dark.main,
              borderBottomLeftRadius: "1rem",
              borderBottomRightRadius: "1rem",
            }}
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            p={1}
          >
            <Button
              onClick={handleUpdate}
              sx={{
                textDecoration: "none",
                color: (theme) => theme.palette.primary.light,
                display: "flex",
                textAlign: "center",
                width: "100%",
                "&:hover": {
                  textDecoration: "none",
                },
              }}
              endIcon={<DownloadIcon />}
            >
              {t("settings.pages.applications.applicationsCard.update")}
            </Button>
          </Box>
        )}
      </>
    );
  }
  return <></>;
}
