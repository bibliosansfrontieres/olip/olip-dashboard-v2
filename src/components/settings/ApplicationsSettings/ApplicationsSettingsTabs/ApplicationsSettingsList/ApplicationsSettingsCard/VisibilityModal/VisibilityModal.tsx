import { ElementRef, useContext, useRef } from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import VisibilitySelect from "src/components/settings/Contents/CategoryForm/VisibilitySelect/VisibiltySelect";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { updateApplicationVisibility } from "src/services/application.service";
import { Application, ApplicationFormValue } from "src/types/application.type";
import { getVisibilityType } from "src/utils/getVisibilityType";

import { Box, Button, DialogProps } from "@mui/material";

export default function ApplicationsSettingsVisibilityModal(
  props: DialogProps & { application: Application },
): JSX.Element {
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);
  const navigate = useNavigate();

  const { onClose, application } = props;

  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  const initialValues: ApplicationFormValue = {
    roleIds: application.visibility.roles.map((role) => role.id),
    userIds: application.visibility.users.map((user) => user.id),
    users: application.visibility.users,
    visibility: getVisibilityType(application.visibility),
    origin: "application",
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values) => {
      try {
        const result = await updateApplicationVisibility(application.id, {
          userIds: values.visibility === "byUser" ? values.userIds : undefined,
          roleIds: values.visibility === "byRole" ? values.roleIds : undefined,
        });

        if (result.statusCode === 200) {
          displaySnackbar(t("snackbar.updateApplicationVisibility.success"));
          navigate("/settings/applications");
        } else {
          let listErrors = [];
          if (!Array.isArray(result.errorMessage)) {
            listErrors.push(result.errorMessage);
          } else {
            listErrors = [...result.errorMessage];
          }
          if (listErrors.length > 0) {
            displaySnackbar(
              t("snackbar.updateApplicationVisibility.error"),
              "error",
            );
          }
        }
      } catch (e) {
        displaySnackbar(
          t("snackbar.updateApplicationVisibility.error"),
          "error",
        );
      } finally {
        handleCancel(undefined);
      }
    },
  });

  return (
    <ModalLayout {...props} maxWidth="sm" onClose={(e: any) => handleCancel(e)}>
      <form onSubmit={formik.handleSubmit} data-testid="visibilityModalForm">
        <VisibilitySelect formik={formik} />
        <Button type="submit" ref={submitRef} sx={{ display: "block" }} />
      </form>
      <Box display="flex" justifyContent="flex-end" mt={4}>
        <OLIPDialogActions
          mainButton={{
            action: () => submitRef.current.click(),
            text: t("common.button.save"),
          }}
          secondaryButton={{
            action: () => handleCancel(undefined),
            text: t("form.category.button.cancel"),
          }}
        />
      </Box>
    </ModalLayout>
  );
}
