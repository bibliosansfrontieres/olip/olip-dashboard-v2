import { useTranslation } from "react-i18next";
import UninstallModal from "src/components/settings/ApplicationsSettings/ApplicationsSettingsTabs/ApplicationsSettingsList/ApplicationsSettingsCard/UninstallModal/UninstallModal";
import UpdateOrInstallButton from "src/components/settings/ApplicationsSettings/ApplicationsSettingsTabs/ApplicationsSettingsList/ApplicationsSettingsCard/UpdateOrInstallButton";
import useModal from "src/hooks/useModal";
import { Application } from "src/types/application.type";
import getEnv from "src/utils/getEnv";
import { getVisibilityTranslationFromVisibility } from "src/utils/getVisibilityType";

import ClearIcon from "@mui/icons-material/Clear";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { Avatar, Box, Button, SxProps, Typography } from "@mui/material";

import VisibilityModal from "./VisibilityModal/VisibilityModal";

export default function ApplicationsSettingsCard({
  application,
  sxCard,
}: {
  application: Application;
  sxCard?: SxProps;
}): JSX.Element {
  const { t } = useTranslation();

  const [openModal, toggleModal] = useModal();
  const [openUninstallModal, toggleUninstallModal] = useModal();

  return (
    <Box
      data-testid="applicationCard"
      sx={{
        backgroundColor: (theme) => theme.palette.primary.light,
        borderRadius: "1rem",
        border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
        color: "inherit",
        display: "flex",
        height: "100%",
        textDecoration: "none",
        flexDirection: "column",
        ...sxCard,
      }}
    >
      <Box display="flex" alignItems="center" justifyContent="center" p={2}>
        <Avatar
          data-testid="applicationAvatar"
          variant="square"
          alt=""
          src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
        />
        <Box>
          <Typography variant="h3" ml={1.5} data-testid="applicationName">
            {application.displayName}
          </Typography>
        </Box>
      </Box>
      <Box display="flex" flexDirection="column" p={2}>
        <Typography
          variant="body1sb"
          sx={{
            marginTop: "0.5rem",
            overflow: "auto",
          }}
          data-testid="applicationType"
        >
          {application.applicationType.applicationTypeTranslations[0].label}
        </Typography>
        <Typography
          variant="body1m"
          sx={{
            flex: "auto",
            marginTop: "0.5rem",
            overflow: "auto",
          }}
          data-testid="applicationDescription"
        >
          {application.applicationTranslations[0].shortDescription}
        </Typography>
      </Box>
      <Box display="flex" flexDirection="column" p={2}>
        <Typography
          variant="body1sb"
          sx={{
            marginTop: "0.5rem",
            overflow: "auto",
          }}
          data-testid="applicationVisibility"
        >
          {t("common.visibility.title")}
        </Typography>
        <Typography
          variant="body1m"
          sx={{
            flex: "2",
            marginTop: "0.5rem",
            overflow: "auto",
          }}
        >
          {t(
            `${getVisibilityTranslationFromVisibility(application.visibility)}`,
          )}
        </Typography>
      </Box>
      <Box display="flex" flexDirection="column" flex="auto" p={2}>
        <Typography
          variant="body1sb"
          sx={{
            marginTop: "0.5rem",
            overflow: "auto",
          }}
        >
          {t("settings.pages.applications.applicationsCard.size")}
        </Typography>
        <Typography
          variant="body1m"
          sx={{
            flex: "2",
            marginTop: "0.5rem",
            overflow: "auto",
          }}
          data-testid="applicationSize"
        >
          {(Number(application.size) / 1024).toFixed(2)} Mo
        </Typography>
      </Box>
      {application.isInstalled && (
        <>
          {application.isOlip ? (
            <Box
              data-testid="applicationOlip"
              sx={{
                backgroundColor: (theme) => theme.palette.grey[100],
                borderBottomLeftRadius: "1rem",
                borderBottomRightRadius: "1rem",
              }}
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              p={4.75}
            />
          ) : (
            <Box
              sx={{
                backgroundColor: (theme) => theme.palette.grey[100],
                borderBottomLeftRadius: "1rem",
                borderBottomRightRadius: "1rem",
              }}
              display="flex"
              justifyContent="space-between"
              p={1}
            >
              <>
                <Button
                  onClick={() => toggleModal()}
                  id={`button-access-${application.id}`}
                  sx={{ padding: "0.5rem" }}
                  startIcon={<VisibilityIcon />}
                  disabled={application.isOlip}
                  data-testid="visibilityButton"
                >
                  {t("common.visibility.title")}
                </Button>
                <VisibilityModal
                  onClose={() => toggleModal()}
                  open={openModal}
                  fullWidth
                  maxWidth="lg"
                  application={application}
                />
                <Button
                  id={`button-access-${application.id}`}
                  onClick={() => toggleUninstallModal()}
                  startIcon={<ClearIcon />}
                  disabled={application.isOlip}
                  data-testid="uninstallButton"
                >
                  {t("settings.pages.applications.applicationsCard.uninstall")}
                </Button>
                <UninstallModal
                  onClose={() => toggleUninstallModal()}
                  open={openUninstallModal}
                  fullWidth
                  maxWidth="lg"
                  application={application}
                />
              </>
            </Box>
          )}
        </>
      )}
      <UpdateOrInstallButton application={application} />
    </Box>
  );
}
