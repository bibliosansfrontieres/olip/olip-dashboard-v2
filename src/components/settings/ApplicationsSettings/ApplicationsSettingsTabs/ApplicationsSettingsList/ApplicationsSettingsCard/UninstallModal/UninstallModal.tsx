import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { uninstallApplicationById } from "src/services/application.service";
import { Application } from "src/types/application.type";
import getEnv from "src/utils/getEnv";

import {
  Avatar,
  Box,
  Button,
  DialogActions,
  DialogProps,
  Typography,
} from "@mui/material";

export default function UninstallModal(
  props: DialogProps & { application: Application },
): JSX.Element {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { displaySnackbar } = useContext(SnackbarContext);
  const [isUninstalling, setIsUninstalling] = useState(false);
  const { onClose, application } = props;

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  async function confirmUninstall() {
    setIsUninstalling(true);
    const result = await uninstallApplicationById({
      id: application.id.toString(),
    });
    if (result.statusCode === 200) {
      setIsUninstalling(false);
      displaySnackbar(t("snackbar.uninstallApplication.success"));
      handleCancel(undefined);
      navigate(".", { replace: true, state: { noNavigationTrigger: true } });
    } else {
      setIsUninstalling(false);
      displaySnackbar(t("snackbar.uninstallApplication.error"), "error");
    }
  }

  return (
    <ModalLayout {...props} maxWidth="sm" onClose={(e: any) => handleCancel(e)}>
      <>
        <Box
          data-testid="applicationLogo"
          display="flex"
          alignItems="center"
          sx={{ paddingY: "1rem" }}
        >
          <Avatar
            variant="square"
            alt=""
            src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
            sx={{
              height: "3.5rem",
              width: "3.5rem",
            }}
          />
          <Box display="flex" flexDirection="column" paddingLeft={1}>
            <Typography variant="h2" data-testid="applicationName">
              {application.displayName}
            </Typography>
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-start" flexDirection="column">
          <Typography
            variant="h3"
            sx={{
              marginTop: "1.5rem",
            }}
            data-testid="uninstallModalQuestion"
          >
            {t(
              "settings.pages.applications.applicationsCard.uninstallModal.question",
            )}
          </Typography>
          <Typography
            variant="body1m"
            sx={{
              marginTop: "1rem",
            }}
            data-testid="uninstallModalSubQuestion"
          >
            {t(
              "settings.pages.applications.applicationsCard.uninstallModal.subQuestion",
            )}
          </Typography>
        </Box>
        <DialogActions sx={{ marginTop: "2rem" }}>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
          >
            <Button
              data-testid="uninstallModalCancel"
              onClick={handleCancel}
              type="button"
              variant="outlined"
              sx={{ marginRight: "1.875rem" }}
            >
              {t("modal.close")}
            </Button>
            <Button
              data-testid="uninstallModalConfirm"
              onClick={confirmUninstall}
              variant="contained"
              disabled={isUninstalling}
            >
              {t("settings.pages.applications.applicationsCard.uninstall")}
            </Button>
          </Box>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
