import { useContext } from "react";
import { useTranslation } from "react-i18next";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import { ApplicationTypesContext } from "src/contexts/applicationTypes.context";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";

import { Select, Skeleton } from "@mui/material";

export default function FilterApplicationsSettingsContent(): JSX.Element {
  const { t } = useTranslation();
  const [searchParams, updateSearchParam] = useHandleSearchParams();
  const applicationTypesResult = useContext(ApplicationTypesContext);
  const handleFilter = (event: any) => {
    updateSearchParam([
      {
        name: "applicationTypeId",
        value:
          event.target.value.indexOf("") === -1
            ? event.target.value
                .filter((category: string) => category !== "")
                .toString()
            : "",
        defaultValue: "",
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };
  const choices = applicationTypesResult.result?.applicationTypes.map(
    (appType) => ({
      name: appType?.applicationTypeTranslations?.[0].label,
      value: appType.id,
    }),
  );
  return (
    <WaitApiResult
      useApiResult={applicationTypesResult}
      loadingElement={
        <Skeleton width={"250px"}>
          <Select />
        </Skeleton>
      }
    >
      <OLIPSelect
        choices={choices ?? [{ name: "", value: "" }]}
        sx={{ width: "15rem" }}
        placeholder={t("application.filter.all")}
        onChange={handleFilter}
        values={searchParams?.["applicationTypeId"]?.split(",") || []}
      />
    </WaitApiResult>
  );
}
