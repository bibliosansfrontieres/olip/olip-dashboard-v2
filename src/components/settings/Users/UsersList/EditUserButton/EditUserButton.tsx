import { useTranslation } from "react-i18next";
import { Box, Button } from "@mui/material";
import EditIcon from "src/assets/icons/Edit";
import { User } from "src/types/user.type";
import EditUserModal from "./EditUserModal/EditUserModal";
import useModal from "src/hooks/useModal";

export default function EditUserButton({
  disabled = false,
  onEditComplete,
  user,
}: {
  disabled: boolean;
  onEditComplete: () => void;
  user: User;
}): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  const handleClose = () => {
    toggleModal();
    onEditComplete();
  };

  return (
    <>
      <Button
        data-testid="editUserButton"
        variant="text"
        disabled={disabled}
        onClick={() => toggleModal()}
        startIcon={<EditIcon />}
        sx={{ paddingY: 0 }}
      >
        <Box
          sx={{
            display: { xs: "none", md: "flex" },
          }}
        >
          {t("common.button.edit")}
        </Box>
      </Button>
      <EditUserModal
        onClose={handleClose}
        open={openModal}
        user={user}
        fullWidth
        maxWidth="sm"
      />
    </>
  );
}
