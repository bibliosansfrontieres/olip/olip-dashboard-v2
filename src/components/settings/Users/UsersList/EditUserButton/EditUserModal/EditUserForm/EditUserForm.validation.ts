import { supportedLanguages } from "src/constants";
import * as yup from "yup";

export default yup.object({
  role: yup
    .number()
    .oneOf([1, 2, 3, 4], "form.user.validation.role.oneOf")
    .required("form.user.validation.role.mandatory"),
  username: yup
    .string()
    .min(6, "form.user.validation.username.min")
    .required("form.user.validation.username.mandatory"),
  language: yup
    .string()
    .oneOf(
      supportedLanguages.map((language) => language.id),
      "form.user.validation.language.oneOf",
    )
    .required("form.user.validation.language.mandatory"),
});
