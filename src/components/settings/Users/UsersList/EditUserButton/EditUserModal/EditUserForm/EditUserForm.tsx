import { MutableRefObject, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useFormik } from "formik";
import UserForm from "src/components/settings/Users/UserForm/UserForm";
import { User, UserFormValues } from "src/types/user.type";
import { updateUser } from "src/services/user.service";
import validationSchema from "./EditUserForm.validation";
import { SnackbarContext } from "src/contexts/snackbar.context";
import getEnv from "src/utils/getEnv";

export default function EditUserForm({
  onSubmitting,
  onSuccess,
  submitRef,
  user,
}: {
  onSubmitting: (value: boolean) => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
  user: User;
}): JSX.Element {
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);

  const initialValues: UserFormValues = {
    mode: "edit",
    role: user.role.id || undefined,
    username: user.username || "",
    password: "",
    language: user.language || getEnv("VITE_DEFAULT_LANGUAGE") || "eng",
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values, { resetForm, setFieldError }) => {
      setDisplayGenericError(false);

      const result = await updateUser(user.id, {
        username: values.username,
        password:
          values.role !== 4 && values.password !== ""
            ? values.password
            : undefined,
        language: values.language,
        roleId: values.role,
      });
      if (result.statusCode === 200) {
        resetForm();
        displaySnackbar(
          t("snackbar.updateUser.success", { username: values.username })
        );
        onSuccess();
      } else {
        let listErrors = [];
        if (!Array.isArray(result.errorMessage)) {
          listErrors.push(result.errorMessage);
        } else {
          listErrors = [...result.errorMessage];
        }
        listErrors.forEach((errorMessage) => {
          switch (errorMessage) {
            case "password must be longer than or equal to 6 characters":
              setFieldError(
                "password",
                t(`form.user.error.${result.errorMessage}`)
              );
              break;
            case "password is needed for this role":
              setFieldError(
                "password",
                t(`form.user.error.${result.errorMessage}`)
              );
              break;
            case "username already exists":
              setFieldError(
                "username",
                t(`form.user.error.${result.errorMessage}`, {
                  user: values.username,
                })
              );
              break;
            default:
              setDisplayGenericError(true);
          }
        });
      }
    },
    validationSchema: validationSchema,
  });

  useEffect(() => {
    onSubmitting(formik.isSubmitting);
  }, [formik.isSubmitting]);

  return (
    <UserForm
      displayGenericError={displayGenericError}
      formik={formik}
      submitRef={submitRef}
    />
  );
}
