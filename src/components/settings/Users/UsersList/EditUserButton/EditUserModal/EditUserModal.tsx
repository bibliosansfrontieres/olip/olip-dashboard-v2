import { ElementRef, useRef, useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  DialogActions,
  DialogProps,
  Typography,
} from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { User } from "src/types/user.type";
import { useTranslation } from "react-i18next";
import EditUserForm from "./EditUserForm/EditUserForm";

export default function EditUserModal({
  user,
  ...props
}: DialogProps & { user: User }): JSX.Element {
  const { t } = useTranslation();
  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const { onClose } = props;
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  return (
    <ModalLayout {...props}>
      <>
        <Typography mb={4} variant="h3">
          {t("modal.updateUser.title")}
        </Typography>
        <EditUserForm
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onSuccess={() => onClose?.({}, "backdropClick")}
          submitRef={submitRef}
          user={user}
        />
        <DialogActions>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
          >
            {isSubmitting && (
              <CircularProgress color="inherit" sx={{ marginX: "1.875rem" }} />
            )}
            <Button
              onClick={handleCancel}
              type="button"
              variant="outlined"
              sx={{ marginRight: "1.875rem" }}
            >
              {t("modal.updateUser.button.cancel")}
            </Button>
            <Button
              data-testid="submitEditUserButton"
              disabled={isSubmitting}
              onClick={() => submitRef.current.click()}
              variant="contained"
            >
              {t("common.button.save")}
            </Button>
          </Box>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
