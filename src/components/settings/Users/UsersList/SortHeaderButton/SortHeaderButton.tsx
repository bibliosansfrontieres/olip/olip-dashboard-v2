import { Button } from "@mui/material";
import SortIcon from "src/assets/icons/Sort";

export default function SortHeaderButton({
  onClick,
  text,
}: {
  onClick: () => void;
  text: string;
}): JSX.Element {
  return (
    <Button
      onClick={onClick}
      sx={{
        fontWeight: 600,
        padding: 0,
        textDecoration: "none",
        "&:hover": {
          "& svg": {
            fontSize: "1.125rem",
          },
        },
      }}>
      {text}{" "}
      <SortIcon
        sx={{
          fontSize: "1rem",
          marginLeft: "0.5rem",
          transitionDuration: "200ms",
          transitionProperty: "font-size",
        }}
      />
    </Button>
  );
}
