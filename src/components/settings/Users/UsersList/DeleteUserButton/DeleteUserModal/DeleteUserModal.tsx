import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Button,
  CircularProgress,
  DialogActions,
  DialogTitle,
  FormHelperText,
} from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { User } from "src/types/user.type";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { deleteUser } from "src/services/user.service";

export default function DeleteUserModal({
  open,
  onClose,
  user,
}: {
  open: boolean;
  onClose: () => void;
  user: User;
}): JSX.Element {
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);
  const [loading, setLoading] = useState<boolean>(false);
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);

  const handleClose = () => {
    setLoading(false);
    setDisplayGenericError(false);
    onClose();
  };

  const handleSubmit = async () => {
    setDisplayGenericError(false);
    setLoading(true);
    const result = await deleteUser(user.id);
    if (result.statusCode === 200) {
      handleClose();
      displaySnackbar(
        t("snackbar.deleteUser.success", { username: user.username }),
      );
    } else {
      setDisplayGenericError(true);
      setLoading(false);
    }
  };

  return (
    <ModalLayout onClose={handleClose} open={open}>
      <>
        <DialogTitle
          sx={{ marginBottom: "1rem" }}
          data-testid="deleteUserModalWarningText"
        >
          {t("modal.deleteUser.title", {
            user: user.username,
          })}
        </DialogTitle>
        <DialogActions disableSpacing sx={{ padding: 0, marginTop: "1rem" }}>
          {loading && (
            <CircularProgress color="inherit" sx={{ marginX: "1.875rem" }} />
          )}
          <Button
            variant="outlined"
            onClick={handleClose}
            data-testid="cancelDeleteUserButton"
          >
            {t("common.button.cancel")}
          </Button>
          <Button
            data-testid="submitDeleteUserButton"
            variant="contained"
            disabled={loading}
            onClick={() => handleSubmit()}
            sx={{ marginLeft: "1.875rem" }}
          >
            {t("common.button.confirm")}
          </Button>
        </DialogActions>
        {displayGenericError && (
          <FormHelperText error sx={{ marginTop: "1rem", textAlign: "right" }}>
            {t("common.error.generic")}
          </FormHelperText>
        )}
      </>
    </ModalLayout>
  );
}
