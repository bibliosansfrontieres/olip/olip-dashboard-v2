import { useTranslation } from "react-i18next";
import { Box, Button } from "@mui/material";
import CloseIcon from "src/assets/icons/Close";
import DeleteUserModal from "./DeleteUserModal/DeleteUserModal";
import { User } from "src/types/user.type";
import useModal from "src/hooks/useModal";

export default function DeleteUserButton({
  disabled = false,
  onDeleteComplete,
  user,
}: {
  disabled: boolean;
  onDeleteComplete: () => void;
  user: User;
}): JSX.Element {
  const { t } = useTranslation();
  const [open, toggleModal] = useModal();

  const handleClose = () => {
    toggleModal();
    onDeleteComplete();
  };

  return (
    <>
      <Button
        data-testid="deleteUserButton"
        variant="text"
        disabled={disabled}
        onClick={() => toggleModal()}
        startIcon={<CloseIcon />}
        sx={{ paddingY: 0, paddingRight: { xs: 0, md: "1.125rem" } }}
      >
        <Box sx={{ display: { xs: "none", md: "flex" } }}>
          {t("settings.pages.users.list.actions.delete")}
        </Box>
      </Button>
      <DeleteUserModal open={open} onClose={handleClose} user={user} />
    </>
  );
}
