import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { useLoaderData, useRevalidator } from "react-router-dom";
import {
  Avatar,
  Box,
  Chip,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@mui/material";
import { UserContext } from "src/contexts/user.context";
import { User } from "src/types/user.type";
import { Pagination as PaginationType } from "src/types";
import SortHeaderButton from "./SortHeaderButton/SortHeaderButton";
import DeleteUserButton from "./DeleteUserButton/DeleteUserButton";
import EditUserButton from "./EditUserButton/EditUserButton";
import defaultProfilePhoto from "/src/assets/images/avatar.png";
import { ROOT } from "src/constants";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import SearchFilter from "src/components/common/SearchFilter/SearchFilter";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import getEnv from "src/utils/getEnv";

export default function UsersList({ users }: { users: User[] }): JSX.Element {
  const { t } = useTranslation();
  const refreshData = useRevalidator();
  const { havePermission, user: myProfile } = useContext(UserContext);
  const { meta } = useLoaderData() as {
    meta: PaginationType;
  };
  const [searchParams, updateSearchParam] = useHandleSearchParams();

  const handleChangePagination = (newParams: {
    page: number;
    take: number;
  }) => {
    if (newParams.take !== meta.take) {
      updateSearchParam([
        {
          name: "take",
          value: newParams.take.toString(),
          defaultValue: "10",
        },
        {
          name: "page",
          value: "1",
          defaultValue: "1",
        },
      ]);
    } else {
      updateSearchParam({
        name: "page",
        value: newParams.page.toString(),
        defaultValue: "1",
      });
    }
  };

  const handleSort = (field: string) => {
    let order = searchParams?.["order"] || "ASC";
    let orderByField = searchParams?.["orderByField"] || "id";

    if (field === orderByField) {
      order = order === "ASC" ? "DESC" : "ASC";
    } else {
      order = "ASC";
      orderByField = field;
    }
    updateSearchParam([
      { name: "order", value: order, defaultValue: "ASC" },
      { name: "orderByField", value: field, defaultValue: "" },
    ]);
  };

  const handleSearch = (event: any) =>
    updateSearchParam({
      name: "usernameLike",
      value: event.target.value,
      defaultValue: "",
    });

  return (
    <>
      <SearchFilter onChange={handleSearch} />
      <PaginationLayout
        onChange={handleChangePagination}
        meta={meta}
        takeList={["10", "25", "50"]}
      >
        <List data-testid="usersList">
          <ListItem sx={{ "& span": { fontWeight: 600 } }}>
            <ListItemAvatar />
            <ListItemText
              primaryTypographyProps={{
                sx: {
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap",
                },
              }}
              sx={{
                maxWidth: "25%",
                overflow: "hidden",
              }}
            >
              <SortHeaderButton
                text={t("settings.pages.users.list.headers.username")}
                onClick={() => handleSort("username")}
              />
            </ListItemText>
            <ListItemText>
              <SortHeaderButton
                text={t("settings.pages.users.list.headers.role")}
                onClick={() => handleSort("role")}
              />
            </ListItemText>
            <ListItemText
              sx={{
                marginRight: { xs: "3.75rem", md: "11.25rem" },
                textAlign: "right",
              }}
            >
              {t("settings.pages.users.list.headers.actions")}
            </ListItemText>
          </ListItem>
          {users.map((user) => (
            <ListItem
              data-testid="userLine"
              key={user.id}
              sx={{
                border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
                borderRadius: "0.5rem",
                marginBottom: "0.625rem",
              }}
            >
              <ListItemAvatar data-testid="listItemAvatar">
                <Avatar
                  data-testid="userPhoto"
                  alt={
                    user?.photo
                      ? `Avatar de ${user.username}`
                      : "Avatar par défaut"
                  }
                  src={
                    user?.photo
                      ? getEnv("VITE_AXIOS_BASEURL") + user.photo
                      : defaultProfilePhoto
                  }
                  sx={{
                    backgroundColor: (theme) => theme.palette.primary.light,
                  }}
                />
              </ListItemAvatar>
              <ListItemText
                data-testid="listItemUsername"
                primaryTypographyProps={{
                  sx: {
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  },
                }}
                sx={{
                  width: "25%",
                  flex: "initial",
                  overflow: "hidden",
                }}
              >
                {user.username}{" "}
                {user.username === ROOT && (
                  <Chip
                    data-testid="chipRootUser"
                    disabled
                    label={t("settings.pages.users.root")}
                    size="small"
                    variant="tagActive"
                  />
                )}
              </ListItemText>
              <ListItemText
                data-testid="listItemRole"
                primaryTypographyProps={{
                  sx: {
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  },
                }}
                sx={{
                  flex: "1 8 auto",
                  overflow: "hidden",
                }}
              >
                {t(`settings.pages.users.roles.${user.role.name}`)}{" "}
                {user.id == myProfile?.id && (
                  <Chip
                    data-testid="chipYou"
                    disabled
                    label={t("settings.pages.users.you")}
                    size="small"
                    variant="tagActive"
                  />
                )}
              </ListItemText>
              {user.id !== myProfile?.id && user.username !== ROOT && (
                <ListItemText
                  data-testid="listItemActions"
                  sx={{
                    display: "flex",
                    flex: "none",
                    justifyContent: "flex-end",
                  }}
                >
                  <Box display="flex">
                    <EditUserButton
                      disabled={!havePermission("update_user")}
                      onEditComplete={() => refreshData.revalidate()}
                      user={user}
                    />
                    <Divider
                      data-testid="listItemDivider"
                      orientation="vertical"
                      flexItem
                      sx={{
                        marginLeft: { xs: "0", md: "1rem" },
                        marginRight: { xs: "0.5rem", md: "1rem" },
                      }}
                    />
                    <DeleteUserButton
                      disabled={!havePermission("delete_user")}
                      onDeleteComplete={() => refreshData.revalidate()}
                      user={user}
                    />
                  </Box>
                </ListItemText>
              )}
            </ListItem>
          ))}
        </List>
      </PaginationLayout>
    </>
  );
}
