import { MutableRefObject, useState } from "react";
import { useTranslation } from "react-i18next";
import { FormikProps } from "formik";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  OutlinedInput,
  RadioGroup,
  Stack,
  SvgIcon,
} from "@mui/material";
import CheckIcon from "src/assets/icons/Check";
import ChevronDownIcon from "src/assets/icons/ChevronDown";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import OLIPRadio from "src/components/common/OLIPRadio/OLIPRadio";
import FormControlLayout from "src/components/layouts/FormControlLayout/FormControlLayout";
import TranslateLanguageList from "src/components/common/forms/TranslateLanguageList/TranslateLanguageList";

export default function UserForm({
  displayGenericError,
  formik,
  submitRef,
}: {
  displayGenericError: boolean;
  formik: FormikProps<{
    language: string;
    mode: "create" | "edit";
    password: string;
    role?: number;
    username: string;
  }>;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const { t } = useTranslation();
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const roles: Array<{ name: string; value: number; permissions: string[] }> = [
    {
      name: "settings.pages.users.roles.admin",
      value: 1,
      permissions: [
        "form.user.permissions.accessContent",
        "form.user.permissions.updateMyProfile",
        "form.user.permissions.manageContent",
        "form.user.permissions.manageUser",
        "form.user.permissions.manageApps",
        "form.user.permissions.accessActivity",
      ],
    },
    {
      name: "settings.pages.users.roles.animator",
      value: 2,
      permissions: [
        "form.user.permissions.accessContent",
        "form.user.permissions.updateMyProfile",
        "form.user.permissions.manageContent",
      ],
    },
    {
      name: "settings.pages.users.roles.user",
      value: 3,
      permissions: [
        "form.user.permissions.accessContent",
        "form.user.permissions.updateMyProfile",
      ],
    },
    {
      name: "settings.pages.users.roles.user_no_password",
      value: 4,
      permissions: [
        "form.user.permissions.accessContent",
        "form.user.permissions.updateMyProfile",
      ],
    },
  ];
  return (
    <form
      onSubmit={formik.handleSubmit}
      style={{ marginBottom: "2.5rem", overflowY: "auto" }}
    >
      <Box
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          borderRadius: "1rem",
          display: "flex",
          flexDirection: "column",
          padding: "1.875rem",
        }}
      >
        <Stack spacing={2}>
          <FormControlLayout
            label={t("common.form.role")}
            error={formik.errors.role}
            touched={formik.touched.role}
          >
            <RadioGroup>
              {roles.map((role) => (
                <Accordion
                  data-testid="roleAccordion"
                  disableGutters
                  elevation={0}
                  expanded={formik.values.role === role.value}
                  key={role.value}
                  onChange={() => formik.setFieldValue("role", role.value)}
                  sx={{
                    backgroundColor: (theme) => theme.palette.primary.light,
                    borderRadius: "0.5rem",
                    marginY: "0.1rem",
                    paddingX: "1.5rem",
                    "&::before": { display: "none" },
                  }}
                >
                  <AccordionSummary
                    data-testid="roleAccordionSummary"
                    expandIcon={<ChevronDownIcon />}
                    sx={{ padding: "0" }}
                  >
                    <FormControlLabel
                      value={role.value}
                      control={
                        <OLIPRadio
                          checked={formik.values.role === role.value}
                        />
                      }
                      label={t(role.name)}
                    />
                  </AccordionSummary>
                  <AccordionDetails data-testid="roleAccordionDetails">
                    <List>
                      {role.permissions.map((permission) => (
                        <ListItem key={permission} data-testid="permissionItem">
                          <ListItemIcon>
                            <CheckIcon
                              sx={{
                                color: (theme) => theme.palette.primary.dark,
                              }}
                            />
                          </ListItemIcon>
                          <ListItemText
                            color="primary"
                            sx={{ textDecoration: "underline" }}
                          >
                            {t(permission)}
                          </ListItemText>
                        </ListItem>
                      ))}
                    </List>
                  </AccordionDetails>
                </Accordion>
              ))}
            </RadioGroup>
          </FormControlLayout>
          <FormControlLayout
            label={t("common.form.username")}
            error={formik.errors.username}
            touched={formik.touched.username}
          >
            <OutlinedInput
              data-testid="createUserInputUsername"
              id="username"
              name="username"
              type="text"
              color="dark"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              placeholder={t("common.form.username")}
              sx={{ backgroundColor: (theme) => theme.palette.primary.light }}
              value={formik.values.username}
            />
          </FormControlLayout>
          {formik.values.role !== 4 && (
            <FormControlLayout
              label={
                formik.values.mode === "create"
                  ? t("common.form.password")
                  : t("form.user.newPassword")
              }
              error={formik.errors.password}
              touched={formik.touched.password}
            >
              <OutlinedInput
                data-testid="createUserInputPassword"
                id="password"
                name="password"
                color="dark"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder={
                  formik.values.mode === "create"
                    ? t("common.form.password")
                    : t("form.user.newPassword")
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton onClick={() => setShowPassword(!showPassword)}>
                      <SvgIcon
                        component={
                          showPassword ? VisibilityOffIcon : VisibilityIcon
                        }
                      />
                    </IconButton>
                  </InputAdornment>
                }
                sx={{
                  backgroundColor: (theme) => theme.palette.primary.light,
                }}
                type={showPassword ? "text" : "password"}
                value={formik.values.password}
              />
            </FormControlLayout>
          )}
          <TranslateLanguageList
            actualLanguage={formik.values.language}
            error={formik.errors.language}
            label={t("common.form.language")}
            onChangeLanguage={(choosedLanguage) =>
              formik.setFieldValue("language", choosedLanguage)
            }
            placeholder={t("common.form.language")}
            touched={formik.touched.language}
          />
          <Button type="submit" ref={submitRef} sx={{ display: "none" }} />
        </Stack>
      </Box>
      {displayGenericError && (
        <FormHelperText error>{t("common.error.generic")}</FormHelperText>
      )}
    </form>
  );
}
