import { useTranslation } from "react-i18next";
import { Button } from "@mui/material";
import AddRoundedIcon from "@mui/icons-material/AddRounded";
import CreateUserModal from "./CreateUserModal/CreateUserModal";
import useModal from "src/hooks/useModal";

export default function CreateUserButton(): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  return (
    <>
      <Button
        data-testid="createUserButton"
        onClick={() => toggleModal()}
        startIcon={
          <AddRoundedIcon sx={{ "&:nth-of-type(1)": { fontSize: "2rem" } }} />
        }
        variant="contained"
      >
        {t("settings.pages.users.buttons.addUser")}
      </Button>
      <CreateUserModal
        onClose={() => toggleModal()}
        open={openModal}
        fullWidth
        maxWidth="sm"
      />
    </>
  );
}
