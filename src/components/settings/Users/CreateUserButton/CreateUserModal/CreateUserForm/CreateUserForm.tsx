import { MutableRefObject, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useFormik } from "formik";
import UserForm from "src/components/settings/Users/UserForm/UserForm";
import { createUser } from "src/services/user.service";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { UserContext } from "src/contexts/user.context";
import { UserFormValues } from "src/types/user.type";
import validationSchema from "./CreateUserForm.validation";
import { useRevalidator } from "react-router-dom";
import getEnv from "src/utils/getEnv";

export default function CreateUserForm({
  onSubmitting,
  onSuccess,
  submitRef,
}: {
  onSubmitting: (value: boolean) => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const { displaySnackbar } = useContext(SnackbarContext);
  const refreshData = useRevalidator();

  const initialValues: UserFormValues = {
    mode: "create",
    role: undefined,
    username: "",
    password: "",
    language: user?.language || getEnv("VITE_DEFAULT_LANGUAGE") || "eng",
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values, { resetForm, setFieldError }) => {
      setDisplayGenericError(false);

      const result = await createUser({
        username: values.username,
        password: values.role !== 4 ? values.password : undefined,
        language: values.language,
        roleId: values.role,
      });
      if (result.statusCode === 201) {
        resetForm();
        displaySnackbar(
          t("snackbar.createUser.success", { username: values.username })
        );
        refreshData.revalidate();
        onSuccess();
      } else {
        let listErrors = [];
        if (!Array.isArray(result.errorMessage)) {
          listErrors.push(result.errorMessage);
        } else {
          listErrors = [...result.errorMessage];
        }
        listErrors.forEach((errorMessage) => {
          switch (errorMessage) {
            case "password must be longer than or equal to 6 characters":
              setFieldError(
                "password",
                t(`form.user.error.${result.errorMessage}`)
              );
              break;
            case "password is needed for this role":
              setFieldError(
                "password",
                t(`form.user.error.${result.errorMessage}`)
              );
              break;
            case "username already exists":
              setFieldError(
                "username",
                t(`form.user.error.${result.errorMessage}`, {
                  user: values.username,
                })
              );
              break;
            default:
              setDisplayGenericError(true);
          }
        });
      }
    },
    validationSchema: validationSchema,
  });

  useEffect(() => {
    onSubmitting(formik.isSubmitting);
  }, [formik.isSubmitting]);

  return (
    <UserForm
      displayGenericError={displayGenericError}
      formik={formik}
      submitRef={submitRef}
    />
  );
}
