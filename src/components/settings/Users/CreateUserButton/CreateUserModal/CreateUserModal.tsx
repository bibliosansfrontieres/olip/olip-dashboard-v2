import { ElementRef, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Box,
  Button,
  CircularProgress,
  DialogActions,
  DialogProps,
  Typography,
} from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import CreateUserForm from "./CreateUserForm/CreateUserForm";

export default function CreateUserModal(props: DialogProps): JSX.Element {
  const { t } = useTranslation();

  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const { onClose } = props;
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  return (
    <ModalLayout {...props} onClose={(e: any) => handleCancel(e)}>
      <>
        <Typography mb={4} variant="h3" data-testid="createUserModalTitle">
          {t("modal.createUser.title")}
        </Typography>
        <CreateUserForm
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onSuccess={() => onClose?.({}, "backdropClick")}
          submitRef={submitRef}
        />
        <DialogActions>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
          >
            {isSubmitting && (
              <CircularProgress color="inherit" sx={{ marginX: "1.875rem" }} />
            )}
            <Button
              data-testid="cancelCreateUserButton"
              onClick={handleCancel}
              type="button"
              variant="outlined"
              sx={{ marginRight: "1.875rem" }}
            >
              {t("modal.createUser.button.cancel")}
            </Button>
            <Button
              data-testid="submitCreateUserButton"
              disabled={isSubmitting}
              onClick={() => submitRef.current.click()}
              variant="contained"
            >
              {t("common.button.save")}
            </Button>
          </Box>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
