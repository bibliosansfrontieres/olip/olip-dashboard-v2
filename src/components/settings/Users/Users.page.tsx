import { useContext } from "react";
import { Box, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import CreateUserButton from "./CreateUserButton/CreateUserButton";
import { getUsers } from "src/services/user.service";
import { useLoaderData } from "react-router-dom";
import { User } from "src/types/user.type";
import { Pagination } from "src/types";
import UsersList from "./UsersList/UsersList";
import { UserContext } from "src/contexts/user.context";

export async function usersLoader({ request }: { request: any }) {
  const url = new URL(request.url);
  const page = url.searchParams.get("page");
  const order = (url.searchParams.get("order") as "ASC" | "DESC") || "ASC";
  const orderByField =
    (url.searchParams.get("orderByField") as
      | "id"
      | "username"
      | "role"
      | undefined) || "id";
  const take = url.searchParams.get("take");
  const usernameLike = url.searchParams.get("usernameLike");

  const { users, meta } = await getUsers({
    order: order || "ASC",
    orderByField: orderByField,
    page: Number(page) || 1,
    take: Number(take) || 10,
    usernameLike: usernameLike || undefined,
  });
  return { users, meta };
}

export default function UsersPage(): JSX.Element {
  const { t } = useTranslation();
  const { users, meta } = useLoaderData() as {
    users: User[];
    meta: Pagination;
  };
  const { havePermission } = useContext(UserContext);

  return (
    <Box>
      <Typography
        variant="h2"
        sx={{ marginBottom: "1rem" }}
        data-testid="userTitle"
      >
        {t("settings.pages.users.title")}
      </Typography>
      <Box
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          borderRadius: "1rem",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "2.5rem",
          padding: "1.25rem",
        }}
      >
        <Typography data-testid="profileCount">
          {t("settings.pages.users.profileCount", {
            count: meta.totalItemsCount,
          })}
        </Typography>
        {havePermission("create_user") && <CreateUserButton />}
      </Box>
      <UsersList users={users} />
    </Box>
  );
}
