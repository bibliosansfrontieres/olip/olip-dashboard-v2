import { useContext } from "react";
import { useTranslation } from "react-i18next";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import CategoryTabs from "src/components/settings/Contents/CategoryTabs/CategoryTabs";
import CreateCategoryButton from "src/components/settings/Contents/CreateCategoryButton/CreateCategoryButton";
import { CategoryBackOfficeListContext } from "src/contexts/categoryBackOfficeList.context";

import { Box, Typography } from "@mui/material";

export default function ContentsPage(): JSX.Element {
  const { t } = useTranslation();
  const categoryBackOfficeListContext = useContext(
    CategoryBackOfficeListContext,
  );

  return (
    <Box>
      <Typography
        variant="h2"
        sx={{ marginBottom: "1rem" }}
        data-testid="contentTitle"
      >
        {t("settings.pages.contents.title")}
      </Typography>
      <Box
        sx={{
          borderRadius: "1rem",
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-end",
          marginBottom: "2.5rem",
          padding: "1.25rem",
        }}
      >
        <CreateCategoryButton />
      </Box>
      <WaitApiResult useApiResult={categoryBackOfficeListContext}>
        <CategoryTabs />
      </WaitApiResult>
    </Box>
  );
}
