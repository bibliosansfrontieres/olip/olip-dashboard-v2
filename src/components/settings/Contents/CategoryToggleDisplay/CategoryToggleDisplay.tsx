import { ReactElement } from "react";
import { t } from "i18next";

import {
  Box,
  CircularProgress,
  FormControl,
  FormControlLabel,
  Grid,
  Stack,
  Switch,
  Typography,
} from "@mui/material";

type Props = {
  isHomepageDisplayed: boolean;
  loading?: boolean;
  onToggle: (newValue: boolean) => void;
};

export default function CategoryToggleDisplay({
  isHomepageDisplayed,
  loading = false,
  onToggle,
}: Props): ReactElement {
  return (
    <Grid item xs={12} data-testid="categoryToggleDisplay">
      <Stack
        p={2}
        borderRadius="0.5rem"
        sx={{ backgroundColor: (theme) => theme.palette.grey["100"] }}
      >
        <Typography variant="info">{t("common.displaySection")}</Typography>
        {loading ? (
          <Box ml={4} mt={1}>
            <CircularProgress color="inherit" size={30} />
          </Box>
        ) : (
          <FormControl>
            <FormControlLabel
              data-testid="categoryToggleDisplaySwitch"
              checked={isHomepageDisplayed}
              control={<Switch />}
              disabled={loading}
              label={t(`common.${isHomepageDisplayed ? "yes" : "no"}`)}
              onChange={(_event: any, value) => onToggle(value)}
            />
          </FormControl>
        )}
      </Stack>
    </Grid>
  );
}
