import {
  Box,
  Button,
  Grid,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { FormikProps } from "formik";
import { MutableRefObject } from "react";
import { useTranslation } from "react-i18next";
import LanguagesStack from "src/components/common/LanguagesStack/LanguagesStack";
import EditImage from "src/components/common/forms/EditImage/EditImage";
import TranslateLanguageList from "src/components/common/forms/TranslateLanguageList/TranslateLanguageList";
import FormControlLayout from "src/components/layouts/FormControlLayout/FormControlLayout";
import useChangeLangFormWhenError from "src/hooks/useChangeLangFormWhenError";
import {
  CategoryFormValue,
  CreateCategoryTranslationDto,
} from "src/types/category.type";
import CategoryToggleDisplay from "../CategoryToggleDisplay/CategoryToggleDisplay";
import VisibilitySelect from "./VisibilitySelect/VisibiltySelect";

export default function CategoryForm({
  formik,
  submitRef,
}: {
  formik: FormikProps<CategoryFormValue>;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const { t } = useTranslation();
  const actualLanguageIndex = () => {
    return formik.values.languages.findIndex(
      (lang) => lang === formik.values.actualLanguage,
    );
  };
  const handleChangeLanguage = (choosedLanguage: string) => {
    formik.setFieldValue("actualLanguage", choosedLanguage);

    // remove previousLanguage if empty
    const previousLanguage = formik.values.actualLanguage;
    const previousTranslations =
      formik.values.createCategoryTranslationsDto.find(
        (translation) => translation.languageIdentifier === previousLanguage,
      );
    const previousLanguageToRemove = previousTranslations?.title === "";
    if (
      formik.values.languages.findIndex((lang) => lang === choosedLanguage) ===
      -1
    ) {
      formik.setFieldValue(
        "languages",
        previousLanguageToRemove
          ? // if previousLanguageEmpty, filter languages to remove it
            [
              ...formik.values.languages.filter(
                (lang) => lang !== previousLanguage,
              ),
              choosedLanguage,
            ]
          : [...formik.values.languages, choosedLanguage],
      );
      formik.setFieldValue(
        "createCategoryTranslationsDto",
        previousLanguageToRemove
          ? // if previousLanguageEmpty, filter categoryTranslations to remove it
            [
              ...formik.values.createCategoryTranslationsDto.filter(
                (translation) =>
                  translation.languageIdentifier !== previousLanguage,
              ),
              {
                description: "",
                languageIdentifier: choosedLanguage,
                title: "",
              },
            ]
          : [
              ...formik.values.createCategoryTranslationsDto,
              {
                description: "",
                languageIdentifier: choosedLanguage,
                title: "",
              },
            ],
      );
    } else {
      if (previousLanguageToRemove) {
        formik.setFieldValue(
          "languages",
          formik.values.languages.filter((lang) => lang !== previousLanguage),
        );
        formik.setFieldValue(
          "createCategoryTranslationsDto",
          formik.values.createCategoryTranslationsDto.filter(
            (translation) =>
              translation.languageIdentifier !== previousLanguage,
          ),
        );
      }
    }
  };

  useChangeLangFormWhenError<CategoryFormValue>({
    formik,
    changeDisplayLanguage: handleChangeLanguage,
    actualLanguageIndex: actualLanguageIndex(),
  });

  function updateIsHomepageDisplayed(newValue: boolean) {
    formik.setFieldValue("isHomepageDisplayed", newValue);
  }

  return (
    <form
      onSubmit={formik.handleSubmit}
      style={{ marginBottom: "2.5rem", overflowY: "auto" }}
    >
      <Grid container spacing={4}>
        <Grid item xs={3}>
          <CategoryToggleDisplay
            isHomepageDisplayed={formik.values.isHomepageDisplayed}
            onToggle={updateIsHomepageDisplayed}
          />
        </Grid>
        <Grid item xs={9}>
          <TranslateLanguageList
            actualLanguage={formik.values.actualLanguage}
            error={formik.errors.actualLanguage}
            onChangeLanguage={handleChangeLanguage}
            touched={formik.touched.actualLanguage}
          />
        </Grid>

        <Grid item xs={3}>
          <EditImage
            imageUrl={formik.values.pictogram}
            onChangeImage={(newImage) =>
              formik.setFieldValue("pictogram", newImage)
            }
            error={formik.errors.pictogram}
            touched={formik.touched.pictogram}
          />
        </Grid>

        <Grid item xs={9}>
          <Stack spacing={4} alignItems="flex-start">
            <FormControlLayout
              label={t("form.category.title")}
              error={
                (
                  formik.errors.createCategoryTranslationsDto?.[
                    actualLanguageIndex()
                  ] as CreateCategoryTranslationDto
                )?.title
              }
              touched={
                formik.touched.createCategoryTranslationsDto?.[
                  actualLanguageIndex()
                ]?.title
              }
            >
              <OutlinedInput
                id={`createCategoryTranslationsDto[${actualLanguageIndex()}].title`}
                name={`createCategoryTranslationsDto[${actualLanguageIndex()}].title`}
                type="text"
                color="dark"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder={t("form.category.title")}
                sx={{ backgroundColor: (theme) => theme.palette.primary.light }}
                value={
                  formik.values.createCategoryTranslationsDto?.[
                    actualLanguageIndex()
                  ]?.title
                }
              />
            </FormControlLayout>
            <FormControlLayout
              label={t("form.category.description")}
              error={
                (
                  formik.errors.createCategoryTranslationsDto?.[
                    actualLanguageIndex()
                  ] as CreateCategoryTranslationDto
                )?.description
              }
              touched={
                formik.touched.createCategoryTranslationsDto?.[
                  actualLanguageIndex()
                ]?.description
              }
            >
              <TextField
                id={`createCategoryTranslationsDto[${actualLanguageIndex()}].description`}
                name={`createCategoryTranslationsDto[${actualLanguageIndex()}].description`}
                color="dark"
                multiline
                rows={4}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={
                  formik.values.createCategoryTranslationsDto?.[
                    actualLanguageIndex()
                  ]?.description
                }
              />
            </FormControlLayout>
            <VisibilitySelect formik={formik} />
            {formik.values.languages.filter(
              (language) => language !== formik.values.actualLanguage,
            ).length > 0 && (
              <Box>
                <Typography>{t("form.category.listLanguages")}</Typography>
                <LanguagesStack
                  languages={formik.values.languages.filter(
                    (language) => language !== formik.values.actualLanguage,
                  )}
                  onClick={handleChangeLanguage}
                />
              </Box>
            )}
          </Stack>
        </Grid>
        <Button type="submit" ref={submitRef} sx={{ display: "block" }} />
      </Grid>
    </form>
  );
}
