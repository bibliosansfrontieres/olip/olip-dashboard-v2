import { FC, useContext } from "react";
import { FormikProps } from "formik";
import { useTranslation } from "react-i18next";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import { LanguageContext } from "src/contexts/language.context";
import useGetRoles from "src/hooks/useGetRoles";
import { ApplicationFormValue } from "src/types/application.type";
import { CategoryFormValue } from "src/types/category.type";

import {
  Box,
  CircularProgress,
  FormControl,
  FormHelperText,
  Typography,
} from "@mui/material";

type Props = {
  formik: FormikProps<CategoryFormValue> | FormikProps<ApplicationFormValue>;
};

const RolesList = ({
  children,
}: {
  children: React.ReactNode;
}): React.ReactNode => {
  const { language } = useContext(LanguageContext);
  const { roles, loading } = useGetRoles({ language });
  const { t } = useTranslation();
  if (loading) {
    return (
      <Box display="flex" justifyContent="center" py={1}>
        <CircularProgress />
      </Box>
    );
  }
  if (roles.length === 0) {
    return (
      <Typography variant="caption" py={1}>
        {t("form.category.visibility.role.error")}
      </Typography>
    );
  }
  return children;
};

export default function RolesSelect({ formik }: Props): ReturnType<FC> {
  const { t } = useTranslation();
  const { language } = useContext(LanguageContext);

  const { roles } = useGetRoles({ language });

  function onSelectRole(event: any) {
    const rolesChoosed = event.target.value.filter(
      (roleId: string) => roleId !== "",
    );
    formik.setFieldValue(
      "roleIds",
      rolesChoosed.map((roleId: string) => Number(roleId)),
    );
  }

  return (
    <FormControl
      error={formik.touched.visibility && Boolean(formik.errors.visibility)}
      sx={{ width: "100%" }}
    >
      {formik.initialValues?.origin === "application" ? (
        <Typography>
          {t("form.category.visibility.role.roleMessage")}
        </Typography>
      ) : (
        <Typography>{t("form.category.visibility.role.title")}</Typography>
      )}
      <RolesList>
        <>
          <OLIPSelect
            choices={roles.map((role) => ({
              name: t(`roles.${role.name}`),
              value: String(role.id),
            }))}
            onChange={onSelectRole}
            placeholder={t("form.category.visibility.role.placeholder")}
            sx={{ marginBottom: "1rem", width: "100%" }}
            values={formik.values.roleIds.map((roleId) => String(roleId))}
          />
          {formik.touched.visibility && formik.errors.visibility && (
            <FormHelperText>{t(formik.errors.visibility)}</FormHelperText>
          )}
        </>
      </RolesList>
    </FormControl>
  );
}
