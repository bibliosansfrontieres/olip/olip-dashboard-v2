import { FC } from "react";
import { FormikProps } from "formik";
import { useTranslation } from "react-i18next";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import EditUsersButton from "src/components/settings/Contents/CategoryForm/VisibilitySelect/EditUsersButton/EditUsersButton";
import RolesSelect from "src/components/settings/Contents/CategoryForm/VisibilitySelect/RolesSelect/RolesSelect";
import { ApplicationFormValue } from "src/types/application.type";
import { CategoryFormValue } from "src/types/category.type";
import { User } from "src/types/user.type";
import { UserSimplified } from "src/types/user/userSimplified.type";

import { FormControl, FormHelperText, Typography } from "@mui/material";

type Props = {
  formik: FormikProps<CategoryFormValue> | FormikProps<ApplicationFormValue>;
};

export default function VisibilitySelect({ formik }: Props): ReturnType<FC> {
  const { t } = useTranslation();
  return (
    <>
      <FormControl
        error={formik.touched.visibility && Boolean(formik.errors.visibility)}
        sx={{ width: "100%" }}
      >
        <Typography>{t("common.visibility.title")}</Typography>
        <OLIPSelect
          choices={[
            {
              name: t("common.visibility.public"),
              value: "public",
            },
            {
              name: t("common.visibility.byRole"),
              value: "byRole",
            },
            {
              name: t("common.visibility.byUser"),
              value: "byUser",
            },
          ]}
          onChange={(event: any) =>
            formik.setFieldValue("visibility", event.target.value)
          }
          sx={{ marginBottom: "1rem", width: "100%" }}
          values={formik.values.visibility}
          valuesSecondary={
            formik.values.visibility === "byUser" && formik.values.users
              ? formik.values.users
                  .map((user: User | UserSimplified) => user.username)
                  .join(", ")
              : undefined
          }
        />
        {formik.touched.visibility && formik.errors.visibility && (
          <FormHelperText>{t(formik.errors.visibility)}</FormHelperText>
        )}
        {formik.values.visibility === "byRole" && (
          <RolesSelect formik={formik} />
        )}
        {formik.values.visibility === "byUser" && (
          <EditUsersButton formik={formik} sx={{ width: "fit-content" }} />
        )}
      </FormControl>
    </>
  );
}
