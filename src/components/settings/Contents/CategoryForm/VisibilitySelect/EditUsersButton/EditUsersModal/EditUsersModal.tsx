import { ReactElement, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { getUsers } from "src/services/user.service";
import { User } from "src/types/user.type";
import { UserSimplified } from "src/types/user/userSimplified.type";

import {
  Autocomplete,
  Box,
  DialogProps,
  TextField,
  Typography,
} from "@mui/material";

import UserChipList from "./UserChipList/UserChipList";

type Props = DialogProps & {
  actualUsers: User[] | UserSimplified[];
  onUpdate: (users: User[] | UserSimplified[]) => void;
};

export default function EditUsersModal({
  actualUsers,
  onUpdate,
  ...props
}: Props): ReactElement {
  const { t } = useTranslation();
  const { onClose } = props;

  const [users, setUsers] = useState(actualUsers);
  const [usersSelected, setUsersSelected] = useState(actualUsers);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [searchText, setSearchText] = useState<any>({ username: "" });

  useEffect(() => {
    const setAutocompleteUsers = async () => {
      const results = await getUsers({
        order: "ASC",
        orderByField: "username",
        page: 1,
        take: 10,
        usernameLike: "",
      });
      setUsers(results.users || []);
    };
    setAutocompleteUsers();
  }, []);

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
    setUsersSelected(actualUsers);
  };

  function selectUser(_e: any, value: any) {
    if (value !== null) {
      setUsersSelected((prevUsersSelected) => [...prevUsersSelected, value]);
      setSearchText({ username: "" });
    }
  }

  function deleteUser(userId: number) {
    setUsersSelected((prevUsersSelected) =>
      prevUsersSelected.filter((user) => user.id !== userId),
    );
  }

  async function searchUsers(search: string) {
    setIsLoading(true);
    try {
      const results = await getUsers({
        order: "ASC",
        orderByField: "username",
        page: 1,
        take: 10,
        usernameLike: search || undefined,
      });
      setUsers(results.users || []);
    } catch (e) {
      console.error(e);
    } finally {
      setIsLoading(false);
    }
  }

  return (
    <ModalLayout {...props}>
      <>
        <Typography mb={4} variant="h3">
          {t("form.category.visibility.users.modal.title")}
        </Typography>
        <Box
          sx={{
            backgroundColor: (theme) => theme.palette.grey[100],
            padding: "1.5rem",
          }}
        >
          <Autocomplete
            autoComplete={false}
            clearOnBlur
            disablePortal
            freeSolo
            getOptionLabel={(option) => (option as User).username}
            id="search"
            loading={isLoading}
            options={
              users?.filter(
                (user) =>
                  !usersSelected.some(
                    (userSelected) => userSelected?.id === user?.id,
                  ),
              ) || []
            }
            onChange={selectUser}
            renderInput={(params) => (
              <TextField
                {...params}
                onChange={(e: any) => searchUsers(e.currentTarget.value)}
                placeholder={t("form.category.visibility.users.modal.search")}
                sx={{ backgroundColor: "white", borderRadius: "0.5rem" }}
              />
            )}
            value={searchText}
            ListboxProps={{ style: { maxHeight: 100, overflow: "auto" } }}
          />
          {usersSelected.length > 0 && (
            <Box marginTop="0.75rem">
              <UserChipList users={usersSelected} onDelete={deleteUser} />
            </Box>
          )}
        </Box>
        <Typography variant="body2" py={2}>
          {t("form.category.visibility.users.modal.information")}
        </Typography>
        <OLIPDialogActions
          isLoading={false}
          mainButton={{
            action: () => onUpdate(usersSelected),
            text: t("common.button.save"),
          }}
          secondaryButton={{
            action: () => handleCancel({}),
            text: t("common.button.cancel"),
          }}
        />
      </>
    </ModalLayout>
  );
}
