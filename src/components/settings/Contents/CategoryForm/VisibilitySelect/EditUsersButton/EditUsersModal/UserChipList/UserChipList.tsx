import { ReactElement } from "react";
import { User } from "src/types/user.type";
import { UserSimplified } from "src/types/user/userSimplified.type";

import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { Chip } from "@mui/material";

type Props = {
  onDelete: (userId: number) => void;
  users: User[] | UserSimplified[];
};

export default function UserChipList({ users, onDelete }: Props): ReactElement {
  return (
    <>
      {users.map((user) => (
        <Chip
          key={user.id}
          label={user.username}
          onDelete={() => onDelete(user.id)}
          deleteIcon={<CloseRoundedIcon />}
          sx={{
            backgroundColor: "white",
            border: (theme) => `1px solid ${theme.palette.grey[500]}`,
            borderRadius: "3rem",
            marginBottom: "0.75rem",
            marginRight: "0.75rem",
            padding: "1.25rem",
            textDecoration: "none",
          }}
        />
      ))}
    </>
  );
}
