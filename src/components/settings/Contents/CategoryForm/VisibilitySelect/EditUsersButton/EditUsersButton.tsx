import { ReactElement } from "react";
import { FormikProps } from "formik";
import { useTranslation } from "react-i18next";
import EditIcon from "src/assets/icons/Edit";
import EditUsersModal from "src/components/settings/Contents/CategoryForm/VisibilitySelect/EditUsersButton/EditUsersModal/EditUsersModal";
import useModal from "src/hooks/useModal";
import { ApplicationFormValue } from "src/types/application.type";
import { CategoryFormValue } from "src/types/category.type";
import { User } from "src/types/user.type";
import { UserSimplified } from "src/types/user/userSimplified.type";

import { Button, ButtonProps } from "@mui/material";

type Props = ButtonProps & {
  formik: FormikProps<CategoryFormValue> | FormikProps<ApplicationFormValue>;
};

export default function EditUsersButton({
  formik,
  ...props
}: Props): ReactElement {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  const handleClose = () => {
    toggleModal();
  };

  function onUpdateUsers(users: User[] | UserSimplified[]) {
    formik.setFieldValue("users", users);
    formik.setFieldValue(
      "userIds",
      users.map((user) => user.id),
    );
    toggleModal();
  }

  return (
    <>
      <Button
        variant="text"
        onClick={() => toggleModal()}
        startIcon={<EditIcon />}
        sx={{ paddingY: 0 }}
        {...props}
      >
        {t(
          formik.values.userIds.length === 0
            ? "form.category.visibility.users.button.add"
            : "form.category.visibility.users.button.edit",
        )}
      </Button>
      <EditUsersModal
        onClose={handleClose}
        onUpdate={onUpdateUsers}
        open={openModal}
        fullWidth
        maxWidth="sm"
        actualUsers={formik.values.users}
      />
    </>
  );
}
