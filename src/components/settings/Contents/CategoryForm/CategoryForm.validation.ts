import { supportedLanguages } from "src/constants";
import * as yup from "yup";

export default yup.object({
  createCategoryTranslationsDto: yup.array().of(
    yup.object({
      description: yup
        .string()
        .max(500, "form.category.validation.description.max")
        .notRequired(),
      languageIdentifier: yup
        .string()
        .oneOf(
          supportedLanguages.map((language) => language.id),
          "form.category.validation.language.oneOf",
        )
        .required("form.category.validation.language.mandatory"),
      title: yup
        .string()
        .max(100, "form.category.validation.title.max")
        // verify description required if its the only translation or description exist
        .test(
          "required",
          "form.category.validation.title.mandatory",
          (value, context) => {
            if (context.from && context.from[1].value.languages.length === 1) {
              return value !== undefined && value !== "";
            }
            if (context.parent.description) {
              return value !== undefined && value !== "";
            }
            return true;
          },
        ),
    }),
  ),
});
