import { useContext } from "react";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import CategoryBackButton from "src/components/settings/Contents/EditCategoryPage/CategoryBackButton/CategoryBackButton";
import CategoryDeleteButton from "src/components/settings/Contents/EditCategoryPage/CategoryDeleteButton/CategoryDeleteButton";
import CategoryMainCard from "src/components/settings/Contents/EditCategoryPage/CategoryMainCard/CategoryMainCard";
import CategoryPlaylistsCard from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistsCard";
import { CategoryBackOfficeContext } from "src/contexts/categoryBackOffice.context";
import { CategoryBackOfficeCategoryPlaylistsContextProvider } from "src/contexts/categoryPlaylistsBackOffice.context";

import { Box, Stack, Typography } from "@mui/material";

export default function EditCategoryPage(): React.ReactNode {
  const categoryBackOfficeResult = useContext(CategoryBackOfficeContext);
  return (
    <WaitApiResult
      useApiResult={{ category: categoryBackOfficeResult }}
      resultChildren={({ category }) => (
        <>
          <Box sx={{ marginBottom: "3rem" }}>
            <CategoryBackButton />
          </Box>
          <Box
            display="flex"
            justifyContent="space-between"
            sx={{ marginBottom: "3rem" }}
          >
            <Typography variant="h2" mb={2} data-testid="categoryTitle">
              {category.categoryTranslations[0].title}
            </Typography>
            <CategoryDeleteButton />
          </Box>
          <Stack spacing={4}>
            <CategoryBackOfficeCategoryPlaylistsContextProvider>
              <CategoryMainCard />
              <CategoryPlaylistsCard />
            </CategoryBackOfficeCategoryPlaylistsContextProvider>
          </Stack>
        </>
      )}
    />
  );
}
