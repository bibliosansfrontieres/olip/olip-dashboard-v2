import { Dispatch, SetStateAction, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import PictureIcon from "src/assets/icons/Picture";
import ContentType from "src/components/common/ContentType/ContentType";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { updateCategoryPlaylistItem } from "src/services/categoryContent.service";
import { Application } from "src/types/application.type";
import { ApplicationWithTranslations } from "src/types/application/applicationWithTranslations.type";
import { CategoryPlaylistItemList } from "src/types/categoryPlaylistItem/categoryPlaylistItemList.type";
import { Item, ItemMaestro } from "src/types/item.type";
import { CategoryPlaylistItemItem } from "src/types/item/categoryPlaylistItemItem.type";
import getEnv from "src/utils/getEnv";

import {
  Avatar,
  Box,
  Switch,
  TableCell,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";

type Props = {
  item: Item | ItemMaestro | CategoryPlaylistItemItem;
  displayId?: number;
  displayValue?: boolean;
  itemApplication?: ApplicationWithTranslations;
  setResult?: Dispatch<SetStateAction<CategoryPlaylistItemList | undefined>>;
};

export default function ItemRow({
  displayId,
  displayValue,
  item,
  itemApplication,
  setResult,
}: Props): JSX.Element {
  const { displaySnackbar } = useContext(SnackbarContext);
  const { t } = useTranslation();

  const [isLoading, setIsLoading] = useState<boolean>(false);
  async function toggleItem(
    itemId: number,
    isDisplayed: boolean,
  ): Promise<boolean> {
    try {
      const result = await updateCategoryPlaylistItem(itemId, { isDisplayed });
      if (result.statusCode === 200 && result.categoryPlaylistItem) {
        if (setResult) {
          setResult((prev) => {
            if (!prev) return;
            const index = prev.categoryPlaylistItems.findIndex(
              (it) => it.id === itemId,
            );
            if (index === -1) return;
            const newItems = [...prev.categoryPlaylistItems];
            newItems[index] = { ...newItems[index], isDisplayed };
            prev.categoryPlaylistItems = newItems;
            return prev;
          });
          displaySnackbar(
            `${t("snackbar.toggleDisplayCategoryPlaylistItem.success")}`,
          );
          return result.categoryPlaylistItem.isDisplayed;
        }
      }

      console.error(`${result.statusCode} - ${result.errorMessage}`);
      displaySnackbar(
        t("snackbar.toggleDisplayCategoryPlaylistItem.error"),
        "error",
      );
    } catch (error) {
      console.error(error);
      displaySnackbar(
        t("snackbar.toggleDisplayCategoryPlaylistItem.error"),
        "error",
      );
    }
    return !isDisplayed;
  }

  const toggleDisplayItem = async () => {
    if (displayId) {
      setIsLoading(true);
      try {
        await toggleItem(displayId, !displayValue);
      } finally {
        setIsLoading(false);
      }
    }
  };

  const application = itemApplication ?? (item.application as Application);

  return (
    <TableRow
      data-testid="itemRow"
      sx={{
        backgroundColor: (theme) => theme.palette.grey[100],
        borderRadius: "0.5rem",
        marginBottom: "0.625rem",
        "& td": {
          borderTop: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          borderBottom: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
        },
        "& td:first-of-type": {
          borderLeft: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          borderTopLeftRadius: "0.5rem",
          borderBottomLeftRadius: "0.5rem",
        },
        "& td:last-of-type": {
          borderRight: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          borderTopRightRadius: "0.5rem",
          borderBottomRightRadius: "0.5rem",
        },
      }}
    >
      <TableCell>
        <Avatar
          variant="rounded"
          alt=""
          src={getEnv("VITE_AXIOS_BASEURL") + item.thumbnail}
          sx={{
            backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          {item?.thumbnail && <PictureIcon />}
        </Avatar>
      </TableCell>
      <TableCell sx={{ overflow: "hidden", maxWidth: 0 }}>
        <Tooltip
          title={
            "dublinCoreItem" in item
              ? item.dublinCoreItem.dublinCoreItemTranslations[0].title
              : item.itemTranslations[0].title
          }
          placement="top-start"
        >
          <Typography
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
            }}
            data-testid="itemTitleCell"
          >
            {"dublinCoreItem" in item
              ? item.dublinCoreItem.dublinCoreItemTranslations[0].title
              : item.itemTranslations[0].title}
          </Typography>
        </Tooltip>
      </TableCell>
      <TableCell align="center">
        <Box display="flex" alignItems="center" justifyContent="center">
          {"dublinCoreItem" in item && (
            <ContentType typeId={item.dublinCoreItem.dublinCoreType.id} />
          )}
        </Box>
      </TableCell>
      <TableCell align="center">
        <Box
          display={application.id === "olip" ? "none" : "flex"}
          alignItems="center"
        >
          <Avatar
            variant="square"
            alt=""
            src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
          />
          <Box>
            <Typography ml={1.5}>{application.displayName}</Typography>
          </Box>
        </Box>
      </TableCell>
      {displayValue !== undefined && displayId && (
        <TableCell align="center">
          <Switch
            checked={displayValue}
            disabled={isLoading}
            onChange={toggleDisplayItem}
          />
        </TableCell>
      )}
    </TableRow>
  );
}
