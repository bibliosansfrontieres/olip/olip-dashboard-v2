import { ReactElement, useContext } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import { useTranslation } from "react-i18next";
import OLIPCheckbox from "src/components/common/OLIPCheckbox/OLIPCheckbox";
import { StrictModeDroppable } from "src/components/common/StrictModeDroppable/StrictModeDroppable";
import CategoryPlaylistRow from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CategoryPlaylistRow";
import { CategoryBackOfficeCategoryPlaylistsContext } from "src/contexts/categoryPlaylistsBackOffice.context";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";
import {
  CellCheckbox,
  ColumnCheckbox,
  getColumns,
} from "src/utils/backOfficeCategoryPlaylistsColumns";

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";

type Props = {
  canChangeOrder?: boolean;
  categoryContents: CategoryPlaylistBackOffice[];
  checkboxLine?: CellCheckbox;
  checkboxColumn?: ColumnCheckbox;
  itemsWithToggleDisplay?: boolean;
};

export default function CategoryPlaylistTable({
  canChangeOrder = false,
  categoryContents,
  checkboxLine,
  checkboxColumn,
  itemsWithToggleDisplay = false,
}: Props): ReactElement {
  const { t } = useTranslation();
  const { changeOrderSelection } = useContext(
    CategoryBackOfficeCategoryPlaylistsContext,
  );

  const onDragEnd = async (result: any) => {
    if (!result.destination) {
      return;
    }
    await changeOrderSelection(
      categoryContents[result.source.index].id,
      result.destination.index,
    );
  };

  const columns = getColumns(checkboxColumn);

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <StrictModeDroppable droppableId="droppable">
        {(provided: any) => (
          <Table
            ref={provided.innerRef}
            sx={{
              borderCollapse: "separate",
              overflowWrap: "break-word",
              paddingY: "0.5rem",
              tableLayout: "fixed",
              width: "100%",
              "& th": { border: "0", paddingBottom: 0 },
            }}
          >
            <TableHead>
              <TableRow>
                {columns.map((column, index) => (
                  <TableCell key={index} width={column.width}>
                    {column.checkbox ? (
                      <OLIPCheckbox {...column.checkbox} />
                    ) : (
                      <Typography
                        variant="tab"
                        data-testid={`tableHeader${column.header_key}`}
                      >
                        {column.header_key && t(`${column.header_key}`)}
                      </Typography>
                    )}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {categoryContents.map((categoryContent, index) => (
                <CategoryPlaylistRow
                  key={categoryContent.id}
                  canChangeOrder={canChangeOrder}
                  categoryContent={categoryContent}
                  checkbox={checkboxLine}
                  index={index}
                  itemsWithToggleDisplay={itemsWithToggleDisplay}
                />
              ))}
              {provided.placeholder}
            </TableBody>
          </Table>
        )}
      </StrictModeDroppable>
    </DragDropContext>
  );
}
