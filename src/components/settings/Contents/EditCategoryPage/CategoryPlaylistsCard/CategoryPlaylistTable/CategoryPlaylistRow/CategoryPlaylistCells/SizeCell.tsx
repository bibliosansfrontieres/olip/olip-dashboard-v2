import { ReactNode } from "react";
import { useTranslation } from "react-i18next";

import { TableCell, Typography } from "@mui/material";

export function SizeCell({ size }: { size: number }): ReactNode {
  const { t } = useTranslation();
  return (
    <TableCell>
      <Typography data-testid="sizeCell">
        {t("common.megabyte", {
          size: (size / 1024 / 1024).toFixed(2),
        })}
      </Typography>
    </TableCell>
  );
}
