import { Dispatch, ReactNode, SetStateAction } from "react";
import ChevronDownIcon from "src/assets/icons/ChevronDown";
import PictureIcon from "src/assets/icons/Picture";
import getEnv from "src/utils/getEnv";

import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import { Avatar, Box, IconButton, TableCell, Theme } from "@mui/material";

export function FirstCell({
  isOpen,
  setIsOpen,
  canChangeOrder,
  image,
}: {
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  canChangeOrder: boolean;
  image: string;
}): ReactNode {
  return (
    <TableCell>
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        ml={1}
      >
        {canChangeOrder && <DragIndicatorIcon fontSize="large" />}
        <IconButton
          size="small"
          onClick={() => setIsOpen(!isOpen)}
          sx={{ marginRight: "0.5rem" }}
        >
          <ChevronDownIcon sx={{ transform: isOpen ? "rotate(180deg)" : "" }} />
        </IconButton>
        <Avatar
          variant="rounded"
          alt=""
          src={getEnv("VITE_AXIOS_BASEURL") + image}
          sx={{
            backgroundColor: (theme: Theme) =>
              `${theme.palette.primary.main}4D`,
            color: (theme: Theme) => theme.palette.grey[500],
          }}
        >
          {image && <PictureIcon />}
        </Avatar>
      </Box>
    </TableCell>
  );
}
