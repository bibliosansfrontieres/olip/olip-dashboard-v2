import { ReactNode } from "react";
import OLIPCheckbox from "src/components/common/OLIPCheckbox/OLIPCheckbox";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";
import { CellCheckbox } from "src/utils/backOfficeCategoryPlaylistsColumns";

import { TableCell } from "@mui/material";

export function CheckboxCell({
  checkbox,
  categoryContent,
}: {
  checkbox: CellCheckbox;
  categoryContent: CategoryPlaylistBackOffice;
}): ReactNode {
  return (
    <TableCell>
      <OLIPCheckbox
        checked={checkbox.checked(categoryContent)}
        disabled={checkbox.disabled}
        onChange={() => checkbox.onChange(categoryContent)}
      />
    </TableCell>
  );
}
