import { ReactNode } from "react";
import { CategoryApplication } from "src/types/application/categoryApplication.type";
import getEnv from "src/utils/getEnv";

import { Avatar, Box, TableCell, Typography } from "@mui/material";

export function ApplicationCell({
  application,
}: {
  application: CategoryApplication | undefined;
}): ReactNode {
  return (
    <TableCell>
      {application && application.id !== "olip" && (
        <Box
          display={application.id === "olip" ? "none" : "flex"}
          alignItems="center"
        >
          <Avatar
            variant="square"
            alt=""
            src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
          />
          <Box>
            <Typography ml={1.5}>{application.displayName}</Typography>
          </Box>
        </Box>
      )}
    </TableCell>
  );
}
