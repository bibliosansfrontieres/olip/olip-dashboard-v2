import { DraggableProvided, DraggableStateSnapshot } from "react-beautiful-dnd";

import { SxProps, Theme } from "@mui/material";

function customStyleDraggableRow(
  style: any,
  snapshot: DraggableStateSnapshot,
): SxProps {
  if (!snapshot.isDragging) {
    return style;
  }
  return {
    ...style,
    display: "table",
    "& td:nth-of-type(1)": { display: "table-cell", width: "10%" },
    "& td:nth-of-type(2)": { width: "50%" },
    "& td:nth-of-type(3)": { width: "20%" },
    "& td:nth-of-type(4)": { width: "15%" },
    "& td:nth-of-type(5)": { width: "5%" },
  };
}

export function draggableTableRowSx(
  provided: DraggableProvided,
  snapshot: DraggableStateSnapshot,
  isOpen: boolean,
) {
  return {
    backgroundColor: (theme: Theme) => theme.palette.grey[100],
    borderRadius: "0.5rem",
    "& td": {
      borderTop: (theme: Theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
    },
    "& td:first-of-type": {
      borderLeft: (theme: Theme) =>
        `0.0625rem solid ${theme.palette.grey[500]}`,
      borderTopLeftRadius: "0.5rem",
      borderBottomLeftRadius: isOpen ? "0" : "0.5rem",
      transition: (theme: Theme) =>
        `border-bottom-left-radius ${theme.transitions.duration.standard}ms`,
    },
    "& td:last-of-type": {
      borderRight: (theme: Theme) =>
        `0.0625rem solid ${theme.palette.grey[500]}`,
      borderTopRightRadius: "0.5rem",
      borderBottomRightRadius: isOpen ? "0" : "0.5rem",
      transition: (theme: Theme) =>
        `border-bottom-right-radius ${theme.transitions.duration.standard}ms`,
    },
    "& > *": { borderBottom: "unset" },
    ...customStyleDraggableRow(provided.draggableProps.style, snapshot),
  };
}
