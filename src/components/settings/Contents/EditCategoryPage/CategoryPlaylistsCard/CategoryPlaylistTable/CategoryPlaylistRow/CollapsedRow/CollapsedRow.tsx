import { ReactNode, useCallback, useContext } from "react";
import { useTranslation } from "react-i18next";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import ItemRow from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/ItemRow/ItemRow";
import ItemTable from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/ItemTable/ItemTable";
import { LanguageContext } from "src/contexts/language.context";
import useNewApi from "src/hooks/useNewApi";
import { getCategoryPlaylistsBackOfficeCategoryPlaylistItems } from "src/services/categoryContent.service";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";

import {
  Box,
  CircularProgress,
  Collapse,
  TableCell,
  TableRow,
  Theme,
  Typography,
} from "@mui/material";

const tableRowSx = {
  backgroundColor: (theme: Theme) => theme.palette.grey[100],
  borderRadius: "0.5rem",
  marginBottom: "0.625rem",
  padding: "2.625rem",
  "& td": {
    borderBottom: "0",
  },
  "& td:first-of-type": {
    borderLeft: (theme: Theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
    borderBottomLeftRadius: "0.5rem",
  },
  "& td:last-of-type": {
    borderRight: (theme: Theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
    borderBottomRightRadius: "0.5rem",
  },
  "& > *": { borderBottom: "unset" },
};

export function CollapsedRow({
  isOpen,
  categoryContent,
  itemsWithToggleDisplay,
}: {
  isOpen: boolean;
  categoryContent: CategoryPlaylistBackOffice;
  itemsWithToggleDisplay: boolean;
}): ReactNode {
  const { t } = useTranslation();
  const { language: languageIdentifier } = useContext(LanguageContext);

  const categoryPlaylistItemsResult = useNewApi(
    useCallback(async () => {
      return await getCategoryPlaylistsBackOfficeCategoryPlaylistItems({
        id: categoryContent.id.toString(),
        languageIdentifier,
      });
    }, [categoryContent.id, languageIdentifier]),
    //only load the items if the row is open
    isOpen,
  );

  return (
    <TableRow sx={tableRowSx}>
      <TableCell colSpan={5} sx={{ padding: "0" }}>
        <Collapse
          in={isOpen}
          timeout="auto"
          unmountOnExit
          sx={{ padding: "2rem" }}
        >
          <Box display="flex">
            <Typography
              variant="body1m"
              sx={{ whiteSpace: "nowrap" }}
              mr={1}
              data-testid="descriptionTitleCollapsed"
            >
              {t(
                "settings.pages.contents.theme.playlistsCard.table.header.description",
              )}
            </Typography>
            <Typography ml={1} data-testid="descriptionInformationCollapsed">
              {categoryContent.playlist.playlistTranslations[0].description}
            </Typography>
          </Box>
          <ItemTable itemsWithToggleDisplay={itemsWithToggleDisplay}>
            <WaitApiResult
              useApiResult={categoryPlaylistItemsResult}
              loadingElement={
                <TableRow>
                  <TableCell colSpan={5}>
                    <Box style={{ display: "flex", justifyContent: "center" }}>
                      <CircularProgress />
                    </Box>
                  </TableCell>
                </TableRow>
              }
              errorElement={
                <TableRow>
                  <TableCell colSpan={5}>
                    <Box display="flex" justifyContent="center">
                      <Typography variant="caption">
                        {t("common.error.generic")}
                      </Typography>
                    </Box>
                  </TableCell>
                </TableRow>
              }
              resultChildren={({ categoryPlaylistItems, applications }) => {
                if (itemsWithToggleDisplay) {
                  return categoryPlaylistItems.map((categoryPlaylistItem) => {
                    const application = applications.find(
                      (app) =>
                        app.id === categoryPlaylistItem.item.application.id,
                    );
                    return (
                      <ItemRow
                        key={categoryPlaylistItem.id}
                        item={categoryPlaylistItem.item}
                        displayId={categoryPlaylistItem.id}
                        displayValue={categoryPlaylistItem.isDisplayed}
                        itemApplication={application}
                        setResult={categoryPlaylistItemsResult.setResult}
                      />
                    );
                  });
                }
                return categoryPlaylistItems
                  .filter(
                    (categoryPlaylistItem) => categoryPlaylistItem.isDisplayed,
                  )
                  .map((categoryPlaylistItem) => {
                    const application = applications.find(
                      (app) =>
                        app.id === categoryPlaylistItem.item.application.id,
                    );
                    return (
                      <ItemRow
                        key={categoryPlaylistItem.id}
                        item={categoryPlaylistItem.item}
                        itemApplication={application}
                      />
                    );
                  });
              }}
            />
          </ItemTable>
        </Collapse>
      </TableCell>
    </TableRow>
  );
}
