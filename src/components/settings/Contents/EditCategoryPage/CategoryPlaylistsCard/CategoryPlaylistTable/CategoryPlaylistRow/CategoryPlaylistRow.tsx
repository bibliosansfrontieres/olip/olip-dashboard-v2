import { ReactNode, useContext, useState } from "react";
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from "react-beautiful-dnd";
import { ApplicationCell } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CategoryPlaylistCells/ApplicationCell";
import { CheckboxCell } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CategoryPlaylistCells/CheckboxCell";
import { FirstCell } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CategoryPlaylistCells/FirstCell";
import { SizeCell } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CategoryPlaylistCells/SizeCell";
import { TitleCell } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CategoryPlaylistCells/TitleCell";
import { draggableTableRowSx } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/categoryPlaylistRowStyle";
import { CollapsedRow } from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistRow/CollapsedRow/CollapsedRow";
import { CategoryBackOfficeCategoryPlaylistsContext } from "src/contexts/categoryPlaylistsBackOffice.context";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";
import { CellCheckbox } from "src/utils/backOfficeCategoryPlaylistsColumns";

import { TableRow } from "@mui/material";

type Props = {
  canChangeOrder?: boolean;
  categoryContent: CategoryPlaylistBackOffice;
  checkbox?: CellCheckbox;
  index: number;
  itemsWithToggleDisplay?: boolean;
};

export default function CategoryPlaylistRow({
  canChangeOrder = false,
  categoryContent,
  checkbox,
  index,
  itemsWithToggleDisplay = false,
}: Props): ReactNode {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const { result } = useContext(CategoryBackOfficeCategoryPlaylistsContext);
  if (!result) return <></>;
  const application = result.applications.find(
    (app) => app.id === categoryContent.playlist.application.id,
  );

  return (
    <Draggable
      key={categoryContent.id}
      draggableId={String(categoryContent.id)}
      index={index}
      isDragDisabled={!canChangeOrder}
    >
      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
        <>
          <TableRow
            data-testid="categoryPlaylistRow"
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            sx={draggableTableRowSx(provided, snapshot, isOpen)}
          >
            <FirstCell
              setIsOpen={setIsOpen}
              isOpen={isOpen}
              canChangeOrder={canChangeOrder}
              image={categoryContent.playlist.image}
            />
            <TitleCell
              title={categoryContent.playlist.playlistTranslations[0].title}
            />
            <ApplicationCell application={application} />
            <SizeCell size={categoryContent.playlist.size} />
            {checkbox && (
              <CheckboxCell
                checkbox={checkbox}
                categoryContent={categoryContent}
              />
            )}
          </TableRow>
          <CollapsedRow
            isOpen={isOpen}
            categoryContent={categoryContent}
            itemsWithToggleDisplay={itemsWithToggleDisplay}
          />
        </>
      )}
    </Draggable>
  );
}
