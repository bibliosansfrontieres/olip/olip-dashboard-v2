import { ReactNode } from "react";

import { TableCell, Tooltip, Typography } from "@mui/material";

export function TitleCell({ title }: { title: string }): ReactNode {
  return (
    <TableCell sx={{ overflow: "hidden", maxWidth: 0 }}>
      <Tooltip placement="top-start" title={title}>
        <Typography
          data-testid="titleCell"
          sx={{
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
        >
          {title}
        </Typography>
      </Tooltip>
    </TableCell>
  );
}
