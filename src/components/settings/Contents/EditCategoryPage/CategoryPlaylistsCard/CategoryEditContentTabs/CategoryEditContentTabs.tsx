import { ReactElement, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import CategoryPlaylistSelectedTab from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/CategoryPlaylistSelectedTab/CategoryPlaylistSelectedTab";
import PlaylistInstalledTab from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/PlaylistInstalledTab/PlaylistInstalledTab";
import PlaylistNoInstalledTab from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/PlaylistNoInstalledTab/PlaylistNoInstalledTab";
import PlaylistUpdatesTab from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/PlaylistUpdatesTab/PlaylistUpdatesTab";
import { CategoryBackOfficeCategoryPlaylistsContext } from "src/contexts/categoryPlaylistsBackOffice.context";
import useAPI from "src/hooks/useAPI";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import {
  getInstalledPlaylists,
  getUninstalledPlaylists,
  getUpdateNeededPlaylists,
} from "src/services/playlist.service";
import {
  GetInstalledPlaylistsResult,
  GetUninstalledPlaylistsResult,
  GetUpdateNeededPlaylistsResult,
} from "src/types/playlist.type";
import { InternetAvailableEvent } from "src/types/socket.type";

import { Tab, Tabs } from "@mui/material";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  isHidden: boolean;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, isHidden, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={isHidden}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {children}
    </div>
  );
}

type Props = {
  categoryId: number;
};

export default function CategoryEditContentTabs({
  categoryId,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [tabSelected, setTabSelected] = useState<number>(0);
  const { event: internetAvailableEvent, isLoading: checkInternetIsLoading } =
    useSocketOnEvent<InternetAvailableEvent>("internet-connection");
  const { reload: refreshSelected, result: selectedResult } = useContext(
    CategoryBackOfficeCategoryPlaylistsContext,
  );
  const [installedParams, setInstalledParams] = useState<{
    applicationIds?: string;
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: "",
    take: 10,
  });
  const {
    isLoading: installedIsLoading,
    refresh: refreshInstalled,
    result: installedResult,
  } = useAPI<GetInstalledPlaylistsResult>(
    (config) =>
      getInstalledPlaylists(
        {
          ...installedParams,
          categoryId: categoryId.toString(),
        },
        config,
      ),
    [installedParams],
  );

  const [uninstalledParams, setUninstalledParams] = useState<{
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: "",
    take: 10,
  });
  const {
    isLoading: uninstalledIsLoading,
    refresh: refreshUninstalled,
    result: uninstalledResult,
  } = useAPI<GetUninstalledPlaylistsResult>(
    (config) =>
      getUninstalledPlaylists(
        {
          ...uninstalledParams,
          categoryId: categoryId.toString(),
        },
        config,
      ),
    [uninstalledParams],
  );

  const [updatesParams, setUpdatesParams] = useState<{
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: "",
    take: 10,
  });
  const {
    isLoading: updatesIsLoading,
    refresh: refreshUpdates,
    result: updatesResult,
  } = useAPI<GetUpdateNeededPlaylistsResult>(
    (config) =>
      getUpdateNeededPlaylists(
        {
          ...updatesParams,
          categoryId: categoryId.toString(),
        },
        config,
      ),
    [updatesParams],
  );

  const refreshLists = async (
    action:
      | "addToSelection"
      | "install"
      | "removeToSelection"
      | "uninstall"
      | "update",
  ) => {
    switch (action) {
      case "addToSelection":
      case "removeToSelection":
        await Promise.all([refreshInstalled(), refreshSelected()]);
        break;
      case "install":
      case "uninstall":
        await Promise.all([refreshInstalled(), refreshUninstalled()]);
        break;
      case "update":
        await refreshUpdates();
        break;
      default:
        break;
    }
  };

  return (
    <>
      <Tabs
        variant="fullWidth"
        value={tabSelected}
        onChange={(_event: React.SyntheticEvent, newValue: number) =>
          setTabSelected(newValue)
        }
      >
        <Tab
          data-testid="tabSelected"
          label={t("form.categoryContent.tab.selected.title", {
            count: selectedResult?.categoryPlaylists.length ?? 0,
          })}
        />
        <Tab
          data-testid="tabInstalled"
          label={t("form.categoryContent.tab.installed.title", {
            count: installedResult?.meta?.totalItemsCount ?? 0,
          })}
        />
        <Tab
          data-testid="tabUninstalled"
          label={t("form.categoryContent.tab.noInstalled.title", {
            count: uninstalledResult?.meta?.totalItemsCount ?? 0,
          })}
        />
        <Tab
          data-testid="tabUpdate"
          label={t("form.categoryContent.tab.updates.title", {
            count: updatesResult?.meta?.totalItemsCount ?? 0,
          })}
        />
      </Tabs>
      <CustomTabPanel index={0} isHidden={tabSelected !== 0}>
        <CategoryPlaylistSelectedTab refreshLists={refreshLists} />
      </CustomTabPanel>
      <CustomTabPanel index={1} isHidden={tabSelected !== 1}>
        <PlaylistInstalledTab
          dataIsLoading={installedIsLoading}
          meta={installedResult?.meta}
          onParamsChange={(newParams) =>
            setInstalledParams((prev) => ({ ...prev, ...newParams }))
          }
          playlists={installedResult?.playlists}
          refreshLists={refreshLists}
        />
      </CustomTabPanel>
      <CustomTabPanel index={2} isHidden={tabSelected !== 2}>
        <PlaylistNoInstalledTab
          dataIsLoading={uninstalledIsLoading}
          internetAvailable={
            !checkInternetIsLoading &&
            Boolean(internetAvailableEvent?.isConnected)
          }
          meta={uninstalledResult?.meta}
          onParamsChange={(newParams) =>
            setUninstalledParams((prev) => ({ ...prev, ...newParams }))
          }
          playlists={uninstalledResult?.playlists}
          refreshLists={refreshLists}
        />
      </CustomTabPanel>
      <CustomTabPanel index={3} isHidden={tabSelected !== 3}>
        <PlaylistUpdatesTab
          dataIsLoading={updatesIsLoading}
          internetAvailable={
            !checkInternetIsLoading &&
            Boolean(internetAvailableEvent?.isConnected)
          }
          meta={updatesResult?.meta}
          onParamsChange={(newParams) =>
            setUpdatesParams((prev) => ({ ...prev, ...newParams }))
          }
          playlists={updatesResult?.playlists}
          refreshLists={refreshLists}
        />
      </CustomTabPanel>
    </>
  );
}
