import WifiOffRoundedIcon from "@mui/icons-material/WifiOffRounded";
import { Box, Skeleton, Stack, Typography } from "@mui/material";
import { ReactElement, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { installPlaylists } from "src/services/playlist.service";
import { Pagination } from "src/types";
import { Playlist } from "src/types/playlist.type";
import PlaylistTable from "../PlaylistTable/PlaylistTable";
import PlaylistNoInstalledHeader from "./PlaylistNoInstalledHeader/PlaylistNoInstalledHeader";

type Props = {
  internetAvailable: boolean;
  dataIsLoading: boolean;
  meta?: Pagination;
  onParamsChange: (newParams: {
    applicationIds?: string;
    page?: number;
    query?: string;
    take?: number;
  }) => void;
  playlists?: Playlist[];
  refreshLists: (action: "install") => Promise<void>;
};

export default function PlaylistNoInstalledTab({
  internetAvailable,
  dataIsLoading,
  meta,
  onParamsChange,
  playlists = [],
  refreshLists,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [playlistsChecked, setPlaylistsChecked] = useState<Playlist[]>([]);
  const { displaySnackbar } = useContext(SnackbarContext);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const getPlaylistCheckedIndex = (playlist: Playlist) =>
    playlistsChecked.findIndex(({ id }) => id === playlist.id);

  const handleFilter = (applicationIds: string[]) => {
    onParamsChange({ applicationIds: applicationIds.join(","), page: 1 });
    setPlaylistsChecked([]);
  };

  const handleSearch = (event: any) => {
    onParamsChange({ query: event.target.value, page: 1 });
    setPlaylistsChecked([]);
  };

  const toggleAllPlaylist = () => {
    if (playlistsChecked.length === playlists.length) {
      setPlaylistsChecked([]);
    } else {
      setPlaylistsChecked(playlists);
    }
  };

  async function install() {
    setIsLoading(true);
    const result = await installPlaylists({
      playlistIds: playlistsChecked.map((playlist) => playlist.id),
    });
    switch (result.statusCode) {
      case 201:
        await refreshLists("install");
        displaySnackbar(
          t("snackbar.installPlaylists.success", {
            count: playlistsChecked.length,
          })
        );
        setPlaylistsChecked([]);
        break;
      default:
        displaySnackbar(
          t("snackbar.installPlaylists.error", {
            count: playlistsChecked.length,
          }),
          "error"
        );
        break;
    }
    setIsLoading(false);
  }

  const updatePlaylistsChecked = (playlist: Playlist) => {
    if (getPlaylistCheckedIndex(playlist) !== -1) {
      const nextList = [...playlistsChecked];
      nextList.splice(getPlaylistCheckedIndex(playlist), 1);
      setPlaylistsChecked(nextList);
    } else {
      setPlaylistsChecked((prevList) => [...prevList, playlist]);
    }
  };

  if (!internetAvailable) {
    return (
      <Box display="flex" justifyContent="center" alignItems="center" my={30}>
        <Stack spacing={4} alignItems="center">
          <WifiOffRoundedIcon fontSize="large" />
          <Typography>
            {t("form.categoryContent.tab.noInstalled.noInternet")}
          </Typography>
        </Stack>
      </Box>
    );
  }

  return (
    <>
      <PlaylistNoInstalledHeader
        disabledActions={isLoading || playlistsChecked.length === 0}
        handleFilter={handleFilter}
        handleSearch={handleSearch}
        onClickInstall={install}
      />
      {dataIsLoading ? (
        <>
          {Array(10)
            .fill("")
            .map((_notUse, index) => (
              <Skeleton
                key={index}
                variant="rounded"
                sx={{
                  height: "4.5rem",
                  marginY: "0.5rem",
                }}
              />
            ))}
        </>
      ) : playlists.length === 0 ? (
        <Box display="flex" justifyContent="center" alignItems="center" my={30}>
          <Typography>
            {t("form.categoryContent.tab.noInstalled.noPlaylist")}
          </Typography>
        </Box>
      ) : (
        <PaginationLayout
          onChange={(newParams) => onParamsChange(newParams)}
          meta={meta}
          takeList={["10", "25", "50"]}>
          {playlists.length === 0 ? (
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              my={30}>
              <Typography>
                {t("form.categoryContent.tab.noInstalled.noPlaylist")}
              </Typography>
            </Box>
          ) : (
            <PlaylistTable
              playlists={playlists}
              checkbox={{
                checked: (playlist: Playlist) =>
                  getPlaylistCheckedIndex(playlist) !== -1,
                disabled: isLoading,
                onChange: updatePlaylistsChecked,
              }}
              columns={[
                { width: "10%" },
                {
                  header: t(
                    "settings.pages.contents.theme.playlistsCard.table.header.name"
                  ),
                  width: "50%",
                },
                {
                  header: t(
                    "settings.pages.contents.theme.playlistsCard.table.header.application"
                  ),
                  width: "20%",
                },
                {
                  header: t(
                    "settings.pages.contents.theme.playlistsCard.table.header.size"
                  ),
                  width: "15%",
                },
                {
                  checkbox: {
                    checked: playlists.length === playlistsChecked.length,
                    disabled: isLoading,
                    onChange: toggleAllPlaylist,
                  },
                  width: "5%",
                },
              ]}
            />
          )}
        </PaginationLayout>
      )}
    </>
  );
}
