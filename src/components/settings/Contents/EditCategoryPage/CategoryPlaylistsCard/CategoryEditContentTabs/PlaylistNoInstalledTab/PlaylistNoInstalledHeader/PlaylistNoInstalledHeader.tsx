import { Box, Button } from "@mui/material";
import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import DownloadRoundedIcon from "@mui/icons-material/DownloadRounded";
import SearchFilter from "src/components/common/SearchFilter/SearchFilter";
import FilterApplicationSelect from "src/components/common/FilterApplicationSelect/FilterApplicationSelect";

type Props = {
  disabledActions: boolean;
  handleFilter: (applicationIds: string[]) => void;
  handleSearch: (event: any) => void;
  onClickInstall: () => void;
};

export default function PlaylistNoInstalledHeader({
  disabledActions,
  handleFilter,
  handleSearch,
  onClickInstall,
}: Props): ReactElement {
  const { t } = useTranslation();

  return (
    <Box display="flex" justifyContent="space-between" mt={5}>
      <Box>
        <FilterApplicationSelect onFilter={handleFilter} />
        <SearchFilter
          onChange={handleSearch}
          sxFormControl={{ marginX: "1rem" }}
        />
      </Box>
      <Box>
        <Button
          variant="contained"
          startIcon={<DownloadRoundedIcon />}
          disabled={disabledActions}
          onClick={onClickInstall}>
          {t("settings.pages.contents.theme.playlistsCard.button.install")}
        </Button>
      </Box>
    </Box>
  );
}
