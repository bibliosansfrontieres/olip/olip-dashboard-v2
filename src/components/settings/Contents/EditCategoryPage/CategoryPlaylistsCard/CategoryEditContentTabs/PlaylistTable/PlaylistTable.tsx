import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { ReactElement } from "react";
import OLIPCheckbox from "src/components/common/OLIPCheckbox/OLIPCheckbox";
import { Playlist } from "src/types/playlist.type";
import PlaylistRow from "./PlaylistRow/PlaylistRow";

type Props = {
  checkbox?: {
    checked: (playlist: Playlist) => boolean;
    disabled: boolean;
    onChange: (playlist: Playlist) => void;
  };
  columns: {
    checkbox?: {
      checked: boolean;
      disabled: boolean;
      onChange: () => void;
    };
    header?: string;
    width?: string;
  }[];
  playlists: Playlist[];
};

export default function PlaylistTable({
  checkbox,
  columns,
  playlists,
}: Props): ReactElement {
  return (
    <Table
      sx={{
        /* borderSpacing: "0 0.625rem", */
        borderCollapse: "separate",
        overflowWrap: "break-word",
        paddingY: "0.5rem",
        tableLayout: "fixed",
        width: "100%",
        "& th": { border: "0", paddingBottom: 0 },
      }}>
      <TableHead>
        <TableRow>
          {columns.map((column, index) => (
            <TableCell key={index} width={column.width}>
              {column.checkbox ? (
                <OLIPCheckbox
                  checked={column.checkbox.checked}
                  onChange={column.checkbox.onChange}
                />
              ) : (
                <Typography variant="tab">{column.header}</Typography>
              )}
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {playlists.map((playlist) => (
          <PlaylistRow
            key={playlist.id}
            checkbox={checkbox}
            playlist={playlist}
          />
        ))}
      </TableBody>
    </Table>
  );
}
