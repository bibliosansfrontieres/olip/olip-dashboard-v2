import { ReactElement, useState } from "react";
import { useTranslation } from "react-i18next";
import ChevronDownIcon from "src/assets/icons/ChevronDown";
import PictureIcon from "src/assets/icons/Picture";
import OLIPCheckbox from "src/components/common/OLIPCheckbox/OLIPCheckbox";
import ItemRow from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/ItemRow/ItemRow";
import ItemTable from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/ItemTable/ItemTable";
import { Playlist } from "src/types/playlist.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import getEnv from "src/utils/getEnv";

import {
  Avatar,
  Box,
  Collapse,
  IconButton,
  TableCell,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";

type Props = {
  playlist: Playlist;
  checkbox?: {
    checked: (playlist: Playlist) => boolean;
    disabled: boolean;
    onChange: (playlist: Playlist) => void;
  };
};
export default function PlaylistRow({
  playlist,
  checkbox,
}: Props): ReactElement {
  const [open, setOpen] = useState<boolean>(false);
  const { t } = useTranslation();

  return (
    <>
      <TableRow
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          borderRadius: "0.5rem",
          "& td": {
            borderTop: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          },
          "& td:first-of-type": {
            borderLeft: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
            borderTopLeftRadius: "0.5rem",
            borderBottomLeftRadius: open ? "0" : "0.5rem",
            transition: (theme) =>
              `border-bottom-left-radius ${theme.transitions.duration.standard}ms`,
          },
          "& td:last-of-type": {
            borderRight: (theme) =>
              `0.0625rem solid ${theme.palette.grey[500]}`,
            borderTopRightRadius: "0.5rem",
            borderBottomRightRadius: open ? "0" : "0.5rem",
            transition: (theme) =>
              `border-bottom-right-radius ${theme.transitions.duration.standard}ms`,
          },
          "& > *": { borderBottom: "unset" },
        }}
      >
        <TableCell>
          <Box
            display="flex"
            justifyContent="space-around"
            alignItems="center"
            ml={1}
          >
            <IconButton
              size="small"
              onClick={() => setOpen(!open)}
              sx={{ marginRight: "0.5rem" }}
            >
              <ChevronDownIcon
                sx={{ transform: open ? "rotate(180deg)" : "" }}
              />
            </IconButton>
            <Avatar
              variant="rounded"
              alt=""
              src={
                playlist.image.includes("http")
                  ? playlist.image
                  : getEnv("VITE_AXIOS_BASEURL") + playlist.image
              }
              sx={{
                backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              {playlist.image && <PictureIcon />}
            </Avatar>
          </Box>
        </TableCell>
        <TableCell sx={{ overflow: "hidden", maxWidth: 0 }}>
          <Tooltip
            placement="top-start"
            title={
              playlist.playlistTranslations[
                findIndexTranslation(playlist.playlistTranslations)
              ].title
            }
          >
            <Typography
              sx={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
              }}
            >
              {
                playlist.playlistTranslations[
                  findIndexTranslation(playlist.playlistTranslations)
                ].title
              }
            </Typography>
          </Tooltip>
        </TableCell>
        <TableCell>
          <Box
            display={playlist.application.isOlip ? "none" : "flex"}
            alignItems="center"
          >
            <Avatar
              variant="square"
              alt=""
              src={getEnv("VITE_AXIOS_BASEURL") + playlist.application.logo}
            />
            <Box>
              <Typography ml={1.5}>
                {playlist.application.displayName}
              </Typography>
            </Box>
          </Box>
        </TableCell>
        <TableCell>
          <Typography>
            {t("common.megabyte", {
              size: (Number(playlist.size) / 1024 / 1024).toFixed(2),
            })}
          </Typography>
        </TableCell>
        {checkbox && (
          <TableCell>
            <OLIPCheckbox
              checked={checkbox.checked(playlist)}
              dataTestId={playlist.id}
              disabled={checkbox.disabled}
              onChange={() => checkbox.onChange(playlist)}
            />
          </TableCell>
        )}
      </TableRow>
      <TableRow
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          borderRadius: "0.5rem",
          marginBottom: "0.625rem",
          padding: "2.625rem",
          "& td": {
            borderBottom: "0",
          },
          "& td:first-of-type": {
            borderLeft: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
            borderBottomLeftRadius: "0.5rem",
          },
          "& td:last-of-type": {
            borderRight: (theme) =>
              `0.0625rem solid ${theme.palette.grey[500]}`,
            borderBottomRightRadius: "0.5rem",
          },
          "& > *": { borderBottom: "unset" },
        }}
      >
        <TableCell colSpan={5} sx={{ padding: "0" }}>
          <Collapse
            in={open}
            timeout="auto"
            unmountOnExit
            sx={{ padding: "2rem" }}
          >
            <Box display="flex">
              <Typography variant="body1m" sx={{ whiteSpace: "nowrap" }} mr={1}>
                {t(
                  "settings.pages.contents.theme.playlistsCard.table.header.description",
                )}
              </Typography>
              <Typography ml={1}>
                {
                  playlist.playlistTranslations[
                    findIndexTranslation(playlist.playlistTranslations)
                  ].description
                }
              </Typography>
            </Box>
            <ItemTable>
              {playlist.items?.map((item) => (
                <ItemRow key={item.id} item={item} />
              ))}
            </ItemTable>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
}
