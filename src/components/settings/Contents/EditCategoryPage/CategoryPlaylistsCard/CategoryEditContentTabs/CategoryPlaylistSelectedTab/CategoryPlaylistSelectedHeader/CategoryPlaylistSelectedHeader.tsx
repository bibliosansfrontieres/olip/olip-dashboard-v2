import { Box, Button } from "@mui/material";
import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import CloseIcon from "src/assets/icons/Close";

type Props = {
  disabledActions: boolean;
  onClickRemove: () => void;
};

export default function CategoryPlaylistSelectedHeader({
  disabledActions,
  onClickRemove,
}: Props): ReactElement {
  const { t } = useTranslation();

  return (
    <Box display="flex" justifyContent="flex-end" mt={5}>
      <Button
        variant="contained"
        startIcon={<CloseIcon />}
        disabled={disabledActions}
        onClick={onClickRemove}>
        {t("settings.pages.contents.theme.playlistsCard.button.remove")}
      </Button>
    </Box>
  );
}
