import { Dispatch, ReactNode, SetStateAction, useContext } from "react";
import { useTranslation } from "react-i18next";
import CategoryPlaylistTable from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistTable";
import { CategoryBackOfficeCategoryPlaylistsContext } from "src/contexts/categoryPlaylistsBackOffice.context";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";

import { Box, Skeleton, Typography } from "@mui/material";

export default function CategoryPlaylistSelectedBody({
  categoryContentsChecked,
  setCategoryContentsChecked,
  isLoadingRemove,
}: {
  categoryContentsChecked: CategoryPlaylistBackOffice[];
  setCategoryContentsChecked: Dispatch<
    SetStateAction<CategoryPlaylistBackOffice[]>
  >;
  isLoadingRemove: boolean;
}): ReactNode {
  const { t } = useTranslation();
  const { result: selectedResult, isLoading } = useContext(
    CategoryBackOfficeCategoryPlaylistsContext,
  );

  if (!selectedResult) return <></>;
  const categoryContents = selectedResult.categoryPlaylists;

  if (isLoading && categoryContents.length === 0) {
    return (
      <>
        {Array(10)
          .fill("")
          .map((_notUse, index) => (
            <Skeleton
              key={index}
              variant="rounded"
              sx={{
                height: "4.5rem",
                marginY: "0.5rem",
              }}
            />
          ))}
      </>
    );
  }

  if (categoryContents.length === 0)
    return (
      <Box display="flex" justifyContent="center" alignItems="center" my={30}>
        <Typography>
          {t("form.categoryContent.tab.selected.noPlaylist")}
        </Typography>
      </Box>
    );

  const existCategoryContentIndex = (
    categoryContent: CategoryPlaylistBackOffice,
  ) => categoryContentsChecked.findIndex(({ id }) => id === categoryContent.id);

  const toggleAllPlaylist = () => {
    if (categoryContentsChecked.length === categoryContents.length) {
      setCategoryContentsChecked([]);
    } else {
      setCategoryContentsChecked(
        categoryContents.map((categoryContent) => categoryContent),
      );
    }
  };

  const updateCategoryContentsChecked = (
    categoryContent: CategoryPlaylistBackOffice,
  ) => {
    if (existCategoryContentIndex(categoryContent) !== -1) {
      const nextList = [...categoryContentsChecked];
      nextList.splice(existCategoryContentIndex(categoryContent), 1);
      setCategoryContentsChecked(nextList);
    } else {
      setCategoryContentsChecked((prevList) => [...prevList, categoryContent]);
    }
  };

  return (
    <CategoryPlaylistTable
      canChangeOrder
      itemsWithToggleDisplay
      categoryContents={categoryContents.sort((a, b) => a.order - b.order)}
      checkboxLine={{
        checked: (categoryContent) =>
          existCategoryContentIndex(categoryContent) !== -1,
        disabled: isLoading,
        onChange: updateCategoryContentsChecked,
      }}
      checkboxColumn={{
        checked: categoryContents.length === categoryContentsChecked.length,
        disabled: isLoadingRemove,
        onChange: toggleAllPlaylist,
      }}
    />
  );
}
