import { ReactElement, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import CategoryPlaylistSelectedBody from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/CategoryPlaylistSelectedTab/CategoryPlaylistSelectedBody/CategoryPlaylistSelectedBody";
import CategoryPlaylistSelectedHeader from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/CategoryPlaylistSelectedTab/CategoryPlaylistSelectedHeader/CategoryPlaylistSelectedHeader";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { removeCategoryPlaylists } from "src/services/categoryContent.service";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";

type Props = {
  refreshLists: (action: "removeToSelection") => Promise<void>;
};

export default function CategoryPlaylistSelectedTab({
  refreshLists,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [categoryContentsChecked, setCategoryContentsChecked] = useState<
    CategoryPlaylistBackOffice[]
  >([]);
  const { displaySnackbar } = useContext(SnackbarContext);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const remove = async () => {
    setIsLoading(true);
    try {
      const result = await removeCategoryPlaylists(
        categoryContentsChecked.map((categoryContent) => categoryContent.id),
      );
      if (result.statusCode === 200) {
        await refreshLists("removeToSelection");
        setCategoryContentsChecked([]);
        displaySnackbar(t("snackbar.removeToSelection.success"));
      } else {
        console.error(`${result.statusCode} - ${result.errorMessage}`);
        displaySnackbar(t("snackbar.removeToSelection.error"), "error");
      }
    } catch (error) {
      console.error(error);
      displaySnackbar(t("snackbar.removeToSelection.error"), "error");
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <>
      <CategoryPlaylistSelectedHeader
        disabledActions={isLoading || categoryContentsChecked.length === 0}
        onClickRemove={remove}
      />
      <CategoryPlaylistSelectedBody
        categoryContentsChecked={categoryContentsChecked}
        setCategoryContentsChecked={setCategoryContentsChecked}
        isLoadingRemove={isLoading}
      />
    </>
  );
}
