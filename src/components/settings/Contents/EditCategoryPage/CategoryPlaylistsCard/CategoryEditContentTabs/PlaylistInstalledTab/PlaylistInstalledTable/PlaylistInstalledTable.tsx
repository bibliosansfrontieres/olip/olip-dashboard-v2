import { Dispatch, SetStateAction } from "react";
import { useTranslation } from "react-i18next";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import { Pagination } from "src/types";
import { Playlist } from "src/types/playlist.type";

import { Box, Typography } from "@mui/material";

import PlaylistTable from "../../PlaylistTable/PlaylistTable";

type Props = {
  isLoading: boolean;
  meta?: Pagination;
  onParamsChange: (newParams: {
    applicationIds?: string;
    page?: number;
    query?: string;
    take?: number;
  }) => void;
  playlists: Playlist[];
  playlistsChecked: Playlist[];
  setPlaylistsChecked: Dispatch<SetStateAction<Playlist[]>>;
};
export default function PlaylistInstalledTable({
  meta,
  playlists,
  onParamsChange,
  isLoading,
  playlistsChecked,
  setPlaylistsChecked,
}: Props) {
  const { t } = useTranslation();

  const getPlaylistCheckedIndex = (playlist: Playlist) =>
    playlistsChecked.findIndex(
      ({ id: playlistId }) => playlistId === playlist.id,
    );
  const toggleAllPlaylist = () => {
    if (playlistsChecked.length === playlists.length) {
      setPlaylistsChecked([]);
    } else {
      setPlaylistsChecked(playlists);
    }
  };

  const updatePlaylistsChecked = (playlist: Playlist) => {
    if (getPlaylistCheckedIndex(playlist) !== -1) {
      const nextList = [...playlistsChecked];
      nextList.splice(getPlaylistCheckedIndex(playlist), 1);
      setPlaylistsChecked(nextList);
    } else {
      setPlaylistsChecked((prevList) => [...prevList, playlist]);
    }
  };
  return (
    <PaginationLayout
      onChange={(newParams) => onParamsChange(newParams)}
      meta={meta}
      takeList={["10", "25", "50"]}
    >
      {playlists.length === 0 ? (
        <Box display="flex" justifyContent="center" alignItems="center" my={30}>
          <Typography>
            {t("form.categoryContent.tab.installed.noPlaylist")}
          </Typography>
        </Box>
      ) : (
        <PlaylistTable
          playlists={playlists}
          checkbox={{
            checked: (playlist: Playlist) =>
              getPlaylistCheckedIndex(playlist) !== -1,
            disabled: isLoading,
            onChange: updatePlaylistsChecked,
          }}
          columns={[
            { width: "10%" },
            {
              header: t(
                "settings.pages.contents.theme.playlistsCard.table.header.name",
              ),
              width: "50%",
            },
            {
              header: t(
                "settings.pages.contents.theme.playlistsCard.table.header.application",
              ),
              width: "20%",
            },
            {
              header: t(
                "settings.pages.contents.theme.playlistsCard.table.header.size",
              ),
              width: "15%",
            },
            {
              checkbox: {
                checked: playlists.length === playlistsChecked.length,
                disabled: isLoading,
                onChange: toggleAllPlaylist,
              },
              width: "5%",
            },
          ]}
        />
      )}
    </PaginationLayout>
  );
}
