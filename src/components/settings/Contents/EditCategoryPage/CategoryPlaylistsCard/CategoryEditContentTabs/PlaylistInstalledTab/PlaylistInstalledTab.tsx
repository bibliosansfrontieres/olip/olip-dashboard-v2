import { ReactElement, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import TableSkeleton from "src/components/layouts/Skeleton/TableSkeleton";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { addPlaylistToCategory } from "src/services/category.service";
import { uninstallPlaylists } from "src/services/playlist.service";
import { Pagination } from "src/types";
import { LinkedPlaylist, Playlist } from "src/types/playlist.type";

import PlaylistInstalledHeader from "./PlaylistInstalledHeader/PlaylistInstalledHeader";
import PlaylistInstalledTable from "./PlaylistInstalledTable/PlaylistInstalledTable";
import UninstallPlaylistModal from "./UninstallPlaylistModal/UninstallPlaylistModal";

type Props = {
  dataIsLoading: boolean;
  meta?: Pagination;
  onParamsChange: (newParams: {
    applicationIds?: string;
    page?: number;
    query?: string;
    take?: number;
  }) => void;
  playlists?: Playlist[];
  refreshLists: (action: "addToSelection" | "uninstall") => Promise<void>;
};

export default function PlaylistInstalledTab({
  dataIsLoading,
  meta,
  onParamsChange,
  playlists = [],
  refreshLists,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [playlistsChecked, setPlaylistsChecked] = useState<Playlist[]>([]);
  const { id } = useParams();
  const { displaySnackbar } = useContext(SnackbarContext);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [uninstallPlaylistImpact, setUninstallPlaylistImpact] = useState<
    LinkedPlaylist[] | undefined
  >();
  const [openModal, setOpenModal] = useState(false);

  async function addToSelection() {
    if (id) {
      setIsLoading(true);
      const result = await addPlaylistToCategory({
        categoryId: id,
        playlistIds: playlistsChecked.map((playlist) => playlist.id),
      });
      if (result.statusCode === 201) {
        await refreshLists("addToSelection");
        setPlaylistsChecked([]);
        displaySnackbar(t("snackbar.addPlaylistToCategory.success"));
      } else {
        displaySnackbar(t("snackbar.addPlaylistToCategory.error"), "error");
      }

      setIsLoading(false);
    }
  }

  async function onClickUninstall() {
    const result = await uninstallPlaylists({
      playlistIds: playlistsChecked.map((playlist) => playlist.id),
    });
    setUninstallPlaylistImpact(result.linkedPlaylists);
    setOpenModal(true);
  }

  async function uninstall() {
    setIsLoading(true);
    const result = await uninstallPlaylists({
      confirm: true,
      playlistIds: playlistsChecked.map((playlist) => playlist.id),
    });
    switch (result.statusCode) {
      case 200:
        await refreshLists("uninstall");
        displaySnackbar(
          t("snackbar.uninstallPlaylists.success", {
            count: playlistsChecked.length,
          }),
        );
        setPlaylistsChecked([]);
        setUninstallPlaylistImpact(undefined);
        break;
      default:
        displaySnackbar(
          t("snackbar.uninstallPlaylists.error", {
            count: playlistsChecked.length,
          }),
          "error",
        );
        break;
    }
    setIsLoading(false);
  }

  const handleModalClose = (confirm?: boolean) => {
    if (confirm) {
      uninstall();
      setOpenModal(false);
    } else {
      setOpenModal(false);
      setUninstallPlaylistImpact(undefined);
    }
  };

  const handleFilter = (applicationIds: string[]) => {
    onParamsChange({ applicationIds: applicationIds.join(","), page: 1 });
    setPlaylistsChecked([]);
  };

  const handleSearch = (event: any) => {
    onParamsChange({ query: event.target.value, page: 1 });
    setPlaylistsChecked([]);
  };

  return (
    <>
      <PlaylistInstalledHeader
        disabledActions={isLoading || playlistsChecked.length === 0}
        handleFilter={handleFilter}
        handleSearch={handleSearch}
        onClickAddToSelection={addToSelection}
        onClickUninstall={onClickUninstall}
      />
      {dataIsLoading ? (
        <TableSkeleton nbLines={10} />
      ) : (
        <>
          <PlaylistInstalledTable
            meta={meta}
            playlists={playlists}
            onParamsChange={onParamsChange}
            playlistsChecked={playlistsChecked}
            isLoading={isLoading}
            setPlaylistsChecked={setPlaylistsChecked}
          />
          <UninstallPlaylistModal
            onClose={handleModalClose}
            open={openModal}
            warningUninstallImpact={uninstallPlaylistImpact}
            playlistsChecked={playlistsChecked}
          />
        </>
      )}
    </>
  );
}
