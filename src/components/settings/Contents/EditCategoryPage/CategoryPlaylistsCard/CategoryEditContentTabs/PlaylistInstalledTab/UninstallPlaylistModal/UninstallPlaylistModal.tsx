import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { LinkedPlaylist, Playlist } from "src/types/playlist.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";

import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";

type Props = {
  open: boolean;
  onClose: (confirm?: boolean) => void;
  warningUninstallImpact: LinkedPlaylist[] | undefined;
  playlistsChecked: Playlist[];
};

export default function UninstallPlaylistModal({
  open,
  onClose,
  warningUninstallImpact,
  playlistsChecked,
}: Props): ReactElement {
  const { t } = useTranslation();

  const unlinkedPlaylists = playlistsChecked.filter(
    (playlist) =>
      !warningUninstallImpact?.some(
        (impact) => impact.playlist.id === playlist.id,
      ),
  );

  return (
    <ModalLayout onClose={() => onClose()} open={open}>
      <>
        <DialogTitle
          sx={{ marginBottom: "1rem" }}
          data-testid="uninstallPlaylistTitle"
        >
          {t("modal.uninstallPlaylist.title")}
        </DialogTitle>
        <DialogContent>
          {warningUninstallImpact && warningUninstallImpact.length > 0 && (
            <>
              <Typography data-testid="uninstallPlaylistWarning">
                {t("modal.uninstallPlaylist.warning", {
                  count: warningUninstallImpact.length,
                })}
              </Typography>
              <ul style={{ margin: "2.5rem 0" }}>
                {warningUninstallImpact.map((impact) => (
                  <li key={impact.playlist.id} style={{ margin: "1.5rem 0" }}>
                    <Typography data-testid="uninstallPlaylistPlaylistTitle">
                      {t("modal.uninstallPlaylist.playlist", {
                        count: impact.categories.length,
                        playlistTitle:
                          impact.playlist.playlistTranslations[
                            findIndexTranslation(
                              impact.playlist.playlistTranslations,
                            )
                          ].title,
                      })}
                    </Typography>
                    <ul>
                      {impact.categories.map((category) => (
                        <li key={category.id} style={{ margin: "0.5rem 0" }}>
                          <Typography data-testid="uninstallPlaylistCategoryTitle">
                            {
                              category.categoryTranslations[
                                findIndexTranslation(
                                  category.categoryTranslations,
                                )
                              ].title
                            }
                          </Typography>
                        </li>
                      ))}
                    </ul>
                  </li>
                ))}
              </ul>
              <Typography
                sx={{ marginBottom: "1.5rem" }}
                data-testid="askConfirm"
              >
                {t("modal.uninstallPlaylist.askConfirm", {
                  count: warningUninstallImpact.length,
                })}
              </Typography>
            </>
          )}
          {unlinkedPlaylists.length > 0 && (
            <>
              <Typography data-testid="uninstallPlaylistUnlink">
                {t("modal.uninstallPlaylist.unlink_playlist", {
                  count: unlinkedPlaylists.length,
                })}
              </Typography>
              <ul>
                {unlinkedPlaylists.map((playlist) => (
                  <li key={playlist.id} style={{ margin: "1.5rem 0" }}>
                    <Typography data-testid="uninstallUnlinkPlaylistTitle">
                      {
                        playlist.playlistTranslations[
                          findIndexTranslation(playlist.playlistTranslations)
                        ].title
                      }
                    </Typography>
                  </li>
                ))}
              </ul>
            </>
          )}
          <Typography>
            {t("modal.uninstallPlaylist.confirm_uninstall")}
          </Typography>
        </DialogContent>

        <DialogActions disableSpacing sx={{ padding: 0, marginTop: "1rem" }}>
          <Button
            variant="outlined"
            onClick={() => onClose()}
            data-testid="cancelButton"
          >
            {t("common.button.cancel")}
          </Button>
          <Button
            variant="contained"
            onClick={() => onClose(true)}
            sx={{ marginLeft: "1.875rem" }}
            data-testid="confirmButton"
          >
            {t("common.button.confirm")}
          </Button>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
