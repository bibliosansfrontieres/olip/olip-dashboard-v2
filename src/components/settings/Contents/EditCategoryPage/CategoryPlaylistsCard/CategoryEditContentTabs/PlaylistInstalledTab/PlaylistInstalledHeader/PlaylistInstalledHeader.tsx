import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import CloseIcon from "src/assets/icons/Close";
import FilterApplicationSelect from "src/components/common/FilterApplicationSelect/FilterApplicationSelect";
import SearchFilter from "src/components/common/SearchFilter/SearchFilter";

import AddRoundedIcon from "@mui/icons-material/AddRounded";
import { Box, Button } from "@mui/material";

type Props = {
  disabledActions: boolean;
  handleFilter: (applicationIds: string[]) => void;
  handleSearch: (event: any) => void;
  onClickAddToSelection: () => void;
  onClickUninstall: () => void;
};

export default function PlaylistInstalledHeader({
  disabledActions,
  handleFilter,
  handleSearch,
  onClickAddToSelection,
  onClickUninstall,
}: Props): ReactElement {
  const { t } = useTranslation();

  return (
    <Box display="flex" justifyContent="space-between" mt={5}>
      <Box>
        <FilterApplicationSelect onFilter={handleFilter} />
        <SearchFilter
          onChange={handleSearch}
          sxFormControl={{ marginX: "1rem" }}
        />
      </Box>
      <Box>
        <Button
          variant="contained"
          data-testid="uninstallButton"
          startIcon={<CloseIcon />}
          disabled={disabledActions}
          onClick={onClickUninstall}
          sx={{ marginRight: "1rem" }}
        >
          {t("settings.pages.contents.theme.playlistsCard.button.uninstall")}
        </Button>
        <Button
          variant="contained"
          data-testid="addToSelectionButton"
          startIcon={
            <AddRoundedIcon
              sx={{ "&:nth-of-type(1)": { fontSize: "1.75rem" } }}
            />
          }
          disabled={disabledActions}
          onClick={onClickAddToSelection}
        >
          {t(
            "settings.pages.contents.theme.playlistsCard.button.addToSelection",
          )}
        </Button>
      </Box>
    </Box>
  );
}
