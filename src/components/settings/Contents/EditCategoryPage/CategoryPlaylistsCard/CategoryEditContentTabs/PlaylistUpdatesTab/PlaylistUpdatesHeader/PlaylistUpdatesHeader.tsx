import { Box, Button } from "@mui/material";
import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import RefreshRoundedIcon from "@mui/icons-material/RefreshRounded";
import SearchFilter from "src/components/common/SearchFilter/SearchFilter";
import FilterApplicationSelect from "src/components/common/FilterApplicationSelect/FilterApplicationSelect";

type Props = {
  disabledActions: boolean;
  handleFilter: (applicationIds: string[]) => void;
  handleSearch: (event: any) => void;
  onClickUpdate: () => void;
};

export default function PlaylistUpdatesHeader({
  disabledActions,
  handleFilter,
  handleSearch,
  onClickUpdate,
}: Props): ReactElement {
  const { t } = useTranslation();

  return (
    <Box display="flex" justifyContent="space-between" mt={5}>
      <Box>
        <FilterApplicationSelect onFilter={handleFilter} />
        <SearchFilter
          onChange={handleSearch}
          sxFormControl={{ marginX: "1rem" }}
        />
      </Box>
      <Box>
        <Button
          variant="contained"
          startIcon={<RefreshRoundedIcon />}
          disabled={disabledActions}
          onClick={onClickUpdate}>
          {t("settings.pages.contents.theme.playlistsCard.button.update")}
        </Button>
      </Box>
    </Box>
  );
}
