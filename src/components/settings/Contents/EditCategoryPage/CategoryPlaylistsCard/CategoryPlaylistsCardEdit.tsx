import { ReactElement, useContext } from "react";
import { useTranslation } from "react-i18next";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import CategoryEditContentTabs from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryEditContentTabs/CategoryEditContentTabs";
import { CategoryBackOfficeContext } from "src/contexts/categoryBackOffice.context";
import { CategoryContentsContextProvider } from "src/contexts/categoryContentsEdit.context";

import { Box } from "@mui/material";

export default function CategoryPlaylistsCardEdit({
  resetCard,
}: {
  resetCard: () => void;
}): ReactElement {
  const { t } = useTranslation();
  const { result: category } = useContext(CategoryBackOfficeContext);

  if (!category) {
    return <></>;
  }
  return (
    <>
      <CategoryContentsContextProvider categoryId={category.id}>
        <CategoryEditContentTabs categoryId={category.id} />
        <Box display="flex" justifyContent="flex-end" mt={4}>
          <OLIPDialogActions
            mainButton={{
              action: resetCard,
              text: t("form.categoryContent.button.close"),
            }}
          />
        </Box>
      </CategoryContentsContextProvider>
    </>
  );
}
