import { PropsWithChildren, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import {
  Table,
  TableBody,
  TableCell,
  TableCellProps,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";

type Props = PropsWithChildren & {
  itemsWithToggleDisplay?: boolean;
};

const getColumns = (
  itemsWithToggleDisplay: boolean,
): {
  width?: string;
  align?: TableCellProps["align"];
  header_key?: string;
}[] => {
  return [
    { width: "5%" },
    {
      header_key:
        "settings.pages.contents.theme.playlistsCard.table.itemTable.header.title",

      width: itemsWithToggleDisplay ? "50%" : "65%",
    },
    {
      align: "center",
      header_key:
        "settings.pages.contents.theme.playlistsCard.table.itemTable.header.type",

      width: "15%",
    },
    {
      header_key:
        "settings.pages.contents.theme.playlistsCard.table.itemTable.header.application",
      width: "15%",
    },
    {
      ...(itemsWithToggleDisplay && {
        align: "center",
        header_key:
          "settings.pages.contents.theme.playlistsCard.table.itemTable.header.display",
        width: "15%",
      }),
    },
  ];
};

export default function ItemTable({
  itemsWithToggleDisplay = false,
  children,
}: Props): ReactElement {
  const { t } = useTranslation();
  return (
    <Table
      data-testid="itemTable"
      sx={{
        borderSpacing: "0 0.625rem",
        borderCollapse: "separate",
        overflowWrap: "break-word",
        paddingY: "0.5rem",
        tableLayout: "fixed",
        width: "100%",
        "& th": { border: "0", paddingBottom: 0 },
      }}
    >
      <TableHead>
        <TableRow>
          {getColumns(itemsWithToggleDisplay).map((column, index) => (
            <TableCell key={index} align={column.align} width={column.width}>
              <Typography
                variant="tab"
                data-testid={`tableHeader${column.header_key}`}
              >
                {column.header_key && t(`${column.header_key}`)}
              </Typography>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>{children}</TableBody>
    </Table>
  );
}
