import { ReactElement, useState } from "react";
import { useTranslation } from "react-i18next";
import SettingCardLayout from "src/components/layouts/SettingCardLayout/SettingCardLayout";
import CategoryPlaylistsCardEdit from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistsCardEdit";
import CategoryPlaylistsCardTable from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistsCardTable";

import { Button } from "@mui/material";

export default function CategoryPlaylistsCard(): ReactElement {
  const { t } = useTranslation();
  const [editMode, setEditMode] = useState<boolean>(false);

  const resetCard = (): void => {
    setEditMode(false);
  };

  return (
    <SettingCardLayout
      title={t("settings.pages.contents.theme.playlistsCard.title")}
      editButton={
        <Button
          data-testid="editCategoryPlaylistsButton"
          variant="outlined"
          onClick={() => setEditMode(true)}
          sx={{ display: editMode ? "none" : "block" }}
        >
          {t("common.button.edit")}
        </Button>
      }
    >
      {editMode ? (
        <CategoryPlaylistsCardEdit resetCard={resetCard} />
      ) : (
        <CategoryPlaylistsCardTable />
      )}
    </SettingCardLayout>
  );
}
