/* eslint-disable no-use-before-define */
import { ReactElement, ReactNode, useContext } from "react";
import { useTranslation } from "react-i18next";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import CategoryPlaylistTable from "src/components/settings/Contents/EditCategoryPage/CategoryPlaylistsCard/CategoryPlaylistTable/CategoryPlaylistTable";
import { CategoryBackOfficeCategoryPlaylistsContext } from "src/contexts/categoryPlaylistsBackOffice.context";
import { CategoryPlaylistBackOffice } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";

import { Box, Typography } from "@mui/material";

export default function CategoryPlaylistsCardTable(): ReactElement {
  const categoryBackOfficeCategoryPlaylistsResult = useContext(
    CategoryBackOfficeCategoryPlaylistsContext,
  );

  const isCategoryEmpty = (categoryPlaylists: CategoryPlaylistBackOffice[]) =>
    categoryPlaylists.length === 0;

  return (
    <WaitApiResult
      useApiResult={{
        categoryContents: categoryBackOfficeCategoryPlaylistsResult,
      }}
      resultChildren={({ categoryContents: { categoryPlaylists } }) => (
        <>
          {isCategoryEmpty(categoryPlaylists) ? (
            <EmptyCategoryPlaylistsTable />
          ) : (
            <CategoryPlaylistTable categoryContents={categoryPlaylists} />
          )}
        </>
      )}
    />
  );
}

export function EmptyCategoryPlaylistsTable(): ReactNode {
  const { t } = useTranslation();
  return (
    <Box display="flex" justifyContent="center" alignItems="center" my={30}>
      <Typography>
        {t("form.categoryContent.tab.selected.noPlaylist")}
      </Typography>
    </Box>
  );
}
