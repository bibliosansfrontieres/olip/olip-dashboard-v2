import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import { Box, Button } from "@mui/material";

export default function CategoryBackButton(): ReactElement {
  const { t } = useTranslation();
  return (
    <Box
      display="flex"
      flexDirection={{ xs: "column", sm: "row" }}
      data-testid="categoryBackButton"
      alignItems={{ xs: "flex-start", sm: "center" }}
    >
      <Button
        variant="rounded"
        component={Link}
        to={"/settings/contents"}
        startIcon={<ArrowBackRoundedIcon />}
        sx={{ marginRight: "2rem", marginBottom: { xs: "1.5rem", sm: "0" } }}
      >
        {t("settings.pages.contents.title")}
      </Button>
    </Box>
  );
}
