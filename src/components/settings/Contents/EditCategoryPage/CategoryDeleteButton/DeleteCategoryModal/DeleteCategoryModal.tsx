import { ReactElement, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { CategoryBackOfficeContext } from "src/contexts/categoryBackOffice.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { deleteCategory } from "src/services/category.service";

import {
  Button,
  CircularProgress,
  DialogActions,
  DialogTitle,
  FormHelperText,
} from "@mui/material";

type Props = {
  onClose: () => void;
  open: boolean;
};

export default function DeleteCategoryModal({
  onClose,
  open,
}: Props): ReactElement {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { displaySnackbar } = useContext(SnackbarContext);
  const [loading, setLoading] = useState<boolean>(false);
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);

  const handleClose = () => {
    setLoading(false);
    setDisplayGenericError(false);
    onClose();
  };

  const categoryBackOfficeResult = useContext(CategoryBackOfficeContext);
  const category = categoryBackOfficeResult.result;
  if (!category) return <></>;

  const handleSubmit = async () => {
    setDisplayGenericError(false);
    setLoading(true);
    const result = await deleteCategory(category.id);
    if (result.statusCode === 200) {
      handleClose();
      displaySnackbar(
        t("snackbar.deleteCategory.success", {
          category: category.categoryTranslations[0].title,
        }),
      );
      navigate("/settings/contents");
    } else {
      setDisplayGenericError(true);
      setLoading(false);
    }
  };

  return (
    <ModalLayout onClose={handleClose} open={open}>
      <>
        <DialogTitle sx={{ marginBottom: "1rem" }}>
          {t("modal.deleteCategory.title", {
            category: category.categoryTranslations[0].title,
          })}
        </DialogTitle>
        {displayGenericError && (
          <FormHelperText error sx={{ marginTop: "1rem", textAlign: "right" }}>
            {t("common.error.generic")}
          </FormHelperText>
        )}
        <DialogActions disableSpacing sx={{ padding: 0, marginTop: "1rem" }}>
          {loading && (
            <CircularProgress color="inherit" sx={{ marginX: "1.875rem" }} />
          )}
          <Button variant="outlined" onClick={handleClose}>
            {t("common.button.cancel")}
          </Button>
          <Button
            variant="contained"
            disabled={loading}
            onClick={() => handleSubmit()}
            sx={{ marginLeft: "1.875rem" }}
          >
            {t("common.button.confirm")}
          </Button>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
