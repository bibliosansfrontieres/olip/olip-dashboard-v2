import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import DeleteIcon from "src/assets/icons/Delete";
import DeleteCategoryModal from "src/components/settings/Contents/EditCategoryPage/CategoryDeleteButton/DeleteCategoryModal/DeleteCategoryModal";
import useModal from "src/hooks/useModal";

import { Button } from "@mui/material";

export default function CategoryDeleteButton(): ReactElement {
  const { t } = useTranslation();
  const [open, toggleModal] = useModal();

  return (
    <>
      <Button
        data-testid="deleteCategoryButton"
        endIcon={<DeleteIcon />}
        onClick={() => toggleModal()}
      >
        {t("settings.pages.contents.categories.editCategoryPage.button.delete")}
      </Button>
      <DeleteCategoryModal onClose={() => toggleModal()} open={open} />
    </>
  );
}
