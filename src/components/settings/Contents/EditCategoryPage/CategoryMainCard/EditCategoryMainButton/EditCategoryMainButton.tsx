import { useTranslation } from "react-i18next";

import { Button } from "@mui/material";

type Props = {
  loading?: boolean;
  editMode: boolean;
  setEditMode: (editMode: boolean) => void;
};

export default function EditCategoryMainButton({
  loading = false,
  editMode,
  setEditMode,
}: Props): JSX.Element {
  const { t } = useTranslation();

  return (
    <>
      <Button
        data-testid="editCategoryMainButton"
        variant="outlined"
        disabled={loading}
        onClick={() => setEditMode(true)}
        sx={{ display: editMode ? "none" : "block" }}
      >
        {t("common.button.edit")}
      </Button>
    </>
  );
}
