import { MutableRefObject, useContext, useEffect } from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import CategoryForm from "src/components/settings/Contents/CategoryForm/CategoryForm";
import validationSchema from "src/components/settings/Contents/CategoryForm/CategoryForm.validation";
import { CategoryBackOfficeContext } from "src/contexts/categoryBackOffice.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { updateCategory } from "src/services/category.service";
import { CategoryFormValue } from "src/types/category.type";
import getEnv from "src/utils/getEnv";
import { getVisibilityType } from "src/utils/getVisibilityType";

export default function EditCategoryMainForm({
  onSubmitting,
  onSuccess,
  submitRef,
}: {
  onSubmitting: (value: boolean) => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { displaySnackbar } = useContext(SnackbarContext);
  const {
    result: category,
    translationToDisplay,
    setDisplayLanguage,
  } = useContext(CategoryBackOfficeContext);

  const initialValues: CategoryFormValue =
    category && translationToDisplay
      ? {
          actualLanguage: translationToDisplay.language.identifier,
          languages: category.categoryTranslations.map(
            (translation) => translation.language.identifier,
          ),
          isHomepageDisplayed: category.isHomepageDisplayed,
          pictogram: category.pictogram
            ? getEnv("VITE_AXIOS_BASEURL") + category.pictogram
            : "",
          roleIds: category.visibility.roles.map((role) => role.id),
          userIds: category.visibility.users.map((user) => user.id),
          users: category.visibility.users,
          visibility: getVisibilityType(category.visibility),
          createCategoryTranslationsDto: category.categoryTranslations.map(
            (translation) => ({
              title: translation.title || "",
              description: translation.description || "",
              languageIdentifier: translation.language.identifier,
            }),
          ),
        }
      : ({} as unknown as CategoryFormValue);

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values, { resetForm }) => {
      if (!category || !translationToDisplay) return;
      onSubmitting(true);
      setDisplayLanguage(values.actualLanguage);
      try {
        let visibilityToSend: { roleIds?: number[]; userIds?: number[] } = {};
        switch (values.visibility) {
          case "byRole":
            visibilityToSend = { roleIds: values.roleIds };
            break;
          case "byUser":
            visibilityToSend = { userIds: values.userIds };
            break;
          case "public":
          default:
            switch (getVisibilityType(category.visibility)) {
              case "byRole":
                visibilityToSend = { roleIds: [] };
                break;
              case "byUser":
                visibilityToSend = { userIds: [] };
                break;
              default:
            }
        }

        // Remove actual language if it's empty
        const actualTranslation = values.createCategoryTranslationsDto.find(
          (translation) =>
            translation.languageIdentifier === values.actualLanguage,
        );
        const languageToRemove =
          actualTranslation?.title === "" &&
          actualTranslation.description === "";

        let newValues = values;
        if (languageToRemove) {
          const newLanguagesValues = values.languages.filter(
            (lang) => lang !== values.actualLanguage,
          );
          const newCreateCategoryTranslationsValues =
            values.createCategoryTranslationsDto.filter(
              (translation) =>
                translation.languageIdentifier !== values.actualLanguage,
            );
          newValues = {
            ...values,
            languages: newLanguagesValues,
            createCategoryTranslationsDto: newCreateCategoryTranslationsValues,
          };
        }

        const result = await updateCategory(category.id, {
          pictogram:
            getEnv("VITE_AXIOS_BASEURL") + category.pictogram ===
            newValues.pictogram
              ? undefined
              : newValues.pictogram,
          isHomepageDisplayed: newValues.isHomepageDisplayed,
          createCategoryTranslationsDto:
            newValues.createCategoryTranslationsDto,
          ...visibilityToSend,
        });

        if (result.statusCode === 200) {
          resetForm();
          displaySnackbar(t("snackbar.updateCategory.success"));
          onSuccess();
          navigate("/settings/contents/theme/" + category.id);
        } else {
          let listErrors = [];
          if (!Array.isArray(result.errorMessage)) {
            listErrors.push(result.errorMessage);
          } else {
            listErrors = [...result.errorMessage];
          }
          if (listErrors.length > 0) {
            displaySnackbar(t("snackbar.updateCategory.error"), "error");
          }
        }
      } catch (e) {
        displaySnackbar(t("snackbar.updateCategory.error"), "error");
      } finally {
        onSubmitting(false);
      }
    },
    validationSchema: validationSchema,
  });

  useEffect(() => {
    onSubmitting(formik.isSubmitting);
  }, [formik.isSubmitting, onSubmitting]);

  return <CategoryForm formik={formik} submitRef={submitRef} />;
}
