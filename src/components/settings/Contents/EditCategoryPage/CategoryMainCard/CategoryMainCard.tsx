import { ElementRef, useContext, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import SettingCardLayout from "src/components/layouts/SettingCardLayout/SettingCardLayout";
import CategoryMainContent from "src/components/settings/Contents/EditCategoryPage/CategoryMainCard/CategoryMainContent/CategoryMainContent";
import EditCategoryMainButton from "src/components/settings/Contents/EditCategoryPage/CategoryMainCard/EditCategoryMainButton/EditCategoryMainButton";
import EditCategoryMainForm from "src/components/settings/Contents/EditCategoryPage/CategoryMainCard/EditCategoryMainForm/EditCategoryMainForm";
import { CategoryBackOfficeContext } from "src/contexts/categoryBackOffice.context";

import { Box } from "@mui/material";

export default function CategoryMainCard(): JSX.Element {
  const { t } = useTranslation();
  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const [editMode, setEditMode] = useState<boolean>(false);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const { reload } = useContext(CategoryBackOfficeContext);

  const resetCard = (): void => {
    setEditMode(false);
    reload();
  };

  return (
    <SettingCardLayout
      title={t("settings.pages.contents.theme.mainCard.title")}
      editButton={
        <EditCategoryMainButton editMode={editMode} setEditMode={setEditMode} />
      }
    >
      {editMode ? (
        <>
          <EditCategoryMainForm
            onSubmitting={(value: boolean) => setIsSubmitting(value)}
            onSuccess={resetCard}
            submitRef={submitRef}
          />
        </>
      ) : (
        <CategoryMainContent />
      )}
      {editMode && (
        <Box display="flex" justifyContent="flex-end" mt={4}>
          <OLIPDialogActions
            isLoading={isSubmitting}
            mainButton={{
              action: () => submitRef.current.click(),
              text: t("common.button.save"),
            }}
            secondaryButton={{
              action: () => resetCard(),
              text: t("form.category.button.cancel"),
            }}
          />
        </Box>
      )}
    </SettingCardLayout>
  );
}
