import { ReactElement, useContext } from "react";
import { useTranslation } from "react-i18next";
import DisplayValue from "src/components/common/previews/DisplayValue/DisplayValue";
import LanguagesAvailable from "src/components/common/previews/LanguagesAvailable/LanguageAvailable";
import PreviewImage from "src/components/common/previews/PreviewImage/PreviewImage";
import CategoryToggleDisplay from "src/components/settings/Contents/CategoryToggleDisplay/CategoryToggleDisplay";
import { CategoryBackOfficeContext } from "src/contexts/categoryBackOffice.context";
import { useVisibilityDetails } from "src/hooks/useVisibilityDetails";
import { getVisibilityType } from "src/utils/getVisibilityType";

import { Grid, Stack, Typography } from "@mui/material";

export default function CategoryMainContent(): ReactElement {
  const { t } = useTranslation();
  const {
    result: category,
    translationToDisplay,
    isUpdateLoading,
    setDisplayLanguage,
    updateIsHomepageDisplayed,
  } = useContext(CategoryBackOfficeContext);
  const visibilityDetails = useVisibilityDetails(category?.visibility);

  if (!category || !translationToDisplay) {
    return <></>;
  }

  return (
    <>
      <Grid container spacing={4} alignItems="stretch" pb={3.5}>
        <Grid item xs={3}>
          <CategoryToggleDisplay
            isHomepageDisplayed={category.isHomepageDisplayed}
            loading={isUpdateLoading}
            onToggle={updateIsHomepageDisplayed}
          />
        </Grid>
        <Grid item xs={9}>
          <LanguagesAvailable
            activeDisplayLanguage={translationToDisplay.language.identifier}
            onChangeDisplayLanguage={setDisplayLanguage}
            translations={category.categoryTranslations}
          />
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item xs={3}>
          <PreviewImage imageUrl={category.pictogram} withBaseUrl />
        </Grid>
        <Grid item xs={9}>
          <Stack spacing={4}>
            <DisplayValue
              label={t("form.category.title")}
              value={translationToDisplay.title}
            />
            <DisplayValue
              label={t("form.category.description")}
              value={translationToDisplay.description}
            />
            <DisplayValue
              label={t("form.category.visibility.title")}
              value={t(
                `common.visibility.${getVisibilityType(category.visibility)}`,
              )}
            >
              <Typography variant="body2">{visibilityDetails}</Typography>
            </DisplayValue>
          </Stack>
        </Grid>
      </Grid>
    </>
  );
}
