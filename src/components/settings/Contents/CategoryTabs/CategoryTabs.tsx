import { ReactElement, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import CategoryList from "src/components/settings/Contents/CategoryTabs/CategoryList/CategoryList";
import { CategoryBackOfficeListContext } from "src/contexts/categoryBackOfficeList.context";
import { TabTypeEnum } from "src/enums/tabType.enum";
import {
  getSortedTabTypes,
  TabTypeDefinition,
} from "src/utils/tabTypeDefinitions";

import { Box, Tab, Tabs } from "@mui/material";

function CustomTabPanel({
  type,
  tabSelected,
  tabTypeDefinition,
  ...props
}: {
  type: TabTypeEnum;
  tabSelected: number;
  tabTypeDefinition: TabTypeDefinition;
}) {
  return (
    <div
      role="tabpanel"
      hidden={tabSelected !== tabTypeDefinition.index}
      aria-labelledby={`simple-tab-${tabTypeDefinition.index}`}
      {...props}
    >
      <Box my={4}>
        <CategoryList tab={type} />
      </Box>
    </div>
  );
}

function CustomTab({
  type,
  ...props
}: {
  type: TabTypeEnum;
  value: number;
}): ReactElement {
  const { t } = useTranslation();
  const { getCount } = useContext(CategoryBackOfficeListContext);

  return (
    <Tab
      data-testid={`${type}CategoryTab`}
      label={t(`settings.pages.contents.categoryTabs.${type}`, {
        count: getCount(type),
      })}
      {...props}
    />
  );
}

export default function CategoryTabs(): ReactElement {
  const [tabSelected, setTabSelected] = useState<number>(0);

  return (
    <>
      <Tabs
        variant="fullWidth"
        value={tabSelected}
        onChange={(_event: React.SyntheticEvent, newValue: number) =>
          setTabSelected(newValue)
        }
      >
        {getSortedTabTypes().map(([type, tabTypeDefinition]) => (
          <CustomTab key={type} type={type} value={tabTypeDefinition.index} />
        ))}
      </Tabs>
      {getSortedTabTypes().map(([type, tabTypeDefinition]) => (
        <CustomTabPanel
          key={type}
          type={type}
          tabTypeDefinition={tabTypeDefinition}
          tabSelected={tabSelected}
        />
      ))}
    </>
  );
}
