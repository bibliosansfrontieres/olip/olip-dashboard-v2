import { ReactElement, useContext, useEffect, useState } from "react";
import {
  DragDropContext,
  Draggable,
  DraggableProvided,
} from "react-beautiful-dnd";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import ChevronDownIcon from "src/assets/icons/ChevronDown";
import NotifDotIcon from "src/assets/icons/NotifDot";
import { StrictModeDroppable } from "src/components/common/StrictModeDroppable/StrictModeDroppable";
import { CategoryBackOfficeListContext } from "src/contexts/categoryBackOfficeList.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { TabTypeEnum } from "src/enums/tabType.enum";
import { updateOrderCategories } from "src/services/category.service";
import { CategoryBackOfficeListCategory } from "src/types/category/categoryBackOfficeList.type";
import getEnv from "src/utils/getEnv";
import { getVisibilityType } from "src/utils/getVisibilityType";

import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import {
  Avatar,
  Box,
  Chip,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  Stack,
  Typography,
} from "@mui/material";

export default function CategoryList({
  tab,
}: {
  tab: TabTypeEnum;
}): ReactElement {
  const { displaySnackbar } = useContext(SnackbarContext);
  const { getCategories, reload } = useContext(CategoryBackOfficeListContext);
  const { t } = useTranslation();
  const [localCategories, setLocalCategories] = useState<
    CategoryBackOfficeListCategory[]
  >([]);

  useEffect(() => {
    const categories = getCategories(tab);
    setLocalCategories(categories);
  }, [tab, getCategories]);

  /**
   * Function called when a category is dropped.
   * @param result The result of the drop, given by the `DragDropContext`.
   * @returns A promise, which resolves when the categories have been reordered.
   */
  const onDragEnd = async (result: any) => {
    if (tab !== TabTypeEnum.ALL) {
      return;
    }
    if (
      !result.destination ||
      result.source.index === result.destination.index
    ) {
      return;
    }

    if (localCategories) {
      const originCategories = [...localCategories];
      const nextOrder: CategoryBackOfficeListCategory[] =
        localCategories.filter(
          (category) => category.id !== localCategories[result.source.index].id,
        );
      nextOrder.splice(
        result.destination.index,
        0,
        localCategories[result.source.index],
      );
      setLocalCategories(nextOrder);
      try {
        const updatedOrderCategories = await updateOrderCategories({
          categoryIds: nextOrder.map((categoryPlaylist) => categoryPlaylist.id),
        });
        if (updatedOrderCategories.statusCode === 200) {
          reload();
          displaySnackbar(t("snackbar.orderCategories.success"));
        } else {
          setLocalCategories(originCategories);
          console.error(
            `${updatedOrderCategories.statusCode} - ${updatedOrderCategories.errorMessage}`,
          );
          displaySnackbar(t("snackbar.orderCategories.error"), "error");
        }
      } catch (error) {
        console.error(error);
        displaySnackbar(t("snackbar.orderCategories.error"), "error");
      }
    }
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <StrictModeDroppable droppableId="droppable">
        {(provided: any) => (
          <List ref={provided.innerRef}>
            {localCategories?.map((category, index) => (
              <Draggable
                key={category.id}
                draggableId={String(category.id)}
                index={index}
                isDragDisabled={tab !== TabTypeEnum.ALL}
              >
                {(draggableProvided: DraggableProvided) => (
                  <ListItem
                    data-testid="categoryListItem"
                    key={category.id}
                    ref={draggableProvided.innerRef}
                    {...draggableProvided.draggableProps}
                    component={Link}
                    to={`/settings/contents/theme/${category.id}`}
                    sx={{
                      backgroundColor: (theme) => theme.palette.primary.light,
                      border: (theme) =>
                        `0.0625rem solid ${theme.palette.grey[500]}`,
                      borderRadius: "1rem",
                      color: "inherit",
                      display: "flex",
                      alignItems: "stretch",
                      justifyContent: "space-between",
                      marginBottom: "1rem",
                      padding: 0,
                      textDecoration: "none",
                      "&:hover": {
                        border: (theme) =>
                          `0.125rem solid ${theme.palette.dark.main}`,
                        marginBottom: "calc(1rem - 0.125rem)",
                      },
                    }}
                  >
                    <ListItemIcon
                      data-testid="categoryListItemIcon"
                      {...draggableProvided.dragHandleProps}
                      sx={{
                        alignItems: "center",
                        backgroundColor: (theme) => theme.palette.grey[200],
                        borderBottomLeftRadius: "1rem",
                        borderTopLeftRadius: "1rem",
                        display:
                          tab === TabTypeEnum.ALL ? "flex-inline" : "none",
                        justifyContent: "center",
                        minWidth: "3rem",
                      }}
                    >
                      <DragIndicatorIcon fontSize="large" />
                    </ListItemIcon>
                    <ListItemAvatar
                      data-testid="categoryListItemAvatar"
                      sx={{
                        alignItems: "center",
                        display: "flex",
                        justifyContent: "center",
                        paddingLeft: "1rem",
                      }}
                    >
                      <Avatar
                        alt={category.categoryTranslations[0].title}
                        src={`${getEnv("VITE_AXIOS_BASEURL")}${
                          category.pictogram
                        }`}
                        sx={{
                          width: "3.125rem",
                          height: "3.125rem",
                          marginRight: "0.625rem",
                        }}
                        variant="rounded"
                      />
                    </ListItemAvatar>
                    <Box
                      display="flex"
                      flexDirection="column"
                      flex=" 1 1 auto"
                      justifyContent="center"
                      p={2}
                    >
                      <Box
                        pb={1}
                        display="flex"
                        justifyContent="space-between"
                        flexWrap="wrap"
                      >
                        <Typography
                          data-testid="categoryTitle"
                          variant="h4"
                          sx={{ textOverflow: "wrap", overflow: "hidden" }}
                        >
                          {category.categoryTranslations[0].title}
                        </Typography>
                        {category.isUpdateNeeded && (
                          <Chip
                            data-testid="categoryUpdateChip"
                            icon={
                              <NotifDotIcon
                                color="success"
                                sx={{ fontSize: "1rem" }}
                              />
                            }
                            label={t(
                              "settings.pages.contents.categories.list.update",
                            )}
                            sx={{
                              backgroundColor: (theme) =>
                                theme.palette.grey[100],
                              border: (theme) =>
                                `0.0625rem solid ${theme.palette.grey[200]}`,
                              paddingX: "0.5rem",
                            }}
                          />
                        )}
                      </Box>
                      <Stack
                        direction={{ xs: "column", sm: "row" }}
                        divider={<Divider orientation="vertical" flexItem />}
                        spacing={{ xs: 0, sm: 2 }}
                      >
                        <Box display="flex" data-testid="categoryOrigin">
                          <Typography>
                            {t(
                              "settings.pages.contents.categories.list.origin.label",
                            )}
                          </Typography>
                          <Typography fontWeight={600} ml={1}>
                            {t(
                              `settings.pages.contents.categories.list.origin.${
                                category.users?.length ? "local" : "bsf"
                              }`,
                            )}
                          </Typography>
                        </Box>
                        <Box
                          display="flex"
                          data-testid="categoryNumberPlaylist"
                        >
                          <Typography>
                            {t(
                              "settings.pages.contents.categories.list.nbPlaylists",
                            )}
                          </Typography>
                          <Typography fontWeight={600} ml={1}>
                            {category.categoryPlaylistsCount}
                          </Typography>
                        </Box>
                        <Box display="flex" data-testid="categoryHomeDisplay">
                          <Typography>
                            {t(
                              "settings.pages.contents.categories.list.homepage",
                            )}
                          </Typography>
                          <Typography fontWeight={600} ml={1}>
                            {t(
                              `common.${
                                category.isHomepageDisplayed ? "yes" : "no"
                              }`,
                            )}
                          </Typography>
                        </Box>
                        <Box display="flex" data-testid="categoryVisibility">
                          <Typography>
                            {t("common.visibility.label")}
                          </Typography>
                          <Typography fontWeight={600} ml={1}>
                            {t(
                              `common.visibility.${getVisibilityType(
                                category.visibility,
                              )}`,
                            )}
                          </Typography>
                        </Box>
                      </Stack>
                    </Box>
                    <ListItemIcon
                      data-testid="categoryListItemIcon"
                      sx={{ alignItems: "center", justifyContent: "center" }}
                    >
                      <ChevronDownIcon sx={{ transform: "rotate(-90deg)" }} />
                    </ListItemIcon>
                  </ListItem>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </List>
        )}
      </StrictModeDroppable>
    </DragDropContext>
  );
}
