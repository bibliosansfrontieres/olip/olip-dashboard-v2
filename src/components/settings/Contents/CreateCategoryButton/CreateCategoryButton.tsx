import { useTranslation } from "react-i18next";
import { Button } from "@mui/material";
import AddRoundedIcon from "@mui/icons-material/AddRounded";
import useModal from "src/hooks/useModal";
import CreateCategoryModal from "./CreateCategoryModal/CreateCategoryModal";

export default function CreateCategoryButton(): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  return (
    <>
      <Button
        data-testid="createCategoryButton"
        onClick={() => toggleModal()}
        startIcon={
          <AddRoundedIcon sx={{ "&:nth-of-type(1)": { fontSize: "2rem" } }} />
        }
        variant="contained"
      >
        {t("settings.pages.contents.categories.buttons.addCategory")}
      </Button>
      <CreateCategoryModal
        onClose={() => toggleModal()}
        open={openModal}
        fullWidth
        maxWidth="lg"
      />
    </>
  );
}
