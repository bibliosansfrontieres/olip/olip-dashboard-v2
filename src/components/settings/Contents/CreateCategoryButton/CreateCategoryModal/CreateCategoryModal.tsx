import { ElementRef, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Box,
  Button,
  CircularProgress,
  DialogActions,
  DialogProps,
  Typography,
} from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import CreateCategoryForm from "./CreateCategoryForm/CreateCategoryForm";

export default function CreateCategoryModal(props: DialogProps): JSX.Element {
  const { t } = useTranslation();

  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const { onClose } = props;
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  const handleSuccess = () => {
    onClose?.({}, "backdropClick");
  };

  return (
    <ModalLayout {...props} onClose={(e: any) => handleCancel(e)}>
      <>
        <Typography mb={4} variant="h3" data-testid="createCategoryTitle">
          {t("modal.createCategory.title")}
        </Typography>
        <CreateCategoryForm
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onSuccess={handleSuccess}
          submitRef={submitRef}
        />
        <DialogActions>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
          >
            {isSubmitting && (
              <CircularProgress color="inherit" sx={{ marginX: "1.875rem" }} />
            )}
            <Button
              onClick={handleCancel}
              type="button"
              variant="outlined"
              sx={{ marginRight: "1.875rem" }}
              data-testid="cancelCreateCategoryButton"
            >
              {t("modal.createCategory.button.cancel")}
            </Button>
            <Button
              disabled={isSubmitting}
              onClick={() => submitRef.current.click()}
              variant="contained"
              data-testid="submitCreateCategoryButton"
            >
              {t("common.button.save")}
            </Button>
          </Box>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
