import { MutableRefObject, useContext, useEffect } from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import CategoryForm from "src/components/settings/Contents/CategoryForm/CategoryForm";
import validationSchema from "src/components/settings/Contents/CategoryForm/CategoryForm.validation";
import { LanguageContext } from "src/contexts/language.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { createCategory } from "src/services/category.service";
import { CategoryFormValue } from "src/types/category.type";

import { Box } from "@mui/material";

export default function CreateCategoryForm({
  onSubmitting,
  onSuccess,
  submitRef,
}: {
  onSubmitting: (value: boolean) => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { displaySnackbar } = useContext(SnackbarContext);
  const { language: languageIdentifier } = useContext(LanguageContext);

  const initialValues: CategoryFormValue = {
    actualLanguage: languageIdentifier,
    languages: [languageIdentifier],
    isHomepageDisplayed: true,
    pictogram: "",
    createCategoryTranslationsDto: [
      {
        title: "",
        description: "",
        languageIdentifier,
      },
    ],
    roleIds: [],
    userIds: [],
    users: [],
    visibility: "public",
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values, { resetForm }) => {
      onSubmitting(true);

      // Remove actual language if it's empty
      const actualTranslation = values.createCategoryTranslationsDto.find(
        (translation) =>
          translation.languageIdentifier === values.actualLanguage,
      );
      const languageToRemove =
        actualTranslation?.title === "" && actualTranslation.description === "";
      if (languageToRemove) {
        const newLanguagesValues = values.languages.filter(
          (lang) => lang !== values.actualLanguage,
        );
        const newCreateCategoryTranslationsValues =
          values.createCategoryTranslationsDto.filter(
            (translation) =>
              translation.languageIdentifier !== values.actualLanguage,
          );
        values = {
          ...values,
          languages: newLanguagesValues,
          createCategoryTranslationsDto: newCreateCategoryTranslationsValues,
        };
      }

      try {
        const result = await createCategory({
          pictogram: values.pictogram,
          isHomepageDisplayed: values.isHomepageDisplayed,
          userIds: values.visibility === "byUser" ? values.userIds : [],
          roleIds: values.visibility === "byRole" ? values.roleIds : [],
          createCategoryTranslationsDto: values.createCategoryTranslationsDto,
        });

        if (result.statusCode === 201) {
          resetForm();
          displaySnackbar(t("snackbar.createCategory.success"));
          onSuccess();
          navigate("/settings/contents/theme/" + result.category?.id);
        } else {
          let listErrors = [];
          if (!Array.isArray(result.errorMessage)) {
            listErrors.push(result.errorMessage);
          } else {
            listErrors = [...result.errorMessage];
          }
          if (listErrors.length > 0) {
            displaySnackbar(t("snackbar.createCategory.error"), "error");
          }
        }
      } catch (e) {
        displaySnackbar(t("snackbar.createCategory.error"), "error");
      } finally {
        onSubmitting(false);
      }
    },
    validationSchema: validationSchema,
  });

  useEffect(() => {
    onSubmitting(formik.isSubmitting);
  }, [formik.isSubmitting]);

  return (
    <Box
      sx={{
        marginBottom: "2rem",
        overflowY: "auto",
        paddingRight: "0.75rem",
      }}
    >
      <CategoryForm formik={formik} submitRef={submitRef} />
    </Box>
  );
}
