import { Box, Button, CircularProgress } from "@mui/material";
import { ElementRef, useContext, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import SettingCardLayout from "src/components/layouts/SettingCardLayout/SettingCardLayout";
import { HighlightContext } from "src/contexts/highlight.context";
import HighlightForm from "./HighlightForm/HighlightForm";
import HighlightPreview from "./HighlightPreview/HighlightPreview";

export default function HighlightsCard(): JSX.Element {
  const { t } = useTranslation();
  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const [editMode, setEditMode] = useState<boolean>(false);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const { highlight, loading, resetContext } = useContext(HighlightContext);

  const resetCard = (): void => {
    setEditMode(false);
    resetContext();
  };

  return (
    <SettingCardLayout
      title={t("settings.pages.dashboard.highlightCard.title")}
      description={t("settings.pages.dashboard.highlightCard.description")}
      editButton={
        highlight && (
          <Button
            variant="outlined"
            disabled={loading}
            onClick={() => setEditMode(true)}
            sx={{ display: editMode ? "none" : "block" }}
          >
            {t("common.button.edit")}
          </Button>
        )
      }
    >
      {loading || isSubmitting ? (
        <Box display="flex" justifyContent="center">
          <CircularProgress color="inherit" sx={{ padding: "4rem" }} />
        </Box>
      ) : editMode ? (
        <HighlightForm
          initialHighlight={highlight}
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onSuccess={resetCard}
          submitRef={submitRef}
        />
      ) : (
        <HighlightPreview highlight={highlight} />
      )}
      {editMode && (
        <Box display="flex" justifyContent="flex-end" mt={4}>
          <OLIPDialogActions
            isLoading={isSubmitting}
            mainButton={{
              action: () => submitRef.current.click(),
              text: t("common.button.save"),
            }}
            secondaryButton={{
              action: () => resetCard(),
              text: t("form.edito.button.cancel"),
            }}
          />
        </Box>
      )}
    </SettingCardLayout>
  );
}
