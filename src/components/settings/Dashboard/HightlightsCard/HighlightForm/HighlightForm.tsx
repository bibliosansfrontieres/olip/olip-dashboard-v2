import { MutableRefObject, useContext, useEffect, useState } from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import DisplayInHomePageToggle from "src/components/common/forms/DisplayInHomePageToggle/DisplayInHomePageToggle";
import TranslateLanguageList from "src/components/common/forms/TranslateLanguageList/TranslateLanguageList";
import LanguagesStack from "src/components/common/LanguagesStack/LanguagesStack";
import FormControlLayout from "src/components/layouts/FormControlLayout/FormControlLayout";
import validationSchema from "src/components/settings/Dashboard/HightlightsCard/HighlightForm/HighlightForm.validation";
import HighlightTabs from "src/components/settings/Dashboard/HightlightsCard/HighlightForm/HighlightTabs/HighlightTabs";
import { HighlightContext } from "src/contexts/highlight.context";
import { LanguageContext } from "src/contexts/language.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import useChangeLangFormWhenError from "src/hooks/useChangeLangFormWhenError";
import { createHighlight } from "src/services/highlight.service";
import {
  CreateHighlightTranslation,
  Highlight,
  HighlightFormValues,
} from "src/types/highlight.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";

import { Box, Button, Grid, OutlinedInput, Typography } from "@mui/material";

export default function HighlightForm({
  initialHighlight,
  onSubmitting,
  onSuccess,
  submitRef,
}: {
  initialHighlight: Highlight | null;
  onSubmitting: (value: boolean) => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);
  const { displayLanguage, saveChangesSelection, setDisplayLanguage } =
    useContext(HighlightContext);
  const { language: languageIdentifier } = useContext(LanguageContext);

  const [initialValues, setInitialValues] = useState<HighlightFormValues>({
    actualLanguage: languageIdentifier,
    languages: [languageIdentifier],
    createHighlightTranslations: [
      {
        languageIdentifier: languageIdentifier,
        title: "",
      },
    ],
  });

  const formik = useFormik({
    initialValues: initialValues,
    enableReinitialize: true,
    onSubmit: async (values) => {
      onSubmitting(true);
      setDisplayLanguage(values.actualLanguage);
      try {
        // Remove actual language if it's empty
        const actualTranslation = values.createHighlightTranslations.find(
          (translation) =>
            translation.languageIdentifier === values.actualLanguage,
        );
        const languageToRemove = actualTranslation?.title === "";
        if (languageToRemove) {
          const newLanguagesValues = values.languages.filter(
            (lang) => lang !== values.actualLanguage,
          );
          const newCreateHighlightTranslationsValues =
            values.createHighlightTranslations.filter(
              (translation) =>
                translation.languageIdentifier !== values.actualLanguage,
            );
          values = {
            ...values,
            languages: newLanguagesValues,
            createHighlightTranslations: newCreateHighlightTranslationsValues,
          };
        }

        const result = await createHighlight(values);
        if (result.statusCode === 201) {
          await saveChangesSelection();
          displaySnackbar(t("snackbar.updateHighlight.success"));
          onSuccess();
        } else {
          let listErrors = [];
          if (!Array.isArray(result.errorMessage)) {
            listErrors.push(result.errorMessage);
          } else {
            listErrors = [...result.errorMessage];
          }
          if (listErrors.length > 0) {
            displaySnackbar(t("snackbar.updateHighlight.error"), "error");
          }
        }
      } catch (e) {
        displaySnackbar(t("snackbar.updateHighlight.error"), "error");
      } finally {
        onSubmitting(false);
      }
    },
    validationSchema: validationSchema,
  });

  const actualLanguageIndex = () => {
    return formik.values.languages.findIndex(
      (lang) => lang === formik.values.actualLanguage,
    );
  };

  useEffect(() => {
    if (initialHighlight) {
      const newInitialValues: HighlightFormValues = {
        actualLanguage:
          findIndexTranslation(initialHighlight.highlightTranslations, {
            strict: true,
          }) === -1
            ? initialHighlight.highlightTranslations[0].language.identifier
            : displayLanguage,
        languages: initialHighlight.highlightTranslations.map(
          (translation) => translation.language.identifier,
        ) || [languageIdentifier],
        createHighlightTranslations: initialHighlight.highlightTranslations.map(
          ({ language, title }) => ({
            languageIdentifier: language.identifier,
            title,
          }),
        ) || [
          {
            languageIdentifier,
            title: "",
          },
        ],
      };
      setInitialValues(newInitialValues);
    }
  }, [initialHighlight]);

  const handleChangeLanguage = (choosedLanguage: string) => {
    formik.setFieldValue("actualLanguage", choosedLanguage);

    // remove previousLanguage if empty
    const previousLanguage = formik.values.actualLanguage;
    const previousTranslations = formik.values.createHighlightTranslations.find(
      (translation) => translation.languageIdentifier === previousLanguage,
    );
    const previousLanguageToRemove = previousTranslations?.title === "";
    if (
      formik.values.languages.findIndex((lang) => lang === choosedLanguage) ===
      -1
    ) {
      formik.setFieldValue(
        "languages",
        previousLanguageToRemove
          ? // if previousLanguageEmpty, filter languages to remove it
            [
              ...formik.values.languages.filter(
                (lang) => lang !== previousLanguage,
              ),
              choosedLanguage,
            ]
          : [...formik.values.languages, choosedLanguage],
      );
      formik.setFieldValue(
        "createHighlightTranslations",
        previousLanguageToRemove
          ? // if previousLanguageEmpty, filter highlightTranslations to remove it
            [
              ...formik.values.createHighlightTranslations.filter(
                (translation) =>
                  translation.languageIdentifier !== previousLanguage,
              ),
              {
                languageIdentifier: choosedLanguage,
                title: "",
              },
            ]
          : [
              ...formik.values.createHighlightTranslations,
              {
                languageIdentifier: choosedLanguage,
                title: "",
              },
            ],
      );
    } else if (previousLanguageToRemove) {
      formik.setFieldValue(
        "languages",
        formik.values.languages.filter((lang) => lang !== previousLanguage),
      );
      formik.setFieldValue(
        "createHighlightTranslations",
        formik.values.createHighlightTranslations.filter(
          (translation) => translation.languageIdentifier !== previousLanguage,
        ),
      );
    }
  };

  useChangeLangFormWhenError<HighlightFormValues>({
    formik,
    changeDisplayLanguage: handleChangeLanguage,
    actualLanguageIndex: actualLanguageIndex(),
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={4}>
        <Grid item xs={4}>
          <DisplayInHomePageToggle settingKey="is_highlight_displayed" />
        </Grid>
        <Grid item xs={8}>
          <TranslateLanguageList
            actualLanguage={formik.values.actualLanguage}
            error={formik.errors.actualLanguage}
            onChangeLanguage={handleChangeLanguage}
            touched={formik.touched.actualLanguage}
          />
        </Grid>
        <Grid item xs={4}>
          <FormControlLayout
            label={t("form.highlight.title")}
            error={
              (
                formik.errors.createHighlightTranslations?.[
                  actualLanguageIndex()
                ] as CreateHighlightTranslation
              )?.title
            }
            touched={
              formik.touched.createHighlightTranslations?.[
                actualLanguageIndex()
              ]?.title
            }
          >
            <OutlinedInput
              id={`createHighlightTranslations[${actualLanguageIndex()}].title`}
              name={`createHighlightTranslations[${actualLanguageIndex()}].title`}
              type="text"
              color="dark"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              placeholder={t("form.highlight.title")}
              sx={{
                backgroundColor: (theme) => theme.palette.primary.light,
              }}
              value={
                formik.values.createHighlightTranslations?.[
                  actualLanguageIndex()
                ]?.title || ""
              }
            />
          </FormControlLayout>
        </Grid>
        <Grid item xs={8}>
          {formik.values.languages.filter(
            (language) => language !== formik.values.actualLanguage,
          ).length > 0 && (
            <Box>
              <Typography>{t("form.highlight.listLanguages")}</Typography>
              <LanguagesStack
                languages={formik.values.languages.filter(
                  (language) => language !== formik.values.actualLanguage,
                )}
                onClick={handleChangeLanguage}
              />
            </Box>
          )}
        </Grid>
        <Grid item xs={12}>
          <HighlightTabs />
        </Grid>
      </Grid>
      <Button type="submit" ref={submitRef} sx={{ display: "none" }} />
    </form>
  );
}
