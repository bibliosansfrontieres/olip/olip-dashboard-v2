import { supportedLanguages } from "src/constants";
import * as yup from "yup";

export default yup.object({
  createHighlightTranslations: yup.array().of(
    yup.object({
      languageIdentifier: yup
        .string()
        .oneOf(
          supportedLanguages.map((language) => language.id),
          "form.highlight.validation.language.oneOf",
        )
        .required("form.highlight.validation.language.mandatory"),
      title: yup
        .string()
        .max(100, "form.highlight.validation.title.max")
        .test(
          "required",
          "form.highlight.validation.title.mandatory",
          // verify title required if its the only translation
          (value, context) => {
            if (context.from && context.from[1].value.languages.length === 1) {
              return value !== undefined && value !== "";
            }
            return true;
          },
        ),
    }),
  ),
});
