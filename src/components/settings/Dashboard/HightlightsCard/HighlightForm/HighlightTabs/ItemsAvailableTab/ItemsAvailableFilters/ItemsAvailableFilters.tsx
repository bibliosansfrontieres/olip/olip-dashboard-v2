import { Stack } from "@mui/material";
import FilterApplicationSelect from "src/components/common/FilterApplicationSelect/FilterApplicationSelect";
import FilterCategorySelect from "src/components/common/FilterCategorySelect/FilterCategorySelect";
import SearchFilter from "src/components/common/SearchFilter/SearchFilter";

export default function ItemsAvailableFilters({
  categoryIdsFiltered,
  onFilter,
  onFilterApplications,
  onSearch,
}: {
  categoryIdsFiltered: string[];
  onFilter: (event: any) => void;
  onFilterApplications: (applicationIds: string[]) => void;
  onSearch: (event: any) => void;
}): JSX.Element {
  return (
    <Stack direction="row" spacing={2}>
      <FilterCategorySelect
        actualValues={categoryIdsFiltered}
        onFilter={onFilter}
        sx={{ width: { xs: "100%", md: "18rem" } }}
      />
      <FilterApplicationSelect
        onFilter={onFilterApplications}
        placeholder="applications.filter"
        sx={{ width: { xs: "100%", md: "18rem" } }}
      />
      <SearchFilter onChange={onSearch} />
    </Stack>
  );
}
