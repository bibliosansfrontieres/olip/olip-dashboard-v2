import {
  Avatar,
  Box,
  Switch,
  TableCell,
  TableRow,
  Typography,
} from "@mui/material";
import { useContext } from "react";
import PictureIcon from "src/assets/icons/Picture";
import ApplicationChip from "src/components/common/ApplicationChip/ApplicationChip";
import ContentType from "src/components/common/ContentType/ContentType";
import { HighlightContext } from "src/contexts/highlight.context";
import { Item } from "src/types/item.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import getEnv from "src/utils/getEnv";

export default function ItemAvailableRow({
  item,
}: {
  item: Item;
}): JSX.Element {
  const { addToSelection, highlightContentsSelection, removeToSelection } =
    useContext(HighlightContext);

  const handleChangeToSelection = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const isChecked = event.target.checked;
    if (isChecked) {
      addToSelection(item.id);
    } else {
      removeToSelection(item.id);
    }
  };

  return (
    <TableRow
      sx={{
        backgroundColor: (theme) => theme.palette.grey[100],
        borderRadius: "0.5rem",
        marginBottom: "0.625rem",
        "& td": {
          borderTop: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          borderBottom: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
        },
        "& td:first-of-type": {
          borderLeft: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          borderTopLeftRadius: "0.5rem",
          borderBottomLeftRadius: "0.5rem",
        },
        "& td:last-of-type": {
          borderRight: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
          borderTopRightRadius: "0.5rem",
          borderBottomRightRadius: "0.5rem",
        },
      }}
    >
      <TableCell>
        <Avatar
          variant="rounded"
          alt=""
          src={getEnv("VITE_AXIOS_BASEURL") + item.thumbnail}
          sx={{
            backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          {item?.thumbnail && <PictureIcon />}
        </Avatar>
      </TableCell>
      <TableCell sx={{ overflow: "hidden", maxWidth: 0 }}>
        <Typography
          sx={{
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
        >
          {
            item.dublinCoreItem.dublinCoreItemTranslations[
              findIndexTranslation(
                item.dublinCoreItem.dublinCoreItemTranslations
              )
            ].title
          }
        </Typography>
      </TableCell>
      <TableCell align="center">
        <Box display="flex" alignItems="center" justifyContent="center">
          {item.application && !item.application.isOlip ? (
            <ApplicationChip application={item.application} />
          ) : (
            <ContentType typeId={item.dublinCoreItem.dublinCoreType.id} />
          )}
        </Box>
      </TableCell>
      <TableCell align="center">
        <Switch
          checked={
            highlightContentsSelection.findIndex(
              (highlightContentSelection) =>
                highlightContentSelection.item.id === item.id
            ) > -1
          }
          onChange={handleChangeToSelection}
        />
      </TableCell>
    </TableRow>
  );
}
