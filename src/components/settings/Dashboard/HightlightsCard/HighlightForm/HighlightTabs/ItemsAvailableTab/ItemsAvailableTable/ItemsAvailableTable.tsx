import { useTranslation } from "react-i18next";
import {
  Box,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { Item } from "src/types/item.type";
import ItemAvailableRow from "./ItemAvailableRow/ItemAvailableRow";

export default function ItemsAvailableTable({
  items,
  loading,
}: {
  items: Item[];
  loading: boolean;
}): JSX.Element {
  const { t } = useTranslation();

  if (loading) {
    return (
      <>
        {Array(10)
          .fill("")
          .map((_notUse, index) => (
            <Skeleton
              key={index}
              variant="rounded"
              sx={{
                height: "4.5rem",
                marginY: "0.5rem",
              }}
            />
          ))}
      </>
    );
  }

  if (items.length === 0) {
    <Box display="flex" justifyContent="center" alignItems="center" my={30}>
      <Typography>
        {t("form.categoryContent.tab.available.noContent")}
      </Typography>
    </Box>;
  }
  return (
    <Table
      sx={{
        borderSpacing: "0 0.625rem",
        borderCollapse: "separate",
        overflowWrap: "break-word",
        paddingY: "0.5rem",
        tableLayout: "fixed",
        width: "100%",
        "& th": { border: "0", paddingBottom: 0 },
      }}>
      <TableHead>
        <TableRow>
          <TableCell width="5%"></TableCell>
          <TableCell width="65%">
            <Typography variant="tab">
              {t("form.highlight.tab.available.table.header.title")}
            </Typography>
          </TableCell>
          <TableCell width="15%" align="center">
            <Typography variant="tab">
              {t("form.highlight.tab.available.table.header.type")}
            </Typography>
          </TableCell>
          <TableCell width="15%" align="center">
            <Typography variant="tab">
              {t("form.highlight.tab.available.table.header.addToSelection")}
            </Typography>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {items.map((item) => (
          <ItemAvailableRow key={item.id} item={item} />
        ))}
      </TableBody>
    </Table>
  );
}
