import { useContext, useEffect, useState } from "react";
import { Box, Typography } from "@mui/material";
import ItemsAvailableTable from "./ItemsAvailableTable/ItemsAvailableTable";
import ItemsAvailableFilters from "./ItemsAvailableFilters/ItemsAvailableFilters";
import { HighlightContext } from "src/contexts/highlight.context";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import { useTranslation } from "react-i18next";

type Props = {
  onNewNbAvailable: (newLength: number) => void;
};

export default function ItemsAvailableTab({
  onNewNbAvailable,
}: Props): JSX.Element {
  const { t } = useTranslation();
  const [applicationIdsFiltered, setApplicationIdsFiltered] = useState<
    string[]
  >([]);
  const [categoryIdsFiltered, setCategoryIdsFiltered] = useState<string[]>([]);
  const [page, setPage] = useState<number>(1);
  const [take, setTake] = useState<string>("10");
  const [titleLike, setTitleLike] = useState<string>("");
  const {
    getItemsAvailable,
    itemsAvailable,
    loadingItemsAvailable,
    metaItemsAvailable,
  } = useContext(HighlightContext);

  useEffect(() => {
    getItemsAvailable({
      applicationIds: applicationIdsFiltered.toString() || "",
      categoryId: categoryIdsFiltered.toString() || "",
      page: page,
      take: Number(take),
      titleLike: titleLike,
    });
  }, [applicationIdsFiltered, categoryIdsFiltered, page, take, titleLike]);

  useEffect(() => {
    if (metaItemsAvailable?.totalItemsCount) {
      onNewNbAvailable(metaItemsAvailable.totalItemsCount);
    }
  }, [metaItemsAvailable?.totalItemsCount]);

  const handleFilter = (event: any) =>
    setCategoryIdsFiltered(
      event.target.value.indexOf("") === -1
        ? event.target.value.filter((category: string) => category !== "")
        : []
    );

  const handleSearch = (event: any) => {
    setTitleLike(event.target.value);
    setPage(1);
  };

  const handleChangePagination = (newParam: { page: number; take: number }) => {
    if (newParam.take.toString() !== take) {
      setTake(newParam.take.toString());
    } else {
      setPage(newParam.page);
    }
  };

  return (
    <Box pt={5}>
      <ItemsAvailableFilters
        categoryIdsFiltered={categoryIdsFiltered}
        onFilter={handleFilter}
        onFilterApplications={(applicationIds: string[]) =>
          setApplicationIdsFiltered(applicationIds)
        }
        onSearch={handleSearch}
      />
      {itemsAvailable.length === 0 ? (
        <Box display="flex" justifyContent="center" alignItems="center" my={30}>
          <Typography>{t("form.highlight.tab.available.noContent")}</Typography>
        </Box>
      ) : (
        <PaginationLayout
          meta={metaItemsAvailable}
          onChange={handleChangePagination}
          takeList={["10", "25", "50"]}>
          <ItemsAvailableTable
            items={itemsAvailable}
            loading={loadingItemsAvailable}
          />
        </PaginationLayout>
      )}
    </Box>
  );
}
