import {
  Avatar,
  Box,
  Button,
  Switch,
  SxProps,
  TableCell,
  TableRow,
  Typography,
} from "@mui/material";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import { useContext } from "react";
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from "react-beautiful-dnd";
import { useTranslation } from "react-i18next";
import CloseIcon from "src/assets/icons/Close";
import PictureIcon from "src/assets/icons/Picture";
import { HighlightContext } from "src/contexts/highlight.context";
import { HighlightContent } from "src/types/highlightContent.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import ContentType from "src/components/common/ContentType/ContentType";
import ApplicationChip from "src/components/common/ApplicationChip/ApplicationChip";
import getEnv from "src/utils/getEnv";

export default function ItemsSelectionRow({
  highlightContent,
  index,
}: {
  highlightContent: HighlightContent;
  index: number;
}): JSX.Element {
  const { t } = useTranslation();
  const { changeDisplayHighlightContent, removeToSelection } =
    useContext(HighlightContext);

  const changeDisplay = (event: React.ChangeEvent<HTMLInputElement>) => {
    const isChecked = event.target.checked;
    changeDisplayHighlightContent(highlightContent.item.id, isChecked);
  };

  function customStyleDraggableRow(
    style: any,
    snapshot: DraggableStateSnapshot
  ): SxProps {
    if (!snapshot.isDragging) {
      return style;
    }
    return {
      ...style,
      display: "table",
      "& td:nth-of-type(1)": { display: "table-cell", width: "10%" },
      "& td:nth-of-type(2)": { width: "50%" },
      "& td:nth-of-type(3)": { width: "15%" },
      "& td:nth-of-type(4)": { width: "10%" },
      "& td:nth-of-type(5)": { width: "15%" },
    };
  }

  return (
    <Draggable
      key={highlightContent.item.id}
      draggableId={String(highlightContent.item.id)}
      index={index}
    >
      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
        <TableRow
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          sx={{
            backgroundColor: (theme) => theme.palette.grey[100],
            borderRadius: "0.5rem",
            cursor: "grab",
            marginBottom: "0.625rem",
            "& td": {
              borderTop: (theme) =>
                `0.0625rem solid ${theme.palette.grey[500]}`,
              borderBottom: (theme) =>
                `0.0625rem solid ${theme.palette.grey[500]}`,
            },
            "& td:first-of-type": {
              borderLeft: (theme) =>
                `0.0625rem solid ${theme.palette.grey[500]}`,
              borderTopLeftRadius: "0.5rem",
              borderBottomLeftRadius: "0.5rem",
            },
            "& td:last-of-type": {
              borderRight: (theme) =>
                `0.0625rem solid ${theme.palette.grey[500]}`,
              borderTopRightRadius: "0.5rem",
              borderBottomRightRadius: "0.5rem",
            },
            ...customStyleDraggableRow(provided.draggableProps.style, snapshot),
          }}
        >
          <TableCell
            sx={{
              paddingLeft: 0,
            }}
          >
            <Box
              display="flex"
              justifyContent="space-around"
              alignItems="center"
            >
              <DragIndicatorIcon fontSize="large" />
              <Avatar
                variant="rounded"
                alt=""
                src={
                  getEnv("VITE_AXIOS_BASEURL") + highlightContent.item.thumbnail
                }
                sx={{
                  backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
                  color: (theme) => theme.palette.grey[500],
                }}
              >
                {highlightContent.item?.thumbnail && <PictureIcon />}
              </Avatar>
            </Box>
          </TableCell>
          <TableCell sx={{ overflow: "hidden", maxWidth: 0 }}>
            <Typography
              sx={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
              }}
            >
              {
                highlightContent.item.dublinCoreItem.dublinCoreItemTranslations[
                  findIndexTranslation(
                    highlightContent.item.dublinCoreItem
                      .dublinCoreItemTranslations
                  )
                ].title
              }
            </Typography>
          </TableCell>
          <TableCell align="center">
            {highlightContent.item.application &&
            !highlightContent.item.application.isOlip ? (
              <ApplicationChip
                application={highlightContent.item.application}
              />
            ) : (
              <ContentType
                typeId={highlightContent.item.dublinCoreItem.dublinCoreType.id}
              />
            )}
          </TableCell>
          <TableCell align="center">
            <Switch
              checked={highlightContent.isDisplayed}
              onChange={changeDisplay}
            />
          </TableCell>
          <TableCell align="center">
            <Button
              onClick={() => removeToSelection(highlightContent.item.id)}
              startIcon={<CloseIcon />}
              sx={{
                justifyContent: "flex-start",
                overflow: "hidden",
                paddingY: "0",
                width: "100%",
              }}
            >
              {t("form.highlight.tab.selected.item.removeToSelection")}
            </Button>
          </TableCell>
        </TableRow>
      )}
    </Draggable>
  );
}
