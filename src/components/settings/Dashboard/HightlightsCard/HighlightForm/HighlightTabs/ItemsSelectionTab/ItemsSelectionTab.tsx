import { useContext, useEffect } from "react";
import { HighlightContext } from "src/contexts/highlight.context";
import ItemsSelectionTable from "./ItemsSelectionTable/ItemsSelectionTable";
import { Box, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

type Props = {
  onNewNbSelection: (newLength: number) => void;
};

export default function ItemsSelectionTab({
  onNewNbSelection,
}: Props): JSX.Element {
  const { t } = useTranslation();
  const { highlightContentsSelection, loading } = useContext(HighlightContext);

  useEffect(() => {
    onNewNbSelection(highlightContentsSelection.length);
  }, [highlightContentsSelection.length]);

  if (highlightContentsSelection.length === 0) {
    return (
      <Box display="flex" justifyContent="center" alignItems="center" my={30}>
        <Typography>{t("form.highlight.tab.selected.noContent")}</Typography>
      </Box>
    );
  }
  return (
    <ItemsSelectionTable
      highlightContents={highlightContentsSelection}
      loading={loading}
    />
  );
}
