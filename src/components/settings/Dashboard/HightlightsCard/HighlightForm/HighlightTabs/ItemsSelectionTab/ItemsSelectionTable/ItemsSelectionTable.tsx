import { useTranslation } from "react-i18next";
import { DragDropContext } from "react-beautiful-dnd";
import {
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useContext } from "react";
import ItemsSelectionRow from "./ItemsSelectionRow/ItemsSelectionRow";
import { HighlightContent } from "src/types/highlightContent.type";
import { HighlightContext } from "src/contexts/highlight.context";
import { StrictModeDroppable } from "src/components/common/StrictModeDroppable/StrictModeDroppable";

export default function ItemsSelectionTable({
  highlightContents,
  loading,
}: {
  highlightContents: HighlightContent[];
  loading: boolean;
}): JSX.Element {
  const { t } = useTranslation();
  const { changeOrderHighlightContent } = useContext(HighlightContext);

  function onDragEnd(result: any) {
    if (!result.destination) {
      return;
    }
    changeOrderHighlightContent(
      highlightContents[result.source.index].item.id,
      result.destination.index
    );
  }

  if (loading) {
    return (
      <>
        {Array(10)
          .fill("")
          .map((_notUse, index) => (
            <Skeleton
              key={index}
              variant="rounded"
              sx={{
                height: "4.5rem",
                marginY: "0.5rem",
              }}
            />
          ))}
      </>
    );
  }
  return (
    <DragDropContext onDragEnd={(result: any) => onDragEnd(result)}>
      <StrictModeDroppable droppableId="droppable">
        {(provided: any) => (
          <Table
            ref={provided.innerRef}
            sx={{
              borderSpacing: "0 0.625rem",
              borderCollapse: "separate",
              overflowWrap: "break-word",
              paddingY: "0.5rem",
              tableLayout: "fixed",
              width: "100%",
              "& th": { border: "0", paddingBottom: 0 },
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell width="10%"></TableCell>
                <TableCell width="50%">
                  <Typography variant="tab">
                    {t("form.highlight.tab.selected.table.header.title")}
                  </Typography>
                </TableCell>
                <TableCell width="15%" align="center">
                  <Typography variant="tab">
                    {t("form.highlight.tab.selected.table.header.type")}
                  </Typography>
                </TableCell>
                <TableCell width="10%" align="center">
                  <Typography variant="tab">
                    {t("form.highlight.tab.selected.table.header.isVisible")}
                  </Typography>
                </TableCell>
                <TableCell width="15%" align="center"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {highlightContents.map((highlightContent, index) => (
                <ItemsSelectionRow
                  key={highlightContent.item.id}
                  highlightContent={highlightContent}
                  index={index}
                />
              ))}
              {provided.placeholder}
            </TableBody>
          </Table>
        )}
      </StrictModeDroppable>
    </DragDropContext>
  );
}
