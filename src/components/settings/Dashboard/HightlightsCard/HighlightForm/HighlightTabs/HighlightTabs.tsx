import { Tab, Tabs } from "@mui/material";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import ItemsAvailableTab from "./ItemsAvailableTab/ItemsAvailableTab";
import ItemsSelectionTab from "./ItemsSelectionTab/ItemsSelectionTab";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  isHidden: boolean;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, isHidden, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={isHidden}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}>
      {children}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function HighlightTabs(): JSX.Element {
  const { t } = useTranslation();
  const [tabSelected, setTabSelected] = useState<number>(0);
  const [nbSelected, setNbSelected] = useState<number | undefined>();
  const [nbAvailable, setNbAvailable] = useState<number | undefined>();

  return (
    <>
      <Tabs
        variant="fullWidth"
        value={tabSelected}
        onChange={(_event: React.SyntheticEvent, newValue: number) =>
          setTabSelected(newValue)
        }>
        <Tab
          label={`${t("form.highlight.tab.selected.title")}${
            nbSelected ? ` (${nbSelected})` : ""
          }`}
          {...a11yProps(0)}
        />
        <Tab
          label={`${t("form.highlight.tab.available.title")}${
            nbAvailable ? ` (${nbAvailable})` : ""
          }`}
          {...a11yProps(1)}
        />
      </Tabs>
      <CustomTabPanel index={0} isHidden={tabSelected !== 0}>
        {/* <Box bgcolor="red">
          <Typography>Panel des items sélectionnés {tabSelected}</Typography>
        </Box> */}
        <ItemsSelectionTab
          onNewNbSelection={(newLength: number) => setNbSelected(newLength)}
        />
      </CustomTabPanel>
      <CustomTabPanel index={1} isHidden={tabSelected !== 1}>
        <ItemsAvailableTab
          onNewNbAvailable={(newLength: number) => setNbAvailable(newLength)}
        />
      </CustomTabPanel>
    </>
  );
}
