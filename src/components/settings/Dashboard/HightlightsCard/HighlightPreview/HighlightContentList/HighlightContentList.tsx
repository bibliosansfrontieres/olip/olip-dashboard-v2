import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import PictureIcon from "src/assets/icons/Picture";
import ApplicationChip from "src/components/common/ApplicationChip/ApplicationChip";
import ContentType from "src/components/common/ContentType/ContentType";
import { HighlightContent } from "src/types/highlightContent.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import getEnv from "src/utils/getEnv";

export default function HighlightContentsList({
  highlightContents,
}: {
  highlightContents: HighlightContent[];
}): JSX.Element {
  return (
    <List
      sx={{
        overflowY: "auto",
        paddingRight: "0.75rem",
      }}
    >
      {highlightContents.map((highlightContent) => (
        <ListItem
          key={highlightContent.id}
          sx={{
            border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
            borderRadius: "0.5rem",
            marginBottom: "0.625rem",
          }}
        >
          <ListItemAvatar>
            <Avatar
              variant="rounded"
              alt=""
              src={
                getEnv("VITE_AXIOS_BASEURL") + highlightContent.item.thumbnail
              }
              sx={{
                backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              {highlightContent.item.thumbnail && <PictureIcon />}
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primaryTypographyProps={{
              sx: {
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
              },
            }}
            sx={{
              flex: "1 8 auto",
              marginLeft: "1rem",
              overflow: "hidden",
            }}
          >
            <Typography
              sx={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
              }}
            >
              {
                highlightContent.item.dublinCoreItem.dublinCoreItemTranslations[
                  findIndexTranslation(
                    highlightContent.item.dublinCoreItem
                      .dublinCoreItemTranslations
                  )
                ].title
              }
            </Typography>
          </ListItemText>
          <ListItemText
            sx={{
              display: "flex",
              flex: "none",
              justifyContent: "flex-end",
              marginRight: "2rem",
            }}
          >
            {highlightContent.item.application &&
            !highlightContent.item.application.isOlip ? (
              <ApplicationChip
                application={highlightContent.item.application}
              />
            ) : (
              <ContentType
                typeId={highlightContent.item.dublinCoreItem.dublinCoreType.id}
              />
            )}
          </ListItemText>
        </ListItem>
      ))}
    </List>
  );
}
