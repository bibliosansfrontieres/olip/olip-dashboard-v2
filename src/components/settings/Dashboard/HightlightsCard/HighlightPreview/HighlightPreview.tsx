import { Box, Grid, Stack, Typography } from "@mui/material";
import { useContext } from "react";
import { useTranslation } from "react-i18next";
import DisplayInHomePageToggle from "src/components/common/forms/DisplayInHomePageToggle/DisplayInHomePageToggle";
import DisplayValue from "src/components/common/previews/DisplayValue/DisplayValue";
import LanguagesAvailable from "src/components/common/previews/LanguagesAvailable/LanguageAvailable";
import { HighlightContext } from "src/contexts/highlight.context";
import { Highlight } from "src/types/highlight.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import HighlightContentsList from "./HighlightContentList/HighlightContentList";

export default function HighlightPreview({
  highlight,
}: {
  highlight: Highlight | null;
}): JSX.Element {
  const { t } = useTranslation();
  const { displayLanguage, setDisplayLanguage } = useContext(HighlightContext);

  if (highlight) {
    return (
      <Grid container spacing={4}>
        <Grid item xs={12} sm={4}>
          <DisplayInHomePageToggle settingKey="is_highlight_displayed" />
        </Grid>
        <Grid item xs={12} sm={8}>
          <LanguagesAvailable
            activeDisplayLanguage={displayLanguage}
            onChangeDisplayLanguage={setDisplayLanguage}
            translations={highlight.highlightTranslations}
          />
        </Grid>
        <Grid item xs={12}>
          <Stack spacing={4} data-testid="highlightStackContent">
            <DisplayValue
              label={t("form.highlight.title")}
              value={
                highlight.highlightTranslations[
                  findIndexTranslation(highlight.highlightTranslations, {
                    language: displayLanguage,
                  })
                ].title
              }
            />
            <DisplayValue label={t("form.highlight.selection")} />
            {highlight.highlightContents.filter(
              (highlightContent) => highlightContent.isDisplayed,
            ).length > 0 ? (
              <HighlightContentsList
                highlightContents={highlight.highlightContents.filter(
                  (highlightContent) => highlightContent.isDisplayed,
                )}
              />
            ) : (
              <Box
                display="flex"
                flexDirection="column"
                alignContent="center"
                justifyContent="center"
                p={4}
                pb={6}
              >
                <Typography textAlign="center">
                  {t("settings.pages.dashboard.highlightCard.noContent")}
                </Typography>
              </Box>
            )}
          </Stack>
        </Grid>
      </Grid>
    );
  } else {
    return (
      <Box
        display="flex"
        flexDirection="column"
        alignContent="center"
        justifyContent="center"
        p={12}
        pb={6}
      >
        <Typography textAlign="center">
          {t("settings.pages.dashboard.highlightCard.noContent")}
        </Typography>
      </Box>
    );
  }
}
