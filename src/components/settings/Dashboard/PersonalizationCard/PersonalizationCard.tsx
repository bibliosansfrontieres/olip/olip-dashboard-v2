import { useTranslation } from "react-i18next";
import { Avatar, Box, Grid, Stack, Typography } from "@mui/material";
import SettingCardLayout from "src/components/layouts/SettingCardLayout/SettingCardLayout";
import EditPersonalizationButton from "./EditPersonalizationButton/EditPersonalizationButton";
import { useContext } from "react";
import { LogoContext } from "src/contexts/logo.context";
import getEnv from "src/utils/getEnv";

export default function PersonalizationCard(): JSX.Element {
  const { t } = useTranslation();
  const { logo } = useContext(LogoContext);

  return (
    <SettingCardLayout
      title={t("settings.pages.dashboard.personalizationCard.title")}
      editButton={
        <EditPersonalizationButton variant="outlined">
          {t("common.button.edit")}
        </EditPersonalizationButton>
      }
    >
      <Grid
        container
        spacing={15}
        display="flex"
        sx={{
          justifyContent: { xs: "center", sm: "flex-start" },
          alignItems: "center",
        }}
      >
        <Grid item>
          <Typography variant="info" data-testid="logoLabel">
            {t("settings.pages.dashboard.personalizationCard.logo")}
          </Typography>
          <Avatar
            data-testid="logoAvatar"
            alt={logo ? `Logo` : "Logo par défaut"}
            src={logo ? getEnv("VITE_AXIOS_BASEURL") + logo : ""}
            sx={{
              backgroundColor: (theme) => theme.palette.primary.light,
              borderRadius: "0.5rem",
              height: { xs: "11rem" },
              width: { xs: "11rem" },
            }}
            variant="rounded"
          />
        </Grid>
        <Grid item>
          <Typography variant="info" data-testid="colorLabel">
            {t("settings.pages.dashboard.personalizationCard.color")}
          </Typography>
          <Stack
            data-testid="colorPalette"
            spacing={2}
            p={2}
            sx={{
              borderRadius: "0.5rem",
              backgroundColor: (theme) => theme.palette.grey["100"],
              height: { xs: "9rem" },
              alignItems: "center",
            }}
            direction="row"
          >
            {["", "66", "4D", "33", "1A"].map((color) => (
              <Box
                key={color}
                sx={{
                  height: { xs: "2rem", sm: "3rem", md: "4rem", xl: "5rem" },
                  width: { xs: "2rem", sm: "3rem", md: "4rem", xl: "5rem" },
                  borderRadius: 1,
                  backgroundColor: (theme) =>
                    `${theme.palette.primary.main}${color}`,
                }}
              />
            ))}
          </Stack>
        </Grid>
      </Grid>
    </SettingCardLayout>
  );
}
