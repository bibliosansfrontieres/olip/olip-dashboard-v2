import { MutableRefObject, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Avatar,
  Box,
  Button,
  FormHelperText,
  Grid,
  Typography,
  useTheme,
  RadioGroup,
  Stack,
  FormControl,
  Radio,
} from "@mui/material";
import { useFormik } from "formik";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { updateSetting } from "src/services/setting.service";
import { APPLICATION_COLOR } from "src/constants";
import { ThemeContext } from "src/contexts/theme.context";
import { LogoContext } from "src/contexts/logo.context";
import getEnv from "src/utils/getEnv";

export default function EditPersonalizationForm({
  onSubmitting,
  onCancel,
  onSuccess,
  submitRef,
}: {
  onSubmitting: (value: boolean) => void;
  onCancel: () => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);
  const theme = useTheme();
  const { logo, refreshLogo } = useContext(LogoContext);

  const { refreshColor } = useContext(ThemeContext);
  const formik = useFormik({
    initialValues: {
      photo: "",
      colorForm: theme.palette.primary.main,
    },
    onSubmit: async (values, { resetForm }) => {
      if (
        (values.photo === logo || values.photo === "") &&
        values.colorForm === theme.palette.primary.main
      ) {
        onCancel();
        return;
      }
      setDisplayGenericError(false);
      const resultLogo = await updateSetting("logo", {
        value: values.photo,
      });
      if (resultLogo.statusCode === 200) {
        if (values.colorForm !== theme.palette.primary.main) {
          const resultColor = await updateSetting("principal_color", {
            value: values.colorForm,
          });
          if (resultColor.statusCode === 200) {
            refreshLogo();
            refreshColor();
            resetForm();
            displaySnackbar(t("snackbar.updatePersonalization.success"));
            onSuccess();
          } else {
            setDisplayGenericError(true);
          }
        } else {
          refreshLogo();
          resetForm();
          displaySnackbar(t("snackbar.updatePersonalization.success"));
          onSuccess();
        }
      } else {
        setDisplayGenericError(true);
      }
    },
  });

  const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    formik.setFieldValue("colorForm", (event.target as HTMLInputElement).value);
  };

  const [errorImage, setErrorImage] = useState<string>("");
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);

  useEffect(() => {
    onSubmitting(formik.isSubmitting);
  }, [formik.isSubmitting]);

  const handleChangePhoto = ({ target: { files } }: any) => {
    setErrorImage("");
    formik.setFieldValue("photo", "");
    if (files[0]) {
      const file = files[0];
      if (!file.type.toString().startsWith("image/")) {
        setErrorImage("common.error.image.format");
        return;
      }
      if (file.size > 1048576) {
        setErrorImage("common.error.image.size");
        return;
      }
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => formik.setFieldValue("photo", reader.result);
      reader.onerror = () => setErrorImage("common.error.image.generic");
    }
  };

  return (
    <form
      onSubmit={formik.handleSubmit}
      style={{ marginBottom: "1rem", overflowY: "auto" }}
    >
      <Grid
        container
        display="flex"
        spacing={5}
        sx={{
          justifyContent: { xs: "center", lg: "flex-start" },
          marginBottom: { xs: 0, md: "6rem" },
        }}
      >
        <Grid item>
          <Avatar
            alt={logo || formik.values.photo ? `Logo` : "Logo par défaut"}
            src={formik.values.photo || getEnv("VITE_AXIOS_BASEURL") + logo}
            sx={{
              backgroundColor: (theme) => theme.palette.primary.light,
              borderRadius: "0.5rem",
              height: { xs: "70vw", sm: "15.625rem" },
              width: { xs: "70vw", sm: "15.625rem" },
            }}
            variant="rounded"
          />

          <Box display="flex" flexDirection="column">
            <Button
              variant="contained"
              onClick={() => document.getElementById("inputFile")?.click()}
              sx={{ marginY: "0.5rem", width: { xs: "70vw", sm: "auto" } }}
            >
              {t("common.image.button")}
            </Button>
            <input
              data-testid="inputFile"
              id="inputFile"
              type="file"
              accept="image/*"
              onChange={handleChangePhoto}
              style={{ display: "none" }}
            />
            <Typography variant="info">{t("common.image.type")}</Typography>
            <Typography variant="info">
              {t("common.image.size", { mo: 1 })}
            </Typography>
            {errorImage && (
              <FormHelperText error>{t(errorImage)}</FormHelperText>
            )}
          </Box>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl>
            <RadioGroup
              value={formik.values.colorForm ?? theme.palette.primary.main}
              onChange={handleRadioChange}
            >
              <Grid spacing={2} container>
                {APPLICATION_COLOR.map(({ color }) => (
                  <Grid item key={color} xs={12} sm={6} lg={4}>
                    <Stack
                      spacing={2}
                      p={2}
                      alignItems="center"
                      sx={{
                        borderRadius: "1rem",
                        backgroundColor: (theme) => theme.palette.grey["100"],
                      }}
                      direction="row"
                    >
                      <Radio
                        data-testid={`colorForm-${color}`}
                        sx={{
                          color: color,
                          "&.Mui-checked": {
                            color: color,
                          },
                        }}
                        value={color}
                      />
                      {["", "66", "4D", "33", "1A"].map((colorInfo) => (
                        <Box
                          key={`${color}${colorInfo}`}
                          sx={{
                            height: "1rem",
                            width: "1rem",
                            borderRadius: 1,
                            backgroundColor: `${color}${colorInfo}`,
                          }}
                        />
                      ))}
                    </Stack>
                  </Grid>
                ))}
              </Grid>
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>
      <Button type="submit" ref={submitRef} sx={{ display: "none" }} />
      {displayGenericError && (
        <FormHelperText error>{t("common.error.generic")}</FormHelperText>
      )}
    </form>
  );
}
