import { useTranslation } from "react-i18next";
import { DialogProps, DialogTitle } from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import { ElementRef, useRef, useState } from "react";
import EditPersonalizationForm from "./EditPersonalizationForm/EditPersonalizationForm";

export default function EditPersonalizationModal(
  props: DialogProps,
): JSX.Element {
  const { t } = useTranslation();
  const { onClose } = props;
  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  return (
    <ModalLayout disableCloseButton {...props}>
      <>
        <DialogTitle
          variant="h4"
          sx={{ marginBottom: "2.5rem", paddingLeft: 0 }}
        >
          {t("modal.updatePersonalization.title")}
        </DialogTitle>
        <EditPersonalizationForm
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onCancel={() => onClose?.({}, "backdropClick")}
          onSuccess={() => onClose?.({}, "backdropClick")}
          submitRef={submitRef}
        />
        <OLIPDialogActions
          id="editPersonalizationModal"
          isLoading={isSubmitting}
          mainButton={{
            text: t("common.button.save"),
            action: () => submitRef.current.click(),
          }}
          secondaryButton={{
            text: t("modal.updatePersonalization.button.cancel"),
            action: () => handleCancel({}),
          }}
        />
      </>
    </ModalLayout>
  );
}
