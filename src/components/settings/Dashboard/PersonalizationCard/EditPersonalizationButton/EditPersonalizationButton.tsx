import { Button, ButtonProps } from "@mui/material";
import { useTranslation } from "react-i18next";
import useModal from "src/hooks/useModal";
import EditPersonalizationModal from "./EditPersonalizationModal/EditPersonalizationModal";

export default function EditPersonalizationButton(
  props: ButtonProps,
): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  return (
    <>
      <Button
        onClick={() => toggleModal()}
        {...props}
        data-testid="editPersonalizationButton"
      >
        {t("common.button.edit")}
      </Button>
      <EditPersonalizationModal
        open={openModal}
        onClose={() => toggleModal()}
        fullWidth
        maxWidth="lg"
      />
    </>
  );
}
