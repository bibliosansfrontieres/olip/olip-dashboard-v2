import { Grid, Stack } from "@mui/material";
import { useContext } from "react";
import { useTranslation } from "react-i18next";
import DisplayValue from "src/components/common/previews/DisplayValue/DisplayValue";
import PreviewImage from "src/components/common/previews/PreviewImage/PreviewImage";
import { EditoContext } from "src/contexts/edito.context";
import { Edito, EditoTranslation } from "src/types/edito.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";

type Props = { edito: Edito };

export default function EditoContent({ edito }: Props): JSX.Element {
  const { t } = useTranslation();
  const { displayLanguage } = useContext(EditoContext);

  const translationToDisplay: EditoTranslation =
    edito.editoTranslations[
      findIndexTranslation(edito.editoTranslations, {
        language: displayLanguage,
      })
    ];

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} sm={4} data-testid="editoImage">
        <PreviewImage imageUrl={edito.image} withBaseUrl />
      </Grid>
      <Grid item xs={12} sm={8}>
        <Stack spacing={4} data-testid="editoStackContent">
          <DisplayValue
            label={t("form.edito.title")}
            value={translationToDisplay.title}
          />
          <DisplayValue
            label={t("form.edito.description")}
            value={translationToDisplay.description}
          />
          {edito.editoParagraphs
            .filter(
              (paragraph) =>
                findIndexTranslation(paragraph.editoParagraphTranslations, {
                  language: displayLanguage,
                  strict: true,
                }) !== -1,
            )
            .map((paragraph, index) => (
              <DisplayValue
                key={paragraph.id}
                label={t("form.edito.paragraph", { number: index + 1 })}
                value={
                  paragraph.editoParagraphTranslations[
                    findIndexTranslation(paragraph.editoParagraphTranslations, {
                      language: displayLanguage,
                    })
                  ].text
                }
              />
            ))}
        </Stack>
      </Grid>
    </Grid>
  );
}
