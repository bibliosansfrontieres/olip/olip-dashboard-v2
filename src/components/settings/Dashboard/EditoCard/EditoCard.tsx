import { Box, Button, CircularProgress } from "@mui/material";
import { ElementRef, useContext, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import EditoForm from "./EditoForm/EditoForm";
import EditoPreview from "./EditoPreview/EditoPreview";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";
import EditoVersionsButton from "./EditoVersionsButton/EditoVersionsButton";
import { EditoContext } from "src/contexts/edito.context";
import SettingCardLayout from "src/components/layouts/SettingCardLayout/SettingCardLayout";

export default function EditoCard(): JSX.Element {
  const { t } = useTranslation();
  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const [editMode, setEditMode] = useState<boolean>(false);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const { edito, loading, refreshEdito } = useContext(EditoContext);

  const resetCard = (): void => {
    setEditMode(false);
    refreshEdito();
  };

  return (
    <SettingCardLayout
      title={t("settings.pages.dashboard.editoCard.title")}
      description={t("settings.pages.dashboard.editoCard.description")}
      editButton={
        edito && (
          <Button
            variant="outlined"
            disabled={loading}
            onClick={() => setEditMode(true)}
            sx={{ display: editMode ? "none" : "block" }}
          >
            {t("common.button.edit")}
          </Button>
        )
      }
    >
      {loading ? (
        <Box display="flex" justifyContent="center">
          <CircularProgress color="inherit" sx={{ padding: "4rem" }} />
        </Box>
      ) : editMode ? (
        <EditoForm
          initialEdito={edito}
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onSuccess={resetCard}
          submitRef={submitRef}
        />
      ) : (
        <EditoPreview edito={edito} onCreate={() => setEditMode(true)} />
      )}
      {editMode && (
        <Box display="flex" justifyContent="space-between" mt={4}>
          <EditoVersionsButton />
          <OLIPDialogActions
            isLoading={isSubmitting}
            mainButton={{
              action: () => submitRef.current.click(),
              text: t("common.button.save"),
            }}
            secondaryButton={{
              action: () => resetCard(),
              text: t("form.edito.button.cancel"),
            }}
          />
        </Box>
      )}
    </SettingCardLayout>
  );
}
