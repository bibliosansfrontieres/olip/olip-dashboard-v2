import { useTranslation } from "react-i18next";
import { Box, Button, Grid, Typography } from "@mui/material";
import DisplayInHomePageToggle from "src/components/common/forms/DisplayInHomePageToggle/DisplayInHomePageToggle";
import { Edito } from "src/types/edito.type";
import EditoContent from "../EditoContent/EditoContent";
import { useContext } from "react";
import { EditoContext } from "src/contexts/edito.context";
import LanguagesAvailable from "src/components/common/previews/LanguagesAvailable/LanguageAvailable";

export default function EditoPreview({
  edito,
  onCreate,
}: {
  edito: Edito | null;
  onCreate: () => void;
}): JSX.Element {
  const { t } = useTranslation();
  const { displayLanguage, setDisplayLanguage } = useContext(EditoContext);

  if (edito) {
    return (
      <>
        <Grid container spacing={4} alignItems="stretch" pb={3.5}>
          <Grid item xs={12} sm={4}>
            <DisplayInHomePageToggle settingKey="is_edito_displayed" />
          </Grid>
          <Grid item xs={12} sm={8}>
            <LanguagesAvailable
              activeDisplayLanguage={displayLanguage}
              onChangeDisplayLanguage={setDisplayLanguage}
              translations={edito.editoTranslations}
            />
          </Grid>
        </Grid>
        <EditoContent edito={edito} />
      </>
    );
  } else {
    return (
      <Box
        display="flex"
        flexDirection="column"
        alignContent="center"
        justifyContent="center"
        p={12}
        pb={6}>
        <Typography textAlign="center">
          {t("settings.pages.dashboard.editoCard.noEdito")}
        </Typography>
        <Box textAlign="center" my={6}>
          <Button variant="contained" onClick={onCreate}>
            {t("settings.pages.dashboard.editoCard.button.create")}
          </Button>
        </Box>
      </Box>
    );
  }
}
