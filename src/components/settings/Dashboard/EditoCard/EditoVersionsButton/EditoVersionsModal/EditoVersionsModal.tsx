import { DialogProps, DialogTitle } from "@mui/material";
import { useTranslation } from "react-i18next";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import EditoVersionsList from "./EditoVersionsList/EditoVersionsList";

export default function EditoVersionsModal(props: DialogProps): JSX.Element {
  const { t } = useTranslation();

  return (
    <ModalLayout {...props}>
      <>
        <DialogTitle
          variant="h4"
          sx={{ marginBottom: "2.5rem", paddingLeft: 0 }}>
          {t("modal.editoVersions.title")}
        </DialogTitle>
        <EditoVersionsList
          onSelectVersion={() => props.onClose?.({}, "backdropClick")}
        />
      </>
    </ModalLayout>
  );
}
