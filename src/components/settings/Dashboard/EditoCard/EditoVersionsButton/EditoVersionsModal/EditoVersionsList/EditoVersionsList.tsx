import { useTranslation } from "react-i18next";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Avatar,
  Button,
  Chip,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Skeleton,
  Stack,
  Typography,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { Edito } from "src/types/edito.type";
import { getEditos } from "src/services/edito.service";
import PictureIcon from "src/assets/icons/Picture";
import ArrowForwardRounded from "@mui/icons-material/ArrowForwardRounded";
import { supportedLanguages } from "src/constants";
import { EditoContext } from "src/contexts/edito.context";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import ChevronDownIcon from "src/assets/icons/ChevronDown";
import EditoContent from "../../../EditoContent/EditoContent";
import getEnv from "src/utils/getEnv";

export default function EditoVersionsList({
  onSelectVersion,
}: {
  onSelectVersion: () => void;
}): JSX.Element {
  const { t } = useTranslation();
  const [editos, setEditos] = useState<Edito[]>([]);
  const [loading, setloading] = useState<boolean>(true);
  const { edito: actualEdito, setEdito } = useContext(EditoContext);

  useEffect(() => {
    getEditos()
      .then(({ editos }) => {
        if (editos) setEditos(editos);
      })
      .finally(() => setloading(false));
  }, []);

  const selectEdito = (selectEdito: Edito) => {
    setEdito(selectEdito);
    onSelectVersion();
  };

  if (loading) {
    return (
      <>
        {Array(10)
          .fill("")
          .map((_notUse, index) => (
            <Skeleton
              key={index}
              variant="rounded"
              sx={{
                height: "4.5rem",
                marginY: "0.5rem",
              }}
            />
          ))}
      </>
    );
  }
  return (
    <List
      sx={{
        overflowY: "auto",
        paddingRight: "0.75rem",
      }}
    >
      {editos.map((edito) => (
        <ListItem
          key={edito.id}
          sx={{
            border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
            borderRadius: "0.5rem",
            marginBottom: "0.625rem",
          }}
        >
          <Accordion disableGutters elevation={0} sx={{ width: "100%" }}>
            <AccordionSummary
              expandIcon={<ChevronDownIcon />}
              sx={{
                padding: "0",
                flexDirection: "row-reverse",
              }}
            >
              <ListItemAvatar sx={{ marginLeft: "1rem", alignSelf: "center" }}>
                <Avatar
                  variant="rounded"
                  alt={
                    edito?.editoTranslations[
                      findIndexTranslation(edito.editoTranslations)
                    ]?.title
                  }
                  src={getEnv("VITE_AXIOS_BASEURL") + edito?.image}
                  sx={{
                    backgroundColor: (theme) =>
                      `${theme.palette.primary.main}4D`,
                    color: (theme) => theme.palette.grey[500],
                  }}
                >
                  {edito?.image && <PictureIcon />}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primaryTypographyProps={{
                  sx: {
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  },
                }}
                sx={{
                  width: "20%",
                  flex: "none",
                  overflow: "hidden",
                  alignSelf: "center",
                }}
              >
                {`#${edito?.id}`}
              </ListItemText>
              <ListItemText
                primaryTypographyProps={{
                  sx: {
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  },
                }}
                sx={{
                  alignSelf: "center",
                  flex: "1 8 auto",
                  overflow: "hidden",
                }}
              >
                <Typography
                  sx={{
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  }}
                >
                  {
                    edito?.editoTranslations[
                      findIndexTranslation(edito.editoTranslations)
                    ]?.title
                  }
                </Typography>
                <Stack direction="row" spacing={1}>
                  {edito.editoTranslations.map((translation) => (
                    <Typography>
                      {
                        supportedLanguages.find(
                          (supportedLanguage) =>
                            supportedLanguage.id ===
                            translation.language.identifier
                        )?.name
                      }
                    </Typography>
                  ))}
                </Stack>
              </ListItemText>
              {actualEdito?.id !== edito.id ? (
                <ListItemText
                  sx={{
                    alignSelf: "center",
                    display: "flex",
                    flex: "none",
                    justifyContent: "flex-end",
                  }}
                >
                  <Button
                    endIcon={<ArrowForwardRounded />}
                    onClick={() => selectEdito(edito)}
                    sx={{ paddingY: "0.5rem" }}
                  >
                    {t("modal.editoVersions.list.button.useVersion")}
                  </Button>
                </ListItemText>
              ) : (
                <Chip
                  variant="tag"
                  disabled
                  label={t("modal.editoVersions.list.actualVersion")}
                  sx={{ alignSelf: "center", marginLeft: "6.25rem" }}
                />
              )}
            </AccordionSummary>
            <AccordionDetails>
              <EditoContent edito={edito} />
            </AccordionDetails>
          </Accordion>
        </ListItem>
      ))}
    </List>
  );
}
