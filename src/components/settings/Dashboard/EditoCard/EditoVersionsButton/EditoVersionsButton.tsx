import { Button, ButtonProps } from "@mui/material";
import { useTranslation } from "react-i18next";
import useModal from "src/hooks/useModal";
import EditoVersionsModal from "./EditoVersionsModal/EditoVersionsModal";

export default function EditoVersionsButton(props: ButtonProps): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  return (
    <>
      <Button onClick={() => toggleModal()} {...props}>
        {t("form.edito.button.seeVersions")}
      </Button>
      <EditoVersionsModal
        open={openModal}
        onClose={() => toggleModal()}
        fullWidth
        maxWidth="lg"
      />
    </>
  );
}
