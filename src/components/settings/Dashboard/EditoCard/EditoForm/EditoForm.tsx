import { MutableRefObject, useContext, useEffect, useState } from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import CloseIcon from "src/assets/icons/Close";
import DisplayInHomePageToggle from "src/components/common/forms/DisplayInHomePageToggle/DisplayInHomePageToggle";
import EditImage from "src/components/common/forms/EditImage/EditImage";
import TranslateLanguageList from "src/components/common/forms/TranslateLanguageList/TranslateLanguageList";
import LanguagesStack from "src/components/common/LanguagesStack/LanguagesStack";
import FormControlLayout from "src/components/layouts/FormControlLayout/FormControlLayout";
import { EditoContext } from "src/contexts/edito.context";
import { LanguageContext } from "src/contexts/language.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import useChangeLangFormWhenError from "src/hooks/useChangeLangFormWhenError";
import { createEdito } from "src/services/edito.service";
import {
  CreateEditoParagraphsDto,
  CreateEditoParagraphTranslationDto,
  CreateEditoTranslationDto,
  Edito,
  EditoFormValues,
} from "src/types/edito.type";
import { findIndexTranslation } from "src/utils/findIndexTranslation";
import getEnv from "src/utils/getEnv";

import AddRoundedIcon from "@mui/icons-material/AddRounded";
import {
  Box,
  Button,
  Grid,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";

import validationSchema from "./EditoForm.validation";

export default function EditoForm({
  initialEdito,
  onSubmitting,
  onSuccess,
  submitRef,
}: {
  initialEdito: Edito | null;
  onSubmitting: (value: boolean) => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);
  const { displayLanguage, setDisplayLanguage } = useContext(EditoContext);
  const { language: languageIdentifier } = useContext(LanguageContext);

  const [initialValues, setInitialValues] = useState<EditoFormValues>({
    actualLanguage: languageIdentifier,
    languages: [languageIdentifier],
    image: "",
    createEditoTranslationsDto: [
      {
        description: "",
        languageIdentifier: languageIdentifier,
        title: "",
      },
    ],
    createEditoParagraphsDto: [],
  });

  const formik = useFormik({
    initialValues: initialValues,
    enableReinitialize: true,
    onSubmit: async (values) => {
      onSubmitting(true);
      setDisplayLanguage(values.actualLanguage);
      try {
        if (values.image.includes("/images/")) {
          values.image = initialEdito?.image || "";
        }
        // Remove actual language if it's empty
        const actualTranslation = values.createEditoTranslationsDto.find(
          (translation) =>
            translation.languageIdentifier === values.actualLanguage,
        );
        let hasParagraphs = false;
        for (const paragraph of values.createEditoParagraphsDto) {
          for (const translation of paragraph.createEditoParagraphTranslationsDto) {
            if (translation.languageIdentifier === values.actualLanguage) {
              if (translation.text) {
                hasParagraphs = true;
                break;
              }
            }
          }
        }
        const languageToRemove =
          actualTranslation?.description === "" &&
          actualTranslation?.title === "" &&
          !hasParagraphs;

        if (languageToRemove) {
          const newLanguagesValues = values.languages.filter(
            (lang) => lang !== values.actualLanguage,
          );
          const newCreateEditoParagraphsDtosValues =
            values.createEditoParagraphsDto.map(
              (createEditoParagraphTranslationsDto) => {
                const newArray =
                  createEditoParagraphTranslationsDto.createEditoParagraphTranslationsDto.filter(
                    (translation) =>
                      translation.languageIdentifier !== values.actualLanguage,
                  );
                return { createEditoParagraphTranslationsDto: newArray };
              },
            );
          const newCreateEditoTranslationsDtosValues =
            values.createEditoTranslationsDto.filter(
              (translation) =>
                translation.languageIdentifier !== values.actualLanguage,
            );
          values = {
            ...values,
            languages: newLanguagesValues,
            createEditoParagraphsDto: newCreateEditoParagraphsDtosValues,
            createEditoTranslationsDto: newCreateEditoTranslationsDtosValues,
          };
        }

        const result = await createEdito(values);
        if (result.statusCode === 201) {
          displaySnackbar(t("snackbar.createEdito.success"));
          onSuccess();
        } else {
          let listErrors = [];
          if (!Array.isArray(result.errorMessage)) {
            listErrors.push(result.errorMessage);
          } else {
            listErrors = [...result.errorMessage];
          }
          if (listErrors.length > 0) {
            displaySnackbar(t("snackbar.createEdito.error"), "error");
          }
        }
      } catch (e) {
        displaySnackbar(t("snackbar.createEdito.error"), "error");
      } finally {
        onSubmitting(false);
      }
    },
    validationSchema: validationSchema,
  });

  const actualLanguageIndex = () => {
    return formik.values.languages.findIndex(
      (lang) => lang === formik.values.actualLanguage,
    );
  };

  useEffect(() => {
    if (initialEdito) {
      const newInitialValues: EditoFormValues = {
        actualLanguage:
          findIndexTranslation(initialEdito.editoTranslations, {
            strict: true,
          }) === -1
            ? initialEdito.editoTranslations[0].language.identifier
            : displayLanguage,
        languages: initialEdito.editoTranslations.map(
          (translation) => translation.language.identifier,
        ) || [displayLanguage],
        image: initialEdito
          ? getEnv("VITE_AXIOS_BASEURL") + initialEdito.image
          : "",
        createEditoTranslationsDto: initialEdito.editoTranslations.map(
          ({ description, language, title }) => ({
            description,
            languageIdentifier: language.identifier,
            title,
          }),
        ) || [
          {
            description: "",
            languageIdentifier: displayLanguage,
            title: "",
          },
        ],
        createEditoParagraphsDto:
          initialEdito?.editoParagraphs.map((paragraph) => ({
            createEditoParagraphTranslationsDto:
              paragraph.editoParagraphTranslations.map((translation) => ({
                languageIdentifier: translation.language.identifier,
                text: translation.text,
              })),
          })) || [],
      };
      setInitialValues(newInitialValues);
    }
  }, [initialEdito]);

  const addParagraph = () => {
    const defaultValue: CreateEditoParagraphTranslationDto = {
      languageIdentifier: formik.values.actualLanguage,
      text: "",
    };
    let newParagraph = true;

    for (
      let index = 0;
      index < formik.values.createEditoParagraphsDto.length;
      index++
    ) {
      const paragraph = { ...formik.values.createEditoParagraphsDto[index] };

      if (
        paragraph.createEditoParagraphTranslationsDto.find(
          (translation) =>
            translation.languageIdentifier === formik.values.actualLanguage,
        ) === undefined
      ) {
        paragraph.createEditoParagraphTranslationsDto.push(defaultValue);
        formik.setFieldValue(`createEditoParagraphsDto.${index}`, paragraph);
        newParagraph = false;
        break;
      }
    }
    if (newParagraph) {
      formik.setFieldValue("createEditoParagraphsDto", [
        ...formik.values.createEditoParagraphsDto,
        { createEditoParagraphTranslationsDto: [defaultValue] },
      ]);
    }
  };

  const removeParagraph = () => {
    const actualLanguage: string = formik.values.actualLanguage;

    for (
      let index = formik.values.createEditoParagraphsDto.length - 1;
      index > -1;
      index--
    ) {
      const paragraph = { ...formik.values.createEditoParagraphsDto[index] };

      if (
        paragraph.createEditoParagraphTranslationsDto.find(
          (translation) => translation.languageIdentifier === actualLanguage,
        ) !== undefined
      ) {
        if (paragraph.createEditoParagraphTranslationsDto.length === 1) {
          const paragraphs = [...formik.values.createEditoParagraphsDto];
          paragraphs.splice(index, 1);
          formik.setFieldValue(`createEditoParagraphsDto`, paragraphs);
          break;
        } else {
          formik.setFieldValue(
            `createEditoParagraphsDto.${index}.createEditoParagraphTranslationsDto`,
            paragraph.createEditoParagraphTranslationsDto.filter(
              (translation) =>
                translation.languageIdentifier !== actualLanguage,
            ),
          );
          break;
        }
      }
    }
  };

  const handleChangeLanguage = (choosedLanguage: string) => {
    formik.setFieldValue("actualLanguage", choosedLanguage);

    // remove previousLanguage if empty
    const previousLanguage = formik.values.actualLanguage;
    const previousTranslations = formik.values.createEditoTranslationsDto.find(
      (translation) => translation.languageIdentifier === previousLanguage,
    );
    let hasPreviousParagraphs = false;
    for (const previousParagraph of formik.values.createEditoParagraphsDto) {
      for (const translation of previousParagraph.createEditoParagraphTranslationsDto) {
        if (translation.languageIdentifier === previousLanguage) {
          if (translation.text) {
            hasPreviousParagraphs = true;
            break;
          }
        }
      }
    }
    const previousLanguageToRemove =
      previousTranslations?.description === "" &&
      previousTranslations.title === "" &&
      !hasPreviousParagraphs;
    if (
      formik.values.languages.findIndex((lang) => lang === choosedLanguage) ===
      -1
    ) {
      formik.setFieldValue(
        "languages",
        previousLanguageToRemove
          ? // if previousLanguageEmpty, filter languages to remove it
            [
              ...formik.values.languages.filter(
                (lang) => lang !== previousLanguage,
              ),
              choosedLanguage,
            ]
          : [...formik.values.languages, choosedLanguage],
      );
      formik.setFieldValue(
        "createEditoTranslationsDto",
        previousLanguageToRemove
          ? // if previousLanguageEmpty, filter editoTranslations to remove it
            [
              ...formik.values.createEditoTranslationsDto.filter(
                (translation) =>
                  translation.languageIdentifier !== previousLanguage,
              ),
              {
                description: "",
                languageIdentifier: choosedLanguage,
                title: "",
              },
            ]
          : [
              ...formik.values.createEditoTranslationsDto,
              {
                description: "",
                languageIdentifier: choosedLanguage,
                title: "",
              },
            ],
      );
    } else if (previousLanguageToRemove) {
      // if nothing in description and title when switching language, remove previous languages
      formik.setFieldValue(
        "languages",
        formik.values.languages.filter((lang) => lang !== previousLanguage),
      );
      formik.setFieldValue(
        "createEditoTranslationsDto",
        formik.values.createEditoTranslationsDto.filter(
          (translation) => translation.languageIdentifier !== previousLanguage,
        ),
      );
    }
  };

  useChangeLangFormWhenError<EditoFormValues>({
    formik,
    actualLanguageIndex: actualLanguageIndex(),
    changeDisplayLanguage: handleChangeLanguage,
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={4}>
        <Grid item xs={4}>
          <DisplayInHomePageToggle settingKey="is_edito_displayed" />
        </Grid>
        <Grid item xs={8}>
          <TranslateLanguageList
            actualLanguage={formik.values.actualLanguage}
            error={formik.errors.actualLanguage}
            onChangeLanguage={handleChangeLanguage}
            touched={formik.touched.actualLanguage}
          />
        </Grid>
        <Grid item xs={4}>
          <EditImage
            imageUrl={formik.values.image}
            onChangeImage={(newImage) =>
              formik.setFieldValue("image", newImage)
            }
            error={formik.errors.image}
            touched={formik.touched.image}
          />
        </Grid>
        <Grid item xs={8}>
          <Stack spacing={4}>
            <FormControlLayout
              label={t("form.edito.title")}
              error={
                (
                  formik.errors.createEditoTranslationsDto?.[
                    actualLanguageIndex()
                  ] as CreateEditoTranslationDto
                )?.title || ""
              }
              touched={
                formik.touched.createEditoTranslationsDto?.[
                  actualLanguageIndex()
                ]?.title
              }
            >
              <OutlinedInput
                id={`createEditoTranslationsDto[${actualLanguageIndex()}].title`}
                name={`createEditoTranslationsDto[${actualLanguageIndex()}].title`}
                type="text"
                color="dark"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder={t("form.edito.title")}
                sx={{
                  backgroundColor: (theme) => theme.palette.primary.light,
                }}
                value={
                  formik.values.createEditoTranslationsDto?.[
                    actualLanguageIndex()
                  ]?.title
                }
              />
            </FormControlLayout>
            <FormControlLayout
              label={t("form.edito.description")}
              error={
                (
                  formik.errors.createEditoTranslationsDto?.[
                    actualLanguageIndex()
                  ] as CreateEditoTranslationDto
                )?.description
              }
              touched={
                formik.touched.createEditoTranslationsDto?.[
                  actualLanguageIndex()
                ]?.description
              }
            >
              <TextField
                id={`createEditoTranslationsDto[${actualLanguageIndex()}].description`}
                name={`createEditoTranslationsDto[${actualLanguageIndex()}].description`}
                color="dark"
                multiline
                rows={4}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={
                  formik.values.createEditoTranslationsDto?.[
                    actualLanguageIndex()
                  ]?.description
                }
              />
            </FormControlLayout>
            {formik.values.createEditoParagraphsDto
              .filter(
                (paragraph) =>
                  paragraph.createEditoParagraphTranslationsDto.findIndex(
                    (translation) =>
                      translation.languageIdentifier ===
                      formik.values.actualLanguage,
                  ) !== -1,
              )
              .map((_paragraph, index) => {
                const indexTranslation = formik.values.createEditoParagraphsDto[
                  index
                ].createEditoParagraphTranslationsDto.findIndex(
                  (translation) =>
                    translation.languageIdentifier ===
                    formik.values.actualLanguage,
                );
                return (
                  <FormControlLayout
                    key={index}
                    label={t("form.edito.paragraph", { number: index + 1 })}
                    error={
                      (
                        formik.errors.createEditoParagraphsDto?.[
                          index
                        ] as CreateEditoParagraphsDto
                      )?.createEditoParagraphTranslationsDto?.[
                        actualLanguageIndex()
                      ]?.text
                    }
                    touched={
                      formik.touched.createEditoParagraphsDto?.[index]
                        ?.createEditoParagraphTranslationsDto?.[
                        indexTranslation
                      ]?.text
                    }
                  >
                    <TextField
                      id={`createEditoParagraphsDto[${index}].createEditoParagraphTranslationsDto[${indexTranslation}].text`}
                      name={`createEditoParagraphsDto[${index}].createEditoParagraphTranslationsDto[${indexTranslation}].text`}
                      color="dark"
                      multiline
                      rows={4}
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={
                        formik.values.createEditoParagraphsDto?.[index]
                          ?.createEditoParagraphTranslationsDto?.[
                          indexTranslation
                        ]?.text
                      }
                    />
                  </FormControlLayout>
                );
              })}
            <Box display="flex" justifyContent="space-between">
              <Box>
                {formik.values.createEditoParagraphsDto.length < 9 && (
                  <Button startIcon={<AddRoundedIcon />} onClick={addParagraph}>
                    {t("form.edito.button.newParagraph")}
                  </Button>
                )}
              </Box>
              {formik.values.createEditoParagraphsDto.length > 0 && (
                <Button endIcon={<CloseIcon />} onClick={removeParagraph}>
                  {t("form.edito.button.deleteParagraph")}
                </Button>
              )}
            </Box>
            {formik.values.languages.filter(
              (language) => language !== formik.values.actualLanguage,
            ).length > 0 && (
              <Box>
                <Typography>{t("form.edito.listLanguages")}</Typography>
                <LanguagesStack
                  languages={formik.values.languages.filter(
                    (language) => language !== formik.values.actualLanguage,
                  )}
                  onClick={handleChangeLanguage}
                />
              </Box>
            )}
          </Stack>
        </Grid>
      </Grid>
      <Button type="submit" ref={submitRef} sx={{ display: "none" }} />
    </form>
  );
}
