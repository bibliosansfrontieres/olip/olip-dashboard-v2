import { supportedLanguages } from "src/constants";
import * as yup from "yup";

export default yup.object({
  image: yup.string().required("form.edito.validation.image.mandatory"),
  createEditoTranslationsDto: yup.array().of(
    yup.object({
      description: yup
        .string()
        .max(500, "form.edito.validation.description.max")
        // verify description required if its the only translation or paragraph exist or title exist
        .test(
          "required",
          "form.edito.validation.description.mandatory",
          (value, context) => {
            if (context.from && context.from[1].value.languages.length === 1) {
              return value !== undefined && value !== "";
            }
            if (context.parent.title) {
              return value !== undefined && value !== "";
            } else {
              if (context.from) {
                for (const paragraph of context.from[1].value
                  .createEditoParagraphsDto) {
                  for (const translation of paragraph.createEditoParagraphTranslationsDto) {
                    if (translation.text) {
                      return value !== undefined && value !== "";
                    }
                  }
                }
              }
              return true;
            }
          },
        ),
      languageIdentifier: yup
        .string()
        .oneOf(
          supportedLanguages.map((language) => language.id),
          "form.edito.validation.language.oneOf",
        )
        .required("form.edito.validation.language.mandatory"),
      title: yup
        .string()
        .max(100, "form.edito.validation.title.max")
        // verify title required if its the only translation or paragraph exist or description exist
        .test(
          "required",
          "form.edito.validation.title.mandatory",
          (value, context) => {
            if (context.from && context.from[1].value.languages.length === 1) {
              return value !== undefined && value !== "";
            }
            if (context.parent.description) {
              return value !== undefined && value !== "";
            } else {
              if (context.from) {
                for (const paragraph of context.from[1].value
                  .createEditoParagraphsDto) {
                  for (const translation of paragraph.createEditoParagraphTranslationsDto) {
                    if (translation.text) {
                      return value !== undefined && value !== "";
                    }
                  }
                }
              }
              return true;
            }
          },
        ),
    }),
  ),
  createEditoParagraphsDto: yup.array().of(
    yup.object({
      createEditoParagraphTranslationsDto: yup.array().of(
        yup.object({
          text: yup.string().max(500, "form.edito.validation.paragraph.max"),
          languageIdentifier: yup
            .string()
            .oneOf(
              supportedLanguages.map((language) => language.id),
              "form.edito.validation.language.oneOf",
            )
            .required("form.edito.validation.language.mandatory"),
        }),
      ),
    }),
  ),
});
