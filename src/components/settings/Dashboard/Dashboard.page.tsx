import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Stack, Typography } from "@mui/material";
import AccountInfoCard from "./AccountInfoCard/AccountInfoCard";
import EditoCard from "./EditoCard/EditoCard";
import PersonalizationCard from "./PersonalizationCard/PersonalizationCard";
import HighlightsCard from "./HightlightsCard/HighlightsCard";
import { EditoContextProvider } from "src/contexts/edito.context";
import { UserContext } from "src/contexts/user.context";
import { HighlightContextProvider } from "src/contexts/highlight.context";

export default function DashboardPage(): JSX.Element {
  const { t } = useTranslation();
  const { havePermission } = useContext(UserContext);

  return (
    <Stack spacing={4}>
      <Typography variant="h2" mb={2} data-testid="DashboardTitle">
        {t("settings.pages.dashboard.title")}
      </Typography>
      <AccountInfoCard />
      {(havePermission("update_personalization") ||
        havePermission("create_edito") ||
        havePermission("update_highlight")) && (
        <Typography
          variant="h3"
          mb={2}
          textAlign="center"
          data-testid="PersonalizeTitle"
        >
          {t("settings.pages.dashboard.personalize")}
        </Typography>
      )}
      {havePermission("update_personalization") && <PersonalizationCard />}
      {havePermission("create_edito") && (
        <EditoContextProvider>
          <EditoCard />
        </EditoContextProvider>
      )}
      {havePermission("update_highlight") && (
        <HighlightContextProvider>
          <HighlightsCard />
        </HighlightContextProvider>
      )}
    </Stack>
  );
}
