import { useContext } from "react";
import { useTranslation } from "react-i18next";
import defaultProfilePhoto from "src/assets/images/avatar.png";
import DisplayValue from "src/components/common/previews/DisplayValue/DisplayValue";
import SettingCardLayout from "src/components/layouts/SettingCardLayout/SettingCardLayout";
import EditProfileButton from "src/components/settings/Dashboard/AccountInfoCard/EditProfileButton/EditProfileButton";
import { supportedLanguages } from "src/constants";
import { UserContext } from "src/contexts/user.context";
import getEnv from "src/utils/getEnv";

import { Avatar, Grid, Stack } from "@mui/material";

export default function AccountInfoCard(): JSX.Element {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);

  return (
    <SettingCardLayout
      title={t("settings.pages.dashboard.accountInfoCard.title")}
      editButton={
        <EditProfileButton variant="outlined" data-testid="editProfileButton">
          {t("common.button.edit")}
        </EditProfileButton>
      }
    >
      <Grid
        container
        spacing={5}
        display="flex"
        sx={{ justifyContent: { xs: "center", sm: "flex-start" } }}
      >
        <Grid item>
          <Avatar
            data-testid="accountInfoCardAvatar"
            alt={
              user?.photo ? `Avatar de ${user.username}` : "Avatar par défaut"
            }
            src={
              user?.photo
                ? getEnv("VITE_AXIOS_BASEURL") + user.photo
                : defaultProfilePhoto
            }
            sx={{
              backgroundColor: (theme) => theme.palette.primary.light,
              borderRadius: "0.5rem",
              height: { xs: "40vw", sm: "15.625rem" },
              width: { xs: "40vw", sm: "15.625rem" },
            }}
            variant="rounded"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack spacing={4} data-testid="accountInfoCardStack">
            <DisplayValue
              label={t(
                "settings.pages.dashboard.accountInfoCard.labels.username",
              )}
              value={user?.username}
            />
            {user?.role.id !== 4 && (
              <DisplayValue
                label={t(
                  "settings.pages.dashboard.accountInfoCard.labels.password",
                )}
                value={t(
                  "settings.pages.dashboard.accountInfoCard.fakePassword",
                )}
              />
            )}
            <DisplayValue
              label={t("settings.pages.dashboard.accountInfoCard.labels.role")}
              value={t(`settings.pages.users.roles.${user?.role.name}`)}
            />
            <DisplayValue
              label={t(
                "settings.pages.dashboard.accountInfoCard.labels.language",
              )}
              value={
                supportedLanguages.find((lang) => lang.id === user?.language)
                  ?.name
              }
            />
          </Stack>
        </Grid>
      </Grid>
    </SettingCardLayout>
  );
}
