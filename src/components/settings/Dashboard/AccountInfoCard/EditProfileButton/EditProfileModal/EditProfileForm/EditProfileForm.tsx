import { MutableRefObject, useContext, useEffect, useState } from "react";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import RadioCheckedIcon from "src/assets/icons/RadioChecked";
import TranslateLanguageList from "src/components/common/forms/TranslateLanguageList/TranslateLanguageList";
import FormControlLayout from "src/components/layouts/FormControlLayout/FormControlLayout";
import { LanguageContext } from "src/contexts/language.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { UserContext } from "src/contexts/user.context";
import { updateProfile } from "src/services/user.service";
import { MyProfileFormValues } from "src/types/user.type";
import getEnv from "src/utils/getEnv";

import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {
  Avatar,
  Box,
  Button,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  OutlinedInput,
  Stack,
  SvgIcon,
  Typography,
} from "@mui/material";

import validationSchema from "./EditProfileForm.validation";

import defaultProfilePhoto from "/src/assets/images/avatar.png";

export default function EditProfileForm({
  onSubmitting,
  onCancel,
  onSuccess,
  submitRef,
}: {
  onSubmitting: (value: boolean) => void;
  onCancel: () => void;
  onSuccess: () => void;
  submitRef: MutableRefObject<HTMLButtonElement>;
}): JSX.Element {
  const { t } = useTranslation();
  const { refreshMyProfile, user } = useContext(UserContext);
  const { displaySnackbar } = useContext(SnackbarContext);
  const { changeLanguage } = useContext(LanguageContext);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [showPasswordRepeat, setShowPasswordRepeat] = useState<boolean>(false);
  const [errorImage, setErrorImage] = useState<string>("");
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);

  const initialValues: MyProfileFormValues = {
    username: user?.username || "",
    password: "",
    passwordRepeat: "",
    photo: "",
    language: user?.language || getEnv("VITE_DEFAULT_LANGUAGE") || "eng",
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values, { resetForm, setFieldError }) => {
      if (
        values.username === user?.username &&
        values.password === "" &&
        values.photo === "" &&
        values.language === user.language
      ) {
        onCancel();
        return;
      }
      setDisplayGenericError(false);

      const result = await updateProfile({
        username:
          values.username === "" || values.username === user?.username
            ? undefined
            : values.username,
        password:
          user?.role.id !== 4 && values.password !== ""
            ? values.password
            : undefined,
        language: values.language,
        photo: values.photo || undefined,
      });
      if (result.statusCode === 200) {
        resetForm();
        displaySnackbar(t("snackbar.updateProfile.success"));
        if (values.language !== user?.language) changeLanguage(values.language);
        refreshMyProfile();
        onSuccess();
      } else {
        let listErrors = [];
        if (!Array.isArray(result.errorMessage)) {
          listErrors.push(result.errorMessage);
        } else {
          listErrors = [...result.errorMessage];
        }
        listErrors.forEach((errorMessage) => {
          switch (errorMessage) {
            case "password must be longer than or equal to 6 characters":
              setFieldError(
                "password",
                t(`modal.updateProfile.error.${result.errorMessage}`),
              );
              break;
            case "password is needed for this role":
              setFieldError(
                "password",
                t(`modal.updateProfile.error.${result.errorMessage}`),
              );
              break;
            case "username already exists":
              setFieldError(
                "username",
                t(`modal.updateProfile.error.${result.errorMessage}`, {
                  user: values.username,
                }),
              );
              break;
            case "admin username cannot be updated":
              setFieldError(
                "username",
                t(`modal.updateProfile.error.${result.errorMessage}`),
              );
              break;
            default:
              setDisplayGenericError(true);
          }
        });
      }
    },
    validationSchema: validationSchema,
  });

  useEffect(() => {
    onSubmitting(formik.isSubmitting);
  }, [formik.isSubmitting]);

  const handleChangePhoto = ({ target: { files } }: any) => {
    setErrorImage("");
    formik.setFieldValue("photo", "");
    if (files[0]) {
      const file = files[0];
      if (!file.type.toString().startsWith("image/")) {
        setErrorImage("common.error.image.format");
        return;
      }
      if (file.size > 1048576) {
        setErrorImage("common.error.image.size");
        return;
      }
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => formik.setFieldValue("photo", reader.result);
      reader.onerror = () => setErrorImage("common.error.image.generic");
    }
  };

  return (
    <form
      onSubmit={formik.handleSubmit}
      style={{ marginBottom: "1rem", overflowY: "auto" }}
    >
      <Grid
        container
        display="flex"
        justifyContent="center"
        spacing={5}
        sx={{ marginBottom: { xs: 0, md: "6rem" } }}
      >
        <Grid item>
          <Avatar
            alt={
              user?.photo || formik.values.photo
                ? `Avatar de ${user?.username}`
                : "Avatar par défaut"
            }
            src={
              formik.values.photo
                ? formik.values.photo
                : user?.photo
                  ? getEnv("VITE_AXIOS_BASEURL") + user.photo
                  : defaultProfilePhoto
            }
            sx={{
              backgroundColor: (theme) => theme.palette.primary.light,
              borderRadius: "0.5rem",
              height: { xs: "70vw", sm: "15.625rem" },
              width: { xs: "70vw", sm: "15.625rem" },
            }}
            variant="rounded"
          />
          <Box display="flex" flexDirection="column">
            <Button
              variant="contained"
              onClick={() => document.getElementById("inputFile")?.click()}
              sx={{ marginY: "0.5rem", width: { xs: "70vw", sm: "auto" } }}
            >
              {t("common.image.button")}
            </Button>
            <input
              id="inputFile"
              type="file"
              accept="image/*"
              onChange={handleChangePhoto}
              style={{ display: "none" }}
            />
            <Typography variant="info">{t("common.image.type")}</Typography>
            <Typography variant="info">
              {t("common.image.size", { mo: 1 })}
            </Typography>
            {errorImage && (
              <FormHelperText error>{t(errorImage)}</FormHelperText>
            )}
          </Box>
        </Grid>
        <Grid item xs>
          <Stack spacing={4} sx={{ width: "95%" }}>
            <FormControlLayout
              label={t("common.form.username")}
              error={formik.errors.username}
              touched={formik.touched.username}
            >
              <OutlinedInput
                disabled={user?.username === "admin"}
                id="username"
                name="username"
                type="text"
                color="dark"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder={t("common.form.username")}
                sx={{
                  backgroundColor: (theme) => theme.palette.primary.light,
                }}
                value={formik.values.username}
              />
            </FormControlLayout>
            {user?.role.id !== 4 && (
              <>
                <FormControlLayout
                  label={t("common.form.password")}
                  error={formik.errors.password}
                  touched={formik.touched.password}
                >
                  <OutlinedInput
                    id="password"
                    name="password"
                    color="dark"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    placeholder={t("common.form.password")}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => setShowPassword(!showPassword)}
                        >
                          <SvgIcon
                            component={
                              showPassword ? VisibilityOffIcon : VisibilityIcon
                            }
                          />
                        </IconButton>
                      </InputAdornment>
                    }
                    sx={{
                      backgroundColor: (theme) => theme.palette.primary.light,
                    }}
                    type={showPassword ? "text" : "password"}
                    value={formik.values.password}
                  />
                </FormControlLayout>
                <FormControlLayout
                  label={t("modal.updateProfile.passwordRepeat")}
                  error={formik.errors.passwordRepeat}
                  touched={formik.touched.passwordRepeat}
                >
                  <OutlinedInput
                    id="passwordRepeat"
                    name="passwordRepeat"
                    color="dark"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    placeholder={t("modal.updateProfile.passwordRepeat")}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() =>
                            setShowPasswordRepeat(!showPasswordRepeat)
                          }
                        >
                          <SvgIcon
                            component={
                              showPasswordRepeat
                                ? VisibilityOffIcon
                                : VisibilityIcon
                            }
                          />
                        </IconButton>
                      </InputAdornment>
                    }
                    sx={{
                      backgroundColor: (theme) => theme.palette.primary.light,
                    }}
                    type={showPasswordRepeat ? "text" : "password"}
                    value={formik.values.passwordRepeat}
                  />
                </FormControlLayout>
              </>
            )}
            <FormControlLayout label={t("common.form.role")}>
              <Box display="flex" alignItems="center" py={1.5}>
                <RadioCheckedIcon sx={{ paddingX: "1rem" }} />
                <Typography variant="body1sb">
                  {t(`settings.pages.users.roles.${user?.role.name}`)}
                </Typography>
              </Box>
            </FormControlLayout>
            <TranslateLanguageList
              actualLanguage={formik.values.language}
              error={formik.errors.language}
              label={t("common.form.language")}
              onChangeLanguage={(choosedLanguage: string) =>
                formik.setFieldValue("language", choosedLanguage)
              }
              placeholder={t("common.form.language")}
              touched={formik.touched.language}
            />
          </Stack>
        </Grid>
      </Grid>
      <Button type="submit" ref={submitRef} sx={{ display: "none" }} />
      {displayGenericError && (
        <FormHelperText error>{t("common.error.generic")}</FormHelperText>
      )}
    </form>
  );
}
