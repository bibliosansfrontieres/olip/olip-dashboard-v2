import { supportedLanguages } from "src/constants";
import * as yup from "yup";

export default yup.object({
  username: yup
    .string()
    .notRequired()
    .test(
      "username",
      "modal.updateProfile.validation.username.min",
      (value) => {
        if (!value || value === "") return true;
        return value === "admin" ? true : value.length >= 6;
      },
    ),
  password: yup
    .string()
    .notRequired()
    .min(6, "modal.updateProfile.validation.password.min")
    .nullable()
    .transform((value: string) => (value !== "" ? value : null)),
  passwordRepeat: yup.string().when("password", {
    is: (value?: string | null) => {
      return value && value.length > 0;
    },
    then: (schema) =>
      schema
        .required("modal.updateProfile.validation.passwordRepeat.mandatory")
        .oneOf(
          [yup.ref("password")],
          "modal.updateProfile.validation.passwordRepeat.oneOf",
        ),
  }),
  language: yup
    .string()
    .oneOf(
      supportedLanguages.map((language) => language.id),
      "modal.updateProfile.validation.language.oneOf",
    )
    .required("modal.updateProfile.validation.language.mandatory"),
});
