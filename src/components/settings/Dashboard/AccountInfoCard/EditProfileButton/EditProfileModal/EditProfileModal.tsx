import { ElementRef, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { DialogProps, DialogTitle } from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import EditProfileForm from "./EditProfileForm/EditProfileForm";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";

export default function EditProfileModal(props: DialogProps): JSX.Element {
  const { t } = useTranslation();
  const submitRef = useRef<ElementRef<"button">>(
    document.createElement("button"),
  );
  const { onClose } = props;
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const handleCancel = (event: any) => {
    onClose?.(event, "backdropClick");
  };

  return (
    <ModalLayout disableCloseButton {...props}>
      <>
        <DialogTitle
          variant="h4"
          sx={{ marginBottom: "2.5rem", paddingLeft: 0 }}
        >
          {t("modal.updateProfile.title")}
        </DialogTitle>
        <EditProfileForm
          onSubmitting={(value: boolean) => setIsSubmitting(value)}
          onCancel={() => onClose?.({}, "backdropClick")}
          onSuccess={() => onClose?.({}, "backdropClick")}
          submitRef={submitRef}
        />
        <OLIPDialogActions
          isLoading={isSubmitting}
          mainButton={{
            text: t("common.button.save"),
            action: () => submitRef.current.click(),
          }}
          secondaryButton={{
            text: t("modal.updateProfile.button.cancel"),
            action: () => handleCancel({}),
          }}
        />
      </>
    </ModalLayout>
  );
}
