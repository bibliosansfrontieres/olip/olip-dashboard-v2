import { Button, ButtonProps } from "@mui/material";
import { useTranslation } from "react-i18next";
import useModal from "src/hooks/useModal";
import EditProfileModal from "./EditProfileModal/EditProfileModal";

export default function EditProfileButton(props: ButtonProps): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  return (
    <>
      <Button onClick={() => toggleModal()} {...props}>
        {t("common.button.edit")}
      </Button>
      <EditProfileModal
        open={openModal}
        onClose={() => toggleModal()}
        fullWidth
        maxWidth="lg"
      />
    </>
  );
}
