import { Box, CircularProgress, Typography, useTheme } from "@mui/material";
import { Doughnut } from "react-chartjs-2";
import "chart.js/auto";
import { useTranslation } from "react-i18next";
import ActivityCardLayout from "src/components/layouts/ActivityCardLayout/ActivityCardLayout";
import { useEffect, useState } from "react";
import { getInstalledContentsByCategory } from "src/services/stat.service";

export default function DoughnutContentsCard(): JSX.Element {
  const { t } = useTranslation();
  const theme = useTheme();
  const [data, setData] = useState<any>();
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    getInstalledContentsByCategory()
      .then((result) => {
        const dataForChart = {
          labels: result.data?.categories.map(
            (categoryStat) =>
              `${categoryStat.category.categoryTranslations[0].title} (${categoryStat.totalItems})`,
          ),
          datasets: [
            {
              label: t("settings.pages.activity.doughnutContentsCard.label"),
              data: result.data?.categories.map(
                (categoryStat) => categoryStat.totalItems,
              ),
              ...generateChartColors(result.data?.categories.length || 0),
              borderWidth: 1,
            },
          ],
        };
        setData(dataForChart);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  function generateChartColors(dataLength: number): {
    backgroundColor: string[];
    borderColor: string[];
  } {
    const chartColors = [
      theme.palette.primary.main,
      theme.palette.dark.main,
      theme.palette.warning.main,
      theme.palette.info.main,
      theme.palette.error.main,
      theme.palette.success.main,
    ];
    const opacity = ["FF", "40", "BF", "80", "1A", "E6", "33", "CC"];

    const backgroundColor: string[] = [];
    const borderColor: string[] = [];

    for (let i = 0; i < dataLength; i++) {
      backgroundColor.push(
        `${chartColors[chartColors.length - (i % chartColors.length) - 1]}${
          opacity[Math.floor(i / chartColors.length)]
        }`,
      );
      borderColor.push(
        chartColors[chartColors.length - (i % chartColors.length) - 1],
      );
    }
    return { backgroundColor, borderColor };
  }

  return (
    <ActivityCardLayout>
      <Typography
        component="h2"
        alignSelf="flex-start"
        data-testid="doughnutContentTitle"
      >
        {t("settings.pages.activity.doughnutContentsCard.title")}
      </Typography>
      <Box sx={{ width: "80%" }}>
        {loading ? (
          <CircularProgress size={24} />
        ) : data ? (
          <Doughnut
            data={data}
            height={400}
            options={{
              layout: { padding: 32 },
              maintainAspectRatio: false,
              plugins: {
                legend: {
                  position: "right",
                },
              },
            }}
            width="80%"
          />
        ) : (
          <Typography
            variant="info"
            fontStyle="italic"
            my={2}
            textAlign="center"
          >
            {t("settings.pages.activity.doughnutContentsCard.isNull")}
          </Typography>
        )}
      </Box>
    </ActivityCardLayout>
  );
}
