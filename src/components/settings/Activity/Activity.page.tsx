import { Container, Grid, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import SsidCard from "./SsidCard/SsidCard";
import InstalledContentsCard from "./InstalledContentsCard/InstalledContentsCard";
import DoughnutContentsCard from "./DoughnutContentsCard/DoughnutContentsCard";
import ActualTotalUsersCard from "./ActualTotalUsersCard/ActualTotalUsersCard";
import ActualConnectedUsersCard from "./ActualConnectedUsersCard/ActualConnectedUsersCard";
import { useContext } from "react";
import { UserContext } from "src/contexts/user.context";

export default function ActivityPage(): JSX.Element {
  const { t } = useTranslation();

  const { havePermission } = useContext(UserContext);

  return (
    <Container>
      <Typography variant="h2" data-testid="activityTitle">
        {t("settings.pages.activity.title")}
      </Typography>
      <Grid
        container
        alignItems="stretch"
        spacing={4}
        sx={{ marginTop: "4rem" }}
        data-testid="ssidGrid"
      >
        {havePermission("read_ssid") && (
          <Grid item xs={12} md={6} xl={4}>
            <SsidCard />
          </Grid>
        )}
        <Grid item xs={12} md={6} xl={4} data-testid="installedContentGrid">
          <InstalledContentsCard />
        </Grid>
        <Grid item xs={12} md={6} xl={4} data-testid="actualTotalUsersGrid">
          <ActualTotalUsersCard />
        </Grid>
        <Grid item xs={12} md={6} xl={4} data-testid="actualConnectedUsersGrid">
          <ActualConnectedUsersCard />
        </Grid>
        <Grid item xs={12} data-testid="doughnutContentGrid">
          <DoughnutContentsCard />
        </Grid>
      </Grid>
    </Container>
  );
}
