import { CircularProgress, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import ActivityCardLayout from "src/components/layouts/ActivityCardLayout/ActivityCardLayout";
import useGetSSID from "src/hooks/useGetSSID";

export default function SsidCard(): JSX.Element {
  const { t } = useTranslation();
  const { ssid, loading } = useGetSSID();

  return (
    <ActivityCardLayout>
      {loading ? (
        <CircularProgress size={24} />
      ) : ssid ? (
        <Typography
          fontWeight={600}
          my={2}
          textAlign="center"
          data-testid="ssidName"
        >
          {ssid}
        </Typography>
      ) : (
        <Typography variant="info" fontStyle="italic" my={2} textAlign="center">
          {t("settings.pages.activity.ssidCard.isNull")}
        </Typography>
      )}
      <Typography component="h2" data-testid="ssidTitle">
        {t("settings.pages.activity.ssidCard.title")}
      </Typography>
    </ActivityCardLayout>
  );
}
