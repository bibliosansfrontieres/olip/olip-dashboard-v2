import { CircularProgress, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import ActivityCardLayout from "src/components/layouts/ActivityCardLayout/ActivityCardLayout";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import { LiveUsersEvent } from "src/types/socket.type";

export default function ActualTotalUsersCard(): JSX.Element {
  const { t } = useTranslation();
  const { event, isLoading } = useSocketOnEvent<LiveUsersEvent>("live-users");

  return (
    <ActivityCardLayout>
      {isLoading ? (
        <CircularProgress size={24} />
      ) : event ? (
        <Typography
          fontWeight={600}
          my={2}
          fontSize="2.5rem"
          textAlign="center"
          data-testid="actualTotalUsersNumber"
        >
          {event.liveUsers}
        </Typography>
      ) : (
        <Typography variant="info" fontStyle="italic" my={2} textAlign="center">
          {t("settings.pages.activity.actualTotalUsersCard.isUndefined")}
        </Typography>
      )}
      <Typography
        component="h2"
        textAlign="center"
        data-testid="actualTotalUsersTitle"
      >
        {t("settings.pages.activity.actualTotalUsersCard.title")}
      </Typography>
    </ActivityCardLayout>
  );
}
