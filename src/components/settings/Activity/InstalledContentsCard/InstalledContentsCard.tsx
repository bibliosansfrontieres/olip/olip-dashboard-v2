import { CircularProgress, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import ActivityCardLayout from "src/components/layouts/ActivityCardLayout/ActivityCardLayout";
import { getInstalledContents } from "src/services/stat.service";

export default function InstalledContentsCard(): JSX.Element {
  const { t } = useTranslation();
  const [installedContents, setInstalledContents] = useState<
    number | undefined
  >();
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    getInstalledContents()
      .then((result) => {
        setInstalledContents(result.installedContents);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return (
    <ActivityCardLayout>
      {loading ? (
        <CircularProgress size={24} />
      ) : installedContents ? (
        <Typography
          fontWeight={600}
          my={2}
          fontSize="2.5rem"
          textAlign="center"
          data-testid="installedContentsNumber"
        >
          {installedContents}
        </Typography>
      ) : (
        <Typography variant="info" fontStyle="italic" my={2} textAlign="center">
          {t("settings.pages.activity.installedContentsCard.isUndefined")}
        </Typography>
      )}
      <Typography component="h2" data-testid="installedContentsTitle">
        {t("settings.pages.activity.installedContentsCard.title")}
      </Typography>
    </ActivityCardLayout>
  );
}
