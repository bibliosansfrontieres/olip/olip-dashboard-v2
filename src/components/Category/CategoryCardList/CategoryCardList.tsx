import { ReactElement, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { CategoryWithTranslation } from "src/types/category.type";

import ArrowDownwardRounded from "@mui/icons-material/ArrowDownwardRounded";
import ArrowUpwardRoundedIcon from "@mui/icons-material/ArrowUpwardRounded";
import { Button, Grid } from "@mui/material";

import CategoryCard from "./CategoryCard/CategoryCard";

type Props = {
  categories: CategoryWithTranslation[];
  initialDisplayCount?: number;
};

export default function CategoryCardList({
  categories,
  initialDisplayCount = 12,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [displayCount, setDisplayCount] = useState<number>(initialDisplayCount);

  const toggleShow = useCallback(() => {
    setDisplayCount((actual) =>
      actual === categories.length ? initialDisplayCount : categories.length,
    );
  }, []);

  return (
    <>
      <Grid container spacing={3.75} mb={5}>
        {categories.slice(0, displayCount).map((category) => (
          <Grid
            item
            key={category.id}
            xs={12}
            sm={6}
            md={4}
            lg={3}
            sx={{
              display: "flex",
              alignItems: "stretch",
            }}
          >
            <CategoryCard category={category} />
          </Grid>
        ))}
      </Grid>
      {categories.length > initialDisplayCount && (
        <Button
          variant="outlined"
          endIcon={
            displayCount < categories.length ? (
              <ArrowDownwardRounded />
            ) : (
              <ArrowUpwardRoundedIcon />
            )
          }
          onClick={toggleShow}
          sx={{ textAlign: "center" }}
        >
          {t(
            displayCount < categories.length
              ? "category.otherCategories.showMore"
              : "category.otherCategories.showLess",
          )}
        </Button>
      )}
    </>
  );
}
