import { ReactElement } from "react";
import { Link } from "react-router-dom";
import { CategoryWithTranslation } from "src/types/category.type";
import getEnv from "src/utils/getEnv";

import { Avatar, Box, Tooltip, Typography } from "@mui/material";

type Props = {
  category: CategoryWithTranslation;
};
export default function CategoryCard({ category }: Props): ReactElement {
  return (
    <Box
      data-testid="categoriesCard"
      component={Link}
      to={`/category/${category.id}`}
      sx={{
        backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
        borderRadius: "1rem",
        display: "flex",
        alignItems: "center",
        minHeight: "6.5rem",
        width: "100%",
        padding: "1rem",
        textDecoration: "none",
        color: "inherit",
        "&:hover": {
          backgroundColor: (theme) => `${theme.palette.primary.main}66`,
        },
      }}
    >
      <Avatar
        alt={category.categoryTranslations[0].title}
        src={`${getEnv("VITE_AXIOS_BASEURL")}${category.pictogram}`}
        sx={{
          width: "3.125rem",
          height: "3.125rem",
          marginRight: "0.625rem",
        }}
        variant="square"
      />
      <Tooltip title={category.categoryTranslations[0].title} placement="top">
        <Typography
          variant="body1sb"
          textAlign="left"
          sx={{
            overflow: "hidden",
            textOverflow: "ellipsis",
            display: "-webkit-box",
            lineClamp: 3,
            WebkitLineClamp: 3,
            WebkitBoxOrient: "vertical",
          }}
        >
          {category.categoryTranslations[0].title}
        </Typography>
      </Tooltip>
    </Box>
  );
}
