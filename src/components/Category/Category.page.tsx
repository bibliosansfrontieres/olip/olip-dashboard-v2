import { useCallback, useContext } from "react";
import ItemsList from "src/components/Category/ItemsList/ItemsList";
import OtherCategories from "src/components/Category/OtherCategories/OtherCategories";
import NavigationBar from "src/components/common/NavigationBar/NavigationBar";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import { LanguageContext } from "src/contexts/language.context";
import { NewCategoryContext } from "src/contexts/newCategory.context";
import useNewApi from "src/hooks/useNewApi";
import { getCategoriesList } from "src/services/category.service";
import { Page } from "src/types";

export default function CategoryPage(): React.ReactNode {
  const { language: languageIdentifier } = useContext(LanguageContext);

  const categoryPlaylistResult = useContext(NewCategoryContext);
  const categoriesListResult = useNewApi(
    useCallback(async () => {
      return await getCategoriesList({
        languageIdentifier,
      });
    }, [languageIdentifier]),
  );

  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    { link: "/catalog", name: "catalog.title" },
  ];
  if (categoryPlaylistResult.result) {
    links.push({
      link: "",
      name:
        categoryPlaylistResult.result.category.categoryTranslations[0].title ||
        "",
    });
  }

  return (
    <>
      <NavigationBar links={links} />
      <WaitApiResult
        useApiResult={categoryPlaylistResult}
        resultChildren={(categoryPlaylists) => (
          <ItemsList
            items={categoryPlaylists.items}
            meta={categoryPlaylists.meta}
            categoryTranslation={
              categoryPlaylists.category.categoryTranslations[0]
            }
          />
        )}
      />
      <WaitApiResult
        useApiResult={categoriesListResult}
        resultChildren={({ categories }) => (
          <OtherCategories
            categories={categories.filter(
              (category) =>
                category.id !== categoryPlaylistResult.result?.category.id,
            )}
          />
        )}
      />
    </>
  );
}
