import { useState } from "react";
import RadioIcon from "src/assets/icons/Radio";
import RadioCheckedIcon from "src/assets/icons/RadioChecked";
import RadioHoveredIcon from "src/assets/icons/RadioHovered";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import { Playlist } from "src/types/playlist.type";
import getEnv from "src/utils/getEnv";

import { Box, Tooltip, Typography } from "@mui/material";

const RadioButton = ({
  hover,
  playlistId,
}: {
  hover: boolean;
  playlistId: string;
}) => {
  const [searchParams] = useHandleSearchParams();
  const isPlaylistSelected = searchParams?.["playlistId"]
    ?.split(",")
    .find((id: string) => id === playlistId);
  if (isPlaylistSelected) {
    return <RadioCheckedIcon />;
  }
  if (hover) {
    return <RadioHoveredIcon />;
  }
  return <RadioIcon />;
};

export default function PlaylistSelectCard({
  onClick,
  playlist,
}: {
  onClick: (playlistId: string) => void;
  playlist: Playlist;
}): React.ReactNode {
  const [hover, setHover] = useState(false);

  return (
    <Box
      component="button"
      data-testid="playlistSelectCard"
      onBlur={() => setHover(false)}
      onClick={() => onClick(playlist.id)}
      onFocus={() => setHover(true)}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      sx={{
        backgroundColor: (theme) => theme.palette.primary.light,
        border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
        borderRadius: "0.5rem",
        display: "flex",
        alignItems: "center",
        height: "7.5rem",
        flex: "0 0 25rem",
        flexWrap: "nowrap",
        cursor: "pointer",
        padding: 0,
        "&:hover": {
          border: (theme) => `0.0625rem solid ${theme.palette.dark.main}`,
          boxShadow: (theme) => `0 0 0 0.0625rem ${theme.palette.dark.main}`,
        },
      }}
    >
      <Box
        sx={{
          backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
            playlist.image
          })`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderBottomLeftRadius: "0.4375rem",
          borderTopLeftRadius: "0.4375rem",
          height: "100%",
          width: "100%",
          flex: "0 0 6.5rem",
        }}
      />
      <Tooltip title={playlist.playlistTranslations[0].title} placement="top">
        <Typography
          variant="body1sb"
          my={2}
          mx={3}
          sx={{
            flex: "auto",
            overflow: "hidden",
            textOverflow: "ellipsis",
            display: "-webkit-box",
            lineClamp: 4,
            WebkitLineClamp: 4,
            WebkitBoxOrient: "vertical",
          }}
        >
          {playlist.playlistTranslations[0].title}
        </Typography>
      </Tooltip>
      <Box px={2.5} display="flex" alignItems="center">
        <RadioButton hover={hover} playlistId={playlist.id} />
      </Box>
    </Box>
  );
}
