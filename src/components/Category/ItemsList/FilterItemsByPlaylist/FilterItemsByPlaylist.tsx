import { useContext } from "react";
import { useTranslation } from "react-i18next";
import PlaylistSelectCard from "src/components/Category/ItemsList/FilterItemsByPlaylist/PlaylistSelectCard/PlaylistSelectCard";
import { NewCategoryContext } from "src/contexts/newCategory.context";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";

import ArrowBackRounded from "@mui/icons-material/ArrowBackRounded";
import ArrowForwardRounded from "@mui/icons-material/ArrowForwardRounded";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import {
  Box,
  Chip,
  Container,
  IconButton,
  Stack,
  Typography,
} from "@mui/material";

export default function FilterItemsByPlaylist(): React.ReactNode {
  const { t } = useTranslation();
  const { result } = useContext(NewCategoryContext);
  const [searchParams, updateSearchParam] = useHandleSearchParams();

  const handleChangeFilter = (playlistId?: string) => {
    const list: string[] = searchParams?.["playlistId"]?.split(",") || [];

    if (playlistId) {
      const index = list.indexOf(playlistId);
      if (index !== -1) {
        list.splice(index, 1);
      } else {
        list.push(playlistId);
      }
    }

    updateSearchParam([
      {
        name: "playlistId",
        value: playlistId ? list.toString() : "",
        defaultValue: "",
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };

  const scrollPlaylists = (appendValue: number) => {
    const element = document.getElementById("playlistStack");
    if (element !== null) {
      element.scrollLeft += appendValue;
    }
  };

  if (!result) {
    return <></>;
  }

  const { category } = result;
  return (
    <>
      <Container
        maxWidth={false}
        disableGutters
        sx={{
          backgroundColor: (theme) => `${theme.palette.primary.main}1A`,
          paddingBottom: { xs: "1rem", sm: "2.5rem" },
          paddingTop: "2.5rem",
        }}
      >
        <Container maxWidth="xl">
          <Box display="flex" justifyContent="space-between">
            <Typography variant="h4" data-testid="categoryPlaylistCount">
              {t("category.filter.playlist.title", {
                count: category.categoryPlaylists?.length || 0,
              })}
            </Typography>
            <Box
              sx={{
                [`@media (max-width: 600px)`]: {
                  display: "none",
                },
                [`@media (min-width: ${
                  (category.categoryPlaylists?.length || 0) * 28
                }rem)`]: {
                  display: "none",
                },
                textAlign: "center",
              }}
            >
              <IconButton
                onClick={() => scrollPlaylists(-400)}
                data-testid="scrollBack"
                sx={{
                  backgroundColor: (theme) => theme.palette.dark.main,
                  color: (theme) => theme.palette.primary.light,
                  padding: "1rem",
                  "&:hover": {
                    backgroundColor: (theme) => theme.palette.dark.dark,
                  },
                }}
              >
                <ArrowBackRounded fontSize="large" />
              </IconButton>
              <IconButton
                onClick={() => scrollPlaylists(400)}
                data-testid="scrollForward"
                sx={{
                  backgroundColor: (theme) => theme.palette.dark.main,
                  color: (theme) => theme.palette.primary.light,
                  marginLeft: "1rem",
                  padding: "1rem",
                  "&:hover": {
                    backgroundColor: (theme) => theme.palette.dark.dark,
                  },
                }}
              >
                <ArrowForwardRounded fontSize="large" />
              </IconButton>
            </Box>
          </Box>
        </Container>
        <Stack
          id="playlistStack"
          data-testid="playlistStack"
          direction="row"
          spacing={6}
          sx={{
            overflowX: { xs: "auto", lg: "hidden" },
            paddingBottom: { xs: "2rem", lg: "0.0625rem" },
            paddingX: "1.5rem",
            paddingTop: "3rem",
            scrollBehavior: "smooth",
            width: "fit-content(min(100%, max(10000%, 100%)))",
            "@media (min-width: 1536px)": {
              "& > div:first-of-type": {
                marginLeft: "calc((100vw - 1536px - 1.5rem)/2)",
              },
              "& > div:last-of-type": {
                marginRight: "calc((100vw - 1536px - 1.5rem)/2)",
              },
            },
          }}
        >
          {category.categoryPlaylists?.map(({ playlist }) => (
            <PlaylistSelectCard
              key={playlist.id}
              onClick={handleChangeFilter}
              playlist={playlist}
            />
          ))}
        </Stack>
      </Container>
      <Container
        maxWidth="xl"
        sx={{ marginBottom: "2.75rem", marginTop: "2.5rem" }}
      >
        {category.categoryPlaylists
          ?.filter(
            ({ playlist }) =>
              searchParams?.["playlistId"]
                ?.split(",")
                .find((id: string) => id === playlist.id) !== undefined,
          )
          .map(({ playlist }) => (
            <Chip
              data-testid="filterPlaylistChip"
              key={playlist.id}
              variant="filter"
              label={playlist.playlistTranslations[0].title}
              onKeyDown={(event: any) =>
                event.key === "Enter" && handleChangeFilter(playlist.id)
              }
              onDelete={() => handleChangeFilter(playlist.id)}
              deleteIcon={<CloseRoundedIcon />}
              sx={{ marginRight: "1.25rem", marginBottom: "1.25rem" }}
            />
          ))}
      </Container>
    </>
  );
}
