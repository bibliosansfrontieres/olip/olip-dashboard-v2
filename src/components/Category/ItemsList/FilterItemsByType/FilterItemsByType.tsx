import { Chip, Stack } from "@mui/material";
import { useTranslation } from "react-i18next";
import AudioIcon from "src/assets/icons/Audio";
import PictureIcon from "src/assets/icons/Picture";
import TextIcon from "src/assets/icons/Text";
import VideoIcon from "src/assets/icons/Video";
import LaunchIcon from "@mui/icons-material/Launch";
import { ITEM_TYPE } from "src/constants";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";

export default function FilterItemsByType(): JSX.Element {
  const { t } = useTranslation();
  const [searchParams, updateSearchParam] = useHandleSearchParams();

  const handleChangeFilter = (typeId?: number) => {
    const list: string[] = searchParams?.["dublinCoreTypeId"]?.split(",") || [];

    if (typeId) {
      const index = list.indexOf(String(typeId));
      if (index !== -1) {
        list.splice(index, 1);
      } else {
        list.push(String(typeId));
      }
    }

    updateSearchParam([
      {
        name: "dublinCoreTypeId",
        value: typeId ? list.toString() : "",
        defaultValue: "",
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };

  return (
    <Stack
      direction="row"
      spacing={2}
      justifyContent="flex-end"
      useFlexGap
      flexWrap="wrap">
      <Chip
        variant={searchParams?.["dublinCoreTypeId"] ? "tag" : "tagActive"}
        label={t("item.filter.all")}
        onClick={() => handleChangeFilter()}
      />
      <Chip
        icon={<TextIcon color="inherit" sx={{ fontSize: "1rem" }} />}
        variant={
          searchParams?.["dublinCoreTypeId"]
            ?.split(",")
            .find((id: string) => Number(id) === ITEM_TYPE.TEXT.id)
            ? "tagActive"
            : "tag"
        }
        label={t("item.filter.text")}
        onClick={() => handleChangeFilter(ITEM_TYPE.TEXT.id)}
      />
      <Chip
        icon={<VideoIcon color="inherit" sx={{ fontSize: "1rem" }} />}
        variant={
          searchParams?.["dublinCoreTypeId"]
            ?.split(",")
            .find((id: string) => Number(id) === ITEM_TYPE.VIDEO.id)
            ? "tagActive"
            : "tag"
        }
        label={t("item.filter.video")}
        onClick={() => handleChangeFilter(ITEM_TYPE.VIDEO.id)}
      />
      <Chip
        icon={<AudioIcon color="inherit" sx={{ fontSize: "1rem" }} />}
        variant={
          searchParams?.["dublinCoreTypeId"]
            ?.split(",")
            .find((id: string) => Number(id) === ITEM_TYPE.AUDIO.id)
            ? "tagActive"
            : "tag"
        }
        label={t("item.filter.audio")}
        onClick={() => handleChangeFilter(ITEM_TYPE.AUDIO.id)}
      />
      <Chip
        icon={<PictureIcon color="inherit" sx={{ fontSize: "1rem" }} />}
        variant={
          searchParams?.["dublinCoreTypeId"]
            ?.split(",")
            .find((id: string) => Number(id) === ITEM_TYPE.IMAGE.id)
            ? "tagActive"
            : "tag"
        }
        label={t("item.filter.image")}
        onClick={() => handleChangeFilter(ITEM_TYPE.IMAGE.id)}
      />
      <Chip
        icon={<LaunchIcon color="inherit" sx={{ fontSize: "1rem" }} />}
        variant={
          searchParams?.["dublinCoreTypeId"]
            ?.split(",")
            .find((id: string) => Number(id) === ITEM_TYPE.OTHER.id)
            ? "tagActive"
            : "tag"
        }
        label={t("item.filter.other")}
        onClick={() => handleChangeFilter(ITEM_TYPE.OTHER.id)}
      />
    </Stack>
  );
}
