import { useTranslation } from "react-i18next";
import FilterItemsByPlaylist from "src/components/Category/ItemsList/FilterItemsByPlaylist/FilterItemsByPlaylist";
import FilterItemsByType from "src/components/Category/ItemsList/FilterItemsByType/FilterItemsByType";
import ItemCard from "src/components/Item/ItemCard/ItemCard";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import { Pagination as PaginationType } from "src/types";
import { CategoryTranslationBase } from "src/types/categoryTranslation/categoryTranslationBase.type";
import { Item } from "src/types/item.type";

import { Box, Container, Grid, Typography } from "@mui/material";

export default function ItemsList({
  items = [],
  categoryTranslation,
  meta,
}: {
  items?: Item[];
  categoryTranslation: CategoryTranslationBase;
  meta?: PaginationType;
}): React.ReactNode {
  const { t } = useTranslation();
  const [, updateSearchParam] = useHandleSearchParams();

  const handleChangePagination = (newParams: {
    page: number;
    take: number;
  }) => {
    if (newParams.take !== meta?.take) {
      updateSearchParam([
        {
          name: "take",
          value: newParams.take.toString(),
          defaultValue: "10",
        },
        {
          name: "page",
          value: "1",
          defaultValue: "1",
        },
      ]);
    } else {
      updateSearchParam({
        name: "page",
        value: newParams.page.toString(),
        defaultValue: "1",
      });
    }
  };

  return (
    <Box my={4}>
      <Box
        mb={3}
        mx={3}
        sx={{
          textAlign: "center",
        }}
      >
        <Typography
          variant="h2"
          sx={{ marginBottom: "1.75rem" }}
          data-testid="categoryPlaylistTitle"
        >
          {categoryTranslation.title}
        </Typography>
        {categoryTranslation.description && (
          <Typography
            style={{ whiteSpace: "pre-wrap" }}
            data-testid="categoryPlaylistDescription"
          >
            {categoryTranslation.description}
          </Typography>
        )}
      </Box>
      <FilterItemsByPlaylist />
      <Container maxWidth="xl">
        <Box
          display="flex"
          mb={4}
          sx={{
            display: "flex",
            flexDirection: { xs: "column", sm: "row" },
            justifyContent: "space-between",
          }}
        >
          <Typography
            variant="h3"
            sx={{
              marginBottom: { xs: "1rem", sm: "0" },
              marginRight: "0.25rem",
            }}
            data-testid="categoryTotalItems"
          >
            {t("category.totalItems", {
              count: meta?.totalItemsCount || items.length,
            })}
          </Typography>
          <FilterItemsByType />
        </Box>
        <PaginationLayout meta={meta} onChange={handleChangePagination}>
          <Grid container spacing={3.75} sx={{ marginBottom: "2rem" }}>
            {items.map((item) => (
              <Grid
                item
                key={item.id}
                xs={12}
                sm={6}
                md={4}
                lg={3}
                xl={3}
                width="100%"
              >
                <ItemCard item={item} />
              </Grid>
            ))}
          </Grid>
        </PaginationLayout>
      </Container>
    </Box>
  );
}
