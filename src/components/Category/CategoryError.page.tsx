import { Box, Container, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import NavigationBar from "src/components/common/NavigationBar/NavigationBar";
import { Page } from "src/types";

export default function CategoryErrorPage(): JSX.Element {
  const { t } = useTranslation();
  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    { link: "/catalog", name: "catalog.title" },
    { link: "", name: "category.error.unavailable" },
  ];

  return (
    <>
      <NavigationBar links={links} />
      <Container
        maxWidth={false}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginY: "2rem",
        }}>
        <Box
          mb={12}
          sx={{
            textAlign: "center",
          }}>
          <Typography variant="h2" sx={{ marginBottom: "1.75rem" }}>
            {t("category.error.title")}
          </Typography>
          <Typography>{t("category.error.description")}</Typography>
        </Box>
      </Container>
    </>
  );
}
