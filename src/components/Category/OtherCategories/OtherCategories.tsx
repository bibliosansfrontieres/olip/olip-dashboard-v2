import { useTranslation } from "react-i18next";
import CategoryCardList from "src/components/Category/CategoryCardList/CategoryCardList";
import { CategoryWithTranslation } from "src/types/category.type";

import { Container, Typography } from "@mui/material";

export default function OtherCategories({
  categories,
}: {
  categories: CategoryWithTranslation[];
}): React.ReactNode {
  const { t } = useTranslation();

  return (
    <Container
      maxWidth="xl"
      component="section"
      sx={{
        my: "5rem",
        textAlign: "center",
      }}
    >
      <Typography variant="h2" mb={6.25}>
        {t("category.otherCategories.title")}
      </Typography>
      <CategoryCardList categories={categories} />
    </Container>
  );
}
