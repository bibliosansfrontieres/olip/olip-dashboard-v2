import { Container, Typography } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import useAPI from "src/hooks/useAPI";
import { getSearchItems } from "src/services/search.service";
import { GetSearchItemsResult } from "src/types/search.type";
import ItemsGrid from "../Grid/ItemsGrid";

export const ItemsTab = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const query = useMemo(() => {
    const url = new URLSearchParams(location.search);
    return url.get("q") ?? "";
  }, [location]);

  const [searchParams, setSearchParams] = useState<{
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: query,
    take: 12,
  });

  useEffect(() => {
    setSearchParams((prev) => ({ ...prev, query: query ?? "", page: 1 }));
  }, [query]);

  const { isLoading: searchItemsIsLoading, result: searchItemsResult } =
    useAPI<GetSearchItemsResult>(
      () =>
        getSearchItems({
          ...searchParams,
          query: query ?? "",
        }),
      [searchParams, query],
    );
  const { data: items, meta } = searchItemsResult?.items ?? {};

  return (
    !searchItemsIsLoading && (
      <Container maxWidth="xl">
        <Typography variant="h3" my={4} data-testid="itemsTabTitle">
          {t("category.totalItems", {
            count: meta?.totalItemsCount || items?.length,
          })}
        </Typography>
        <PaginationLayout
          meta={meta}
          onChange={(newParams) =>
            setSearchParams((prev) => ({ ...prev, ...newParams }))
          }
        >
          <ItemsGrid items={items} />
        </PaginationLayout>
      </Container>
    )
  );
};
