import { Container, Typography } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import useAPI from "src/hooks/useAPI";
import { getSearchPlaylists } from "src/services/search.service";
import { GetSearchPlaylistsResult } from "src/types/search.type";
import PlaylistsGrid from "../Grid/PlaylistsGrid";

export const PlaylistsTab = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const query = useMemo(() => {
    const url = new URLSearchParams(location.search);
    return url.get("q") ?? "";
  }, [location]);

  const [searchParams, setSearchParams] = useState<{
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: query,
    take: 12,
  });

  useEffect(() => {
    setSearchParams((prev) => ({ ...prev, query: query ?? "", page: 1 }));
  }, [query]);

  const { isLoading: searchPlaylistsIsLoading, result: searchPlaylistsResult } =
    useAPI<GetSearchPlaylistsResult>(() => {
      return getSearchPlaylists({
        ...searchParams,
        query: query ?? "",
      });
    }, [searchParams, query]);
  const { data: categoryPlaylists, meta } =
    searchPlaylistsResult?.categoryPlaylists ?? {};

  return (
    !searchPlaylistsIsLoading && (
      <>
        <Container maxWidth="xl">
          <Typography variant="h3" my={4} data-testid="playlistsTabTitle">
            {t("category.totalItems", {
              count: meta?.totalItemsCount || categoryPlaylists?.length,
            })}
          </Typography>
          <PaginationLayout
            meta={meta}
            onChange={(newParams) =>
              setSearchParams((prev) => ({ ...prev, ...newParams }))
            }
          >
            <PlaylistsGrid categoryPlaylists={categoryPlaylists} />
          </PaginationLayout>
        </Container>
      </>
    )
  );
};
