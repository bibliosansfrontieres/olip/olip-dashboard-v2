import { Box, Button, Container, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import ApplicationsGrid from "../Grid/ApplicationsGrid";
import PlaylistsGrid from "../Grid/PlaylistsGrid";
import ItemsGrid from "../Grid/ItemsGrid";
import CategoriesGrid from "../Grid/CategoriesGrid";
import { GetSearchResultWithMeta } from "src/types/search.type";

export const AllTab = ({
  elements,
  changeTab,
}: {
  elements: GetSearchResultWithMeta;
  changeTab: (tab: number) => void;
}) => {
  const { t } = useTranslation();
  return (
    <Container maxWidth="xl">
      {Object.keys(elements).map((elementName, index) => (
        <Box key={elementName + index}>
          {elements[elementName as keyof GetSearchResultWithMeta]?.meta
            .totalItemsCount ? (
            <Typography
              variant="h3"
              my={4}
              data-testid={`${elementName}AllTabTitle`}
            >
              {t(`searchPage.${elementName}.totalItems`, {
                count:
                  elements[elementName as keyof GetSearchResultWithMeta]?.meta
                    .totalItemsCount ?? 0,
              })}
            </Typography>
          ) : (
            <></>
          )}
          {elementName === "items" && (
            <ItemsGrid items={elements.items?.data} />
          )}
          {elementName === "playlists" && (
            <PlaylistsGrid categoryPlaylists={elements.playlists?.data} />
          )}
          {elementName === "categories" && (
            <CategoriesGrid categories={elements.categories?.data} />
          )}
          {elementName === "applications" && (
            <ApplicationsGrid applications={elements.applications?.data} />
          )}
          {!(
            elements[elementName as keyof GetSearchResultWithMeta]?.meta
              .totalItemsCount === 0
          ) && (
            <Container sx={{ display: "flex", justifyContent: "center" }}>
              <Button
                variant="outlined"
                sx={{
                  marginBottom: "1.5rem",
                }}
                data-testid={`${elementName}ChangeTabButton`}
                onClick={() => changeTab(index + 1)}
              >
                {t(`searchPage.${elementName}.button`)}
              </Button>
            </Container>
          )}
        </Box>
      ))}
    </Container>
  );
};
