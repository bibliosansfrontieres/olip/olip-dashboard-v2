import { Container, Typography } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import useAPI from "src/hooks/useAPI";
import { getSearchCategories } from "src/services/search.service";
import { GetSearchCategoriesResult } from "src/types/search.type";
import CategoriesGrid from "../Grid/CategoriesGrid";

export const CategoriesTab = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const query = useMemo(() => {
    const url = new URLSearchParams(location.search);
    return url.get("q") ?? "";
  }, [location]);

  const [searchParams, setSearchParams] = useState<{
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: query,
    take: 12,
  });

  useEffect(() => {
    setSearchParams((prev) => ({ ...prev, query: query ?? "", page: 1 }));
  }, [query]);

  const {
    isLoading: searchCategoriesIsLoading,
    result: searchCategoriesResult,
  } = useAPI<GetSearchCategoriesResult>(() => {
    return getSearchCategories({
      ...searchParams,
      query: query ?? "",
    });
  }, [searchParams, location]);
  const { data: categories, meta } = searchCategoriesResult?.categories ?? {};

  return (
    !searchCategoriesIsLoading && (
      <>
        <Container maxWidth="xl">
          <Typography variant="h3" my={4} data-testid="categoriesTabTitle">
            {t("category.totalItems", {
              count: meta?.totalItemsCount || categories?.length,
            })}
          </Typography>
          <PaginationLayout
            meta={meta}
            onChange={(newParams) =>
              setSearchParams((prev) => ({ ...prev, ...newParams }))
            }
          >
            <CategoriesGrid categories={categories} />
          </PaginationLayout>
        </Container>
      </>
    )
  );
};
