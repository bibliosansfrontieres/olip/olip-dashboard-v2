import { Tab, Tabs, styled, tabClasses, tabsClasses } from "@mui/material";
import { ReactElement, useState } from "react";
import { useTranslation } from "react-i18next";

import { ItemsTab } from "./ItemsTab/ItemsTab";
import { AllTab } from "./AllTab/AllTab";
import { CategoriesTab } from "./CategoriesTab/CategoriesTab";
import { ApplicationsTab } from "./ApplicationsTab/ApplicationsTab";
import { PlaylistsTab } from "./PlaylistsTab/PlaylistsTab";
import {
  ApplicationsWithMeta,
  CategoriesWithMeta,
  CategoryPlaylistsWithMeta,
  ItemsWithMeta,
} from "src/types/search.type";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  isHidden: boolean;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, isHidden, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={isHidden}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {children}
    </div>
  );
}
const TabItem = styled(Tab)(({ theme }) => ({
  [`&.${tabClasses.selected}`]: {
    backgroundColor: theme.palette.primary?.dark,
    border: "none",
    borderRadius: "0.5rem",
    color: theme.palette.text?.secondary,
    fontSize: "0.875rem",
    padding: "0 0.5rem",
    "&:hover": {
      border: `0.125rem solid ${theme.palette.primary?.dark}`,
      backgroundColor: theme.palette.grey?.[200],
      color: theme.palette.text?.primary,
      padding: "0.5rem 0.375rem",
    },
  },
  minHeight: "32px",
  backgroundColor: theme.palette.grey?.[200],
  border: "none",
  borderRadius: "0.5rem",
  fontSize: "0.875rem",
  padding: "0 0.5rem",
  margin: "8px",
  fontWeight: 500,
  "&:hover": {
    border: `0.125rem solid ${theme.palette.primary?.dark}`,
    padding: "0.5rem 0.375rem",
    minHeight: "32px",
    height: "32px",
  },
}));

export default function FilterByContentTabs({
  items,
  playlists,
  categories,
  applications,
  totalCount,
}: {
  items?: ItemsWithMeta;
  playlists?: CategoryPlaylistsWithMeta;
  categories?: CategoriesWithMeta;
  applications?: ApplicationsWithMeta;
  totalCount: number;
}): ReactElement {
  const { t } = useTranslation();
  const [tabSelected, setTabSelected] = useState<number>(0);
  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  const elements = {
    items: items,
    playlists: playlists,
    categories: categories,
    applications: applications,
  };
  return (
    <>
      <Tabs
        value={tabSelected}
        onChange={(_event: React.SyntheticEvent, newValue: number) =>
          setTabSelected(newValue)
        }
        variant="scrollable"
        scrollButtons={true}
        allowScrollButtonsMobile
        sx={{
          [`& .${tabsClasses.indicator}`]: {
            bottom: "unset",
          },
        }}
      >
        {totalCount > 0 && (
          <TabItem
            data-testid="allTab"
            label={t("searchPage.tabs.all.title", {
              count: totalCount ?? 0,
            })}
            {...a11yProps(0)}
          />
        )}
        {items?.meta?.totalItemsCount && items?.meta?.totalItemsCount > 0 && (
          <TabItem
            data-testid="itemsTab"
            sx={{
              display: `${
                items?.meta?.totalItemsCount && items?.meta?.totalItemsCount > 0
                  ? "flex"
                  : "none"
              }`,
            }}
            label={t("searchPage.tabs.items.title", {
              count: items?.meta.totalItemsCount ?? 0,
            })}
            {...a11yProps(1)}
          />
        )}

        <TabItem
          data-testid="playlistsTab"
          sx={{
            display: `${
              playlists?.meta?.totalItemsCount &&
              playlists?.meta?.totalItemsCount > 0
                ? "flex"
                : "none"
            }`,
          }}
          label={t("searchPage.tabs.playlists.title", {
            count: playlists?.meta.totalItemsCount ?? 0,
          })}
          {...a11yProps(2)}
        />
        <TabItem
          data-testid="categoriesTab"
          sx={{
            display: `${
              categories?.meta?.totalItemsCount &&
              categories?.meta?.totalItemsCount > 0
                ? "flex"
                : "none"
            }`,
          }}
          label={t("searchPage.tabs.categories.title", {
            count: categories?.meta.totalItemsCount ?? 0,
          })}
          {...a11yProps(3)}
        />
        {applications?.meta?.totalItemsCount &&
          applications?.meta?.totalItemsCount > 0 && (
            <TabItem
              data-testid="applicationsTab"
              sx={{
                display: `${
                  applications?.meta?.totalItemsCount &&
                  applications?.meta?.totalItemsCount > 0
                    ? "flex"
                    : "none"
                }`,
              }}
              label={t("searchPage.tabs.applications.title", {
                count: applications?.meta.totalItemsCount ?? 0,
              })}
              {...a11yProps(4)}
            />
          )}
      </Tabs>
      <CustomTabPanel index={0} isHidden={tabSelected !== 0}>
        <AllTab elements={elements} changeTab={setTabSelected} />
      </CustomTabPanel>
      <CustomTabPanel index={1} isHidden={tabSelected !== 1}>
        <ItemsTab />
      </CustomTabPanel>
      <CustomTabPanel index={2} isHidden={tabSelected !== 2}>
        <PlaylistsTab />
      </CustomTabPanel>
      <CustomTabPanel index={3} isHidden={tabSelected !== 3}>
        <CategoriesTab />
      </CustomTabPanel>
      <CustomTabPanel index={4} isHidden={tabSelected !== 4}>
        <ApplicationsTab />
      </CustomTabPanel>
    </>
  );
}
