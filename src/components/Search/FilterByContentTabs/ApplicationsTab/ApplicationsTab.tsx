import { Container, Typography } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import useAPI from "src/hooks/useAPI";
import { getSearchApplications } from "src/services/search.service";
import { GetSearchApplicationsResult } from "src/types/search.type";
import ApplicationsGrid from "../Grid/ApplicationsGrid";

export const ApplicationsTab = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const query = useMemo(() => {
    const url = new URLSearchParams(location.search);
    return url.get("q") ?? "";
  }, [location]);

  const [searchParams, setSearchParams] = useState<{
    page: number;
    query: string;
    take: number;
  }>({
    page: 1,
    query: query,
    take: 12,
  });

  useEffect(() => {
    setSearchParams((prev) => ({ ...prev, query: query ?? "", page: 1 }));
  }, [query]);

  const {
    isLoading: searchApplicationsIsLoading,
    result: searchApplicationsResult,
  } = useAPI<GetSearchApplicationsResult>(() => {
    return getSearchApplications({
      ...searchParams,
      query: query ?? "",
    });
  }, [searchParams, location]);
  const { data: applications, meta } =
    searchApplicationsResult?.applications ?? {};

  return (
    !searchApplicationsIsLoading && (
      <>
        <Container maxWidth="xl">
          <Typography variant="h3" my={4} data-testid="applicationsTabTitle">
            {t("category.totalItems", {
              count: meta?.totalItemsCount || applications?.length,
            })}
          </Typography>
          <PaginationLayout
            meta={meta}
            onChange={(newParams) =>
              setSearchParams((prev) => ({ ...prev, ...newParams }))
            }
          >
            <ApplicationsGrid applications={applications} />
          </PaginationLayout>
        </Container>
      </>
    )
  );
};
