import { Grid } from "@mui/material";
import ItemCard from "src/components/Item/ItemCard/ItemCard";
import { Item } from "src/types/item.type";

const ItemsGrid = ({ items }: { items?: Item[] }) => {
  return (
    { items } && (
      <Grid container spacing={3.75} mb={5}>
        {items?.map((item) => (
          <Grid
            item
            key={item.id}
            xs={12}
            sm={6}
            md={4}
            lg={3}
            xl={3}
            width="100%"
          >
            <ItemCard item={item} />
          </Grid>
        ))}
      </Grid>
    )
  );
};
export default ItemsGrid;
