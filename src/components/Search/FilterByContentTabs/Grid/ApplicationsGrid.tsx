import { Grid } from "@mui/material";
import ApplicationCard from "src/components/Applications/ApplicationCard/ApplicationCard";
import { Application } from "src/types/application.type";

const ApplicationsGrid = ({
  applications,
}: {
  applications?: Application[];
}) => {
  return (
    <Grid container spacing={3.75} mb={5}>
      {applications?.map((application) => (
        <Grid
          item
          key={application.id}
          xs={12}
          sm={6}
          md={6}
          lg={3}
          xl={3}
          width="100%"
        >
          <ApplicationCard application={application} />
        </Grid>
      ))}
    </Grid>
  );
};
export default ApplicationsGrid;
