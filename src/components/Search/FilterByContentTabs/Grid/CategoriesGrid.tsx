import { Grid } from "@mui/material";
import CategoryCard from "src/components/Category/CategoryCardList/CategoryCard/CategoryCard";
import { Category } from "src/types/category.type";

const CategoriesGrid = ({ categories }: { categories?: Category[] }) => {
  return (
    <Grid container spacing={3.75} mb={5}>
      {categories?.map((category) => (
        <Grid
          item
          key={category.id}
          xs={12}
          sm={6}
          md={4}
          lg={3}
          sx={{
            display: "flex",
            alignItems: "stretch",
          }}
        >
          <CategoryCard category={category} />
        </Grid>
      ))}
    </Grid>
  );
};
export default CategoriesGrid;
