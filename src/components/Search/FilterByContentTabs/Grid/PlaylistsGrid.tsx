import { Grid } from "@mui/material";
import PlaylistCard from "src/components/Catalog/PlaylistCard/PlaylistCard";
import { CategoryPlaylist } from "src/types/category.type";

const PlaylistsGrid = ({
  categoryPlaylists,
}: {
  categoryPlaylists?: CategoryPlaylist[];
}) => {
  return (
    <Grid container spacing={3.75} mb={5}>
      {categoryPlaylists?.map((categoryPlaylist: CategoryPlaylist) => (
        <Grid
          item
          key={categoryPlaylist.id}
          sm={12}
          md={6}
          lg={6}
          xl={4}
          width="100%"
        >
          <PlaylistCard
            categoryParentId={categoryPlaylist.category.id}
            categoryPlaylist={categoryPlaylist}
          />
        </Grid>
      ))}
    </Grid>
  );
};
export default PlaylistsGrid;
