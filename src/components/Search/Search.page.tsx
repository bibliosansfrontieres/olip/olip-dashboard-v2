import { Box, Container, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { useLoaderData, useLocation } from "react-router-dom";

import FilterByContentTabs from "./FilterByContentTabs/FilterByContentTabs";
import SearchBar from "../layouts/MainLayout/Header/SearchBar/SearchBar";
import { getSearch } from "src/services/search.service";
import { GetSearchResult } from "src/types/search.type";
import { useMemo } from "react";
import NavigationBar from "../common/NavigationBar/NavigationBar";
import { Page } from "src/types";

export async function searchLoader({ request }: { request: Request }) {
  const url = new URL(request.url);
  const search = url.searchParams.get("q");
  const resultAll = await getSearch({
    query: search || "",
  });
  return resultAll;
}

export default function SearchPage(): JSX.Element {
  const location = useLocation();
  const { t } = useTranslation();
  const query = useMemo(() => {
    const url = new URLSearchParams(location.search);
    return url.get("q") ?? "";
  }, [location]);
  const { items, playlists, categories, applications } =
    useLoaderData() as GetSearchResult;
  const totalCount =
    (items?.meta.totalItemsCount ?? 0) +
    (playlists?.meta.totalItemsCount ?? 0) +
    (categories?.meta.totalItemsCount ?? 0) +
    (applications?.meta.totalItemsCount ?? 0);
  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    { link: "/search", name: "searchPage.title" },
  ];
  return (
    <>
      <NavigationBar links={links} />

      <Container
        maxWidth="xl"
        sx={{
          marginY: "2rem",
        }}
      >
        <Box
          display="flex"
          flexDirection={{ xs: "column" }}
          alignItems={{ xs: "flex-start", sm: "center" }}
          sx={{ padding: "2.5rem" }}
        >
          <SearchBar />
          <Typography variant="h2" data-testid="searchPageQuery">
            "{query}"
          </Typography>
          <Typography data-testid="searchPageCountResult">
            {t("searchPage.results", { count: totalCount })}
          </Typography>
        </Box>
        <FilterByContentTabs
          items={items}
          playlists={playlists}
          categories={categories}
          applications={applications}
          totalCount={totalCount}
        />
      </Container>
    </>
  );
}
