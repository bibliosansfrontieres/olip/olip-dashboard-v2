import { ReactElement, useCallback, useContext } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useSearchParams } from "react-router-dom";
import FilterCategorySelect from "src/components/Catalog/FilterCategorySelect/FilterCategorySelect";
import NewPlaylistCard from "src/components/Catalog/PlaylistCard/NewPlaylistCard";
import SortCategorySelect from "src/components/Catalog/SortCategorySelect/SortCategorySelect";
import FilterApplicationSelect from "src/components/common/FilterApplicationSelect/FilterApplicationSelect";
import NavigationBar from "src/components/common/NavigationBar/NavigationBar";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import { LanguageContext } from "src/contexts/language.context";
import { CategorySortBy } from "src/enums/categorySortBy.enum";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import useNewApi from "src/hooks/useNewApi";
import {
  getCategoriesCatalog,
  getCategoriesList,
} from "src/services/category.service";
import { Page } from "src/types";
import { GetCategoriesListResult } from "src/types/category.type";

import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material";

export default function CatalogPage(): ReactElement {
  const { t } = useTranslation();
  const [params] = useSearchParams();
  const navigate = useNavigate();
  const { language: languageIdentifier } = useContext(LanguageContext);
  const [searchParams, updateSearchParams] = useHandleSearchParams();

  const catalogResult = useNewApi(
    useCallback(async () => {
      return await getCategoriesCatalog({
        languageIdentifier,
        applicationIds: params.get("applicationIds") || undefined,
        categoryIds: params.get("categoryIds") || undefined,
        sortBy: (params.get("sortBy") as CategorySortBy) || undefined,
      });
    }, [languageIdentifier, params]),
  );

  const categoriesListResult = useNewApi<GetCategoriesListResult>(
    useCallback(
      () => getCategoriesList({ languageIdentifier }),
      [languageIdentifier],
    ),
  );

  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    { link: "/catalog", name: "catalog.title" },
  ];

  const handleFilterApplications = (applicationsIds: string[]) => {
    updateSearchParams({
      name: "applicationIds",
      value: applicationsIds.toString(),
      defaultValue: "",
    });
  };

  return (
    <>
      <>
        <NavigationBar links={links} />
        <Container
          maxWidth="xl"
          sx={{
            marginY: "2rem",
          }}
        >
          <Box
            sx={{
              textAlign: "center",
            }}
          >
            <Typography
              variant="h2"
              sx={{ marginBottom: "1.75rem" }}
              data-testid="catalogPageTitle"
            >
              {t("catalog.title")}
            </Typography>
            <Typography data-testid="catalogPageDescription">
              {t("catalog.description")}
            </Typography>
          </Box>
          <WaitApiResult
            useApiResult={{
              catalog: catalogResult,
              categoriesList: categoriesListResult,
            }}
            resultChildren={({ categoriesList, catalog }) => (
              <>
                <Stack
                  alignItems="center"
                  direction={{ xs: "column", md: "row" }}
                  display="flex"
                  justifyContent="center"
                  p={3}
                  mb={12}
                  spacing={{ xs: 1, md: 3 }}
                >
                  <FilterCategorySelect
                    sx={{ width: { xs: "100%", md: "20rem" } }}
                    categories={categoriesList.categories}
                  />
                  <FilterApplicationSelect
                    actualValues={searchParams?.["applicationIds"]?.split(",")}
                    onFilter={handleFilterApplications}
                    placeholder="applications.filter"
                    sx={{ width: { xs: "100%", md: "20rem" } }}
                  />

                  <SortCategorySelect
                    sx={{ width: { xs: "100%", md: "15rem" } }}
                  />
                </Stack>
                {catalog.categories.map((category) => (
                  <div key={category.id}>
                    <Box
                      component="section"
                      key={category.id}
                      mb={10}
                      data-testid="catalogPageCategorySection"
                    >
                      <Box display="flex" justifyContent="space-between" mb={4}>
                        <Typography
                          variant="h3"
                          data-testid="catalogPageCategoryTitle"
                        >
                          {t("catalog.category.title", {
                            title: category.categoryTranslations[0].title,
                            countPlaylist: category.categoryPlaylists?.length,
                          })}
                        </Typography>
                        <Button
                          data-testid="catalogPageCategoryButton"
                          variant="outlined"
                          onClick={() => navigate(`/category/${category.id}`)}
                        >
                          {t("catalog.category.button")}
                        </Button>
                      </Box>
                      <Grid container spacing={3.75}>
                        {category.categoryPlaylists?.map((categoryPlaylist) => (
                          <Grid
                            item
                            key={categoryPlaylist.id}
                            sm={12}
                            md={6}
                            lg={6}
                            xl={4}
                            width="100%"
                          >
                            <NewPlaylistCard
                              categoryParentId={category.id}
                              categoryPlaylist={categoryPlaylist}
                              application={catalog.applications.find(
                                (app) =>
                                  app.id ===
                                  categoryPlaylist.playlist.application.id,
                              )}
                            />
                          </Grid>
                        ))}
                      </Grid>
                    </Box>
                  </div>
                ))}
              </>
            )}
          />
        </Container>
      </>
    </>
  );
}
