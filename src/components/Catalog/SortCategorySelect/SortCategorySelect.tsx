import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import OLIPSelect, {
  Choice,
} from "src/components/common/OLIPSelect/OLIPSelect";
import { CategorySortBy } from "src/enums/categorySortBy.enum";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";

import { SxProps } from "@mui/material";

type Props = {
  sx?: SxProps;
};

export default function SortCategorySelect({ sx }: Props): ReactElement {
  const { t } = useTranslation();
  const [searchParams, updateSearchParam] = useHandleSearchParams();

  const sortChoices: Choice[] = Object.values(CategorySortBy).map(
    (categorySort) => ({
      name: t(`catalog.sort.${categorySort}`),
      value: categorySort,
    }),
  );

  return (
    <OLIPSelect
      choices={sortChoices.sort((a, b) => a.name.localeCompare(b.name))}
      onChange={(event: any) =>
        updateSearchParam({
          name: "sortBy",
          value: event.target.value,
          defaultValue: "",
        })
      }
      placeholder={t("catalog.sort.placeholder")}
      sx={{ width: "15rem", ...sx }}
      values={searchParams?.["sortBy"] || ""}
    />
  );
}
