import { Box, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import PlaylistIcon from "src/assets/icons/Playlist";
import ApplicationChip from "src/components/common/ApplicationChip/ApplicationChip";
import { CategoryPlaylist } from "src/types/category.type";
import getEnv from "src/utils/getEnv";

export default function PlaylistCard({
  categoryParentId,
  categoryPlaylist,
}: {
  categoryParentId: number;
  categoryPlaylist: CategoryPlaylist;
}): JSX.Element {
  const { t } = useTranslation();

  return (
    <Box
      data-testid="playlistsCard"
      component={Link}
      to={`/category/${categoryParentId}?playlistId=${categoryPlaylist.playlist.id}`}
      sx={{
        alignItems: "center",
        backgroundColor: (theme) => `${theme.palette.primary.main}1A`,
        borderRadius: "1rem",
        color: "inherit",
        display: "flex",
        height: "17.25rem",
        textDecoration: "none",
        "&:hover": {
          backgroundColor: (theme) => `${theme.palette.primary.main}33`,
        },
      }}
    >
      <Box
        sx={{
          backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
            categoryPlaylist.playlist.image
          })`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderBottomLeftRadius: "inherit",
          borderTopLeftRadius: "inherit",
          flex: 1,
          height: "100%",
          width: "100%",
        }}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          marginX: "1.5rem",
          maxHeight: "90%",
          overflowY: "auto",
        }}
      >
        <Box display="flex" alignItems="center">
          <PlaylistIcon color="primary" sx={{ marginRight: "1rem" }} />
          <Typography>
            {t("catalog.category.playlist.documentCount", {
              count: categoryPlaylist.totalItems || 0,
            })}
          </Typography>
        </Box>
        <Typography variant="h4" my={1}>
          {categoryPlaylist.playlist.playlistTranslations[0].title}
        </Typography>
        {categoryPlaylist.playlist.application &&
          !categoryPlaylist.playlist.application.isOlip && (
            <Box>
              <Typography variant="body2">
                {t("catalog.category.playlist.provided")}
              </Typography>
              <ApplicationChip
                application={categoryPlaylist.playlist.application}
                sx={{ justifyContent: "flex-start" }}
              />
            </Box>
          )}
      </Box>
    </Box>
  );
}
