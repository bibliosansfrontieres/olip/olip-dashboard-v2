import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import PlaylistIcon from "src/assets/icons/Playlist";
import NewApplicationChip from "src/components/common/ApplicationChip/NewApplicationChip";
import { CategoryApplication } from "src/types/application/categoryApplication.type";
import { CategoryPlaylist } from "src/types/categoryPlaylist/categoryPlaylist.type";
import { isApplicationOlip } from "src/utils/applications";
import getEnv from "src/utils/getEnv";

import { Box, Tooltip, Typography } from "@mui/material";

export default function NewPlaylistCard({
  categoryParentId,
  categoryPlaylist,
  application,
}: {
  categoryParentId: number;
  categoryPlaylist: CategoryPlaylist;
  application?: CategoryApplication;
}): JSX.Element {
  const { t } = useTranslation();

  return (
    <Box
      data-testid="playlistsCard"
      component={Link}
      to={`/category/${categoryParentId}?playlistId=${categoryPlaylist.playlist.id}`}
      sx={{
        alignItems: "center",
        backgroundColor: (theme) => `${theme.palette.primary.main}1A`,
        borderRadius: "1rem",
        color: "inherit",
        display: "flex",
        height: "17.25rem",
        textDecoration: "none",
        "&:hover": {
          backgroundColor: (theme) => `${theme.palette.primary.main}33`,
        },
      }}
    >
      <Box
        sx={{
          backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
            categoryPlaylist.playlist.image
          })`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderBottomLeftRadius: "inherit",
          borderTopLeftRadius: "inherit",
          flex: 1,
          height: "100%",
          width: "100%",
        }}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          marginX: "1.5rem",
          maxHeight: "90%",
          overflowY: "auto",
        }}
      >
        <Box display="flex" alignItems="center">
          <PlaylistIcon color="primary" sx={{ marginRight: "1rem" }} />
          <Typography data-testid="playlistCardDocumentCount">
            {t("catalog.category.playlist.documentCount", {
              count: categoryPlaylist.categoryPlaylistItemsCount || 0,
            })}
          </Typography>
        </Box>
        <Tooltip
          title={categoryPlaylist.playlist.playlistTranslations[0].title}
          placement="top"
        >
          <Typography variant="h4" my={1} data-testid="playlistCardTitle">
            {categoryPlaylist.playlist.playlistTranslations[0].title}
          </Typography>
        </Tooltip>

        {application && !isApplicationOlip(application) && (
          <Box>
            <Typography variant="body2">
              {t("catalog.category.playlist.provided")}
            </Typography>
            <NewApplicationChip
              application={application}
              sx={{ justifyContent: "flex-start" }}
            />
          </Box>
        )}
      </Box>
    </Box>
  );
}
