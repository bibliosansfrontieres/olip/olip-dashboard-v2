import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import { GetCategoriesListResult } from "src/types/category.type";
import { formatValue } from "src/utils/olipSelect";

import { SelectChangeEvent, SxProps } from "@mui/material";

type Props = {
  sx?: SxProps;
  categories: GetCategoriesListResult["categories"];
};

export default function FilterCategorySelect({
  sx,
  categories,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [searchParams, updateSearchParams] = useHandleSearchParams();

  const handleFilterCategories = (
    event: SelectChangeEvent<string[] | string>,
  ) => {
    updateSearchParams({
      name: "categoryIds",
      value: formatValue(event),
      defaultValue: "",
    });
  };
  return (
    <>
      <OLIPSelect
        choices={categories.map((category) => ({
          name: category.categoryTranslations[0].title,
          value: category.id.toString(),
        }))}
        onChange={handleFilterCategories}
        placeholder={t("catalog.filter.placeholder")}
        sx={{ width: "15rem", ...sx }}
        values={searchParams?.["categoryIds"]?.split(",") || []}
      />
    </>
  );
}
