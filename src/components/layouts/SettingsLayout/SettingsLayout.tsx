import { Outlet } from "react-router-dom";

import SettingsMenu from "./SettingsMenu/SettingsMenu";
import { Box, Container } from "@mui/material";

export default function SettingsLayout(): JSX.Element {
  return (
    <Box display="flex">
      <SettingsMenu />
      <Container
        component="main"
        disableGutters
        maxWidth={false}
        sx={{
          marginY: "2.5rem",
          paddingLeft: { xs: "2.5rem", lg: "4.25rem" },
          paddingRight: "2.5rem",
        }}>
        <Outlet />
      </Container>
    </Box>
  );
}
