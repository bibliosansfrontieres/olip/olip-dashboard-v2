import { Button } from "@mui/material";
import { useCallback, useContext } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { UserContext } from "src/contexts/user.context";

export default function LogoutButton(): JSX.Element {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { logout, user } = useContext(UserContext);

  const handleClick = useCallback(() => {
    logout();
    navigate("/");
  }, []);

  return (
    <Button
      disabled={!user}
      variant="contained"
      onClick={handleClick}
      data-testid="logoutButton"
    >
      {t("settings.menu.buttons.logout")}
    </Button>
  );
}
