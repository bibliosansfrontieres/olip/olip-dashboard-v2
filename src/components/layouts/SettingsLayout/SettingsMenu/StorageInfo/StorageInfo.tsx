import { useTranslation } from "react-i18next";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import { StorageEvent } from "src/types/socket.type";
import { getStoragePercentageColor } from "src/utils/storage";

import {
  Box,
  CircularProgress,
  LinearProgress,
  Typography,
} from "@mui/material";

export default function StorageInfo(): JSX.Element | null {
  const { t } = useTranslation();
  const { event, isLoading } = useSocketOnEvent<StorageEvent>("storage");

  function bytesToGigabytes(bytes: number): number {
    return bytes / 1024 / 1024 / 1024;
  }

  function purcentageStorageUsed(event: StorageEvent) {
    return ((event.totalBytes - event.availableBytes) / event.totalBytes) * 100;
  }

  if (event) {
    return (
      <Box
        sx={{
          borderRadius: "0.5rem",
          backgroundColor: (theme) => `${theme.palette.primary.light}80`,
          margin: "0.625rem",
          padding: "1.25rem 0.625rem 1.625rem 0.625rem",
        }}
      >
        <Box display="flex" justifyContent="space-between" mb={3}>
          <Typography fontWeight={600}>
            {t("settings.storage.title")}
          </Typography>
          {!isLoading && (
            <Typography fontSize="0.875rem" fontWeight={600}>
              {t("settings.storage.quantity", {
                actualSize: (
                  bytesToGigabytes(event.totalBytes || 0) -
                  bytesToGigabytes(event.availableBytes || 0)
                ).toFixed(2),
                totalSize: bytesToGigabytes(event.totalBytes || 0).toFixed(2),
                unit: "Go",
              })}
            </Typography>
          )}
        </Box>
        {isLoading ? (
          <CircularProgress size={30} />
        ) : (
          <LinearProgress
            variant="determinate"
            color={getStoragePercentageColor(purcentageStorageUsed(event))}
            value={purcentageStorageUsed(event)}
          />
        )}
      </Box>
    );
  }
  return null;
}
