import { useContext } from "react";
import { useTranslation } from "react-i18next";
import BatteryIcon from "src/components/layouts/SettingsLayout/SettingsMenu/BatteryInfo/BatteryIcon";
import { BatteryContext } from "src/contexts/battery.context";
import { BatteryInformationEvent } from "src/types/socket.type";
import { getBatteryPercentageDefinition } from "src/utils/batteryPercentageDefinition";

import BatteryUnknownIcon from "@mui/icons-material/BatteryUnknown";
import { Box, LinearProgress, Typography } from "@mui/material";

const BatteryPercentageInfo = ({
  batteryInformation,
}: {
  batteryInformation: BatteryInformationEvent;
}) => {
  const { percentage, isCharging, isPluggedIn, hasBattery } =
    batteryInformation;
  return (
    <Box>
      {hasBattery ? (
        <Box display="flex">
          <Typography fontWeight={600} sx={{ marginX: "0.5rem" }} color="black">
            {`${percentage}%`}
          </Typography>
          <BatteryIcon
            percentage={percentage}
            isCharging={isCharging || isPluggedIn}
          />
        </Box>
      ) : (
        <BatteryUnknownIcon color="error" />
      )}
    </Box>
  );
};

export default function BatteryInfo({
  inHeader = false,
}: {
  inHeader?: boolean;
}): React.ReactNode {
  const { t } = useTranslation();
  const batteryInformation = useContext(BatteryContext);
  if (!batteryInformation) return null;
  const { percentage, hasBattery } = batteryInformation;
  if (inHeader) {
    return <BatteryPercentageInfo batteryInformation={batteryInformation} />;
  }
  return (
    <>
      <Box
        sx={{
          borderRadius: "0.5rem",
          backgroundColor: (theme) => `${theme.palette.primary.light}80`,
          margin: "0.625rem",
          padding: "1.25rem 0.625rem 1.625rem 0.625rem",
        }}
      >
        <>
          <Box display="flex" justifyContent="space-between" mb={3}>
            <Typography fontWeight={600}>
              {t("settings.battery.title")}
            </Typography>
            <BatteryPercentageInfo batteryInformation={batteryInformation} />
          </Box>
          {hasBattery ? (
            <Box>
              <LinearProgress
                variant="determinate"
                value={percentage}
                color={getBatteryPercentageDefinition(percentage)?.color}
              />
            </Box>
          ) : (
            <Typography fontWeight={600}>
              {t("settings.battery.no_battery")}
            </Typography>
          )}
        </>
      </Box>
    </>
  );
}
