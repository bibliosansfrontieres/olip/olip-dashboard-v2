import { useCallback, useEffect, useState } from "react";
import { getBatteryPercentageDefinition } from "src/utils/batteryPercentageDefinition";

export default function BatteryIcon({
  isCharging,
  percentage,
}: {
  isCharging: boolean;
  percentage: number;
}): React.ReactNode {
  const [iconPercentage, setIconPercentage] = useState(percentage);

  const chargingAnimation = useCallback(() => {
    setIconPercentage((prev) => {
      if (prev < 100) return prev + 10;
      return percentage;
    });
  }, [percentage]);

  useEffect(() => {
    let interval = undefined;

    if (isCharging && percentage <= 100) {
      interval = setInterval(chargingAnimation, 500);
    }

    return () => {
      if (interval) clearInterval(interval);
    };
  }, [percentage, chargingAnimation, isCharging]);

  const batteryPercentageDefinition =
    getBatteryPercentageDefinition(percentage);

  if (!batteryPercentageDefinition) return <></>;
  if (isCharging) {
    const chargingBatteryPercentageDefinition =
      getBatteryPercentageDefinition(iconPercentage);
    if (!chargingBatteryPercentageDefinition) return <></>;
    return (
      <chargingBatteryPercentageDefinition.ChargingIcon
        color={batteryPercentageDefinition.color}
      />
    );
  }
  return (
    <batteryPercentageDefinition.DefaultIcon
      color={batteryPercentageDefinition.color}
    />
  );
}
