import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import { SETTINGS_LIST_PAGES } from "src/constants";
import { UserContext } from "src/contexts/user.context";

import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
} from "@mui/material";

import BatteryInfo from "./BatteryInfo/BatteryInfo";
import LogoutButton from "./LogoutButton/LogoutButton";
import SsidInfo from "./SsidInfo/SsidInfo";
import StorageInfo from "./StorageInfo/StorageInfo";

export default function SettingsMenu(): JSX.Element {
  const { t } = useTranslation();
  const location = useLocation();
  const { havePermission } = useContext(UserContext);

  return (
    <Box
      sx={{
        display: { xs: "none", lg: "block" },
        textAlign: "center",
        minWidth: "20rem",
      }}
    >
      <Box
        component="nav"
        sx={{
          backgroundColor: (theme) => `${theme.palette.primary.main}1A`,
          paddingBottom: "3.75rem",
        }}
      >
        <List sx={{ paddingBottom: "1.75rem", paddingTop: "0.75rem" }}>
          {SETTINGS_LIST_PAGES.map((page) => {
            if (havePermission(page.permission)) {
              const actualGroupPage =
                location.pathname.split("/")[2] === page.link.split("/")[2];

              return (
                <ListItem key={page.name}>
                  <ListItemButton
                    component={Link}
                    to={page.link}
                    sx={{
                      backgroundColor: (theme) =>
                        actualGroupPage ? theme.palette.primary.light : "none",
                      borderLeft: (theme) =>
                        `0.5rem solid ${
                          actualGroupPage
                            ? theme.palette.primary.main
                            : "#FFFFFF00"
                        }`,
                      borderRadius: "0.5rem",
                      paddingY: "1rem",
                      span: {
                        fontWeight: actualGroupPage ? 600 : 400,
                      },
                      "&:hover": {
                        backgroundColor: (theme) => theme.palette.primary.light,
                        span: { fontWeight: 600 },
                      },
                    }}
                  >
                    <ListItemText
                      sx={{
                        "& span": {
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                        },
                      }}
                    >
                      {t(page.name)}
                    </ListItemText>
                  </ListItemButton>
                </ListItem>
              );
            }
            return null;
          })}
        </List>
        {havePermission("read_storage") && <StorageInfo />}
        {havePermission("read_ssid") && <SsidInfo />}
        <BatteryInfo />
        <LogoutButton />
      </Box>
    </Box>
  );
}
