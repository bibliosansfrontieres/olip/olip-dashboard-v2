import { Box, CircularProgress, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import useGetSSID from "src/hooks/useGetSSID";

export default function SsidInfo(): JSX.Element | null {
  const { t } = useTranslation();
  const { ssid, loading } = useGetSSID();

  if (ssid) {
    return (
      <Box
        sx={{
          borderRadius: "0.5rem",
          backgroundColor: (theme) => `${theme.palette.primary.light}80`,
          margin: "0.625rem",
          padding: "1.25rem 0.625rem 1.625rem 0.625rem",
        }}>
        <Box display="flex" justifyContent="space-between" mb={3}>
          <Typography fontWeight={600}>{t("settings.ssid.title")}</Typography>
        </Box>
        {loading ? (
          <CircularProgress size={24} />
        ) : ssid ? (
          <Typography fontWeight={600} my={2} textAlign="center">
            {ssid}
          </Typography>
        ) : (
          <Typography
            variant="info"
            fontStyle="italic"
            my={2}
            textAlign="center">
            {t("settings.pages.activity.ssidCard.isNull")}
          </Typography>
        )}
      </Box>
    );
  }
  return null;
}
