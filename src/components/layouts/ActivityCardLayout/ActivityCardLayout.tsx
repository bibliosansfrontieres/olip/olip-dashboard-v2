import { Box } from "@mui/material";
import { ReactNode } from "react";

export default function ActivityCardLayout({
  children,
}: {
  children: ReactNode;
}): JSX.Element {
  return (
    <Box
      className="card"
      display="flex"
      alignItems="center"
      flexDirection="column"
      height={"calc(100% - 2.5rem)"}
      justifyContent="center"
      p={3}
      sx={{
        borderRadius: "1rem",
        boxShadow: "0px 0px 30px 0px rgba(206, 206, 206, 0.38)",
      }}>
      {children}
    </Box>
  );
}
