import { useCallback, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import MenuIcon from "src/assets/icons/Menu";
import LanguageButton from "src/components/layouts/MainLayout/Header/LanguageButton/LanguageButton";
import UserButton from "src/components/layouts/MainLayout/Header/UserButton/UserButton";
import BatteryInfo from "src/components/layouts/SettingsLayout/SettingsMenu/BatteryInfo/BatteryInfo";
import LogoutButton from "src/components/layouts/SettingsLayout/SettingsMenu/LogoutButton/LogoutButton";
import StorageInfo from "src/components/layouts/SettingsLayout/SettingsMenu/StorageInfo/StorageInfo";
import { menuListPages, SETTINGS_LIST_PAGES } from "src/constants";
import { UserContext } from "src/contexts/user.context";

import {
  Box,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  ListSubheader,
} from "@mui/material";

export default function MenuButton(): JSX.Element {
  const { t } = useTranslation();
  const location = useLocation();
  const { havePermission, user } = useContext(UserContext);
  const [isMenuOpened, setIsMenuOpened] = useState(false);

  const toggleDrawer = useCallback(
    (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setIsMenuOpened(open);
    },
    [open],
  );

  return (
    <>
      <Box
        component="button"
        data-testid="menuButton"
        onClick={toggleDrawer(true)}
        sx={{
          alignItems: "center",
          backgroundColor: (theme) => `${theme.palette.primary.main}33`,
          border: 0,
          display: "flex",
          paddingX: "1.75rem",
          "&:hover": {
            cursor: "pointer",
          },
        }}
      >
        <MenuIcon />
      </Box>
      <Drawer
        anchor="right"
        open={isMenuOpened}
        onClose={toggleDrawer(false)}
        sx={{ zIndex: "1202" }}
      >
        <Box>
          <UserButton
            sx={{ height: "7.5rem" }}
            onNavigate={() => setIsMenuOpened(false)}
          />

          <List disablePadding>
            {menuListPages.map((page) => (
              <ListItem key={page.name} disablePadding>
                <ListItemButton
                  data-testid={`drawer-${page.name}`}
                  component={Link}
                  to={page.link}
                  onClick={() => setIsMenuOpened(false)}
                  sx={{
                    borderLeft: (theme) =>
                      `0.5rem solid ${
                        location.pathname === page.link
                          ? theme.palette.primary.main
                          : "#FFFFFF00"
                      }`,
                  }}
                >
                  <ListItemText>{t(page.name)}</ListItemText>
                </ListItemButton>
              </ListItem>
            ))}
            {user && (
              <>
                <Divider sx={{ marginY: "1rem" }} />
                <ListSubheader
                  sx={{ color: (theme) => theme.palette.primary.dark }}
                >
                  {t("header.menu.settings")}
                </ListSubheader>
                {SETTINGS_LIST_PAGES.map((page) => {
                  if (havePermission(page.permission)) {
                    return (
                      <ListItem key={page.name} disablePadding>
                        <ListItemButton
                          component={Link}
                          to={page.link}
                          onClick={() => setIsMenuOpened(false)}
                          sx={{
                            borderLeft: (theme) =>
                              `0.5rem solid ${
                                location.pathname === page.link
                                  ? theme.palette.primary.main
                                  : "#FFFFFF00"
                              }`,
                          }}
                        >
                          <ListItemText>{t(page.name)}</ListItemText>
                        </ListItemButton>
                      </ListItem>
                    );
                  }
                })}
              </>
            )}
          </List>
          {user && (
            <Box mb={2} mt={4} textAlign="center">
              <LogoutButton />
            </Box>
          )}
          <Box
            sx={{
              display: { xs: "flex", sm: "none" },
              height: "5rem",
              justifyContent: "center",
            }}
          >
            <LanguageButton />
          </Box>
          {havePermission("read_storage") && <StorageInfo />}
          <BatteryInfo />
        </Box>
      </Drawer>
    </>
  );
}
