import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { UserContext } from "src/contexts/user.context";
import useModal from "src/hooks/useModal";
import getEnv from "src/utils/getEnv";

import { Avatar, Box, SxProps, Typography } from "@mui/material";

import SignInModal from "./SignInModal/SignInModal";

import defaultProfilePhoto from "/src/assets/images/avatar.png";

export default function UserButton({
  onNavigate,
  sx,
}: {
  onNavigate?: () => void;
  sx?: SxProps;
}): JSX.Element {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const [openModal, toggleModal] = useModal();

  function handleClick() {
    if (user) {
      onNavigate?.();
      navigate("/settings");
    } else {
      toggleModal();
    }
  }

  return (
    <>
      <Box
        component="button"
        onClick={() => handleClick()}
        data-testid="loginButton"
        sx={{
          ...sx,
          alignItems: "center",
          backgroundColor: (theme) => `${theme.palette.primary.main}33`,
          border: 0,
          boxSizing: "content-box",
          display: "flex",
          paddingX: "1.75rem",
          "&:hover": {
            cursor: "pointer",
            "& span": {
              fontWeight: 600,
            },
          },
          width: "12.75rem",
        }}
      >
        <Avatar
          alt={user?.photo ? `Avatar de ${user.username}` : "Avatar par défaut"}
          imgProps={{
            sx: !user?.photo
              ? {
                  objectFit: "none",
                  paddingTop: "1.25rem",
                }
              : {},
          }}
          src={
            user?.photo
              ? getEnv("VITE_AXIOS_BASEURL") + user.photo
              : defaultProfilePhoto
          }
          sx={{
            backgroundColor: (theme) => theme.palette.primary.light,
            height: "4.8125rem",
            width: "4.8125rem",
          }}
        />
        <Box
          pl={2}
          display="flex"
          flexDirection="column"
          sx={{ overflow: "hidden", textOverflow: "ellipsis" }}
        >
          {user ? (
            <>
              <Typography
                variant="body1sb"
                sx={{ overflow: "hidden", textOverflow: "ellipsis" }}
              >
                {user?.username}
              </Typography>{" "}
              <Typography
                variant="link"
                sx={{ fontSize: "0.875rem" }}
                data-testid="myAccountLink"
              >
                {t("header.userButton.account")}
              </Typography>
            </>
          ) : (
            <Typography variant="link" sx={{ fontSize: "0.875rem" }}>
              {t("header.userButton.login")}
            </Typography>
          )}
        </Box>
      </Box>
      <SignInModal open={openModal} onClose={() => toggleModal()} />
    </>
  );
}
