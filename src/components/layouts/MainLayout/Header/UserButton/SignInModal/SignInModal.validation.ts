import * as yup from "yup";

export default yup.object({
  noPassword: yup.boolean(),
  username: yup.string().required("modal.signin.validation.username.mandatory"),
  password: yup.string().when("noPassword", {
    is: false,
    then: (schema) =>
      schema.required("modal.signin.validation.password.mandatory"),
  }),
});
