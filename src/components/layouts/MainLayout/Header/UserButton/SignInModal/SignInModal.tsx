import { useContext, useEffect, useState } from "react";
import {
  Box,
  FormControl,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInput,
  RadioGroup,
  SvgIcon,
  Typography,
} from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { useTranslation } from "react-i18next";
import OLIPCheckbox from "src/components/common/OLIPCheckbox/OLIPCheckbox";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { useFormik } from "formik";
import validationSchema from "./SignInModal.validation";
import { UserContext } from "src/contexts/user.context";
import { useNavigate } from "react-router-dom";
import OLIPDialogActions from "src/components/common/OLIPDialogActions/OLIPDialogActions";

export default function SignInModal({
  onClose,
  open = false,
}: {
  onClose: () => void;
  open: boolean;
}): JSX.Element {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { login } = useContext(UserContext);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [displayGenericError, setDisplayGenericError] =
    useState<boolean>(false);
  const formik = useFormik({
    initialValues: {
      noPassword: false,
      username: "",
      password: "",
    },
    onSubmit: async (values, { setFieldError, setFieldValue }) => {
      setDisplayGenericError(false);
      const result = await login(
        values.username,
        values.noPassword ? undefined : values.password,
      );
      if (result.statusCode === 200) {
        onClose();
        navigate("/");
      } else {
        let listErrors = [];
        if (!Array.isArray(result.errorMessage)) {
          listErrors.push(result.errorMessage);
        } else {
          listErrors = [...result.errorMessage];
        }
        listErrors.forEach((errorMessage) => {
          switch (errorMessage) {
            case "password should not be empty":
            case "password is needed for this user":
              setFieldError(
                "password",
                t(`modal.signin.error.${result.errorMessage}`),
              );
              setFieldValue("noPassword", false, false);
              break;
            case "password is not needed for this user":
              setFieldError(
                "password",
                t(`modal.signin.error.${result.errorMessage}`),
              );
              break;
            case "incorrect password":
              setFieldError(
                "password",
                t(`modal.signin.error.${result.errorMessage}`),
              );
              break;
            case "user not found":
              setFieldError(
                "username",
                t(`modal.signin.error.${result.errorMessage}`, {
                  user: values.username,
                }),
              );
              break;
            default:
              setDisplayGenericError(true);
          }
        });
      }
    },
    validationSchema: validationSchema,
  });

  const handleClose = () => {
    setDisplayGenericError(false);
    onClose();
  };

  useEffect(() => {
    if (!open) {
      formik.resetForm();
    }
  }, [open]);

  return (
    <ModalLayout onClose={handleClose} open={open} fullWidth maxWidth="xs">
      <form onSubmit={formik.handleSubmit} style={{ overflowY: "auto" }}>
        <Box px={4} py={2} sx={{ display: "flex", flexDirection: "column" }}>
          <Typography
            variant="h4"
            sx={{ marginBottom: "2rem" }}
            data-testid="loginTitle"
          >
            {t("modal.signin.title")}
          </Typography>
          <FormControl
            error={formik.touched.username && Boolean(formik.errors.username)}
            sx={{ marginBottom: "1rem", maxWidth: "50ch", width: "100%" }}
          >
            <Typography>{t("common.form.username")}</Typography>
            <OutlinedInput
              id="username"
              name="username"
              type="text"
              color="dark"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              placeholder={t("common.form.username")}
              value={formik.values.username}
              data-testid="usernameInput"
            />
            {formik.touched.username && formik.errors.username && (
              <FormHelperText data-testid="usernameError">
                {t(formik.errors.username)}
              </FormHelperText>
            )}
          </FormControl>
          {!formik.values.noPassword && (
            <FormControl
              error={formik.touched.password && Boolean(formik.errors.password)}
              sx={{ marginBottom: "1rem", maxWidth: "50ch", width: "100%" }}
            >
              <Typography>{t("common.form.password")}</Typography>
              <OutlinedInput
                id="password"
                name="password"
                color="dark"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder={t("common.form.password")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton onClick={() => setShowPassword(!showPassword)}>
                      <SvgIcon
                        component={
                          showPassword ? VisibilityOffIcon : VisibilityIcon
                        }
                      />
                    </IconButton>
                  </InputAdornment>
                }
                type={showPassword ? "text" : "password"}
                value={formik.values.password}
                data-testid="passwordInput"
              />
              {formik.touched.password && formik.errors.password && (
                <FormHelperText data-testid="passwordError">
                  {t(formik.errors.password)}
                </FormHelperText>
              )}
            </FormControl>
          )}
          <FormControl sx={{ marginBottom: "2.5rem" }}>
            <RadioGroup row data-testid="noPasswordCheckbox">
              <FormControlLabel
                id="noPassword"
                name="noPassword"
                control={<OLIPCheckbox />}
                checked={formik.values.noPassword}
                onChange={formik.handleChange}
                label={t("modal.signin.noPassword")}
              />
            </RadioGroup>
          </FormControl>
          {displayGenericError && (
            <FormHelperText error>{t("common.error.generic")}</FormHelperText>
          )}
          <OLIPDialogActions
            dialogActionsProps={{ sx: { paddingX: 0 } }}
            isLoading={formik.isSubmitting}
            mainButton={{
              text: t("modal.signin.button.signin"),
              action: formik.submitForm,
            }}
            id="login"
          />
        </Box>
      </form>
    </ModalLayout>
  );
}
