import SearchAutocomplete from "src/components/common/SearchAutocomplete/SearchAutocomplete";

export default function SearchBar({ ...props }) {
  return <SearchAutocomplete {...props} />;
}
