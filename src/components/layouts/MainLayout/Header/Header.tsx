import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import LanguageButton from "src/components/layouts/MainLayout/Header/LanguageButton/LanguageButton";
import MenuButton from "src/components/layouts/MainLayout/Header/MenuButton/MenuButton";
import SearchBar from "src/components/layouts/MainLayout/Header/SearchBar/SearchBar";
import UserButton from "src/components/layouts/MainLayout/Header/UserButton/UserButton";
import BatteryInfo from "src/components/layouts/SettingsLayout/SettingsMenu/BatteryInfo/BatteryInfo";
import { menuListPages } from "src/constants";
import { LogoContext } from "src/contexts/logo.context";
import getEnv from "src/utils/getEnv";

import {
  AppBar,
  Avatar,
  Box,
  Container,
  Toolbar,
  Typography,
} from "@mui/material";

export default function Header(): JSX.Element {
  const { t } = useTranslation();
  const location = useLocation();
  const { logo } = useContext(LogoContext);

  return (
    <AppBar
      position="relative"
      sx={{
        backgroundColor: (theme) => theme.palette.primary.light,
        boxShadow: "0px 0px 30px 0px rgba(206, 206, 206, 0.38)",
        height: { xs: "5rem", lg: "7.5rem" },
        zIndex: (theme) => theme.zIndex.drawer + 1,
      }}
    >
      <Container disableGutters maxWidth={false} sx={{ height: "100%" }}>
        <Toolbar
          disableGutters
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignContent: "center",
            height: "100%",
            width: "100%",
          }}
        >
          <Box
            id="logo"
            component={Link}
            to="/"
            sx={{
              backgroundSize: "contain",
              marginLeft: { xs: "1rem", sm: "1.5rem" },
            }}
            data-testid="logo"
          >
            <Avatar
              alt={logo ? "Logo" : "Logo par défaut"}
              src={logo ? getEnv("VITE_AXIOS_BASEURL") + logo : ""}
              sx={{
                borderRadius: "0.5rem",
                height: { xs: "3.625rem", lg: "5rem" },
                width: { xs: "3.625rem", lg: "5rem" },
              }}
              variant="rounded"
            />
          </Box>
          <Box
            display="flex"
            height="100%"
            sx={{ display: { xs: "none", lg: "flex" } }}
          >
            {!(
              location.pathname === "/" || location.pathname === "/search"
            ) && <SearchBar sx={{ marginX: "2rem", alignSelf: "center" }} />}
            <Box
              sx={{
                display: "flex",
                height: "100%",
                marginRight: "1.25rem",
              }}
            >
              {menuListPages.map((page, index) => (
                <Box
                  data-testid={page.name}
                  display="flex"
                  alignItems="center"
                  key={page.name}
                  component={Link}
                  to={page.link}
                  sx={{
                    borderBottom: (theme) =>
                      `0.5rem solid ${
                        location.pathname === page.link
                          ? theme.palette.primary.main
                          : "#FFFFFF00"
                      }`,
                    color: "inherit",
                    flexGrow: 1,
                    paddingTop: "0.5rem",
                    marginRight:
                      index + 1 === menuListPages.length ? 0 : "2rem",
                    textDecoration: "none",
                    "& span::after": {
                      fontWeight: 600,
                      visibility: "hidden",
                      overflow: "hidden",
                      userSelect: "none",
                      pointerEvents: "none",
                      height: 0,
                      content: "attr(data-text)",
                    },
                    "&:hover": {
                      fontWeight: 600,
                    },
                  }}
                >
                  <Typography
                    component="span"
                    data-text={t(page.name)}
                    sx={{
                      color: "#363636",
                      display: "inline-flex",
                      flexDirection: "column",
                      alignItems: "center",
                      fontWeight:
                        location.pathname === page.link ? 600 : "inherit",
                    }}
                  >
                    {t(page.name)}
                  </Typography>
                </Box>
              ))}
            </Box>
            <LanguageButton />
            {!location.pathname.includes("settings") && (
              <Box display="flex" alignItems="center" marginX="1rem">
                <BatteryInfo inHeader />
              </Box>
            )}
            <UserButton />
          </Box>
          <Box
            display="flex"
            height="100%"
            sx={{ display: { xs: "none", sm: "flex", lg: "none" } }}
          >
            {!(
              location.pathname === "/" || location.pathname === "/search"
            ) && <SearchBar sx={{ marginX: "2rem", alignSelf: "center" }} />}
            <LanguageButton />
            <MenuButton />
          </Box>
          <Box
            display="flex"
            height="100%"
            sx={{ display: { xs: "flex", sm: "none" } }}
          >
            {!(
              location.pathname === "/" || location.pathname === "/search"
            ) && <SearchBar sx={{ marginX: "2rem", alignSelf: "center" }} />}
            <MenuButton />
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
