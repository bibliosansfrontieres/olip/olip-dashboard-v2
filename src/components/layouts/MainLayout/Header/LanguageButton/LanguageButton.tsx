import { useContext, useState } from "react";
import CheckIcon from "src/assets/icons/Check";
import { supportedLanguages } from "src/constants";
import { LanguageContext } from "src/contexts/language.context";
import { ThemeContext } from "src/contexts/theme.context";

import ExpandLessRoundedIcon from "@mui/icons-material/ExpandLessRounded";
import ExpandMoreRoundedIcon from "@mui/icons-material/ExpandMoreRounded";
import { Box, Grid, Menu, MenuItem, Typography } from "@mui/material";

export default function LanguageButton(): JSX.Element {
  const { changeLanguage, language } = useContext(LanguageContext);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const isMenuOpened = Boolean(anchorEl);
  const { refreshDirection } = useContext(ThemeContext);
  const onClickChangeLanguage = (newLanguage: string) => {
    changeLanguage(newLanguage);
    refreshDirection(newLanguage);
    setAnchorEl(null);
  };
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Box
        component="button"
        data-testid="languageButtonMenu"
        onClick={handleClick}
        sx={{
          alignItems: "center",
          backgroundColor: "inherit",
          border: 0,
          borderLeft: (theme) => ({
            sm: `0.0625rem solid ${theme.palette.grey[200]}`,
          }),
          borderRight: (theme) => ({
            sm: `0.0625rem solid ${theme.palette.grey[200]}`,
          }),
          boxSizing: "content-box",
          display: "flex",
          justifyContent: "space-around",
          paddingY: 0,
          paddingX: "1rem",
          "&:hover": {
            cursor: "pointer",
            "& p": {
              fontWeight: "600",
            },
          },
          width: "10rem",
        }}
        aria-controls={isMenuOpened ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={isMenuOpened ? "true" : undefined}
      >
        <Typography
          fontSize="1rem"
          fontWeight={500}
          lineHeight="1rem"
          sx={{ marginX: "0.5rem" }}
        >
          {supportedLanguages.filter((lg) => lg.id === language)[0].name}
        </Typography>
        {isMenuOpened ? <ExpandLessRoundedIcon /> : <ExpandMoreRoundedIcon />}
      </Box>
      <Menu
        id="basic-menu"
        open={isMenuOpened}
        onClose={handleClose}
        anchorEl={anchorEl}
        sx={{
          maxHeight: { xs: "30rem", sm: "40rem" },
        }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <Grid
          container
          direction={"column"}
          sx={{
            paddingX: "0.5rem",
            justifyContent: "space-around",
          }}
        >
          {supportedLanguages.map((suLanguage) => (
            <Grid
              item
              key={suLanguage.id}
              sx={{
                display: "grid",
                paddingTop: "0.5rem",
              }}
            >
              <MenuItem
                data-testid={`languageButtonStack-${suLanguage.id}`}
                component="button"
                onClick={() => onClickChangeLanguage(suLanguage.id)}
                sx={{
                  backgroundColor: "inherit",
                  border: (theme) =>
                    language === suLanguage.id
                      ? `0.125rem solid ${theme.palette.primary.dark}`
                      : "none",
                  borderRadius: "0.5rem",
                  display: "flex",
                  justifyContent: "space-between",
                  marginX: { xs: "0.5rem", md: "1rem" },
                  "&:focus, &:hover": {
                    backgroundColor: (theme) => theme.palette.grey[100],
                  },
                }}
              >
                <Box
                  display="flex"
                  alignItems="center"
                  sx={{ maxWidth: "8.5rem" }}
                >
                  {language === suLanguage.id && <CheckIcon fontSize="small" />}
                  <Typography
                    fontSize="1rem"
                    fontWeight={500}
                    lineHeight="1.5rem"
                    sx={{ marginX: "0.5rem" }}
                    noWrap
                  >
                    {suLanguage.name}
                  </Typography>
                </Box>
              </MenuItem>
            </Grid>
          ))}
        </Grid>
      </Menu>
    </>
  );
}
