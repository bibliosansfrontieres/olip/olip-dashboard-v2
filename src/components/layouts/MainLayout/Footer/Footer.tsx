import { Box, Typography } from "@mui/material";
import OLIPIcon from "src/assets/icons/OLIP";
import BSFLogo from "/src/assets/images/BSF_logo.png";

export default function Footer(): JSX.Element {
  return (
    <Box
      component="footer"
      sx={{
        alignItems: "center",
        backgroundColor: (theme) => theme.palette.primary.light,
        display: "flex",
        justifyContent: "space-between",
        height: "2.5rem",
        paddingLeft: "2.5rem",
        paddingRight: "1.5rem",
      }}
    >
      <Box display="flex" alignItems="center" height="100%" width="100%">
        <OLIPIcon sx={{ fontSize: "2.5rem" }} data-testid="OLIPIcon" />
        <Typography
          variant="body2"
          fontWeight={600}
          mx={1.5}
          data-testid="OLIPText"
        >
          is made with love by
        </Typography>
        {
          <img
            src={BSFLogo}
            alt="BSF Logo"
            style={{ filter: "grayscale(100%)", height: "100%" }}
            data-testid="BSFLogo"
          />
        }
      </Box>
      <Typography variant="body2" fontWeight={600} ml={2} data-testid="year">
        2025
      </Typography>
    </Box>
  );
}
