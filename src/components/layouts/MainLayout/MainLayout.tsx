import { Outlet, useLocation } from "react-router-dom";

import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import { Box, Container } from "@mui/material";
import { useContext, useEffect } from "react";
import { ThemeContext } from "src/contexts/theme.context";

export default function MainLayout(): JSX.Element {
  const { pathname } = useLocation();
  const { direction } = useContext(ThemeContext);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{
        display: "flex",
        flexDirection: "column",
        height: "100vh",
      }}
      dir={direction}
    >
      <Header />
      <Box sx={{ flex: "1" }}>
        <Outlet />
      </Box>
      <Footer />
    </Container>
  );
}
