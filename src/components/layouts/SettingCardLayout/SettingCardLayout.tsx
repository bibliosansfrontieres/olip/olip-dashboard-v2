import { PropsWithChildren, ReactElement, ReactNode } from "react";

import { Box, Typography } from "@mui/material";

type Props = PropsWithChildren & {
  title?: string;
  description?: string;
  editButton?: ReactNode;
};
export default function SettingCardLayout({
  title,
  children,
  description,
  editButton,
}: Props): ReactElement {
  return (
    <Box
      component="section"
      data-testid="settingCardLayout"
      className="card"
      p={3.75}
      sx={{
        borderRadius: "1rem",
        boxShadow: "0px 0px 30px 0px rgba(206, 206, 206, 0.38)",
      }}
    >
      <Box
        display="flex"
        justifyContent="space-between"
        sx={{ marginBottom: "1.5rem" }}
      >
        <Box>
          <Box>
            <Typography
              variant="h4"
              mr={1}
              data-testid="settingCardLayoutTitle"
            >
              {title}
            </Typography>
            <Typography>{description}</Typography>
          </Box>
        </Box>
        <Box sx={{ display: { xs: "none", sm: "block" } }}>{editButton}</Box>
      </Box>
      {children}
      <Box
        sx={{
          display: { xs: "block", sm: "none" },
          marginTop: "2rem",
          "& button": { width: "100%" },
        }}
      >
        {editButton}
      </Box>
    </Box>
  );
}
