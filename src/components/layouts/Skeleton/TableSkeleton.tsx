import { Skeleton } from "@mui/material";

export default function TableSkeleton({ nbLines }: { nbLines: number }) {
  return (
    <>
      {Array(nbLines)
        .fill("")
        .map((_notUse, index) => (
          <Skeleton
            key={index}
            variant="rounded"
            sx={{
              height: "4.5rem",
              marginY: "0.5rem",
            }}
          />
        ))}
    </>
  );
}
