import { Box, Pagination } from "@mui/material";
import { PropsWithChildren, ReactElement, useEffect } from "react";
import DisplayCountSelect from "src/components/common/DisplayCountSelect/DisplayCountSelect";
import { Pagination as PaginationType } from "src/types";

type Props = PropsWithChildren & {
  meta?: PaginationType;
  onChange: (newParams: { page: number; take: number }) => void;
  takeList?: string[];
};
export default function PaginationLayout({
  children,
  meta,
  onChange,
  takeList = ["12", "24", "48"],
}: Props): ReactElement {
  useEffect(() => {
    if (meta && meta.pageCount > 0 && meta.page > meta.pageCount) {
      onChange({
        page: meta.pageCount,
        take: meta.take,
      });
    }
  }, [meta]);

  const handleChangeCount = (event: any) =>
    onChange({
      page: 1,
      take: event.target.value,
    });

  const handlePagination = (
    _event: React.ChangeEvent<unknown>,
    value: number,
  ) => {
    if (meta) {
      onChange({
        page: value,
        take: meta.take,
      });
    }
  };

  return (
    <>
      {children}
      {meta && (
        <Box
          data-testid="boxPaginationLayout"
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          my={2}
          sx={{
            flexDirection: {
              xs: "column",
              md: "row",
            },
          }}
        >
          <DisplayCountSelect
            choices={takeList}
            onChange={handleChangeCount}
            values={String(meta.take) || takeList[0]}
            sx={{ width: "auto" }}
            sxSelect={{ paddingRight: "1.25rem" }}
          />
          <Pagination
            count={meta.pageCount}
            onChange={handlePagination}
            page={Number(meta.page)}
            shape="rounded"
            sx={{ marginTop: "1rem" }}
          />
        </Box>
      )}
    </>
  );
}
