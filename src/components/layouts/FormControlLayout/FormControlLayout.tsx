import { FormControl, FormHelperText, Typography } from "@mui/material";
import { PropsWithChildren, ReactElement } from "react";
import { useTranslation } from "react-i18next";

type Props = PropsWithChildren & {
  error?: string;
  label: string;
  touched?: boolean;
};

export default function FormControlLayout({
  children,
  error,
  label,
  touched,
}: Props): ReactElement {
  const { t } = useTranslation();
  return (
    <FormControl
      error={touched && Boolean(error)}
      fullWidth
      data-testid="formControlLayout"
    >
      <Typography>{label}</Typography>
      {children}
      {touched && Boolean(error) && (
        <FormHelperText data-testid="formHelperText">
          {t(error || "")}
        </FormHelperText>
      )}
    </FormControl>
  );
}
