import { useTranslation } from "react-i18next";
import {
  Box,
  Button,
  Dialog,
  DialogProps,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

export default function ModalLayout({
  disableCloseButton,
  ...props
}: DialogProps & { disableCloseButton?: boolean }): JSX.Element {
  const { t } = useTranslation();
  const theme = useTheme();

  const { children, onClose } = props;

  return (
    <Dialog
      fullScreen={useMediaQuery(theme.breakpoints.down("sm"))}
      {...props}
      PaperProps={{
        sx: {
          backgroundColor: (theme) => theme.palette.primary.light,
          borderRadius: "1rem",
          boxShadow: "0px 0px 30px 0px rgba(206, 206, 206, 0.38)",
          display: "flex",
          flexDirection: "column",
          maxHeight: "90vh",
          overflow: "hidden",
          padding: { xs: "1rem", sm: "1.875rem" },
        },
      }}>
      {!disableCloseButton && (
        <Box
          sx={{ display: "flex", justifyContent: "flex-end", width: "100%" }}>
          <Button
            variant="text"
            endIcon={<CloseRoundedIcon />}
            onClick={() => onClose?.({}, "backdropClick")}>
            {t("modal.close")}
          </Button>
        </Box>
      )}
      {children}
    </Dialog>
  );
}
