import { PropsWithChildren, useEffect, useState } from "react";
import { socket } from "src/socket.io";

export default function MainWebSocket({
  children,
}: PropsWithChildren): JSX.Element {
  const [, setIsConnected] = useState(socket.connected);

  useEffect(() => {
    function onConnect() {
      setIsConnected(true);
    }

    function onDisconnect() {
      setIsConnected(false);
    }

    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);

    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
    };
  }, []);

  return <>{children}</>;
}
