import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { LanguageContext } from "src/contexts/language.context";
import useEdito from "src/hooks/useEdito";
import getEnv from "src/utils/getEnv";

import {
  Box,
  Button,
  Container,
  Grid,
  Skeleton,
  Typography,
} from "@mui/material";

export default function EditoSection(): JSX.Element | null {
  const { t } = useTranslation();
  const { language } = useContext(LanguageContext);
  const { edito } = useEdito({
    language: language,
    checkIfAvailable: true,
  });

  if (!edito) {
    return null;
  }

  return (
    <Container
      maxWidth="lg"
      sx={{ paddingY: { xs: "2.25rem", lg: "6.25rem" } }}
    >
      <Grid container spacing={{ xs: 6, lg: 11 }}>
        <Grid item xs={12} lg={6} display="flex" justifyContent="center">
          {edito ? (
            <Box
              sx={{
                backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
                  edito?.image
                })`,
                backgroundPosition: "center",
                backgroundSize: "cover",
                borderRadius: "10rem",
                flex: 1,
                aspectRatio: "4/3",
                maxWidth: { xs: "75%", sm: "50%", lg: "100%" },
              }}
            />
          ) : (
            <Skeleton
              variant="rectangular"
              sx={{
                aspectRatio: "4/3",
                borderRadius: "10rem",
                height: "100%",
                width: { xs: "75%", sm: "50%", lg: "100%" },
              }}
            />
          )}
        </Grid>
        <Grid
          item
          xs={12}
          lg={6}
          display="flex"
          flexDirection="column"
          justifyContent="center"
          sx={{ alignItems: { xs: "center", lg: "flex-start" } }}
        >
          {edito ? (
            <>
              <Typography variant="h2" mb={{ xs: "2.25rem", lg: "3rem" }}>
                {edito.editoTranslations[0].title}
              </Typography>
              <Typography mb="1.5rem" style={{ whiteSpace: "pre-wrap" }}>
                {edito.editoTranslations[0].description}
              </Typography>
              <Box>
                <Button component={Link} to="/edito" variant="outlined">
                  {t("home.edito.button")}
                </Button>
              </Box>
            </>
          ) : (
            <>
              <Skeleton
                variant="rectangular"
                sx={{
                  height: "3.5rem",
                  marginBottom: { xs: "2.25rem", lg: "4rem" },
                  width: "100%",
                }}
              />
              <Skeleton
                variant="rectangular"
                sx={{ height: "18rem", marginBottom: "1.5rem", width: "100%" }}
              />
              <Skeleton
                variant="rectangular"
                sx={{ height: "3.5rem", width: "10rem" }}
              />
            </>
          )}
        </Grid>
      </Grid>
    </Container>
  );
}
