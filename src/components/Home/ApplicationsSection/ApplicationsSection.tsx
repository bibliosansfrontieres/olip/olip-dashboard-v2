import { ReactElement, useCallback, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import ApplicationCard from "src/components/Applications/ApplicationCard/ApplicationCard";
import { LanguageContext } from "src/contexts/language.context";
import useNewApi from "src/hooks/useNewApi";
import { getApplications } from "src/services/application.service";
import { GetApplicationsResult } from "src/types/application.type";

import { Box, Button, Container, Grid, Typography } from "@mui/material";

export default function ApplicationsSection(): ReactElement | null {
  const { t } = useTranslation();
  const { language: languageIdentifier } = useContext(LanguageContext);
  const { result } = useNewApi<GetApplicationsResult>(
    useCallback(async () => {
      return await getApplications({
        status: "installed",
        take: 1000,
        languageIdentifier,
      });
    }, [languageIdentifier]),
  );
  const [seeAll, setSeeAll] = useState<boolean>(false);

  if (result?.applications && result.applications.length > 0) {
    return (
      <Container
        data-testid="applicationsSection"
        component="section"
        maxWidth={false}
        sx={{
          backgroundColor: (theme) => `${theme.palette.primary.main}1A`,
          paddingBottom: "5rem",
          paddingTop: "3.75rem",
        }}
      >
        <Container maxWidth="xl">
          <Box display="flex" justifyContent="center" mb={8}>
            <Typography variant="h2">{t("home.applications.title")}</Typography>
          </Box>
          <Grid
            container
            spacing={3.75}
            sx={{ marginBottom: "2rem" }}
            alignItems="stretch"
          >
            {result.applications
              .slice(0, seeAll ? 1000 : 4)
              .map((application) => (
                <Grid
                  item
                  key={application.id}
                  xs={12}
                  sm={6}
                  md={6}
                  lg={3}
                  xl={3}
                >
                  <ApplicationCard application={application} />
                </Grid>
              ))}
          </Grid>
          {result.applications.length > 4 && (
            <Box display="flex" justifyContent="center">
              <Button variant="outlined" onClick={() => setSeeAll(!seeAll)}>
                {t(
                  seeAll
                    ? "home.applications.seeLess"
                    : "home.applications.seeAll",
                )}
              </Button>
            </Box>
          )}
        </Container>
      </Container>
    );
  }
  return null;
}
