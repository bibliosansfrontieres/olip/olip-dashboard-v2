import { Box, Button, SxProps, Typography } from "@mui/material";
import ArrowForwardRoundedIcon from "@mui/icons-material/ArrowForwardRounded";
import { Item } from "src/types/item.type";
import { Link, useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import ContentType from "src/components/common/ContentType/ContentType";
import ApplicationChip from "src/components/common/ApplicationChip/ApplicationChip";
import LaunchIcon from "@mui/icons-material/Launch";
import getEnv from "src/utils/getEnv";

export default function HighlightCard({
  item,
  sxCard,
}: {
  item: Item;
  sxCard?: SxProps;
}): JSX.Element {
  const { t } = useTranslation();
  const { categoryId } = useParams();

  function fromExternalApplication(item: Item): boolean {
    return item.application && !item.application.isOlip;
  }

  return (
    <Box
      component={fromExternalApplication(item) ? "a" : Link}
      to={
        fromExternalApplication(item)
          ? undefined
          : `/item/${item.id}${categoryId ? `?category=${categoryId}` : ""}`
      }
      href={
        fromExternalApplication(item)
          ? item?.url
            ? "http://" + item.url
            : item.application?.url
            ? "http://" + item.application.url
            : undefined
          : undefined
      }
      target={fromExternalApplication(item) ? "_blank" : undefined}
      rel={fromExternalApplication(item) ? "noreferrer" : undefined}
      sx={{
        backgroundColor: (theme) => theme.palette.primary.light,
        borderRadius: "1rem",
        border: (theme) => `0.125rem solid ${theme.palette.dark.main}00`,
        color: "inherit",
        display: "flex",
        padding: "1rem",
        textDecoration: "none",
        height: "9.25rem",
        "&:hover": {
          border: (theme) => `0.125rem solid ${theme.palette.dark.main}`,
          "& button": {
            fontWeight: "600",
          },
        },
        ...sxCard,
      }}
    >
      <Box
        sx={{
          backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
            item.thumbnail
          })`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderRadius: "0.5rem",
          marginRight: "2rem",
          flex: 1,
          width: "100%",
        }}
      >
        {item?.dublinCoreItem?.dublinCoreType?.id && (
          <Box
            sx={{
              borderRadius: "0.5rem",
              backgroundColor: (theme) => theme.palette.primary.light,
              width: "fit-content",
              margin: "0.625rem",
              padding: "0.5rem",
            }}
          >
            {fromExternalApplication(item) ? (
              <ApplicationChip application={item.application} />
            ) : (
              <ContentType
                typeId={item.dublinCoreItem.dublinCoreType.id}
                iconProps={{ fontSize: "small" }}
              />
            )}
          </Box>
        )}
      </Box>
      <Box
        flex="2"
        display="flex"
        alignItems="flex-start"
        flexDirection="column"
        justifyContent="space-between"
      >
        <Typography variant="body1m">
          {item.dublinCoreItem.dublinCoreItemTranslations[0].title}
        </Typography>
        <Button
          id={`button-access-${item.id}`}
          endIcon={
            fromExternalApplication(item) ? (
              <LaunchIcon />
            ) : (
              <ArrowForwardRoundedIcon />
            )
          }
          sx={{ paddingLeft: 0 }}
        >
          {t(`item.card.link.${item?.dublinCoreItem?.dublinCoreType?.id || 1}`)}
        </Button>
      </Box>
    </Box>
  );
}
