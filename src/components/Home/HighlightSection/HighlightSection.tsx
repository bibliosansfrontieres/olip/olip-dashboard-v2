import { useContext } from "react";
import HighlightCard from "src/components/Home/HighlightSection/HighlightCard/HighlightCard";
import HighlightCarousel from "src/components/Home/HighlightSection/HighlightCarousel/HighlightCarousel";
import HighlightMainCard from "src/components/Home/HighlightSection/HighlightMainCard/HighlightMainCard";
import { LanguageContext } from "src/contexts/language.context";
import useHighlight from "src/hooks/useHighlight";

import {
  Box,
  Container,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";

export default function HighlightSection(): JSX.Element | null {
  const theme = useTheme();
  const { language } = useContext(LanguageContext);
  const isMobileMode = !useMediaQuery(theme.breakpoints.up("sm"));
  const { highlight } = useHighlight({
    language,
    checkIfAvailable: true,
  });

  if (
    !highlight ||
    !highlight.highlightContents ||
    highlight.highlightContents.length === 0
  ) {
    return null;
  }

  const highlightToDisplay = highlight?.highlightContents.filter(
    ({ isDisplayed }) => isDisplayed,
  );

  return (
    <Container
      component="section"
      maxWidth={false}
      sx={{
        backgroundColor: (t) => `${t.palette.primary.main}1A`,
        paddingBottom: "6.5rem",
        paddingTop: "3.75rem",
      }}
    >
      <Container maxWidth="xl">
        <Box display="flex" justifyContent="center" mb={8}>
          <Typography variant="h2">
            {highlight.highlightTranslations[0].title}
          </Typography>
        </Box>
        {isMobileMode ? (
          <HighlightCarousel items={highlightToDisplay} />
        ) : (
          <>
            {highlightToDisplay.length >= 3 && (
              <Grid
                container
                display="flex"
                alignItems="stretch"
                spacing={3.75}
                direction={{ xs: "column", lg: "row" }}
              >
                <Grid item display="flex" xs={6}>
                  <HighlightMainCard item={highlightToDisplay[0].item} />
                </Grid>
                <Grid
                  item
                  display="flex"
                  xs={6}
                  flexDirection="column"
                  sx={{ height: "39rem" }}
                >
                  <Stack spacing={3}>
                    {highlightToDisplay.slice(1).map(({ item }) => (
                      <HighlightCard key={item.id} item={item} />
                    ))}
                  </Stack>
                </Grid>
              </Grid>
            )}
            {highlightToDisplay.length === 2 && (
              <Grid
                container
                display="flex"
                alignItems="stretch"
                spacing={3.75}
              >
                <Grid item display="flex" xs={6}>
                  <HighlightMainCard item={highlightToDisplay[0].item} />
                </Grid>
                <Grid item display="flex" xs={6} sx={{ height: "39rem" }}>
                  <HighlightMainCard item={highlightToDisplay[1].item} />
                </Grid>
              </Grid>
            )}
            {highlightToDisplay.length === 1 && (
              <Grid
                container
                display="flex"
                alignItems="stretch"
                spacing={3.75}
                justifyContent="center"
              >
                <Grid item display="flex" xs={6} sx={{ height: "39rem" }}>
                  <HighlightMainCard item={highlightToDisplay[0].item} />
                </Grid>
              </Grid>
            )}
          </>
        )}
      </Container>
    </Container>
  );
}
