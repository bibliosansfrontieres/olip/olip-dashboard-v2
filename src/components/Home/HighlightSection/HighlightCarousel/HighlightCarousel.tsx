import HighlightMainCard from "../HighlightMainCard/HighlightMainCard";
import { HighlightContent } from "src/types/highlightContent.type";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Grid } from "@mui/material";
import DotSelectIcon from "src/assets/icons/DotSelect";
import DotIcon from "src/assets/icons/Dot";

export default function HighlightCarousel({
  items,
}: {
  items: Array<HighlightContent>;
}): JSX.Element {
  const settings = {
    appendDots: (dots: any) => {
      return (
        <ul style={{ bottom: "-3rem" }}>
          {dots.map((dot: any) => (
            <li key={dot.key} style={{ margin: "0 0.9375rem" }}>
              {dot.props.className.includes("slick-active") ? (
                <DotSelectIcon
                  fontSize="small"
                  onClick={dot.props.children.props.onClick}
                />
              ) : (
                <DotIcon
                  fontSize="small"
                  onClick={dot.props.children.props.onClick}
                />
              )}
            </li>
          ))}
        </ul>
      );
    },

    dots: true,
    infinite: true,
    speed: 500,
    slidesToScroll: 1,
  };

  return (
    <Grid>
      <Slider {...settings}>
        {items.map((highlightContent) => (
          <div key={highlightContent.id}>
            <Grid item display="flex" xs={6}>
              <HighlightMainCard
                item={highlightContent.item}
                sxCard={{ minHeight: "25rem" }}
              />
            </Grid>
          </div>
        ))}
      </Slider>
    </Grid>
  );
}
