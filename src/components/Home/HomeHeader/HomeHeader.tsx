import { Box, Container, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import SearchBar from "src/components/layouts/MainLayout/Header/SearchBar/SearchBar";

export default function HomeHeader(): JSX.Element {
  const { t } = useTranslation();

  return (
    <Container
      maxWidth={false}
      sx={{
        backgroundColor: (theme) => `${theme.palette.primary.main}1A`,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "30rem",
      }}
    >
      <Box
        sx={{
          marginX: { xs: "1.25rem", sm: "3.25rem", lg: "14rem" },
          textAlign: "center",
        }}
      >
        <Typography variant="h1" data-testid="homeHeaderTitle">
          {t("home.header.title", { projectName: "OLIP" })}
        </Typography>
        <SearchBar
          sx={{
            marginTop: "3rem",
          }}
        />
      </Box>
    </Container>
  );
}
