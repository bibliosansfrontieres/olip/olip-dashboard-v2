import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import CategoryCardList from "src/components/Category/CategoryCardList/CategoryCardList";
import { GetCategoriesListResult } from "src/types/category.type";

import { Box, Container, Typography } from "@mui/material";

export default function CategoriesSection({
  result,
}: {
  result: GetCategoriesListResult;
}): ReactElement {
  const { t } = useTranslation();
  return (
    <Container
      data-testid="categoriesSection"
      maxWidth={false}
      sx={{
        backgroundColor: (theme) => `${theme.palette.grey[200]}33`,
        paddingY: { xs: "2rem", lg: "5rem" },
      }}
    >
      <Container maxWidth="lg">
        <Box display="flex" justifyContent="center">
          <Typography variant="h2" data-testid="categoriesSectionTitle">
            {t("home.categories.title", { count: result.categories.length })}
          </Typography>
        </Box>
        <Box
          sx={{
            my: "3rem",
            textAlign: "center",
          }}
        >
          <CategoryCardList
            categories={result.categories}
            initialDisplayCount={8}
          />
        </Box>
      </Container>
    </Container>
  );
}
