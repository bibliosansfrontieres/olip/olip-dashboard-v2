import { ReactElement, useCallback, useContext } from "react";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import CategoriesSection from "src/components/Home/CategoriesSection/CategoriesSection";
import CategoriesSelectionSection from "src/components/Home/CategoriesSection/CategoriesSelectionSection/CategoriesSelectionSection";
import { LanguageContext } from "src/contexts/language.context";
import useNewApi from "src/hooks/useNewApi";
import { getCategoriesList } from "src/services/category.service";
import { GetCategoriesListResult } from "src/types/category.type";

export default function CategoriesSections(): ReactElement {
  const { language: languageIdentifier } = useContext(LanguageContext);

  const categoriesListResult = useNewApi<GetCategoriesListResult>(
    useCallback(
      () =>
        getCategoriesList({
          languageIdentifier,
        }),
      [languageIdentifier],
    ),
  );

  return (
    <WaitApiResult
      useApiResult={categoriesListResult}
      errorTranslationKey="home.categories.error"
      resultChildren={(result) => {
        return (
          <>
            <CategoriesSection result={result} />
            <CategoriesSelectionSection
              result={result}
              languageIdentifier={languageIdentifier}
            />
          </>
        );
      }}
    />
  );
}
