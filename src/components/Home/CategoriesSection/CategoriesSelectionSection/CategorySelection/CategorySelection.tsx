import { memo, ReactElement, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import ItemCard from "src/components/Item/ItemCard/ItemCard";
import useNewApi from "src/hooks/useNewApi";
import { getCategorySelection } from "src/services/category.service";
import { CategoryWithTranslation } from "src/types/category.type";

import { Box, Button, Grid, Typography } from "@mui/material";

type Props = {
  category: CategoryWithTranslation;
  languageIdentifier: string;
};

function CategorySelection({
  category,
  languageIdentifier,
}: Props): ReactElement | null {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const { result } = useNewApi(
    useCallback(
      () =>
        getCategorySelection({
          categoryId: category.id,
          languageIdentifier,
        }),
      [category.id, languageIdentifier],
    ),
  );

  if (
    result?.categoryPlaylistItems &&
    result.categoryPlaylistItems?.length > 0
  ) {
    return (
      <Box key={category.id} mb={5} mt={10} data-testid="categorySelection">
        <Box display="flex" justifyContent="space-between" mb={4} gap={2}>
          <Typography variant="h3" data-testid="categorySelectionTitle">
            {category.categoryTranslations[0].title}
          </Typography>
          <Button
            variant="outlined"
            onClick={() => navigate(`/category/${category.id}`)}
            data-testid="categorySelectionButton"
          >
            {t("home.categorySelection.button")}
          </Button>
        </Box>
        <Grid container spacing={3.75}>
          {result.categoryPlaylistItems.map(({ item }) => (
            <Grid item key={item.id} xs={12} sm={6} md={4} lg={3} xl={3}>
              <ItemCard item={item} />
            </Grid>
          ))}
        </Grid>
      </Box>
    );
  }
  return null;
}

export default memo(CategorySelection, (prevProps, nextProps) => {
  return (
    JSON.stringify(prevProps.category) === JSON.stringify(nextProps.category)
  );
});
