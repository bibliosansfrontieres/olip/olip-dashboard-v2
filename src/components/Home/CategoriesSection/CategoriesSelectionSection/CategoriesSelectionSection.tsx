import { memo, ReactElement } from "react";
import { useTranslation } from "react-i18next";
import CategorySelection from "src/components/Home/CategoriesSection/CategoriesSelectionSection/CategorySelection/CategorySelection";
import { GetCategoriesListResult } from "src/types/category.type";

import { Container, Typography } from "@mui/material";

function CategoriesSelectionSection({
  result,
  languageIdentifier,
}: {
  result: GetCategoriesListResult;
  languageIdentifier: string;
}): ReactElement {
  const { t } = useTranslation();

  return (
    <Container
      data-testid="categoriesSelectionSection"
      maxWidth="lg"
      sx={{
        paddingY: { xs: "2rem", lg: "5rem" },
      }}
    >
      <Typography variant="h2" data-testid="categoriesSelectionSectionTitle">
        {t("home.categorySelection.title")}
      </Typography>

      {result.categories.map((category) => (
        <CategorySelection
          key={category.id}
          category={category}
          languageIdentifier={languageIdentifier}
        />
      ))}
    </Container>
  );
}

export default memo(
  CategoriesSelectionSection,
  (prevProps, nextProps) =>
    JSON.stringify(prevProps.result) === JSON.stringify(nextProps.result),
);
