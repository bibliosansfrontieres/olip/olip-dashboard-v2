import EditoSection from "./EditoSection/EditoSection";
import HomeHeader from "./HomeHeader/HomeHeader";
import HighlightSection from "./HighlightSection/HighlightSection";
import ApplicationsSection from "./ApplicationsSection/ApplicationsSection";
import CategoriesSections from "./CategoriesSection/CategoriesSections";

export default function HomePage(): JSX.Element {
  return (
    <>
      <HomeHeader />
      <EditoSection />
      <HighlightSection />
      <CategoriesSections />
      <ApplicationsSection />
    </>
  );
}
