import { memo, ReactNode, useEffect, useState } from "react";
import CheckIcon from "src/assets/icons/Check";
import ChevronDownIcon from "src/assets/icons/ChevronDown";

import {
  Box,
  FormControl,
  InputAdornment,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  SxProps,
  Typography,
} from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select/SelectInput";

export type Choice = { icon?: string; name: string; value: string };
export type OLIPSelectProps = {
  choices: string[] | Array<Choice>; //Choix possibles
  onChange: (event: SelectChangeEvent<string[] | string>) => void;
  placeholder?: string;
  sx?: SxProps;
  sxSelect?: SxProps;
  values: string | string[];
  valuesSecondary?: string | string[];
};

export default memo(function OLIPSelect({
  choices,
  onChange,
  placeholder,
  sx,
  sxSelect,
  values,
  valuesSecondary,
}: OLIPSelectProps): JSX.Element | null {
  const [formattedChoices, setFormattedChoices] = useState<
    Array<Choice> | undefined
  >(undefined);

  useEffect(() => {
    setFormattedChoices(
      choices.map((choice) =>
        typeof choice === "string"
          ? { name: choice as string, value: choice }
          : (choice as Choice),
      ),
    );
  }, [choices]);

  function renderValues(selected: any): ReactNode {
    if (selected.length === 0) {
      return placeholder;
    }

    const valueToDisplay = Array.isArray(selected)
      ? selected
          .map(
            (value) =>
              formattedChoices?.find((choice) => choice.value === value)?.name,
          )
          .join(", ")
      : formattedChoices?.find((choice) => choice.value.toString() === selected)
          ?.name;

    return (
      <>
        <Typography>{valueToDisplay}</Typography>
        {valuesSecondary && (
          <Typography variant="body2">{valuesSecondary}</Typography>
        )}
      </>
    );
  }

  if (!formattedChoices) {
    return null;
  }
  return (
    <FormControl sx={sx || { my: 1, width: 300 }}>
      <Select
        data-testid="olipSelect"
        displayEmpty
        IconComponent={ChevronDownIcon}
        multiple={Array.isArray(values)}
        value={values}
        onChange={onChange}
        input={
          <OutlinedInput
            color="dark"
            placeholder={placeholder}
            startAdornment={
              <InputAdornment position="start">
                {formattedChoices?.find(
                  (choice) =>
                    choice.value ===
                      (Array.isArray(values) ? values[0] : values) &&
                    choice.icon,
                ) && (
                  <Box
                    height="1.5rem"
                    mr={2}
                    width="1.5rem"
                    sx={{
                      backgroundImage: `url(${
                        formattedChoices?.find(
                          (choice) =>
                            choice.value ===
                            (Array.isArray(values) ? values[0] : values),
                        )?.icon
                      })`,
                      backgroundPositionY: "center",
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "contain",
                      borderRadius: "0.0625rem",
                    }}
                  ></Box>
                )}
              </InputAdornment>
            }
          />
        }
        MenuProps={{
          MenuListProps: {
            sx: {
              borderRadius: "1.25rem",
              margin: "0",
              padding: "0.62rem",
              "& li": {
                borderRadius: "0.25rem",
                "&:hover": {
                  backgroundColor: (theme) => theme.palette.grey[200],
                },
                "&.Mui-selected": {
                  backgroundColor: (theme) => theme.palette.primary.dark,
                  color: (theme) => theme.palette.primary.light,
                  "&:focus, &:hover": {
                    backgroundColor: (theme) => theme.palette.grey[200],
                    color: (theme) => theme.palette.primary.dark,
                  },
                },
              },
            },
          },
          PaperProps: {
            sx: {
              border: (theme) => `0.125rem solid ${theme.palette.primary.dark}`,
              borderRadius: "0.5rem",
              boxShadow: "none",
              marginTop: "0.31rem",
            },
          },
        }}
        renderValue={renderValues}
        sx={{
          "& svg": {
            marginRight: "0.75rem",
            width: "1.25rem",
          },
          textAlign: "left",
          ...sxSelect,
        }}
      >
        {placeholder !== undefined && (
          <MenuItem value="">
            <ListItemText primary={placeholder} />
          </MenuItem>
        )}
        {formattedChoices?.map((choice) => {
          const isSelected = Array.isArray(values)
            ? values.findIndex((value) => value === choice.value) !== -1
            : values === choice.value;
          return (
            <MenuItem
              data-testid="menuItem"
              key={choice.value}
              value={choice.value}
            >
              {choice.icon && (
                <Box
                  height="1.5rem"
                  mr={2}
                  width="1.5rem"
                  sx={{
                    backgroundImage: `url(${choice.icon})`,
                    backgroundPositionY: "center",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "contain",
                    borderRadius: "0.0625rem",
                  }}
                ></Box>
              )}
              <ListItemText primary={choice.name} />
              {isSelected && (
                <CheckIcon fontSize="small" sx={{ marginLeft: "1rem" }} />
              )}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
});
