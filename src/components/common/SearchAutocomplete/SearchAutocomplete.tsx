import { useEffect, useState } from "react";
import { useFormik } from "formik";
import {
  Autocomplete,
  FormControl,
  FormControlProps,
  IconButton,
  InputAdornment,
  OutlinedInput,
  SvgIcon,
  Typography,
} from "@mui/material";
import { GetSuggestionsResult } from "src/types/search.type";
import useAPI from "src/hooks/useAPI";
import { getSuggestions } from "src/services/search.service";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import SearchIcon from "src/assets/icons/SearchIcon";
import { useLocation, useNavigate } from "react-router-dom";
import { BaseAPIResult } from "src/types";
import { useTranslation } from "react-i18next";

type FormValues = {
  searchInput: string | null;
};
type Props = FormControlProps;

export default function SearchAutocomplete({ ...props }: Props): JSX.Element {
  const [searchInput, setSearchInput] = useState<string>("");
  const navigate = useNavigate();
  const { t } = useTranslation();
  const location = useLocation();

  useEffect(() => {
    const url = new URLSearchParams(location.search);
    if (url.get("q") && url.get("q") !== "") {
      setSearchInput(url.get("q") ?? "");
    }
  }, [location]);

  const { result: suggestionResult } = useAPI<GetSuggestionsResult>(
    (config) => {
      if (searchInput.length >= 3) {
        return getSuggestions(
          {
            query: searchInput ?? "",
          },
          config,
        );
      }
      return { suggestions: [] } as unknown as Promise<
        BaseAPIResult & {
          suggestions?: {
            suggestion: string;
            id: string;
          }[];
        }
      >;
    },
    [searchInput],
  );

  const formik = useFormik<FormValues>({
    initialValues: {
      searchInput: null,
    },
    onSubmit: () => {
      if (searchInput !== "") {
        navigate("/search?q=" + searchInput);
      }
    },
  });

  return (
    <FormControl sx={{ m: 1 }} {...props}>
      <form onSubmit={formik.handleSubmit} data-testid="searchForm">
        <Autocomplete
          data-testid="searchAutocomplete"
          value={searchInput}
          options={suggestionResult?.suggestions ?? []}
          renderOption={(props, option) => {
            return (
              <Typography
                {...props}
                key={option?.id}
                data-testid="suggestionAutocomplete"
              >
                {option?.suggestion}
              </Typography>
            );
          }}
          onInputChange={(_, newInputValue) => setSearchInput(newInputValue)}
          onChange={(_, value) =>
            setSearchInput(
              typeof value === "string" ? value : value?.suggestion ?? "",
            )
          }
          getOptionLabel={(option) =>
            typeof option === "string" ? option : option?.suggestion ?? ""
          }
          freeSolo
          renderInput={(params) => (
            <OutlinedInput
              fullWidth
              data-testid="searchInput"
              inputProps={params.inputProps}
              ref={params.InputProps.ref}
              color="dark"
              type="text"
              placeholder={t("searchPage.search_bar.placeholder")}
              sx={{
                backgroundColor: (theme) => theme.palette.primary.light,
                "&:hover": {
                  "& svg": { color: (theme) => theme.palette.grey[400] },
                },
              }}
              endAdornment={
                <InputAdornment position="end">
                  {searchInput && (
                    <IconButton
                      onClick={() => setSearchInput("")}
                      edge="end"
                      disabled={!searchInput || searchInput.length === 0}
                      sx={{
                        borderRadius: "0.5rem",
                        marginRight: "0.5rem",
                        marginY: "0.5rem",
                        "&:hover": {
                          backgroundColor: (theme) => theme.palette.grey[200],
                        },
                      }}
                    >
                      <CloseRoundedIcon />
                    </IconButton>
                  )}
                  <IconButton
                    type="submit"
                    sx={{
                      marginRight: "-2.1rem",
                      backgroundColor: (theme) => theme.palette.primary.dark,
                      borderRadius: "0.5rem",
                      "&:hover": {
                        backgroundColor: (theme) => theme.palette.dark.dark,
                      },
                    }}
                  >
                    <SvgIcon
                      data-testid="searchIcon"
                      component={SearchIcon}
                      sx={{ color: (theme) => theme.palette.primary.light }}
                    />
                    {!searchInput && (
                      <Typography
                        sx={{
                          display: { xs: "none", md: "flex" },
                          color: (theme) => theme.palette.primary.light,
                          marginLeft: "0.25rem",
                        }}
                      >
                        {t("searchPage.search_bar.icon")}
                      </Typography>
                    )}
                  </IconButton>
                </InputAdornment>
              }
            />
          )}
        />
      </form>
    </FormControl>
  );
}
