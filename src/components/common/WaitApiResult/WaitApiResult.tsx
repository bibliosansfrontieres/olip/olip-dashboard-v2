import React from "react";
import CircularProgressCentered from "src/components/common/CircularProgressCentered/CircularProgressCentered";
import ErrorBox from "src/components/common/Error/ErrorBox";

export type ApiResult<T> = {
  result: T | undefined;
  isLoading: boolean;
  hasError: boolean;
};

type ApiResults<T extends Record<string, unknown>> = {
  [K in keyof T]: ApiResult<T[K]>;
};

type ApiResultChildren<T> = {
  [K in keyof T]: Exclude<NonNullable<T[K]>, undefined>;
};

function isApiResult<T extends Record<string, unknown>>(
  useApiResult: ApiResults<T> | ApiResult<T>,
): useApiResult is ApiResult<T> {
  // Is useApiResult a single ApiResult and not ApiResults
  return (
    "isLoading" in useApiResult &&
    "hasError" in useApiResult &&
    "result" in useApiResult
  );
}

/**
 * Determines the loading and error status of API results.
 *
 * If `useApiResult` is a single `ApiResult`, the function returns its `isLoading` and `hasError` properties.
 * If `useApiResult` is an object of `ApiResults`, the function checks if any of the `ApiResult` objects
 * within the object are loading or have an error, and returns the aggregate status.
 *
 * @template T The type of data contained within the API result(s).
 * @param {ApiResults<T> | ApiResult<T>} useApiResult - A single `ApiResult` or an object of `ApiResults`.
 * @returns {{ isLoading: boolean, hasError: boolean }} An object containing the loading and error status.
 */
function getResultsStatus<T extends Record<string, unknown>>(
  useApiResult: ApiResults<T> | ApiResult<T>,
): {
  isLoading: boolean;
  hasError: boolean;
} {
  if (isApiResult(useApiResult)) {
    return {
      isLoading: useApiResult.isLoading,
      hasError: useApiResult.hasError,
    };
  }

  // concatenate api results in an array
  const apiResultsArray = Object.values(useApiResult);
  // if any api result is loading
  const isLoading = apiResultsArray.some((aR) => aR.isLoading);
  // if any api result has error
  const hasError = apiResultsArray.some((aR) => aR.hasError);

  return {
    isLoading,
    hasError,
  };
}
/**
 * Extracts the result from useApiResult, which can be either a single ApiResult or an object of ApiResults.
 *
 * If useApiResult is a single ApiResult, return ApiResult.result.
 * If useApiResult is an object of ApiResults, it will be mapped and the result of each ApiResult will be returned in an object.
 *
 * @param {ApiResults<T> | ApiResult<T>} useApiResult  ApiResult or ApiResults
 * @returns {ApiResultChildren<T>} ApiResult.result if useApiResult is a single ApiResult, otherwise an object of ApiResult.result
 */
function getResult<T extends Record<string, unknown>>(
  useApiResult: ApiResults<T> | ApiResult<T>,
): ApiResultChildren<T> {
  if (isApiResult(useApiResult)) {
    // If useApiResult is a single ApiResult, return ApiResult.result
    return useApiResult.result as ApiResultChildren<T>;
  }

  // Create array of [key, value] from useApiResult, map value to ApiResult.result
  const mappedApiResults = Object.entries(useApiResult).map(([key, apiRes]) => [
    key,
    apiRes.result,
  ]);

  // Create an object from mappedApiResults array
  const apiResults = Object.fromEntries(
    mappedApiResults,
  ) as ApiResultChildren<T>;

  return apiResults;
}

type Props<T extends Record<string, unknown>> = {
  useApiResult: ApiResults<T> | ApiResult<T>;
  errorTranslationKey?: string | undefined;
  resultChildren?:
    | ((result: ApiResultChildren<T>) => React.ReactNode)
    | React.ReactNode;
  children?: React.ReactNode;
  loadingElement?: React.ReactNode;
  errorElement?: React.ReactNode;
};

/**
 * Wait for all API calls to be finished and display the results.
 *
 * `WaitApiResult` is a higher-order component that waits for all API calls to be finished,
 * and then displays the result.
 *
 * If `useApiResult` is an object of `ApiResults`, it will be mapped and the result of each `ApiResult` will be
 * returned in an object.
 *
 * If `errorTranslationKey` is provided and any of the API calls has an error, an `ErrorBox` will be displayed with
 * the given translation key.
 *
 * If `resultChildren` is provided, it will be called with the result of the API calls and the result of this call
 * will be displayed. If `resultChildren` is not a function, it will be displayed as is.
 *
 * If `children` is provided, it will be displayed when the API calls are finished.
 *
 * If none of the above props are provided, an empty `React.Fragment` will be returned.
 *
 * @param {Props<T extends Record<string, unknown>>} props Components props
 * @returns {React.ReactNode} The result of the API calls
 */
export default function WaitApiResult<T extends Record<string, unknown>>({
  useApiResult,
  errorTranslationKey,
  resultChildren,
  children,
  loadingElement,
  errorElement,
}: Props<T>): React.ReactNode {
  // get results from useApiResult
  const result = getResult(useApiResult);

  const { isLoading, hasError } = getResultsStatus(useApiResult);

  if (isLoading) {
    if (loadingElement) {
      return loadingElement;
    }
    // Display while loading API calls
    return <CircularProgressCentered />;
  } else if (hasError) {
    if (errorElement) {
      return errorElement;
    }
    // Display if any error found
    return (
      <ErrorBox
        translationKey={errorTranslationKey ?? "common.error.generic"}
      />
    );
  } else if (result) {
    // Display when result (wait all results if ApiResults) is available

    if (resultChildren) {
      // If resultChildren is sent in props

      if (typeof resultChildren === "function") {
        // If resultChildren is a function
        return resultChildren(result);
      }

      // If resultChildren is a ReactNode
      return resultChildren;
    }

    // If component has children
    if (children) {
      return children;
    }
  }

  // Why not
  return <></>;
}

/*

# useApiResults definition
##########################

Should be an object typed as :

type ApiResult = {
  result: unknown extends { [key: string]: unknown},
  isLoading: boolean,
  hasError: boolean,
}

OR an object typed as :

{
  [key: string]: ApiResult
}

# Wait for multiple calls and use the API results
#################################################

const categoryContext = useContext(CategoryContext);
const categoriesResult = useNewApi<GetCategories>(
  useCallback(
    () => getCategories({ languageIdentifier }),
    [languageIdentifier],
  ),
);

<WaitApiResult
  useApiResult={{
    category: categoryContext,
    categories: categoriesResult,
  }}
  resultChildren={({ category, categories }) => (
    <>
      <MyMultipleCallsComponent category={category} categories={categories} />
      <CategoryComponent category={category} />
      <CategoriesComponent categories={categories} />
    </>
  )}
/>

# Wait for single call and use the API result
#############################################

// return { result, isLoading, hasError } and result = { categories: Category[] }
const categoriesResult = useNewApi<GetCategories>(
  useCallback(
    () => getCategories({ languageIdentifier }),
    [languageIdentifier],
  ),
);

<WaitApiResult
  useApiResult={categoriesResult}
  resultChildren={({categories}) => (
    <CategoriesComponent categories={categories} />
  )}
/>

# Wait for single call (usually a context) and don't use results
################################################################

const categoryContext = useContext(CategoryContext);

<WaitApiResult useApiResult={categoriesResult}>
  <CategoryComponent />
</WaitApiResult>

*/
