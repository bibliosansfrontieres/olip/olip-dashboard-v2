import { SelectChangeEvent, SxProps } from "@mui/material";
import { ReactElement, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import { getCategories } from "src/services/category.service";
import { Category } from "src/types/category.type";

type Props = {
  actualValues?: string[];
  onFilter: (event: SelectChangeEvent<unknown>) => void;
  sx?: SxProps;
};

export default function FilterCategorySelect({
  actualValues,
  onFilter,
  sx,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    getCategories()
      .then((result) => setCategories(result.categories || []))
      .catch();
  }, []);

  return (
    <OLIPSelect
      choices={categories.map((category) => ({
        name: category.categoryTranslations[0].title,
        value: category.id.toString(),
      }))}
      onChange={onFilter}
      placeholder={t("catalog.filter.placeholder")}
      sx={{ width: "15rem", ...sx }}
      values={actualValues || []}
    />
  );
}
