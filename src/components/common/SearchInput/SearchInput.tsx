import { useState } from "react";
import {
  FormControl,
  FormControlProps,
  IconButton,
  InputAdornment,
  OutlinedInput,
  SvgIcon,
} from "@mui/material";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

import SearchIcon from "src/assets/icons/SearchIcon";

type Props = FormControlProps;

export default function SearchInput({ ...props }: Props): JSX.Element {
  const [value, setValue] = useState("");
  return (
    <FormControl sx={{ m: 1 }} {...props}>
      <OutlinedInput
        className="searchInput"
        color="dark"
        type="text"
        endAdornment={
          <InputAdornment position="end">
            {value && (
              <IconButton
                className="clearButton"
                onClick={() => setValue("")}
                edge="end"
                sx={{
                  borderRadius: "0.5rem",
                  marginRight: "0.5rem",
                  marginY: "0.5rem",
                  "&:hover": {
                    backgroundColor: (theme) => theme.palette.grey[200],
                  },
                }}
              >
                <CloseRoundedIcon
                  sx={{ color: (theme) => theme.palette.primary.dark }}
                />
              </IconButton>
            )}
            <IconButton
              className="searchButton"
              edge="end"
              sx={{
                backgroundColor: (theme) => theme.palette.primary.dark,
                borderRadius: "0.5rem",
                marginRight: "-0.375rem",
                marginY: "0.5rem",
                "&:hover": {
                  backgroundColor: (theme) => theme.palette.dark.dark,
                },
              }}
            >
              {
                <SvgIcon
                  component={SearchIcon}
                  sx={{ color: (theme) => theme.palette.primary.light }}
                />
              }
            </IconButton>
          </InputAdornment>
        }
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setValue(e.target.value)
        }
        placeholder="Rechercher"
        sx={{
          backgroundColor: (theme) => theme.palette.primary.light,
          "&:hover": {
            "& svg": { color: (theme) => theme.palette.grey[400] },
          },
        }}
        value={value}
      />
    </FormControl>
  );
}
