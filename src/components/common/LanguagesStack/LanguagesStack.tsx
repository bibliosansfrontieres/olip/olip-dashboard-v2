import { supportedLanguages } from "src/constants";

import { Chip, Stack, Typography } from "@mui/material";

type Props = {
  languages?: string[];
  languageActive?: string;
  onClick?: (languageClicked: string) => void;
};

function LanguageTypography({
  languageName,
}: {
  languageName?: string;
}): JSX.Element | null {
  if (!languageName) {
    return null;
  }
  return <Typography>{languageName}</Typography>;
}

export default function LanguagesStack({
  languages,
  languageActive,
  onClick,
}: Props): JSX.Element {
  return (
    <Stack
      direction="row"
      spacing={2}
      sx={{ marginY: "0.25rem" }}
      data-testid="languagesStack"
    >
      {languages?.map((language) => {
        if (onClick) {
          return (
            <Chip
              data-testid="languageChip"
              variant={
                languageActive === language ? "tagPrimaryActive" : "tagPrimary"
              }
              clickable={false}
              key={language}
              label={
                <LanguageTypography
                  languageName={
                    supportedLanguages.find(
                      (supportedLanguage) => supportedLanguage.id === language,
                    )?.name
                  }
                />
              }
              {...(onClick && {
                onClick: () => onClick(language),
              })}
            />
          );
        }
        return (
          <LanguageTypography
            languageName={
              supportedLanguages.find(
                (supportedLanguage) => supportedLanguage.id === language,
              )?.name
            }
          />
        );
      })}
    </Stack>
  );
}
