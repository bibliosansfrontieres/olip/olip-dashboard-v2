import { Breadcrumbs, Link as MUILink, Typography } from "@mui/material";
import NavigateNextRoundedIcon from "@mui/icons-material/NavigateNextRounded";
import { Link } from "react-router-dom";
import { Page } from "src/types";
import { useTranslation } from "react-i18next";

export default function OLIPBreadcrumbs({
  links,
}: {
  links: Page[];
}): JSX.Element {
  const { t } = useTranslation();
  return (
    <Breadcrumbs
      separator={<NavigateNextRoundedIcon fontSize="small" />}
      sx={{
        "& a::after": {
          fontWeight: 600,
          visibility: "hidden",
          overflow: "hidden",
          userSelect: "none",
          pointerEvents: "none",
          height: 0,
          content: "attr(data-text)",
        },
      }}>
      {links.map((link, index) =>
        index + 1 !== links.length ? (
          <MUILink
            data-text={t(link.name)}
            key={index}
            component={Link}
            to={link.link}
            variant="breadcrumb"
            sx={{
              display: "inline-flex",
              flexDirection: "column",
              "&:hover": {
                cursor: "pointer",
                fontWeight: 600,
              },
            }}>
            {t(link.name)}
          </MUILink>
        ) : (
          <Typography key={index} variant="breadcrumbSelect">
            {t(link.name)}
          </Typography>
        )
      )}
    </Breadcrumbs>
  );
}
