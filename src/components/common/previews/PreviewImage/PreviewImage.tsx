import { ReactElement } from "react";
import PictureIcon from "src/assets/icons/Picture";
import getEnv from "src/utils/getEnv";

import { Box, SxProps } from "@mui/material";

type Props = {
  imageUrl?: string;
  sx?: SxProps;
  withBaseUrl?: boolean;
};

export default function PreviewImage({
  imageUrl,
  sx,
  withBaseUrl = false,
}: Props): ReactElement {
  if (imageUrl) {
    return (
      <Box
        data-testid="previewImage"
        sx={{
          aspectRatio: "4/3",
          backgroundImage: `url(${
            (withBaseUrl ? getEnv("VITE_AXIOS_BASEURL") : "") + imageUrl
          })`,
          borderRadius: "0.5rem",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          ...sx,
        }}
      />
    );
  }

  return (
    <Box
      sx={{
        aspectRatio: "4/3",
        borderRadius: "0.5rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: (theme) => `${theme.palette.primary.main}4D`,
        color: (theme) => theme.palette.grey[500],
        ...sx,
      }}
    >
      <PictureIcon sx={{ color: (theme) => theme.palette.grey[500] }} />
    </Box>
  );
}
