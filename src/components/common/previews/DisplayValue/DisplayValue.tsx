import { ReactElement } from "react";

import { Box, Typography } from "@mui/material";

type Props = {
  children?: ReactElement;
  label: string;
  value?: string | null;
};

export default function DisplayValue({
  children,
  label,
  value,
}: Props): ReactElement {
  return (
    <Box>
      <Typography data-testid="displayValueLabel">{label}</Typography>
      <Typography
        variant="body1sb"
        data-testid="displayValueValue"
        style={{ whiteSpace: "pre-wrap" }}
      >
        {value}
      </Typography>
      {children}
    </Box>
  );
}
