import { useTranslation } from "react-i18next";
import LanguagesStack from "src/components/common/LanguagesStack/LanguagesStack";
import { APILanguage } from "src/types";
import { WithLanguageSimplified } from "src/types/language/withLanguageSimplified.type";

import { Box, Typography } from "@mui/material";

type Props = {
  activeDisplayLanguage: string;
  onChangeDisplayLanguage: (newDisplayLanguage: string) => void;
  translations:
    | Array<{ language: APILanguage }>
    | (object & WithLanguageSimplified)[];
};

export default function LanguagesAvailable({
  activeDisplayLanguage,
  onChangeDisplayLanguage,
  translations,
}: Props): JSX.Element {
  const { t } = useTranslation();
  return (
    <Box
      p={2}
      borderRadius="0.5rem"
      sx={{ backgroundColor: (theme) => theme.palette.grey["100"] }}
    >
      <Typography variant="info" data-testid="languagesAvailable">
        {t("common.preview.languagesAvailable")}
      </Typography>
      <LanguagesStack
        languages={translations.map(
          (translation) => translation.language.identifier,
        )}
        languageActive={activeDisplayLanguage}
        onClick={(languageClicked) => onChangeDisplayLanguage(languageClicked)}
      />
    </Box>
  );
}
