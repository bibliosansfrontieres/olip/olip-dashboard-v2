import { Box, Typography } from "@mui/material";
import OLIPSelect, { OLIPSelectProps } from "../OLIPSelect/OLIPSelect";
import { useTranslation } from "react-i18next";

export default function DisplayCountSelect(
  props: OLIPSelectProps
): JSX.Element {
  const { t } = useTranslation();
  return (
    <Box display="flex" alignItems="center">
      <Typography mr={2}>{t("common.displayCount")} </Typography>
      <OLIPSelect {...props} />
    </Box>
  );
}
