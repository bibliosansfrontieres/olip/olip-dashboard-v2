import { useContext } from "react";
import { Navigate, Outlet } from "react-router-dom";
import { UserContext } from "src/contexts/user.context";
import BackdropLoading from "../BackdropLoading/BackdropLoading";

export default function ProtectedRoute(): JSX.Element {
  const { accessTokenIsPresent, authIsLoading, user } = useContext(UserContext);

  if (authIsLoading) {
    return <BackdropLoading />;
  }
  return user && accessTokenIsPresent() ? (
    <Outlet />
  ) : (
    <Navigate to="/" replace />
  );
}
