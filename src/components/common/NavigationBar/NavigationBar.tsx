import { useTranslation } from "react-i18next";
import { Box, Button } from "@mui/material";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";

import OLIPBreadcrumbs from "src/components/common/OLIPBreadcrumbs/OLIPBreadcrumbs";
import { Page } from "src/types";
import { useLocation, useNavigate } from "react-router-dom";

export default function NavigationBar({
  links,
}: {
  links: Page[];
}): JSX.Element {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const location = useLocation();
  const canGoBack = location.key !== "default";

  return (
    <Box
      display="flex"
      flexDirection={{ xs: "column", sm: "row" }}
      alignItems={{ xs: "flex-start", sm: "center" }}
      sx={{ padding: "2.5rem" }}
    >
      <Button
        variant="rounded"
        onClick={() => navigate(-1)}
        disabled={!canGoBack}
        startIcon={<ArrowBackRoundedIcon />}
        sx={{
          marginRight: "2rem",
          marginBottom: { xs: "1.5rem", sm: "0" },
          borderColor: (theme) =>
            canGoBack ? theme.palette.text.primary : "#bdbdbd",
        }}
      >
        {t("actions.back")}
      </Button>
      <OLIPBreadcrumbs links={links} />
    </Box>
  );
}
