import { useTranslation } from "react-i18next";

import { Box, SxProps, Typography } from "@mui/material";

export default function ErrorBox({
  translationKey,
  sx,
}: {
  translationKey: string;
  sx?: SxProps;
}): JSX.Element {
  const { t } = useTranslation();
  return (
    <>
      <Box display="flex" justifyContent="center" sx={{ ...sx }}>
        <Typography variant="caption">{t(translationKey)}</Typography>
      </Box>
    </>
  );
}
