import { Box, CircularProgress } from "@mui/material";

export default function CircularProgressCentered(): React.ReactNode {
  return (
    <Box display="flex" justifyContent="center" alignItems="center">
      <CircularProgress style={{ marginTop: "17.5rem" }} />
    </Box>
  );
}
