import { memo, useState } from "react";
import { Radio } from "@mui/material";

import RadioCheckedIcon from "src/assets/icons/RadioChecked";
import RadioHoveredIcon from "src/assets/icons/RadioHovered";
import RadioIcon from "src/assets/icons/Radio";

export default memo(function OLIPRadio(props: any): JSX.Element {
  const [hover, setHover] = useState(false);

  return (
    <Radio
      checkedIcon={<RadioCheckedIcon />}
      color={props.color || props.disabled ? "success" : "dark"}
      disableRipple
      icon={hover ? <RadioHoveredIcon /> : <RadioIcon />}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      {...props}
    />
  );
});
