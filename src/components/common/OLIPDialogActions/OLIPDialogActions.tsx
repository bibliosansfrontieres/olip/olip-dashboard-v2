import {
  Box,
  Button,
  CircularProgress,
  DialogActions,
  DialogActionsProps,
} from "@mui/material";

type ButtonParams = {
  action: () => void;
  text: string;
};

type Props = {
  dialogActionsProps?: DialogActionsProps;
  isLoading?: boolean;
  mainButton: ButtonParams;
  secondaryButton?: ButtonParams;
  id?: string;
};

export default function OLIPDialogActions({
  dialogActionsProps,
  isLoading = false,
  mainButton,
  secondaryButton,
  id,
}: Props): JSX.Element {
  return (
    <DialogActions {...dialogActionsProps}>
      <Box
        sx={{
          display: "flex",
          flexDirection: { xs: "column-reverse", sm: "row" },
          justifyContent: { xs: "center", sm: "flex-end" },
        }}
        alignItems="center"
        width="100%"
      >
        {isLoading ? (
          <CircularProgress
            color="inherit"
            sx={{ marginX: "1.875rem", marginTop: { xs: "1rem", sm: 0 } }}
          />
        ) : (
          secondaryButton && (
            <Button
              data-testid="secondaryButtonOlipDialog"
              onClick={secondaryButton.action}
              type="button"
              variant="outlined"
              sx={{
                marginRight: { xs: 0, sm: "1.875rem" },
                marginTop: { xs: "1rem", sm: 0 },
                width: { xs: "100%", sm: "auto" },
              }}
            >
              {secondaryButton.text}
            </Button>
          )
        )}

        <Button
          type="submit"
          disabled={isLoading}
          onClick={mainButton.action}
          variant="contained"
          data-testid={id ? `submitButton-${id}` : "submitButton"}
          sx={{
            marginTop: { xs: "1rem", sm: 0 },
            width: { xs: "100%", sm: "auto" },
          }}
        >
          {mainButton.text}
        </Button>
      </Box>
    </DialogActions>
  );
}
