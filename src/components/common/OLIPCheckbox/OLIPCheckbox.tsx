import { memo, useState } from "react";
import CheckboxIcon from "src/assets/icons/Checkbox";
import CheckboxCheckedIcon from "src/assets/icons/CheckboxChecked";
import CheckboxHoveredIcon from "src/assets/icons/CheckboxHovered";

import { Checkbox } from "@mui/material";

export default memo(function OLIPCheckbox(props: any): JSX.Element {
  const [hover, setHover] = useState(false);

  return (
    <Checkbox
      checkedIcon={<CheckboxCheckedIcon />}
      color={props.color || props.disabled ? "success" : "dark"}
      disableRipple
      icon={hover ? <CheckboxHoveredIcon /> : <CheckboxIcon />}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      onFocus={() => setHover(true)}
      onBlur={() => setHover(false)}
      data-testid={`${props.dataTestId ?? ""}Checkbox`}
      {...props}
    />
  );
});
