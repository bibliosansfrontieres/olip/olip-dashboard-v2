import { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import OLIPSelect from "../../OLIPSelect/OLIPSelect";
import { supportedLanguages } from "src/constants";
import FormControlLayout from "src/components/layouts/FormControlLayout/FormControlLayout";

type Props = {
  actualLanguage: string;
  error?: string;
  label?: string;
  onChangeLanguage: (choosedLanguage: string) => void;
  placeholder?: string;
  touched?: boolean;
};

export default function TranslateLanguageList({
  actualLanguage,
  error,
  label,
  onChangeLanguage,
  placeholder,
  touched,
}: Props): ReactElement {
  const { t } = useTranslation();

  function handleChangeLanguage(event: any) {
    const choosedLanguage = event.target.value;
    onChangeLanguage(choosedLanguage);
  }

  return (
    <FormControlLayout
      label={label || t("common.form.translateLanguageList.label")}
      error={error}
      touched={touched}
    >
      <OLIPSelect
        choices={supportedLanguages.map((lang) => ({
          name: lang.name,
          value: lang.id,
        }))}
        onChange={handleChangeLanguage}
        placeholder={placeholder}
        sx={{ width: "100%" }}
        sxSelect={{
          backgroundColor: (theme: any) => theme.palette.primary.light,
        }}
        values={actualLanguage}
      />
    </FormControlLayout>
  );
}
