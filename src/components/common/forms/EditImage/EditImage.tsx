import { ReactElement, useState } from "react";
import PreviewImage from "../../previews/PreviewImage/PreviewImage";
import { Box, Button, FormHelperText, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

type Props = {
  error?: string;
  imageUrl?: string;
  maxSize?: number;
  onChangeImage: (newImage: string | ArrayBuffer | null) => void;
  touched?: boolean;
};

export default function EditImage({
  error,
  imageUrl,
  maxSize = 1048576,
  onChangeImage,
  touched,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [localeErrorImage, setLocaleErrorImage] = useState<string>("");

  const handleChangeImage = ({ target: { files } }: any) => {
    setLocaleErrorImage("");
    onChangeImage("");
    if (files[0]) {
      const file = files[0];
      if (!file.type.toString().startsWith("image/")) {
        setLocaleErrorImage("common.error.image.format");
        return;
      }
      if (file.size > maxSize) {
        setLocaleErrorImage("common.error.image.size");
        return;
      }
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => onChangeImage(reader.result);
      reader.onerror = () => setLocaleErrorImage("common.error.image.generic");
    }
  };

  return (
    <>
      <PreviewImage imageUrl={imageUrl} />
      <Box display="flex" flexDirection="column">
        <Button
          variant="contained"
          onClick={() => document.getElementById("inputFile")?.click()}
          sx={{ marginY: "0.5rem", width: { xs: "70vw", sm: "auto" } }}
        >
          {t("common.image.button")}
        </Button>
        <input
          id="inputFile"
          type="file"
          accept="image/*"
          onChange={handleChangeImage}
          style={{ display: "none" }}
        />
        <Typography variant="info">{t("common.image.type")}</Typography>
        <Typography variant="info">
          {t("common.image.size", { mo: maxSize / 1024 / 1024 })}
        </Typography>
        {localeErrorImage && (
          <FormHelperText error>{t(localeErrorImage)}</FormHelperText>
        )}
        {touched && error && <FormHelperText error>{t(error)}</FormHelperText>}
      </Box>
    </>
  );
}
