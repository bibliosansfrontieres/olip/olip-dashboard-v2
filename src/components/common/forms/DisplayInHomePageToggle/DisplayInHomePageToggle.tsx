import { useContext, useEffect, useState } from "react";
import {
  Box,
  CircularProgress,
  FormControl,
  FormControlLabel,
  Stack,
  Switch,
  Typography,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import { getSetting, updateSetting } from "src/services/setting.service";
import { SnackbarContext } from "src/contexts/snackbar.context";
import { SettingKey } from "src/types/setting.service.type";

export default function DisplayInHomePageToggle({
  settingKey,
}: {
  settingKey: SettingKey;
}): JSX.Element {
  const { t } = useTranslation();
  const { displaySnackbar } = useContext(SnackbarContext);
  const [isDisplayed, setIsDisplayed] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    getSetting(settingKey)
      .then(({ setting }) =>
        setIsDisplayed(setting?.value === "true" ? true : false),
      )
      .finally(() => setLoading(false));
  }, []);

  const toggleDisplay = async () => {
    setLoading(true);
    try {
      const { setting } = await updateSetting(settingKey, {
        value: (!isDisplayed).toString(),
      });
      setIsDisplayed(setting?.value === "true" ? true : false);
      displaySnackbar(t("snackbar.updateDisplayEdito.success"));
    } catch (e) {
      displaySnackbar(t("snackbar.updateDisplayEdito.error"), "error");
    } finally {
      setLoading(false);
    }
  };

  return (
    <Stack
      p={2}
      borderRadius="0.5rem"
      sx={{ backgroundColor: (theme) => theme.palette.grey["100"] }}
    >
      <Typography variant="info" data-testid="displayInHomePageLabel">
        {t("common.displaySection")}
      </Typography>
      {loading ? (
        <Box ml={4} mt={1}>
          <CircularProgress color="inherit" size={30} />
        </Box>
      ) : (
        <FormControl>
          <FormControlLabel
            data-testid="displayInHomePageToggle"
            checked={isDisplayed}
            control={<Switch />}
            disabled={loading}
            label={t(`common.${isDisplayed ? "yes" : "no"}`)}
            onChange={toggleDisplay}
          />
        </FormControl>
      )}
    </Stack>
  );
}
