import { ReactNode } from "react";
import AlertIcon from "src/assets/icons/Alert";
import InfoIcon from "src/assets/icons/Info";
import KOIcon from "src/assets/icons/KO";
import OKIcon from "src/assets/icons/OK";

import { Alert, AlertColor, Snackbar } from "@mui/material";

export default function OLIPSnackbar({
  message,
  onClose,
  open,
  timestamp,
  severity = "success",
}: {
  message: string;
  onClose: (event: React.SyntheticEvent | Event, reason?: string) => void;
  open: boolean;
  timestamp: number;
  severity?: AlertColor;
}): JSX.Element {
  let icon: ReactNode | undefined;
  switch (severity) {
    case "error":
      icon = <KOIcon />;
      break;
    case "warning":
      icon = <AlertIcon />;
      break;
    case "info":
      icon = <InfoIcon />;
      break;
    default:
      icon = <OKIcon />;
  }

  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      open={open}
      autoHideDuration={6000}
      onClose={onClose}
      id={timestamp.toString()}
    >
      <Alert
        data-testid="OLIPSnackbar"
        icon={icon}
        onClose={onClose}
        severity={severity}
      >
        {message}
      </Alert>
    </Snackbar>
  );
}
