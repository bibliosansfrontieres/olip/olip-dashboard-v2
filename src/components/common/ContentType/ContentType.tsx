import { Box, SvgIconProps, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import AudioIcon from "src/assets/icons/Audio";
import LaunchIcon from "@mui/icons-material/Launch";
import PictureIcon from "src/assets/icons/Picture";
import TextIcon from "src/assets/icons/Text";
import VideoIcon from "src/assets/icons/Video";
import { ITEM_TYPE } from "src/constants";

export default function ContentType({
  iconProps,
  typeId,
}: {
  iconProps?: SvgIconProps;
  typeId: number;
}): JSX.Element {
  const { t } = useTranslation();

  return (
    <Box display="flex" alignItems="center" justifyContent="center">
      {typeId === ITEM_TYPE.AUDIO.id && <AudioIcon {...iconProps} />}
      {typeId === ITEM_TYPE.IMAGE.id && <PictureIcon {...iconProps} />}
      {typeId === ITEM_TYPE.OTHER.id && <LaunchIcon {...iconProps} />}
      {typeId === ITEM_TYPE.TEXT.id && <TextIcon {...iconProps} />}
      {typeId === ITEM_TYPE.VIDEO.id && <VideoIcon {...iconProps} />}
      <Typography variant="breadcrumbSelect" ml={1}>
        {t(`item.type.${typeId}`)}
      </Typography>
    </Box>
  );
}
