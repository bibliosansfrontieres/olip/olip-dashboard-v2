import { ReactElement } from "react";
import { CategoryApplication } from "src/types/application/categoryApplication.type";

import { Box, SxProps, Typography } from "@mui/material";

type Props = { application: CategoryApplication; sx?: SxProps };
export default function NewApplicationChip({
  application,
  sx,
}: Props): ReactElement {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      sx={{ ...sx }}
    >
      {/* TODO: add logo when added in back */}
      {/* <Avatar
        variant="square"
        alt=""
        src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
        sx={{ width: "1.5rem", height: "1.5rem" }}
      /> */}
      <Typography
        variant="breadcrumbSelect"
        ml={1}
        sx={{
          overflow: "hidden",
          textOverflow: "ellipsis",
          whiteSpace: "nowrap",
        }}
      >
        {application.displayName}
      </Typography>
    </Box>
  );
}
