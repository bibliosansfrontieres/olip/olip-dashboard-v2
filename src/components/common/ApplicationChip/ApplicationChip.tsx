import { Avatar, Box, SxProps, Typography } from "@mui/material";
import { ReactElement } from "react";
import { Application } from "src/types/application.type";
import getEnv from "src/utils/getEnv";

type Props = { application: Application; sx?: SxProps };
export default function ApplicationChip({
  application,
  sx,
}: Props): ReactElement {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      sx={{ ...sx }}
    >
      <Avatar
        variant="square"
        alt=""
        src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
        sx={{ width: "1.5rem", height: "1.5rem" }}
      />
      <Typography
        variant="breadcrumbSelect"
        ml={1}
        sx={{
          overflow: "hidden",
          textOverflow: "ellipsis",
          whiteSpace: "nowrap",
        }}
      >
        {application.displayName}
      </Typography>
    </Box>
  );
}
