import { ReactElement, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import OLIPSelect from "src/components/common/OLIPSelect/OLIPSelect";
import { ApplicationTypeNameEnum } from "src/enums/applicationTypeName.enum";
import { getApplications } from "src/services/application.service";
import { getApplicationTypes } from "src/services/default.service";
import { Application } from "src/types/application.type";
import getEnv from "src/utils/getEnv";

import { SxProps } from "@mui/material";

type Props = {
  onFilter: (applicationIds: string[]) => void;
  placeholder?: string;
  sx?: SxProps;
  actualValues?: string[];
};
export default function FilterApplicationSelect({
  onFilter,
  placeholder,
  sx,
  actualValues,
}: Props): ReactElement {
  const { t } = useTranslation();
  const [applications, setApplications] = useState<Application[]>([]);
  const [applicationsFiltered, setApplicationFiltered] = useState<string[]>(
    actualValues ?? [],
  );
  const handleFilter = (event: any) => {
    const newValues =
      event.target.value.indexOf("") === -1
        ? event.target.value.filter(
            (applicationId: string) => applicationId !== "",
          )
        : [];
    setApplicationFiltered(newValues);
    onFilter(newValues);
  };
  useEffect(() => {
    setApplicationFiltered(actualValues ?? []);
  }, [actualValues]);

  useEffect(() => {
    const getApp = async () => {
      const applicationTypesResult = await getApplicationTypes();
      getApplications({
        status: "installed",
        withOlip: "true",
        applicationTypeId: applicationTypesResult?.applicationTypes.find(
          (appType) => appType.name === ApplicationTypeNameEnum.CONTENT_HOST,
        )?.id,
      })
        .then((result) => setApplications(result.applications || []))
        .catch();
    };

    getApp();
  }, []);

  return (
    <OLIPSelect
      choices={applications.map((application) => ({
        name: application.displayName,
        value: application.id.toString(),
        icon: getEnv("VITE_AXIOS_BASEURL") + application.logo,
      }))}
      onChange={handleFilter}
      placeholder={t(placeholder || "common.filter")}
      sx={{ width: "15rem", ...sx }}
      values={applicationsFiltered || []}
    />
  );
}
