import {
  FormControl,
  InputAdornment,
  OutlinedInput,
  SvgIcon,
  SxProps,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import SearchIcon from "src/assets/icons/SearchIcon";

type Props = {
  onChange: (event: any) => void;
  sxFormControl?: SxProps;
};

export default function SearchFilter({
  onChange,
  sxFormControl,
}: Props): JSX.Element {
  const { t } = useTranslation();

  return (
    <FormControl
      data-testid="searchFilter"
      sx={{ mb: "1rem", width: "auto", ...sxFormControl }}
      onChange={onChange}
    >
      <OutlinedInput
        color="dark"
        placeholder={t("common.search")}
        endAdornment={
          <InputAdornment position="start">
            <SvgIcon
              component={SearchIcon}
              sx={{
                paddingRight: "0.5rem",
              }}
            />
          </InputAdornment>
        }
        type="text"
      />
    </FormControl>
  );
}
