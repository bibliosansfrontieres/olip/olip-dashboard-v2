import { Box, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import NavigationBar from "../common/NavigationBar/NavigationBar";
import { getApplications } from "src/services/application.service";
import ApplicationsList from "./ApplicationsList/ApplicationsList";
import { Page, Pagination } from "src/types";
import { Application } from "src/types/application.type";
import { useLoaderData } from "react-router-dom";

export async function applicationsLoader({ request }: { request: Request }) {
  const url = new URL(request.url);
  const applicationTypeId = url.searchParams.get("applicationTypeId");
  const take = url.searchParams.get("take");
  const page = url.searchParams.get("page");
  const { applications, meta } = await getApplications({
    page: Number(page) || 1,
    take: Number(take) || 12,
    applicationTypeId: applicationTypeId || undefined,
    status: "installed",
  });
  return { applications, meta };
}

export default function ApplicationsPage(): JSX.Element {
  const { t } = useTranslation();
  const { applications, meta } = useLoaderData() as {
    applications: Application[];
    meta: Pagination;
  };

  const links: Page[] = [
    { link: "/", name: "home.pageTitle" },
    { link: "", name: "applications.title" },
  ];

  return (
    <>
      <NavigationBar links={links} />
      <Box my={4}>
        <Box
          mb={12}
          mx={3}
          sx={{
            textAlign: "center",
          }}
        >
          <Typography variant="h2" sx={{ marginBottom: "1.75rem" }}>
            {t("applications.title")}
          </Typography>
          <Typography>{t("applications.description")}</Typography>
        </Box>
        <ApplicationsList applications={applications} meta={meta} />
      </Box>
    </>
  );
}
