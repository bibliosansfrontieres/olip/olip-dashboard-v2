import { useTranslation } from "react-i18next";
import {
  Avatar,
  Box,
  Button,
  DialogActions,
  DialogProps,
  Typography,
} from "@mui/material";
import ModalLayout from "src/components/layouts/ModalLayout/ModalLayout";
import { Application } from "src/types/application.type";
import LaunchIcon from "@mui/icons-material/Launch";
import getEnv from "src/utils/getEnv";

export default function ApplicationModal(
  props: DialogProps & { application: Application },
): JSX.Element {
  const { t } = useTranslation();

  const { onClose, application } = props;

  const handleCancel = (event: object) => {
    onClose?.(event, "backdropClick");
  };

  return (
    <ModalLayout
      {...props}
      maxWidth="sm"
      onClose={(e: object) => handleCancel(e)}
    >
      <>
        <Box display="flex" alignItems="center" sx={{ paddingY: "1rem" }}>
          <Avatar
            variant="square"
            alt=""
            src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
            sx={{
              height: "3.5rem",
              width: "3.5rem",
            }}
          />
          <Box display="flex" flexDirection="column" paddingLeft={1}>
            <Typography variant="h2">{application.displayName}</Typography>
          </Box>
        </Box>
        <Box sx={{ paddingRight: "1rem", overflow: "auto" }}>
          <Box
            sx={{
              backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
                application.image
              })`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              borderRadius: "0.5rem",
              aspectRatio: "16/9",
              position: "relative",
            }}
          ></Box>
          <Box display="flex" alignItems="flex-start" flexDirection="column">
            <Typography
              variant="h3"
              sx={{
                marginTop: "1.5rem",
              }}
            >
              {application.applicationType.applicationTypeTranslations[0].label}
            </Typography>
            <Typography
              variant="body1m"
              sx={{
                marginTop: "1rem",
              }}
            >
              {application.applicationTranslations[0].longDescription}
            </Typography>
          </Box>
        </Box>
        <DialogActions sx={{ marginTop: "2rem" }}>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
          >
            <Button
              onClick={handleCancel}
              type="button"
              variant="outlined"
              sx={{ marginRight: "1.875rem" }}
            >
              {t("modal.close")}
            </Button>
            <Button
              target="_blank"
              component="a"
              href={"http://" + application.url}
              variant="contained"
              endIcon={<LaunchIcon />}
            >
              {t("modal.open")} {application.displayName}
            </Button>
          </Box>
        </DialogActions>
      </>
    </ModalLayout>
  );
}
