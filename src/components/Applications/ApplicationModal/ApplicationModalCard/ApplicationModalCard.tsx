import { Box, SxProps, Typography } from "@mui/material";
import { Application } from "src/types/application.type";
import getEnv from "src/utils/getEnv";

export default function ApplicationModalCard({
  application,
  sxCard,
}: {
  application: Application;
  sxCard?: SxProps;
}): JSX.Element {
  return (
    <Box
      sx={{
        border: (theme) => `0.125rem solid ${theme.palette.dark.main}00`,
        display: "flex",
        flexDirection: "column",
        minHeight: "35rem",
        ...sxCard,
      }}
    >
      <Box
        flex={4}
        sx={{
          backgroundImage: `url(${getEnv("VITE_AXIOS_BASEURL")}${
            application.image
          })`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderRadius: "0.5rem",
          flex: 4,
          height: "100%",
          position: "relative",
          width: "100%",
        }}
      ></Box>
      <Box
        flex="2"
        display="flex"
        alignItems="flex-start"
        flexDirection="column"
      >
        <Typography
          variant="h3"
          sx={{
            marginTop: "1.5rem",
            overflow: "auto",
          }}
        >
          {application.applicationType.name}
        </Typography>
        <Typography
          variant="body1m"
          sx={{
            marginTop: "1rem",
            overflow: "auto",
          }}
        >
          {application.applicationTranslations[0].longDescription}
        </Typography>
      </Box>
    </Box>
  );
}
