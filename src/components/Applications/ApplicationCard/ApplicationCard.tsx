import { Avatar, Box, Button, SxProps, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import useModal from "src/hooks/useModal";
import { Application } from "src/types/application.type";
import ApplicationModal from "../ApplicationModal/ApplicationModal";
import LaunchIcon from "@mui/icons-material/Launch";
import getEnv from "src/utils/getEnv";

export default function ApplicationCard({
  application,
  sxCard,
}: {
  application: Application;
  sxCard?: SxProps;
}): JSX.Element {
  const { t } = useTranslation();
  const [openModal, toggleModal] = useModal();

  return (
    <Box
      data-testid="applicationsCard"
      sx={{
        backgroundColor: (theme) => theme.palette.primary.light,
        borderRadius: "1rem",
        border: (theme) => `0.0625rem solid ${theme.palette.grey[500]}`,
        color: "inherit",
        display: "flex",
        height: "100%",
        textDecoration: "none",
        flexDirection: "column",
        ...sxCard,
      }}
    >
      <Box display="flex" alignItems="center" justifyContent="center" p={2}>
        <Avatar
          variant="square"
          alt=""
          src={getEnv("VITE_AXIOS_BASEURL") + application.logo}
        />
        <Box>
          <Typography variant="h3" ml={1.5}>
            {application.displayName}
          </Typography>
        </Box>
      </Box>
      <Box
        display="flex"
        alignItems="flex-start"
        flex="auto"
        flexDirection="column"
        justifyContent="space-between"
        p={2}
      >
        <Typography
          variant="body1sb"
          sx={{
            marginTop: "0.5rem",
            overflow: "auto",
          }}
        >
          {application.applicationType.applicationTypeTranslations[0].label}
        </Typography>
        <Typography
          variant="body1m"
          sx={{
            flex: "2",
            marginTop: "0.5rem",
            overflow: "auto",
          }}
        >
          {application.applicationTranslations[0].shortDescription}
        </Typography>
      </Box>
      <Box
        sx={{
          backgroundColor: (theme) => `${theme.palette.primary.main}33`,
          borderBottomLeftRadius: "1rem",
          borderBottomRightRadius: "1rem",
        }}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        p={2}
      >
        <Button
          id={`button-access-${application.id}`}
          onClick={() => toggleModal()}
          sx={{ padding: "0.5rem" }}
        >
          {t("application.readMore")}
        </Button>
        <Button
          target="_blank"
          component="a"
          href={"http://" + application.url}
          sx={{ marginRight: "1rem" }}
          variant="rounded"
          endIcon={<LaunchIcon />}
        >
          {t("modal.open")}
        </Button>
        <ApplicationModal
          onClose={() => toggleModal()}
          open={openModal}
          fullWidth
          maxWidth="lg"
          application={application}
        />
      </Box>
    </Box>
  );
}
