import { useTranslation } from "react-i18next";
import ApplicationCard from "src/components/Applications/ApplicationCard/ApplicationCard";
import FilterApplicationsByType from "src/components/Applications/ApplicationsList/FilterApplicationsByType/FilterApplicationsByType";
import PaginationLayout from "src/components/layouts/PaginationLayout/PaginationLayout";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import { Pagination } from "src/types";
import { Application } from "src/types/application.type";

import { Box, Container, Grid, Typography } from "@mui/material";

export default function ApplicationsList({
  applications = [],
  meta,
}: {
  applications?: Application[];
  meta?: Pagination;
}): JSX.Element {
  const { t } = useTranslation();
  const [, updateSearchParam] = useHandleSearchParams();

  const handleChangePagination = (newParams: {
    page: number;
    take: number;
  }) => {
    if (newParams.take !== meta?.take) {
      updateSearchParam([
        {
          name: "take",
          value: newParams.take.toString(),
          defaultValue: "10",
        },
        {
          name: "page",
          value: "1",
          defaultValue: "1",
        },
      ]);
    } else {
      updateSearchParam({
        name: "page",
        value: newParams.page.toString(),
        defaultValue: "1",
      });
    }
  };

  return (
    <>
      <Container maxWidth="xl">
        <Box
          display="flex"
          mb={4}
          sx={{
            display: "flex",
            flexDirection: { xs: "column", sm: "row" },
            justifyContent: "space-between",
          }}
        >
          <Typography
            variant="h3"
            sx={{ marginBottom: { xs: "1rem", sm: "0" } }}
          >
            {t("applications.totalItems", {
              count: meta?.totalItemsCount || applications.length,
            })}
          </Typography>
          <FilterApplicationsByType />
        </Box>
        {!applications || !meta || meta.totalItemsCount === 0 ? (
          <Box
            mb={12}
            mx={3}
            sx={{
              textAlign: "center",
            }}
          >
            <Typography>{t("applications.empty")}</Typography>
          </Box>
        ) : (
          <PaginationLayout meta={meta} onChange={handleChangePagination}>
            <Grid
              container
              spacing={3.75}
              sx={{ marginBottom: "2rem" }}
              alignItems="stretch"
            >
              {applications.map((application) => (
                <Grid
                  item
                  key={application.id}
                  xs={12}
                  sm={6}
                  md={6}
                  lg={3}
                  xl={3}
                >
                  <ApplicationCard application={application} />
                </Grid>
              ))}
            </Grid>
          </PaginationLayout>
        )}
      </Container>
    </>
  );
}
