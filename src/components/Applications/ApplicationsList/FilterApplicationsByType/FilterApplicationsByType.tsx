import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import WaitApiResult from "src/components/common/WaitApiResult/WaitApiResult";
import useHandleSearchParams from "src/hooks/useHandleSearchParams";
import useNewApi from "src/hooks/useNewApi";
import { getApplicationTypes } from "src/services/default.service";

import { Chip, Skeleton, Stack } from "@mui/material";

export default function FilterApplicationsByType(): JSX.Element {
  const { t } = useTranslation();
  const [searchParams, updateSearchParam] = useHandleSearchParams();
  const applicationTypesResult = useNewApi(
    useCallback(async () => {
      return await getApplicationTypes();
    }, []),
  );
  const handleChangeFilter = (typeId?: string) => {
    const list: string[] =
      searchParams?.["applicationTypeId"]?.split(",") || [];

    if (typeId) {
      const index = list.indexOf(typeId);
      if (index !== -1) {
        list.splice(index, 1);
      } else {
        list.push(typeId);
      }
    }

    updateSearchParam([
      {
        name: "applicationTypeId",
        value: typeId ? list.toString() : "",
        defaultValue: "",
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };

  return (
    <Stack direction="row" spacing={2}>
      <WaitApiResult
        useApiResult={applicationTypesResult}
        resultChildren={({ applicationTypes }) => (
          <>
            <Chip
              variant={
                searchParams?.["applicationTypeId"] ? "tag" : "tagActive"
              }
              label={t("item.filter.all")}
              onClick={() => handleChangeFilter()}
            />

            {applicationTypes.map((appType) => (
              <Chip
                key={appType.id}
                variant={
                  searchParams?.["applicationTypeId"]
                    ?.split(",")
                    .find((id: string): boolean => id === appType.id)
                    ? "tagActive"
                    : "tag"
                }
                label={appType?.applicationTypeTranslations?.[0].label}
                onClick={() => handleChangeFilter(appType.id)}
              />
            ))}
          </>
        )}
        loadingElement={
          <Skeleton animation="pulse">
            <Chip label="Loading" />
          </Skeleton>
        }
      />
    </Stack>
  );
}
