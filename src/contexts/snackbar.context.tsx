import { createContext, ReactNode, useState } from "react";
import OLIPSnackbar from "src/components/common/OLIPSnackbar/OLIPSnackbar";

import { AlertColor } from "@mui/material";

type ContextValue = {
  displaySnackbar: (message: string, severity?: AlertColor) => void;
};

const defaultValue = {
  displaySnackbar: () => {
    /* expected empty method */
  },
};

export const SnackbarContext = createContext<ContextValue>(defaultValue);

export const SnackbarContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [open, setOpen] = useState<boolean>(false);
  const [timestamp, setTimestamp] = useState<number>(0);
  const [message, setMessage] = useState<string>("");
  const [severity, setSeverity] = useState<AlertColor>("success");

  function displaySnackbar(
    snackbarMessage: string,
    snackbarSeverity: AlertColor = "success",
  ) {
    setTimestamp(Date.now());
    setMessage(snackbarMessage);
    setSeverity(snackbarSeverity);
    setOpen(true);
  }

  const handleClose = (
    _event: React.SyntheticEvent | Event,
    reason?: string,
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const contextValue = {
    displaySnackbar,
  };
  return (
    <SnackbarContext.Provider value={contextValue}>
      {children}
      <OLIPSnackbar
        open={open}
        message={message}
        timestamp={timestamp}
        onClose={handleClose}
        severity={severity}
      />
    </SnackbarContext.Provider>
  );
};
