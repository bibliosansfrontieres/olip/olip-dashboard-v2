import { createContext, ReactNode, useCallback } from "react";
import useNewApi from "src/hooks/useNewApi";
import { getApplicationTypes } from "src/services/default.service";
import { GetApplicationTypesResult } from "src/types/applicationType.type";
import {
  useApiDefaultValueSimplified,
  UseApiTypeSimplified,
} from "src/types/useApi.type";

type ContextValue = UseApiTypeSimplified<GetApplicationTypesResult>;

const defaultValue = useApiDefaultValueSimplified;

export const ApplicationTypesContext =
  createContext<ContextValue>(defaultValue);

export const ApplicationTypesContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const applicationTypesResult = useNewApi(
    useCallback(async () => {
      return await getApplicationTypes();
    }, []),
  );
  const contextValue = applicationTypesResult;

  return (
    <ApplicationTypesContext.Provider value={contextValue}>
      {children}
    </ApplicationTypesContext.Provider>
  );
};
