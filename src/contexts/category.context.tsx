import {
  createContext,
  PropsWithChildren,
  useContext,
  useEffect,
  useState,
} from "react";
import { LanguageContext } from "src/contexts/language.context";
import useAPI from "src/hooks/useAPI";
import {
  getCategory,
  updateCategory as updateCategoryAPI,
} from "src/services/category.service";
import { getCategoryContent } from "src/services/categoryContent.service";
import { Category, UpdateCategoryParams } from "src/types/category.type";
import { CategoryContent } from "src/types/categoryContent.type";
import getEnv from "src/utils/getEnv";

type ContextValue = {
  category: Category;
  categoryContents: CategoryContent[] | undefined;
  displayLanguage: string;
  isLoading: boolean;
  refreshCategory: () => void;
  refreshCategoryContents: () => void;
  setDisplayLanguage: (language: string) => void;
  updateCategory: (newValue: UpdateCategoryParams) => void;
};

const defaultValue = {
  category: {} as Category,
  categoryContents: [],
  displayLanguage: getEnv("VITE_DEFAULT_LANGUAGE"),
  isLoading: false,
  refreshCategory: () => {
    /* expected empty method */
  },
  refreshCategoryContents: () => {
    /* expected empty method */
  },
  setDisplayLanguage: () => {
    /* expected empty method */
  },
  updateCategory: () => {
    /* expected empty method */
  },
};

export const CategoryContext = createContext<ContextValue>(defaultValue);

type Props = PropsWithChildren & {
  category: Category;
};
export const CategoryContextProvider = ({
  category: initCategory,
  children,
}: Props) => {
  const [category, setCategory] = useState<Category>(initCategory);
  const [categoryContents, setCategoryContents] = useState<
    CategoryContent[] | undefined
  >(undefined);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [displayLanguage, setDisplayLanguage] = useState<string>(
    getEnv("VITE_DEFAULT_LANGUAGE"),
  );
  const { language } = useContext(LanguageContext);

  const { result: resultCategory, refresh: refreshCategory } = useAPI(() =>
    getCategory({
      getAllCategoryTranslations: "true",
      id: String(category.id),
      isVisibilityEnabled: false,
    }),
  );
  const { result: resultCategoryContents, refresh: refreshCategoryContents } =
    useAPI(() => getCategoryContent({ id: String(category.id) }));

  useEffect(() => {
    if (resultCategory?.category) {
      setCategory(resultCategory.category);
    }
  }, [resultCategory]);

  useEffect(() => {
    if (resultCategoryContents?.contents) {
      setCategoryContents(resultCategoryContents.contents);
    }
  }, [resultCategoryContents]);

  useEffect(() => {
    setDisplayLanguage(language);
  }, [language]);

  async function updateCategory(newValues: UpdateCategoryParams) {
    setIsLoading(true);
    try {
      await updateCategoryAPI(category.id, newValues);
      setCategory((prevCategory) => ({ ...prevCategory, ...newValues }));
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  const contextValue: ContextValue = {
    category,
    categoryContents,
    displayLanguage,
    isLoading,
    refreshCategory,
    refreshCategoryContents,
    setDisplayLanguage,
    updateCategory,
  };

  return (
    <CategoryContext.Provider value={contextValue}>
      {children}
    </CategoryContext.Provider>
  );
};
