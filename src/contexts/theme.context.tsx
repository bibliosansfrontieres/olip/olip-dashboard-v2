import { createContext, ReactNode, useMemo, useState } from "react";
import useAPI from "src/hooks/useAPI";
import { getSetting } from "src/services/setting.service";
import { paletteFunction, themeFunction } from "src/theme";
import rtlPlugin from "stylis-plugin-rtl";

import createCache from "@emotion/cache";
import { CacheProvider, ThemeProvider } from "@emotion/react";
import { createTheme, ThemeOptions } from "@mui/material";
type ThemeContextValue = {
  direction: "ltr" | "rtl";
  refreshColor: () => void;
  refreshDirection: (language: string) => void;
};

const defaultValue = {
  direction: "ltr" as "ltr" | "rtl",
  refreshColor: () => {
    /* expected empty method */
  },
  refreshDirection: () => {
    /* expected empty method */
  },
};
export const ThemeContext = createContext<ThemeContextValue>(defaultValue);

export const ThemeContextProvider = ({ children }: { children: ReactNode }) => {
  const { result, refresh: refreshColor } = useAPI(() =>
    getSetting("principal_color"),
  );
  const [direction, setDirection] = useState(defaultValue.direction);
  const getDirectionDocument = (language: string) => {
    if (["ara", "fas"].includes(language)) {
      return "rtl";
    }
    return "ltr";
  };

  const contextValue = {
    direction,
    refreshColor,
    refreshDirection: (language: string) =>
      setDirection(getDirectionDocument(language)),
  };

  const mainColorTheme = useMemo(
    () =>
      createTheme(
        themeFunction(
          paletteFunction(result?.setting?.value || "#fdb851"),
          direction,
        ) as ThemeOptions,
      ),
    [result, direction],
  );
  const cacheLtr = createCache({
    key: "muiltr",
  });
  const cacheRtl = createCache({
    key: "muirtl",
    stylisPlugins: [rtlPlugin],
  });

  return (
    <CacheProvider value={direction === "ltr" ? cacheLtr : cacheRtl}>
      <ThemeContext.Provider value={contextValue}>
        <ThemeProvider theme={mainColorTheme}>{children}</ThemeProvider>
      </ThemeContext.Provider>
    </CacheProvider>
  );
};
