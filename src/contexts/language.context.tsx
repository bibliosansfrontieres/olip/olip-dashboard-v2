import {
  createContext,
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import i18next from "i18next";
import getEnv from "src/utils/getEnv";

type ContextValue = {
  language: string;
  changeLanguage: (newLanguage: string) => void;
};

const defaultValue = {
  language: "",
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  changeLanguage: () => {},
};

export const LanguageContext = createContext<ContextValue>(defaultValue);

export const LanguageContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const defaultLanguage = getEnv("VITE_DEFAULT_LANGUAGE") || "eng";
  const [language, setLanguage] = useState(
    localStorage.getItem("language") ?? defaultLanguage,
  );

  const changeLanguage = useCallback(
    (newLanguage: string) => {
      if (language === newLanguage) return;
      setLanguage(newLanguage);
      localStorage.setItem("language", newLanguage);
    },
    [language],
  );

  useEffect(() => {
    i18next.changeLanguage(language);
  }, [language]);

  const contextValue = useMemo(
    () => ({
      language,
      changeLanguage,
    }),
    [language, changeLanguage],
  );

  return (
    <LanguageContext.Provider value={contextValue}>
      {children}
    </LanguageContext.Provider>
  );
};
