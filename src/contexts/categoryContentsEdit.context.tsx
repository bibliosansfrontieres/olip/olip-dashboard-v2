import { createContext, PropsWithChildren, useContext } from "react";
import useAPI from "src/hooks/useAPI";
import {
  getCategoryContent,
  updateCategoryPlaylistItem,
  updateOrderCategoryPlaylists,
} from "src/services/categoryContent.service";
import {
  CategoryContent,
  GetCategoryContentResult,
} from "src/types/categoryContent.type";
import { SnackbarContext } from "./snackbar.context";
import { useTranslation } from "react-i18next";

type ContextValue = {
  changeOrderSelection: (
    categoryPlaylistId: number,
    newIndex: number
  ) => Promise<void>;
  refreshSelected: () => void;
  selectedIsLoading: boolean;
  selectedResult?: GetCategoryContentResult;
  toggleDisplayItem: (id: number, isDisplay: boolean) => Promise<boolean>;
};

const defaultValue = {
  changeOrderSelection: async () => {
    /* expected empty method */
  },
  refreshSelected: () => {
    /* expected empty method */
  },
  selectedIsLoading: false,
  selectedResult: undefined,
  toggleDisplayItem: async () => false,
};

export const CategoryContentsContext =
  createContext<ContextValue>(defaultValue);

type Props = PropsWithChildren & {
  contents?: CategoryContent[];
  categoryId: number;
};
export const CategoryContentsContextProvider = ({
  children,
  categoryId,
}: Props) => {
  const { displaySnackbar } = useContext(SnackbarContext);
  const { t } = useTranslation();
  const {
    isLoading: selectedIsLoading,
    refresh: refreshSelected,
    result: selectedResult,
  } = useAPI<GetCategoryContentResult>(() =>
    getCategoryContent({ id: String(categoryId) })
  );

  async function changeOrderSelection(
    categoryPlaylistId: number,
    newIndex: number
  ): Promise<void> {
    if (selectedResult?.contents) {
      const nextOrder: number[] = selectedResult.contents
        .sort((a, b) => a.order - b.order)
        .map((categoryPlaylist) => categoryPlaylist.id)
        .filter((id) => id !== categoryPlaylistId);
      nextOrder.splice(newIndex, 0, categoryPlaylistId);
      try {
        const result = await updateOrderCategoryPlaylists({
          categoryId: categoryId,
          categoryPlaylistIds: nextOrder,
        });
        if (result.statusCode === 200) {
          await refreshSelected();
          displaySnackbar(
            t("snackbar.orderCategoryPlaylistsSelection.success")
          );
        } else {
          console.error(`${result.statusCode} - ${result.errorMessage}`);
          displaySnackbar(
            t("snackbar.orderCategoryPlaylistsSelection.error"),
            "error"
          );
        }
      } catch (error) {
        console.error(error);
        displaySnackbar(
          t("snackbar.orderCategoryPlaylistsSelection.error"),
          "error"
        );
      }
    }
  }

  async function toggleDisplayItem(
    itemId: number,
    isDisplayed: boolean
  ): Promise<boolean> {
    try {
      const result = await updateCategoryPlaylistItem(itemId, { isDisplayed });
      if (result.statusCode === 200 && result.categoryPlaylistItem) {
        await refreshSelected();
        displaySnackbar(
          t("snackbar.toggleDisplayCategoryPlaylistItem.success")
        );
        return result.categoryPlaylistItem.isDisplayed;
      }
      console.error(`${result.statusCode} - ${result.errorMessage}`);
      displaySnackbar(
        t("snackbar.toggleDisplayCategoryPlaylistItem.error"),
        "error"
      );
    } catch (error) {
      console.error(error);
      displaySnackbar(
        t("snackbar.toggleDisplayCategoryPlaylistItem.error"),
        "error"
      );
    }
    return !isDisplayed;
  }

  const contextValue: ContextValue = {
    changeOrderSelection,
    refreshSelected,
    selectedIsLoading,
    selectedResult,
    toggleDisplayItem,
  };

  return (
    <CategoryContentsContext.Provider value={contextValue}>
      {children}
    </CategoryContentsContext.Provider>
  );
};
