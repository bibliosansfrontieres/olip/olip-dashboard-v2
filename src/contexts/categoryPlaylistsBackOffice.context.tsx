import { createContext, ReactNode, useCallback, useContext } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { LanguageContext } from "src/contexts/language.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import useNewApi from "src/hooks/useNewApi";
import { getCategoryBackOfficeCategoryPlaylists } from "src/services/category.service";
import { updateOrderCategoryPlaylists } from "src/services/categoryContent.service";
import { CategoryBackOfficeCategoryPlaylists } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";
import { useApiDefaultValue, UseApiType } from "src/types/useApi.type";

type ContextValue = UseApiType<CategoryBackOfficeCategoryPlaylists> & {
  changeOrderSelection: (
    categoryPlaylistId: number,
    newIndex: number,
  ) => Promise<void>;
};
const defaultValue = {
  ...useApiDefaultValue,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  changeOrderSelection: async () => {},
};
export const CategoryBackOfficeCategoryPlaylistsContext =
  createContext<ContextValue>(defaultValue);

export const CategoryBackOfficeCategoryPlaylistsContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const { displaySnackbar } = useContext(SnackbarContext);
  const { t } = useTranslation();

  const { language: languageIdentifier } = useContext(LanguageContext);

  const { id } = useParams();
  const isCategoryBackOfficeCategoryPlaylistsEnabled = id !== undefined;
  const categoryBackOfficeCategoryPlaylistsResult =
    useNewApi<CategoryBackOfficeCategoryPlaylists>(
      useCallback(async () => {
        if (id)
          return await getCategoryBackOfficeCategoryPlaylists({
            id,
            languageIdentifier,
          });
      }, [id, languageIdentifier]),
      isCategoryBackOfficeCategoryPlaylistsEnabled,
    );
  async function changeOrderSelection(
    categoryPlaylistId: number,
    newIndex: number,
  ): Promise<void> {
    if (!id) return;
    const { result: selectedResult, reload: refreshSelected } =
      categoryBackOfficeCategoryPlaylistsResult;
    if (selectedResult?.categoryPlaylists) {
      const nextOrder: number[] = selectedResult.categoryPlaylists
        .sort((a, b) => a.order - b.order)
        .map((categoryPlaylist) => categoryPlaylist.id)
        .filter((catId) => catId !== categoryPlaylistId);
      nextOrder.splice(newIndex, 0, categoryPlaylistId);
      try {
        const result = await updateOrderCategoryPlaylists({
          categoryId: Number(id),
          categoryPlaylistIds: nextOrder,
        });
        if (result.statusCode === 200) {
          await refreshSelected();
          displaySnackbar(
            t("snackbar.orderCategoryPlaylistsSelection.success"),
          );
        } else {
          console.error(`${result.statusCode} - ${result.errorMessage}`);
          displaySnackbar(
            t("snackbar.orderCategoryPlaylistsSelection.error"),
            "error",
          );
        }
      } catch (error) {
        console.error(error);
        displaySnackbar(
          t("snackbar.orderCategoryPlaylistsSelection.error"),
          "error",
        );
      }
    }
  }

  const contextValue = {
    ...categoryBackOfficeCategoryPlaylistsResult,
    changeOrderSelection,
  };

  return (
    <CategoryBackOfficeCategoryPlaylistsContext.Provider value={contextValue}>
      {children}
    </CategoryBackOfficeCategoryPlaylistsContext.Provider>
  );
};
