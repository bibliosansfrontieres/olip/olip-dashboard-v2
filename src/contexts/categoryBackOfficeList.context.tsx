import { createContext, ReactNode, useCallback, useContext } from "react";
import { LanguageContext } from "src/contexts/language.context";
import { TabTypeEnum } from "src/enums/tabType.enum";
import useNewApi from "src/hooks/useNewApi";
import { getCategoriesBackOfficeList } from "src/services/category.service";
import {
  CategoryBackOfficeList,
  CategoryBackOfficeListCategory,
} from "src/types/category/categoryBackOfficeList.type";
import { useApiDefaultValue, UseApiType } from "src/types/useApi.type";
import { tabTypeDefinitions } from "src/utils/tabTypeDefinitions";

type ContextValue = UseApiType<CategoryBackOfficeList> & {
  getCount: (type: TabTypeEnum) => number;
  getCategories: (type: TabTypeEnum) => CategoryBackOfficeListCategory[];
};

const defaultValue = {
  ...useApiDefaultValue,
  getCount: () => 0,
  getCategories: () => [],
};

export const CategoryBackOfficeListContext =
  createContext<ContextValue>(defaultValue);

export const CategoryBackOfficeListContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const { language: languageIdentifier } = useContext(LanguageContext);

  const categoryBackOfficeList = useNewApi<CategoryBackOfficeList>(
    useCallback(async () => {
      return await getCategoriesBackOfficeList({ languageIdentifier });
    }, [languageIdentifier]),
  );

  function getCategories(type: TabTypeEnum) {
    const definition = tabTypeDefinitions[type];
    return (
      categoryBackOfficeList.result?.categories.filter(definition.predicate) ??
      []
    );
  }

  function getCount(type: TabTypeEnum) {
    return getCategories(type).length;
  }

  const contextValue = {
    ...categoryBackOfficeList,
    getCount,
    getCategories,
  };

  return (
    <CategoryBackOfficeListContext.Provider value={contextValue}>
      {children}
    </CategoryBackOfficeListContext.Provider>
  );
};
