import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useParams } from "react-router-dom";
import { LanguageContext } from "src/contexts/language.context";
import useNewApi from "src/hooks/useNewApi";
import {
  getCategoryBackOffice,
  updateCategory,
} from "src/services/category.service";
import { CategoryBackOffice } from "src/types/category/categoryBackOffice.type";
import { useApiDefaultValue, UseApiType } from "src/types/useApi.type";

type ContextValue = UseApiType<CategoryBackOffice> & {
  translationToDisplay:
    | CategoryBackOffice["categoryTranslations"][0]
    | undefined;
  setDisplayLanguage: (languageIdentifier: string) => void;
  updateIsHomepageDisplayed: (isHomepageDisplayed: boolean) => Promise<void>;
  isUpdateLoading: boolean;
};
const defaultValue = {
  ...useApiDefaultValue,
  translationToDisplay: undefined,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setDisplayLanguage: () => {},
  updateIsHomepageDisplayed: () =>
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    new Promise<void>(() => {}),
  isUpdateLoading: true,
};
export const CategoryBackOfficeContext =
  createContext<ContextValue>(defaultValue);

export const CategoryBackOfficeContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const { language: languageIdentifier } = useContext(LanguageContext);

  const [displayLanguage, setDisplayLanguage] =
    useState<string>(languageIdentifier);
  const [isUpdateLoading, setIsUpdateLoading] = useState(false);

  useEffect(() => {
    setDisplayLanguage(languageIdentifier);
  }, [languageIdentifier]);

  const { id } = useParams();
  const isCategoryBackOfficeEnabled = id !== undefined;
  const categoryBackOfficeResult = useNewApi<CategoryBackOffice>(
    useCallback(async () => {
      if (id) return await getCategoryBackOffice({ languageIdentifier, id });
    }, [languageIdentifier, id]),
    isCategoryBackOfficeEnabled,
  );

  const updateIsHomepageDisplayed = async (isHomepageDisplayed: boolean) => {
    if (!id) return;
    setIsUpdateLoading(true);
    await updateCategory(+id, { isHomepageDisplayed });
    categoryBackOfficeResult.setResult((prev) => {
      if (!prev) return;
      return { ...prev, isHomepageDisplayed };
    });
    setIsUpdateLoading(false);
  };

  const translationToDisplay = useMemo(() => {
    return (
      categoryBackOfficeResult.result?.categoryTranslations.find(
        (categoryTranslation) =>
          categoryTranslation.language.identifier === displayLanguage,
      ) ?? categoryBackOfficeResult.result?.categoryTranslations[0]
    );
  }, [categoryBackOfficeResult.result?.categoryTranslations, displayLanguage]);

  const contextValue = {
    ...categoryBackOfficeResult,
    isUpdateLoading,
    translationToDisplay,
    setDisplayLanguage,
    updateIsHomepageDisplayed,
  };
  return (
    <CategoryBackOfficeContext.Provider value={contextValue}>
      {children}
    </CategoryBackOfficeContext.Provider>
  );
};
