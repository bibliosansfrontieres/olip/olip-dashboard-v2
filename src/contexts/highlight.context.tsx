import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { useTranslation } from "react-i18next";
import { LanguageContext } from "src/contexts/language.context";
import { SnackbarContext } from "src/contexts/snackbar.context";
import useHighlight from "src/hooks/useHighlight";
import {
  addHighlightContent,
  removeHighlightContent,
  updateDisplayHighlightContent,
  updateOrderHighlightContent,
} from "src/services/highlightContent.service";
import { getItems } from "src/services/item.service";
import { BaseAPIResult, Pagination as PaginationType } from "src/types";
import { Highlight } from "src/types/highlight.type";
import { HighlightContent } from "src/types/highlightContent.type";
import { Item } from "src/types/item.type";
import getEnv from "src/utils/getEnv";

type ContextValue = {
  addToSelection: (id: string) => void;
  changeDisplayHighlightContent: (id: string, newValue: boolean) => void;
  changeOrderHighlightContent: (id: string, newIndex: number) => void;
  displayLanguage: string;
  getHighlight: () => Promise<Highlight | null>;
  getItemsAvailable: ({
    applicationIds,
    categoryId,
    page,
    take,
    titleLike,
  }: {
    applicationIds?: string;
    categoryId?: string;
    page: number;
    take: number;
    titleLike?: string;
  }) => Promise<void>;
  highlight: Highlight | null;
  itemsAvailable: Item[];
  highlightContentsSelection: HighlightContent[];
  loading: boolean;
  loadingItemsAvailable: boolean;
  metaItemsAvailable?: PaginationType;
  resetContext: () => void;
  removeToSelection: (id: string) => void;
  saveChangesSelection: () => Promise<void>;
  setDisplayLanguage: (language: string) => void;
};

const defaultValue = {
  addToSelection: () => {
    /* expected empty method */
  },
  changeDisplayHighlightContent: () => {
    /* expected empty method */
  },
  changeOrderHighlightContent: () => {
    /* expected empty method */
  },
  displayLanguage: getEnv("VITE_DEFAULT_LANGUAGE"),
  getHighlight: async () => null,
  getItemsAvailable: async () => {
    /* expected empty method */
  },
  highlight: null,
  itemsAvailable: [],
  highlightContentsSelection: [],
  loading: true,
  loadingItemsAvailable: true,
  metaItemsAvailable: undefined,
  resetContext: () => {
    /* expected empty method */
  },
  removeToSelection: () => {
    /* expected empty method */
  },
  saveChangesSelection: async () => {
    /* expected empty method */
  },
  setDisplayLanguage: () => {
    /* expected empty method */
  },
};

export const HighlightContext = createContext<ContextValue>(defaultValue);

export const HighlightContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const { t } = useTranslation();
  const { language } = useContext(LanguageContext);

  const { displaySnackbar } = useContext(SnackbarContext);
  const [displayLanguage, setDisplayLanguage] = useState<string>(
    getEnv("VITE_DEFAULT_LANGUAGE"),
  );
  const { getHighlight, highlight, loading, refreshHighlight } = useHighlight();

  const [itemsAvailable, setItemsAvailable] = useState<Item[]>([]);
  const [highlightContentsSelection, setHighlightContentsSelection] = useState<
    HighlightContent[]
  >([]);
  const [loadingItemsAvailable, setLoadingItemsAvailable] =
    useState<boolean>(true);
  const [metaItemsAvailable, setMetaItemsAvailable] = useState<
    PaginationType | undefined
  >(undefined);

  useEffect(() => {
    setDisplayLanguage(language);
  }, [language]);

  useEffect(() => {
    if (highlightContentsSelection.length === 0) {
      setHighlightContentsSelection(
        highlight?.highlightContents.map(
          (highlightContent) => highlightContent,
        ) || [],
      );
    }
  }, [highlight]);

  const addToSelection = (itemId: string) => {
    const item = itemsAvailable.find(
      (itemAvailable) => itemAvailable.id === itemId,
    );
    if (item) {
      setHighlightContentsSelection((prevHighlightContentsSelection) => [
        ...prevHighlightContentsSelection,
        {
          id: 0,
          isDisplayed: false,
          item: item,
          order: prevHighlightContentsSelection.length + 1,
        },
      ]);
      displaySnackbar(t("snackbar.addItemToHighlight.success"));
    }
  };

  const changeDisplayHighlightContent = (itemId: string, newValue: boolean) => {
    if (
      newValue === true &&
      highlightContentsSelection.filter(({ isDisplayed }) => isDisplayed)
        .length === 4
    ) {
      displaySnackbar(t("snackbar.updateHighlight.maxDisplay"), "warning");
    } else {
      const highlightContentIndex = highlightContentsSelection.findIndex(
        (highlightContent) => highlightContent.item.id === itemId,
      );
      if (highlightContentIndex > -1) {
        const nextHighlightContentSelection = [...highlightContentsSelection];
        nextHighlightContentSelection[highlightContentIndex].isDisplayed =
          newValue;
        setHighlightContentsSelection(nextHighlightContentSelection);
      }
    }
  };

  const changeOrderHighlightContent = (itemId: string, newIndex: number) => {
    const highlightContentIndex = highlightContentsSelection.findIndex(
      (highlightContent) => highlightContent.item.id === itemId,
    );
    if (highlightContentIndex > -1) {
      const nextHighlightContentsSelection = [...highlightContentsSelection];
      nextHighlightContentsSelection.splice(highlightContentIndex, 1);
      nextHighlightContentsSelection.splice(
        newIndex,
        0,
        highlightContentsSelection[highlightContentIndex],
      );
      for (const [
        index,
        highlightContent,
      ] of highlightContentsSelection.entries()) {
        highlightContent.order = index + 1;
      }
      setHighlightContentsSelection(nextHighlightContentsSelection);
    }
  };

  const getItemsAvailable = async ({
    applicationIds,
    categoryId,
    page,
    take,
    titleLike,
  }: {
    applicationIds?: string;
    categoryId?: string;
    page: number;
    take: number;
    titleLike?: string;
  }) => {
    getItems({
      applicationIds: applicationIds || "",
      categoryId: categoryId || "",
      page: page,
      take: take,
      titleLike: titleLike,
    })
      .then(({ items: itemsFetched, meta }) => {
        setItemsAvailable(itemsFetched || []);
        setMetaItemsAvailable(meta || undefined);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoadingItemsAvailable(false));
  };

  const removeToSelection = (itemId: string) => {
    const highlightContentIndex = highlightContentsSelection.findIndex(
      (highlightContent) => highlightContent.item.id === itemId,
    );
    if (highlightContentIndex > -1) {
      const nextItemsSelection = [...highlightContentsSelection];
      nextItemsSelection.splice(highlightContentIndex, 1);
      setHighlightContentsSelection(nextItemsSelection);
    }
  };

  const resetContext = () => {
    setHighlightContentsSelection([]);
    refreshHighlight();
  };

  const saveChangesSelection = async () => {
    const listOfChanges: Promise<BaseAPIResult>[] = [];
    const highlightDisplayIds: number[] = [];

    try {
      let lastServerHighlight = await getHighlight();

      for (const highlightContent of highlightContentsSelection) {
        if (
          lastServerHighlight?.highlightContents.findIndex(
            (lastServerHighlightContent) =>
              lastServerHighlightContent.item.id === highlightContent.item.id,
          ) === -1
        ) {
          listOfChanges.push(addHighlightContent(highlightContent.item.id));
        }
      }

      for (const { id, item } of lastServerHighlight?.highlightContents || []) {
        if (
          highlightContentsSelection.findIndex(
            (highlightContent) => highlightContent.item.id === item.id,
          ) === -1
        ) {
          listOfChanges.push(removeHighlightContent(id));
        }
      }

      await Promise.all(listOfChanges);

      if (listOfChanges.length !== 0) {
        lastServerHighlight = await getHighlight();
      }

      const highlightContentIdsOrder: number[] = [];

      for (const localHighlightContent of highlightContentsSelection || []) {
        const equivalentServerHighlightContent =
          lastServerHighlight?.highlightContents.find(
            (lastServerHighlightContent) =>
              lastServerHighlightContent.item.id ===
              localHighlightContent.item.id,
          ) || null;

        if (equivalentServerHighlightContent) {
          highlightContentIdsOrder.push(equivalentServerHighlightContent.id);
          if (localHighlightContent.isDisplayed === true) {
            highlightDisplayIds.push(equivalentServerHighlightContent.id);
          }
        }
      }
      await updateDisplayHighlightContent({
        highlightContentIds: highlightDisplayIds,
      });

      await updateOrderHighlightContent(highlightContentIdsOrder);
    } catch {
      throw new Error();
    }
  };

  const contextValue = {
    addToSelection,
    changeDisplayHighlightContent,
    changeOrderHighlightContent,
    displayLanguage,
    getHighlight,
    getItemsAvailable,
    highlight,
    itemsAvailable,
    highlightContentsSelection,
    loading,
    loadingItemsAvailable,
    metaItemsAvailable,
    refreshHighlight,
    removeToSelection,
    resetContext,
    saveChangesSelection,
    setDisplayLanguage,
  };

  return (
    <HighlightContext.Provider value={contextValue}>
      {children}
    </HighlightContext.Provider>
  );
};
