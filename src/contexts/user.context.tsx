import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { LanguageContext } from "src/contexts/language.context";
import { login as loginService } from "src/services/auth.service";
import { getMyProfile } from "src/services/user.service";
import { socket } from "src/socket.io";
import { PermissionName, User } from "src/types/user.type";

type LocaleLoginResult = {
  errorMessage?: string | string[];
  statusCode: number;
};

type ContextValue = {
  accessTokenIsPresent: () => boolean;
  authIsLoading: boolean;
  havePermission: (permission: PermissionName | null) => boolean;
  login: (username: string, password?: string) => Promise<LocaleLoginResult>;
  logout: () => void;
  refreshMyProfile: () => void;
  user: User | null;
};

const defaultValue = {
  accessTokenIsPresent: () => false,
  authIsLoading: true,
  havePermission: () => false,
  login: async () => (await {}) as LocaleLoginResult,
  logout: () => {
    /* exptected empty method */
  },
  refreshMyProfile: () => {
    /* expected empty method */
  },
  user: null,
};

export const UserContext = createContext<ContextValue>(defaultValue);

function accessTokenIsPresent(): boolean {
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    return true;
  }
  return false;
}

export const UserContextProvider = ({ children }: { children: ReactNode }) => {
  const [authIsLoading, setAuthIsLoading] = useState<boolean>(true);
  const [user, setUser] = useState<User | null>(null);
  const { changeLanguage, language } = useContext(LanguageContext);

  function logout() {
    setAuthIsLoading(false);
    setUser(null);
    localStorage.removeItem("accessToken");
    socket.auth = { ...socket.auth, token: null };
    socket.disconnect().connect();
  }

  useEffect(() => {
    if (accessTokenIsPresent()) {
      setAuthIsLoading(true);
      getMyProfile()
        .then((profileResult) => {
          if (profileResult.user) {
            setUser(profileResult.user);
            if (
              profileResult.user.language !== language &&
              !localStorage.getItem("language")
            )
              changeLanguage(profileResult.user.language);
          } else {
            localStorage.removeItem("accessToken");
          }
        })
        .catch(() => localStorage.removeItem("accessToken"))
        .finally(() => setAuthIsLoading(false));
    } else {
      setAuthIsLoading(false);
    }
  }, [changeLanguage, language]);

  useEffect(() => {
    function onLocalStorageUpdate() {
      if (!accessTokenIsPresent()) {
        logout();
      }
    }

    window.addEventListener("storage", onLocalStorageUpdate);
    return () => {
      window.removeEventListener("storage", onLocalStorageUpdate);
    };
  }, []);

  function havePermission(requirePermission: PermissionName | null): boolean {
    return (
      requirePermission === null ||
      user?.role.permissions.find(
        (permission) => permission.name === requirePermission,
      ) !== undefined
    );
  }

  async function login(
    username: string,
    password?: string,
  ): Promise<LocaleLoginResult> {
    const authResult = await loginService(username, password);
    if (authResult.accessToken) {
      localStorage.setItem("accessToken", authResult.accessToken);
      socket.auth = { ...socket.auth, token: authResult.accessToken };
      socket.disconnect().connect();
      const profileResult = await getMyProfile();
      if (profileResult.user) {
        setUser(profileResult.user);
        changeLanguage(profileResult.user.language);
      }
      delete profileResult.user;
      return profileResult;
    }
    delete authResult.accessToken;
    return authResult;
  }

  async function refreshMyProfile() {
    const profileResult = await getMyProfile();
    if (profileResult.user) {
      setUser(profileResult.user);
    }
  }

  const contextValue = {
    accessTokenIsPresent,
    authIsLoading,
    havePermission,
    login,
    logout,
    refreshMyProfile,
    user,
  };

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  );
};
