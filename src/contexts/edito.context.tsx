import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { LanguageContext } from "src/contexts/language.context";
import useEdito from "src/hooks/useEdito";
import { Edito } from "src/types/edito.type";
import getEnv from "src/utils/getEnv";

type ContextValue = {
  displayLanguage: string;
  edito: Edito | null;
  loading: boolean;
  refreshEdito: () => void;
  setDisplayLanguage: (language: string) => void;
  setEdito: (edito: Edito | null) => void;
};

const defaultValue = {
  displayLanguage: getEnv("VITE_DEFAULT_LANGUAGE"),
  edito: null,
  loading: true,
  refreshEdito: () => {
    /* expected empty method */
  },
  setDisplayLanguage: () => {
    /* expected empty method */
  },
  setEdito: () => {
    /* expected empty method */
  },
};

export const EditoContext = createContext<ContextValue>(defaultValue);

export const EditoContextProvider = ({ children }: { children: ReactNode }) => {
  const { language } = useContext(LanguageContext);

  const [displayLanguage, setDisplayLanguage] = useState<string>(
    getEnv("VITE_DEFAULT_LANGUAGE"),
  );
  const { edito, loading, refreshEdito, setEdito } = useEdito();

  useEffect(() => {
    setDisplayLanguage(language);
  }, [language]);

  const contextValue = {
    displayLanguage,
    edito,
    loading,
    refreshEdito,
    setDisplayLanguage,
    setEdito,
  };

  return (
    <EditoContext.Provider value={contextValue}>
      {children}
    </EditoContext.Provider>
  );
};
