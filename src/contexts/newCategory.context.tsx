import { createContext, ReactNode, useCallback, useContext } from "react";
import { useParams, useSearchParams } from "react-router-dom";
import { LanguageContext } from "src/contexts/language.context";
import useNewApi from "src/hooks/useNewApi";
import { getCategory } from "src/services/category.service";
import { GetCategoryResult } from "src/types/category.type";

type ContextValue = {
  result: GetCategoryResult | undefined;
  isLoading: boolean;
  hasError: boolean;
};

const defaultValue: ContextValue = {
  result: undefined,
  isLoading: true,
  hasError: false,
};

export const NewCategoryContext = createContext<ContextValue>(defaultValue);

export const NewCategoryContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [params] = useSearchParams();
  const { language: languageIdentifier } = useContext(LanguageContext);
  const { categoryId } = useParams();

  const isCategoryEnabled = categoryId !== undefined;
  const categoryResult = useNewApi<GetCategoryResult>(
    useCallback(async () => {
      if (categoryId) {
        return await getCategory({
          languageIdentifier,
          dublinCoreTypeId: params.get("dublinCoreTypeId") || undefined,
          playlistId: params.get("playlistId") || undefined,
          page: Number(params.get("page")) || undefined,
          take: Number(params.get("take")) || undefined,
          id: categoryId,
        });
      }
    }, [params, categoryId, languageIdentifier]),
    isCategoryEnabled,
  );
  const contextValue = categoryResult;
  return (
    <NewCategoryContext.Provider value={contextValue}>
      {children}
    </NewCategoryContext.Provider>
  );
};
