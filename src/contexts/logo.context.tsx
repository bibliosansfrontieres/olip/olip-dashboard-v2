import { createContext, ReactNode } from "react";
import { getSetting } from "src/services/setting.service";
import useAPI from "src/hooks/useAPI";

type ContextValue = {
  logo: string;
  refreshLogo: () => void;
};

const defaultValue = {
  logo: "",
  refreshLogo: () => {
    /* expected empty method */
  },
};

export const LogoContext = createContext<ContextValue>(defaultValue);

export const LogoContextProvider = ({ children }: { children: ReactNode }) => {
  const { result, refresh: refreshLogo } = useAPI(() => getSetting("logo"));

  const contextValue = {
    logo: result?.setting?.value || "",
    refreshLogo,
  };

  return (
    <LogoContext.Provider value={contextValue}>{children}</LogoContext.Provider>
  );
};
