import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { useTranslation } from "react-i18next";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import { BatteryInformationEvent } from "src/types/socket.type";

import { SnackbarContext } from "./snackbar.context";

type ContextValue = BatteryInformationEvent | null;

const defaultValue: ContextValue = {
  hasBattery: false,
  percentage: 0,
  isCharging: false,
  isPluggedIn: false,
  lowBatteryPercentage: 5,
};

export const BatteryContext = createContext<ContextValue>(defaultValue);

export const BatteryContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const { displaySnackbar } = useContext(SnackbarContext);
  const { t } = useTranslation();
  const [hasBeenWarned, setHasBeenWarned] = useState(false);

  const { event } = useSocketOnEvent<BatteryInformationEvent>(
    "battery-information",
  );

  useEffect(() => {
    if (!event) return;
    if (
      event.hasBattery &&
      event.percentage <= event.lowBatteryPercentage &&
      !(event.isCharging || event.isPluggedIn) &&
      !hasBeenWarned
    ) {
      displaySnackbar(t("snackbar.battery.error"), "error");
      setHasBeenWarned(true);
    }
  }, [event, hasBeenWarned, displaySnackbar, t]);

  useEffect(() => {
    if (!event) return;
    if (event.isCharging && hasBeenWarned) {
      setHasBeenWarned(false);
    }
  }, [event, hasBeenWarned]);

  const contextValue = event;
  return (
    <BatteryContext.Provider value={contextValue}>
      {children}
    </BatteryContext.Provider>
  );
};
