import ReactDOM from "react-dom/client";
import { I18nextProvider } from "react-i18next";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import ApplicationsPage, {
  applicationsLoader,
} from "src/components/Applications/Applications.page.tsx";
import CatalogPage from "src/components/Catalog/Catalog.page.tsx";
import CategoryPage from "src/components/Category/Category.page.tsx";
import CategoryErrorPage from "src/components/Category/CategoryError.page.tsx";
import ProtectedRoute from "src/components/common/ProtectedRoute/ProtectedRoute.tsx";
import EditoPage, { editoLoader } from "src/components/Edito/Edito.page.tsx";
import HomePage from "src/components/Home/Home.page.tsx";
import ItemPage, { itemLoader } from "src/components/Item/Item.page.tsx";
import ItemErrorPage from "src/components/Item/ItemError.page.tsx";
import MainLayout from "src/components/layouts/MainLayout/MainLayout.tsx";
import MainWebSocket from "src/components/layouts/MainWebSocket/MainWebSocket.tsx";
import SettingsLayout from "src/components/layouts/SettingsLayout/SettingsLayout.tsx";
import SearchPage, {
  searchLoader,
} from "src/components/Search/Search.page.tsx";
import ActivityPage from "src/components/settings/Activity/Activity.page.tsx";
import ApplicationsSettingsPage, {
  applicationsSettingsLoader,
} from "src/components/settings/ApplicationsSettings/ApplicationsSettings.page.tsx";
import ContentsPage from "src/components/settings/Contents/Contents.page.tsx";
import EditCategoryPage from "src/components/settings/Contents/EditCategoryPage/EditCategoryPage.tsx";
import DashboardPage from "src/components/settings/Dashboard/Dashboard.page.tsx";
import UsersPage, {
  usersLoader,
} from "src/components/settings/Users/Users.page.tsx";
import UikitPage from "src/components/Uikit/Uikit.page.tsx";
import { BatteryContextProvider } from "src/contexts/battery.context";
import { CategoryBackOfficeContextProvider } from "src/contexts/categoryBackOffice.context";
import { CategoryBackOfficeListContextProvider } from "src/contexts/categoryBackOfficeList.context";
import { LanguageContextProvider } from "src/contexts/language.context";
import { LogoContextProvider } from "src/contexts/logo.context.tsx";
import { NewCategoryContextProvider } from "src/contexts/newCategory.context";
import { SnackbarContextProvider } from "src/contexts/snackbar.context.tsx";
import { ThemeContextProvider } from "src/contexts/theme.context.tsx";
import { UserContextProvider } from "src/contexts/user.context.tsx";
import ErrorPage from "src/error-page.tsx";
import i18n from "src/i18n.ts";

const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: "/applications",
        element: <ApplicationsPage />,
        loader: applicationsLoader,
      },
      {
        path: "/catalog",
        element: <CatalogPage />,
      },
      {
        path: "/category/:categoryId?",
        element: (
          <NewCategoryContextProvider>
            <CategoryPage />
          </NewCategoryContextProvider>
        ),
        errorElement: <CategoryErrorPage />,
      },
      {
        path: "/edito",
        element: <EditoPage />,
        loader: editoLoader,
      },
      {
        path: "/item/:itemId?",
        element: <ItemPage />,
        errorElement: <ItemErrorPage />,
        loader: itemLoader,
      },
      {
        path: "/search",
        element: <SearchPage />,
        loader: searchLoader,
      },
      {
        path: "/settings",
        element: <ProtectedRoute />,
        children: [
          {
            element: <SettingsLayout />,
            children: [
              {
                path: "",
                element: <ProtectedRoute />,
                children: [
                  {
                    index: true,
                    element: <DashboardPage />,
                  },
                ],
              },
              {
                path: "activity",
                element: <ProtectedRoute />,
                children: [
                  {
                    index: true,
                    element: <ActivityPage />,
                  },
                ],
              },
              {
                path: "applications",
                element: <ProtectedRoute />,
                children: [
                  {
                    index: true,
                    element: <ApplicationsSettingsPage />,
                    loader: applicationsSettingsLoader,
                  },
                ],
              },
              {
                path: "contents",
                element: <ProtectedRoute />,
                children: [
                  {
                    path: "",
                    element: (
                      <CategoryBackOfficeListContextProvider>
                        <ContentsPage />
                      </CategoryBackOfficeListContextProvider>
                    ),
                  },
                  {
                    path: "theme/:id",
                    element: (
                      <CategoryBackOfficeContextProvider>
                        <EditCategoryPage />
                      </CategoryBackOfficeContextProvider>
                    ),
                  },
                ],
              },
              {
                path: "users",
                element: <ProtectedRoute />,
                children: [
                  {
                    index: true,
                    element: <UsersPage />,
                    loader: usersLoader,
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        path: "/uikit",
        element: <UikitPage />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <I18nextProvider i18n={i18n}>
    <ThemeContextProvider>
      <LanguageContextProvider>
        <SnackbarContextProvider>
          <UserContextProvider>
            <LogoContextProvider>
              <MainWebSocket>
                <BatteryContextProvider>
                  <RouterProvider router={router} />
                </BatteryContextProvider>
              </MainWebSocket>
            </LogoContextProvider>
          </UserContextProvider>
        </SnackbarContextProvider>
      </LanguageContextProvider>
    </ThemeContextProvider>
  </I18nextProvider>,
);
