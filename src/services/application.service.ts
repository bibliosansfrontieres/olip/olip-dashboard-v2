import i18next from "i18next";
import Axios from "src/axios";
import {
  GetApplicationsParams,
  GetApplicationsParamsAPI,
  GetApplicationsResult,
  InstallApplicationParams,
  InstallApplicationResult,
  UninstallApplicationParams,
  UninstallApplicationResult,
  UpdateApplicationParams,
  UpdateApplicationResult,
  UpdateApplicationVisibilityParams,
  UpdateApplicationVisibilityResult,
} from "src/types/application.type";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const getApplications = async (
  params?: GetApplicationsParams,
): Promise<GetApplicationsResult> => {
  const path = "/api/applications";

  const apiParams: GetApplicationsParamsAPI = {
    withOlip: "false",
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      applications: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const installApplicationById = async (
  params: InstallApplicationParams,
): Promise<InstallApplicationResult> => {
  const path = "/api/applications/install/" + params.id;

  const apiParams: InstallApplicationParams = {
    ...params,
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      application: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateApplicationById = async (
  params: UpdateApplicationParams,
): Promise<UpdateApplicationResult> => {
  const path = "/api/applications/update/" + params.id;

  const apiParams: InstallApplicationParams = {
    ...params,
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const uninstallApplicationById = async (
  params: UninstallApplicationParams,
): Promise<UninstallApplicationResult> => {
  const path = "/api/applications/uninstall/" + params.id;

  const apiParams: UninstallApplicationParams = {
    ...params,
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateApplicationVisibility = async (
  id: number,
  newCategory: UpdateApplicationVisibilityParams,
): Promise<UpdateApplicationVisibilityResult> => {
  const path = `api/applications/${id}`;

  try {
    const result = await Axios.patch(path, newCategory);
    return { application: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const forceCheckUpdate = async (): Promise<boolean | undefined> => {
  const path = "/api/applications/force-check-update";

  try {
    await Axios.get(path);
    return true;
  } catch (error) {
    apiHandleError(error);
    return undefined;
  }
};
