import Axios from "src/axios";
import axios from "axios";
import { LoginResult } from "src/types/auth.service.type";

export const login = async (
  username: string,
  password?: string,
): Promise<LoginResult> => {
  const path = "/api/auth/signin";

  try {
    const result = await Axios.post(path, { username, password });
    return { accessToken: result.data.accessToken, statusCode: result.status };
  } catch (error) {
    if (axios.isAxiosError(error)) {
      return {
        errorMessage: error?.response?.data.message,
        statusCode: error?.response?.data.statusCode,
      };
    }
    return { errorMessage: "Erreur inconnue", statusCode: 500 };
  }
};
