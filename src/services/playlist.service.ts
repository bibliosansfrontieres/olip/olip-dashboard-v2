import { AxiosRequestConfig } from "axios";
import i18next from "i18next";
import Axios from "src/axios";
import {
  GetInstalledPlaylistsParams,
  GetInstalledPlaylistsParamsAPI,
  GetInstalledPlaylistsResult,
  GetUninstalledPlaylistsParams,
  GetUninstalledPlaylistsParamsAPI,
  GetUninstalledPlaylistsResult,
  GetUpdateNeededPlaylistsParams,
  GetUpdateNeededPlaylistsParamsAPI,
  GetUpdateNeededPlaylistsResult,
  InstallPlaylistsBody,
  InstallPlaylistsParams,
  InstallPlaylistsResult,
  UninstallPlaylistsBody,
  UninstallPlaylistsParams,
  UninstallPlaylistsResult,
  UpdatePlaylistsBody,
  UpdatePlaylistsParams,
  UpdatePlaylistsResult,
} from "src/types/playlist.type";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const getInstalledPlaylists = async (
  params: GetInstalledPlaylistsParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<GetInstalledPlaylistsResult> => {
  const path = "/api/playlists/installed";

  const apiParams: GetInstalledPlaylistsParamsAPI = {
    ...params,
    applicationIds: params.applicationIds || undefined,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
    query: params.query || "",
  };

  try {
    const result = await Axios.get(path, { params: apiParams, ...axiosConfig });
    return {
      playlists: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getUninstalledPlaylists = async (
  params: GetUninstalledPlaylistsParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<GetUninstalledPlaylistsResult> => {
  const path = "/api/playlists/uninstalled";

  const apiParams: GetUninstalledPlaylistsParamsAPI = {
    ...params,
    applicationIds: params.applicationIds || undefined,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
    query: params.query || "",
  };

  try {
    const result = await Axios.get(path, { params: apiParams, ...axiosConfig });
    return {
      playlists: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getUpdateNeededPlaylists = async (
  params: GetUpdateNeededPlaylistsParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<GetUpdateNeededPlaylistsResult> => {
  const path = "/api/playlists/update-needed";

  const apiParams: GetUpdateNeededPlaylistsParamsAPI = {
    ...params,
    applicationIds: params.applicationIds || undefined,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
    query: params.query || "",
    withItems: params.withItems || true,
  };

  try {
    const result = await Axios.get(path, { params: apiParams, ...axiosConfig });
    return {
      playlists: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const installPlaylists = async (
  params: InstallPlaylistsParams,
): Promise<InstallPlaylistsResult> => {
  const path = "/api/playlists/install";

  const body: InstallPlaylistsBody = { ...params };

  try {
    const result = await Axios.post(path, body);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const uninstallPlaylists = async (
  params: UninstallPlaylistsParams,
): Promise<UninstallPlaylistsResult> => {
  const path = "/api/playlists";

  const body: UninstallPlaylistsBody = {
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.delete(path, { data: body });
    return {
      linkedPlaylists: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updatePlaylists = async (
  params: UpdatePlaylistsParams,
): Promise<UpdatePlaylistsResult> => {
  const path = "/api/playlists/update";

  const body: UpdatePlaylistsBody = { ...params };

  try {
    const result = await Axios.post(path, body);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
