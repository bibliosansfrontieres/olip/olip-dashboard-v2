import Axios from "src/axios";
import {
  GetVisibilityParams,
  GetVisibilityResult,
} from "src/types/visibility.type";

import { apiHandleError } from "src/utils/apiHandleError";

export const getVisibility = async (
  params: GetVisibilityParams,
): Promise<GetVisibilityResult> => {
  const path = `/api/visibilities/${params.id}`;

  try {
    const result = await Axios.get(path);
    return {
      visibility: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
