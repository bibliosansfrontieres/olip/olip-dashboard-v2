import Axios from "src/axios";
import {
  GetItemParams,
  GetItemParamsAPI,
  GetItemResult,
  GetItemsParams,
  GetItemsParamsAPI,
  GetItemsResult,
  GetSuggestionsItemsParams,
  GetSuggestionsItemsParamsAPI,
  GetSuggestionsItemsResult,
} from "src/types/item.type";
import i18next from "i18next";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const getItem = async (
  params: GetItemParams,
): Promise<GetItemResult> => {
  const path = `/api/items/${params.id}`;

  const apiParams: GetItemParamsAPI = {
    languageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      item: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getItems = async (
  params?: GetItemsParams,
): Promise<GetItemsResult> => {
  const path = `/api/items`;

  const apiParams: GetItemsParamsAPI = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
    ...params,
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      items: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getSuggestionsItems = async (
  params: GetSuggestionsItemsParams,
): Promise<GetSuggestionsItemsResult> => {
  const path = `/api/items/suggestions/${params.id}`;

  const apiParams: GetSuggestionsItemsParamsAPI = {
    ...params,
    languageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      items: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
