import i18next from "i18next";
import Axios from "src/axios";
import {
  CreateHighlightResult,
  GetLastHighlightParams,
  GetLastHighlightParamsAPI,
  GetLastHighlightResult,
  HighlightFormValues,
} from "src/types/highlight.type";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const createHighlight = async (
  newHighlight: HighlightFormValues,
): Promise<CreateHighlightResult> => {
  const path = "/api/highlights";

  try {
    const result = await Axios.post(path, newHighlight);
    return { highlight: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getLastHighlight = async (
  params: GetLastHighlightParams,
): Promise<GetLastHighlightResult> => {
  const path = `/api/highlights`;

  const apiParams: GetLastHighlightParamsAPI = {
    ...params,
    itemsLanguageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      highlight: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
