import Axios from "src/axios";
import {
  CreateEditoResult,
  EditoFormValues,
  GetEditosResult,
  GetLastEditoParams,
  GetLastEditoParamsAPI,
  GetLastEditoResult,
} from "src/types/edito.type";
import { apiHandleError } from "src/utils/apiHandleError";

export const createEdito = async (
  newEdito: EditoFormValues,
): Promise<CreateEditoResult> => {
  const path = "/api/editos";

  try {
    const result = await Axios.post(path, newEdito);
    return { edito: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getEditos = async (): Promise<GetEditosResult> => {
  const path = `/api/editos`;

  try {
    const result = await Axios.get(path);
    return {
      editos: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getLastEdito = async (
  params: GetLastEditoParams,
): Promise<GetLastEditoResult> => {
  const path = `/api/editos/last`;

  const apiParams: GetLastEditoParamsAPI = params;

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      edito: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
