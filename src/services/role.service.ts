import i18next from "i18next";
import Axios from "src/axios";
import {
  GetRolesParams,
  GetRolesParamsAPI,
  GetRolesResult,
} from "src/types/role.type";
import getEnv from "src/utils/getEnv";

import { apiHandleError } from "src/utils/apiHandleError";

export const getRoles = async (
  params: GetRolesParams,
): Promise<GetRolesResult> => {
  const path = `/api/roles`;

  const apiParams: GetRolesParamsAPI = {
    languageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      roles: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
