import i18next from "i18next";
import Axios from "src/axios";
import {
  GetInstalledContentsByCategoryParamsAPI,
  GetInstalledContentsByCategoryResult,
  GetInstalledContentsResult,
} from "src/types/stat.type";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const getInstalledContents =
  async (): Promise<GetInstalledContentsResult> => {
    const path = `/api/stats/installed-contents`;

    try {
      const result = await Axios.get(path);
      return {
        installedContents: result.data.installedContents,
        statusCode: result.status,
      };
    } catch (error) {
      return apiHandleError(error);
    }
  };

export const getInstalledContentsByCategory =
  async (): Promise<GetInstalledContentsByCategoryResult> => {
    const path = `/api/stats/installed-contents-by-category`;

    const apiParams: GetInstalledContentsByCategoryParamsAPI = {
      languageIdentifier: i18next.language || getEnv("VITE_DEFAULT_LANGUAGE"),
    };

    try {
      const result = await Axios.get(path, { params: apiParams });
      return {
        data: result.data,
        statusCode: result.status,
      };
    } catch (error) {
      return apiHandleError(error);
    }
  };
