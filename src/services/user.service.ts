import Axios from "src/axios";
import {
  CreateUserBody,
  CreateUserResult,
  DeleteUserResult,
  GetMyProfileResult,
  GetUsersParams,
  GetUsersResult,
  UpdateUserBody,
  UpdateUserResult,
} from "src/types/user.service.type";
import { apiHandleError } from "src/utils/apiHandleError";

export const createUser = async (
  newUser: CreateUserBody,
): Promise<CreateUserResult> => {
  const path = "/api/users";

  try {
    const result = await Axios.post(path, newUser);
    return { user: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const deleteUser = async (id: number): Promise<DeleteUserResult> => {
  const path = `/api/users/${id}`;

  try {
    const result = await Axios.delete(path);
    return { user: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getMyProfile = async (): Promise<GetMyProfileResult> => {
  const path = "/api/users/profile";

  try {
    const result = await Axios.get(path);
    return { user: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getUsers = async (
  params: GetUsersParams,
): Promise<GetUsersResult> => {
  const path = "/api/users";

  try {
    const result = await Axios.get(path, { params: params });
    return {
      users: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateProfile = async (
  user: UpdateUserBody,
): Promise<UpdateUserResult> => {
  const path = `/api/users/profile`;

  try {
    const result = await Axios.patch(path, user);
    return { user: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateUser = async (
  id: number,
  user: UpdateUserBody,
): Promise<UpdateUserResult> => {
  const path = `/api/users/${id}`;

  try {
    const result = await Axios.patch(path, user);
    return { user: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};
