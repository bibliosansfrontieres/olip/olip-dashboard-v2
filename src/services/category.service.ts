import i18next from "i18next";
import Axios from "src/axios";
import { ApiRequestResponse } from "src/types/apiRequestResponse.type";
import {
  AddPlaylistToCategoryBody,
  AddPlaylistToCategoryParams,
  AddPlaylistToCategoryResult,
  CreateCategoryParams,
  CreateCategoryResult,
  DeleteCategoryResult,
  GetCategoriesListParams,
  GetCategoriesListResult,
  GetCategoriesParams,
  GetCategoriesParamsAPI,
  GetCategoriesResult,
  GetCategoryBackOfficeParams,
  GetCategoryParams,
  GetCategoryParamsAPI,
  GetCategoryResult,
  GetCategorySelectionParams,
  GetCategorySelectionParamsAPI,
  GetCategorySelectionResult,
  NewGetCategoriesParams,
  UpdateCategoryParams,
  UpdateCategoryResult,
  UpdateOrderCategoriesBody,
  UpdateOrderCategoriesResult,
} from "src/types/category.type";
import { CategoryBackOffice } from "src/types/category/categoryBackOffice.type";
import { CategoryBackOfficeCategoryPlaylists } from "src/types/category/categoryBackOfficeCategoryPlaylists.type";
import { CategoryBackOfficeList } from "src/types/category/categoryBackOfficeList.type";
import { CategoryCatalog } from "src/types/category/categoryCatalog.type";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const addPlaylistToCategory = async (
  params: AddPlaylistToCategoryParams,
): Promise<AddPlaylistToCategoryResult> => {
  const path = `api/categories/${params.categoryId}/playlists`;

  const body: AddPlaylistToCategoryBody = {
    playlistIds: params.playlistIds,
  };

  try {
    const result = await Axios.post(path, body);
    return { statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const createCategory = async (
  newCategory: CreateCategoryParams,
): Promise<CreateCategoryResult> => {
  const path = "api/categories";

  try {
    const result = await Axios.post(path, newCategory);
    return { category: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const deleteCategory = async (
  id: number,
): Promise<DeleteCategoryResult> => {
  const path = `/api/categories/${id}`;

  try {
    const result = await Axios.delete(path);
    return { category: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getCategories = async (
  params?: GetCategoriesParams,
): Promise<GetCategoriesResult> => {
  const path = "/api/categories";

  const apiParams: GetCategoriesParamsAPI = {
    ...params,
    applicationIds: params?.applicationIds,
    categoryId: params?.categoryId,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
    order: params?.order || "popularity",
    withPlaylists: params?.withPlaylists || "false",
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      categories: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getCategoriesList = async (
  params?: GetCategoriesListParams,
): Promise<ApiRequestResponse<GetCategoriesListResult>> => {
  const path = "/api/categories/list";

  const apiParams: GetCategoriesListParams = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    return undefined;
  }
};

export const getCategoriesCatalog = async (
  params?: NewGetCategoriesParams,
): Promise<ApiRequestResponse<CategoryCatalog>> => {
  const path = "/api/categories/catalog";

  const apiParams: GetCategoriesListParams = {
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    return undefined;
  }
};

export const getCategoriesBackOfficeList = async (
  params?: GetCategoriesListParams,
): Promise<ApiRequestResponse<CategoryBackOfficeList>> => {
  const path = "/api/categories/back-office/list";
  const apiParams: GetCategoriesListParams = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };
  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    return undefined;
  }
};

export const getCategoryBackOffice = async (
  params: GetCategoryBackOfficeParams,
): Promise<ApiRequestResponse<CategoryBackOffice>> => {
  const path = `/api/categories/back-office/${params.id}`;
  const apiParams: GetCategoriesListParams = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };
  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    return undefined;
  }
};

export const getCategoryBackOfficeCategoryPlaylists = async (
  params: GetCategoryBackOfficeParams,
): Promise<ApiRequestResponse<CategoryBackOfficeCategoryPlaylists>> => {
  const path = `/api/categories/back-office/${params.id}/category-playlists`;
  const apiParams: GetCategoriesListParams = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };
  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    return undefined;
  }
};
export const getCategory = async (
  params: GetCategoryParams,
  withOnlyPlaylists = false,
): Promise<ApiRequestResponse<GetCategoryResult>> => {
  const path = `/api/categories/${params.id}${withOnlyPlaylists ? "/playlists" : ""}`;

  const apiParams: GetCategoryParamsAPI = {
    ...params,
    languageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      category: result.data.category,
      items: result.data.data,
      meta: result.data.meta,
    };
  } catch (error) {
    return undefined;
  }
};

export const getCategorySelection = async (
  params: GetCategorySelectionParams,
): Promise<GetCategorySelectionResult> => {
  const path = `/api/categories/selection/${params.categoryId}`;

  const apiParams: GetCategorySelectionParamsAPI = {
    languageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return { categoryPlaylistItems: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateCategory = async (
  id: number,
  newCategory: UpdateCategoryParams,
): Promise<UpdateCategoryResult> => {
  const path = `api/categories/${id}`;

  try {
    const result = await Axios.patch(path, newCategory);
    return { category: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateOrderCategories = async (
  body: UpdateOrderCategoriesBody,
): Promise<UpdateOrderCategoriesResult> => {
  const path = `api/categories/order`;

  try {
    const result = await Axios.patch(path, body);
    return { statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};
