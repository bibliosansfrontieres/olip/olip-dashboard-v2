import i18next from "i18next";
import Axios from "src/axios";
import { ApiRequestResponse } from "src/types/apiRequestResponse.type";
import { GetCategoryBackOfficeParams } from "src/types/category.type";
import {
  GetCategoryContentParams,
  GetCategoryContentParamsAPI,
  GetCategoryContentResult,
  RemoveCategoryPlaylistsBody,
  RemoveCategoryPlaylistsResult,
  UpdateCategoryPlaylistItemParams,
  UpdateCategoryPlaylistItemResult,
  UpdateOrderCategoryPlaylistsBody,
  UpdateOrderCategoryPlaylistsResult,
} from "src/types/categoryContent.type";
import { CategoryPlaylistItemList } from "src/types/categoryPlaylistItem/categoryPlaylistItemList.type";
import { apiHandleError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const getCategoryContent = async (
  params: GetCategoryContentParams,
): Promise<GetCategoryContentResult> => {
  const path = `/api/categories/content/${params.id}`;

  const apiParams: GetCategoryContentParamsAPI = {
    ...params,
    languageIdentifier:
      params.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      contents: result.data.data,
      meta: result.data.meta,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const removeCategoryPlaylists = async (
  playlistIds: number[],
): Promise<RemoveCategoryPlaylistsResult> => {
  const path = `/api/category-playlists`;

  const body: RemoveCategoryPlaylistsBody = {
    ids: playlistIds,
  };
  try {
    const result = await Axios.delete(path, { data: body });
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateCategoryPlaylistItem = async (
  itemId: number,
  updates: UpdateCategoryPlaylistItemParams,
): Promise<UpdateCategoryPlaylistItemResult> => {
  const path = `/api/category-playlist-items/${itemId}`;

  try {
    const result = await Axios.patch(path, updates);
    return {
      categoryPlaylistItem: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateOrderCategoryPlaylists = async (
  body: UpdateOrderCategoryPlaylistsBody,
): Promise<UpdateOrderCategoryPlaylistsResult> => {
  const path = `/api/category-playlists/order`;

  try {
    const result = await Axios.patch(path, body);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getCategoryPlaylistsBackOfficeCategoryPlaylistItems = async (
  params: GetCategoryBackOfficeParams,
): Promise<ApiRequestResponse<CategoryPlaylistItemList>> => {
  const path = `/api/category-playlists/back-office/${params.id}/category-playlist-items`;
  const apiParams = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };
  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    return undefined;
  }
};
