import i18next from "i18next";
import Axios from "src/axios";
import { apiHandleError } from "src/utils/apiHandleError";
import {
  GetSearchItemsResult,
  GetSearchCategoriesResult,
  GetSearchParams,
  GetSearchParamsAPI,
  GetSearchResult,
  GetSearchApplicationsResult,
  GetSearchPlaylistsResult,
  GetSuggestionParams,
  GetSuggestionsResult,
} from "src/types/search.type";
import getEnv from "src/utils/getEnv";
import { AxiosRequestConfig } from "axios";

export const getSearch = async (
  params?: GetSearchParams,
): Promise<GetSearchResult> => {
  const path = "/api/search";
  const apiParams: GetSearchParamsAPI = {
    ...params,
    take: 4,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      items: result.data.items,
      playlists: result.data.playlists,
      categories: result.data.categories,
      applications: result.data.applications,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getSearchItems = async (
  params?: GetSearchParams,
): Promise<GetSearchItemsResult> => {
  const path = "/api/search/items";
  const apiParams: GetSearchParamsAPI = {
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      items: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getSearchCategories = async (
  params?: GetSearchParams,
): Promise<GetSearchCategoriesResult> => {
  const path = "/api/search/categories";
  const apiParams: GetSearchParamsAPI = {
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      categories: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getSearchApplications = async (
  params?: GetSearchParams,
): Promise<GetSearchApplicationsResult> => {
  const path = "/api/search/applications";
  const apiParams: GetSearchParamsAPI = {
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      applications: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getSearchPlaylists = async (
  params?: GetSearchParams,
): Promise<GetSearchPlaylistsResult> => {
  const path = "/api/search/playlists";
  const apiParams: GetSearchParamsAPI = {
    ...params,
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return {
      categoryPlaylists: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const getSuggestions = async (
  params: GetSuggestionParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<GetSuggestionsResult> => {
  const path = "/api/search/suggestions";

  try {
    const result = await Axios.get(path, { params, ...axiosConfig });
    return {
      suggestions: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
