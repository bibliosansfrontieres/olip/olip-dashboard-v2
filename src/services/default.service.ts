import i18next from "i18next";
import Axios from "src/axios";
import { ApiRequestResponse } from "src/types/apiRequestResponse.type";
import {
  GetApplicationTypesParams,
  GetApplicationTypesResult,
} from "src/types/applicationType.type";
import { handleApiError } from "src/utils/apiHandleError";
import getEnv from "src/utils/getEnv";

export const getApplicationTypes = async (
  params?: GetApplicationTypesParams,
): Promise<ApiRequestResponse<GetApplicationTypesResult>> => {
  const path = "/api/application-types";

  const apiParams: GetApplicationTypesParams = {
    languageIdentifier:
      params?.languageIdentifier ||
      i18next.language ||
      getEnv("VITE_DEFAULT_LANGUAGE"),
  };

  try {
    const result = await Axios.get(path, { params: apiParams });
    return result.data;
  } catch (error) {
    handleApiError(error);
    return undefined;
  }
};
