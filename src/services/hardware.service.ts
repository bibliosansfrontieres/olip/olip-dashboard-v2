import Axios from "src/axios";
import { GetSSIDResult } from "src/types/hardware.type";
import { apiHandleError } from "src/utils/apiHandleError";

export const getSSID = async (): Promise<GetSSIDResult> => {
  const path = `/api/hardware/ssid`;

  try {
    const result = await Axios.get(path);
    return {
      ssid: result.data.ssid,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
