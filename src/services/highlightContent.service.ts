import Axios from "src/axios";
import {
  AddHighlightContentBody,
  AddHighlightContentResult,
  RemoveHighlightContentResult,
  UpdateDisplayHighlightContentParams,
  UpdateDisplayHighlightContentResult,
  UpdateOrderHighlightContentBody,
  UpdateOrderHighlightContentResult,
} from "src/types/highlightContent.type";
import { apiHandleError } from "src/utils/apiHandleError";

export const addHighlightContent = async (
  itemId: string,
): Promise<AddHighlightContentResult> => {
  const path = `/api/highlight-contents`;

  const apiBody: AddHighlightContentBody = {
    itemId: itemId.toString(),
  };

  try {
    const result = await Axios.post(path, apiBody);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const removeHighlightContent = async (
  highlightContentId: number,
): Promise<RemoveHighlightContentResult> => {
  const path = `/api/highlight-contents/${highlightContentId}`;

  try {
    const result = await Axios.delete(path);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateDisplayHighlightContent = async (
  params: UpdateDisplayHighlightContentParams,
): Promise<UpdateDisplayHighlightContentResult> => {
  const path = `/api/highlight-contents/display`;

  try {
    const result = await Axios.patch(path, params);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateOrderHighlightContent = async (
  highlightContentIdsOrder: number[],
): Promise<UpdateOrderHighlightContentResult> => {
  const path = `/api/highlight-contents/order`;

  const apiBody: UpdateOrderHighlightContentBody = {
    highlightContentIds: highlightContentIdsOrder,
  };

  try {
    const result = await Axios.patch(path, apiBody);
    return {
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};
