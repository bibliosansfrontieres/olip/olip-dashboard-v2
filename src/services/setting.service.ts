import Axios from "src/axios";
import {
  SettingKey,
  GetSettingResult,
  UpdateSettingBody,
  UpdateSettingResult,
} from "src/types/setting.service.type";
import { apiHandleError } from "src/utils/apiHandleError";

export const getSetting = async (
  key: SettingKey,
): Promise<GetSettingResult> => {
  const path = `/api/settings/${key}`;

  try {
    const result = await Axios.get(path);
    return {
      setting: result.data,
      statusCode: result.status,
    };
  } catch (error) {
    return apiHandleError(error);
  }
};

export const updateSetting = async (
  key: SettingKey,
  setting: UpdateSettingBody,
): Promise<UpdateSettingResult> => {
  const path = `/api/settings/${key}`;

  try {
    const result = await Axios.patch(path, setting);
    return { setting: result.data, statusCode: result.status };
  } catch (error) {
    return apiHandleError(error);
  }
};
