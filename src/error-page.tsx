import { useTranslation } from "react-i18next";
import { useNavigate, useRouteError } from "react-router-dom";

import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import { Box, Button } from "@mui/material";

export default function ErrorPage() {
  const error = useRouteError() as Error;
  console.error(error);
  const navigate = useNavigate();
  const { t } = useTranslation();

  return (
    <Box
      sx={{
        backgroundColor: (theme) => theme.palette.primary.light,
        color: "inherit",
        display: "flex",
        height: "100%",
        textDecoration: "none",
        flexDirection: "column",
        margin: "0 3rem",
      }}
      id="error-page"
    >
      <h1>Oopsy!</h1>
      <p>Sorry, an unexpected error has occurred.</p>
      <p>
        <i>{error.message}</i>
      </p>
      <Button
        variant="rounded"
        onClick={() => navigate(-1)}
        startIcon={<ArrowBackRoundedIcon />}
        sx={{
          marginBottom: { xs: "1.5rem", sm: "0" },
          marginTop: "2.5rem",
        }}
      >
        {t("actions.back")}
      </Button>
    </Box>
  );
}
