import { useState } from "react";

const useModal = () => {
  const [open, setOpen] = useState<boolean>(false);

  function toggle(value?: boolean) {
    setOpen(value || !open);
  }

  return [open, toggle] as const;
};

export default useModal;
