import { useEffect, useState } from "react";
import { getLastEdito } from "src/services/edito.service";
import { getSetting } from "src/services/setting.service";
import { Edito } from "src/types/edito.type";

type Params = { checkIfAvailable?: boolean; id?: number; language?: string };

const useEdito = (params?: Params) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [edito, setEdito] = useState<Edito | null>(null);

  useEffect(() => {
    getEdito(params || {});
  }, [params?.checkIfAvailable, params?.id, params?.language]);

  const getEdito = async (getEditoParam: Params): Promise<void> => {
    setLoading(true);
    let available = true;
    try {
      if (getEditoParam.checkIfAvailable) {
        const { setting } = await getSetting("is_edito_displayed");
        available = setting?.value === "true";
      }
      if (available) {
        if (getEditoParam.id) {
          console.log("A implémenter");
        } else {
          const { edito } = await getLastEdito({
            languageIdentifier: getEditoParam?.language,
          });
          setEdito(edito || null);
        }
      } else {
        setEdito(null);
      }
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
    }
  };

  const refreshEdito = (): void => {
    getEdito(params || {});
  };

  return { edito, loading, refreshEdito, setEdito } as const;
};

export default useEdito;
