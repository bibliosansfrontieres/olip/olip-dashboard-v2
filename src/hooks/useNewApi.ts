import { useEffect, useState } from "react";
import { ApiRequestResponse } from "src/types/apiRequestResponse.type";
import { UseApiType } from "src/types/useApi.type";

import useSkipFirstRender from "./useSkipFirstRender";

/**
 * Hook to handle API requests and provide a simple way to manage loading state and hasErrors.
 * @template T The type of data returned by the API.
 * @param {function(): Promise<ApiRequestResponse<T>>} callApi A function that returns a Promise of type ApiRequestResponse<T>, where T is the type of data returned by the API.
 * @param {boolean} isEnabled A boolean indicating whether the API request should be done or not.
 * @returns {{isLoading: boolean, hasError: boolean, result: T | undefined, setReload: Dispatch<SetStateAction<boolean>>}} An object with four properties: isLoading, hasError, result, and setReload.
 *   - isLoading: true if the API request is currently in progress.
 *   - hasError: true if an hasError occurred during the API request.
 *   - result: The data returned by the API, of type T.
 *   - setReload: A function that triggers a reload of the data.
 */
const useNewApi = <T>(
  callApi: () => Promise<ApiRequestResponse<T>>,
  isEnabled = true,
): UseApiType<T> => {
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const [result, setResult] = useState<T>();
  const [load, setLoad] = useState(true);
  const reload = () => {
    setIsLoading(true);
    setLoad(true);
  };
  useEffect(() => {
    if (!isEnabled || !load) {
      return;
    }
    /**
     * Initiates an API request using the provided callApi function and handles the response.
     *
     * @async
     * @returns {Promise<void>}
     */
    const launchCallApi = async () => {
      // set loading true when callApi change
      setIsLoading(true);
      const resultCall = await callApi();
      if (resultCall) {
        setResult(resultCall);
        setHasError(false);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        setHasError(true);
      }
      // wait for the resultcall before changing load
      if (load) {
        setLoad(false);
      }
    };
    launchCallApi();
  }, [callApi, load, isEnabled]);

  // do not call on first render, used for to reload when the call api change (ie with searchParams)
  useSkipFirstRender(() => {
    if (!isEnabled) return;
    setLoad(true);
  }, [callApi, isEnabled]);

  return {
    isLoading,
    result,
    hasError,
    reload,
    setResult,
  };
};

export default useNewApi;
