import { useEffect, useState } from "react";
import { socket } from "src/socket.io";

export type UseSocketOnEventReturns<T> = {
  event: T | null;
  isLoading: boolean;
};
const useSocketOnEvent = <T>(eventName: string): UseSocketOnEventReturns<T> => {
  const [event, setEvent] = useState<T | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    function onEvent(newEvent: T) {
      setEvent(newEvent);
      setIsLoading(false);
    }
    socket.on(eventName, onEvent);

    socket.emit(eventName);

    return () => {
      socket.off(eventName, onEvent);
    };
  }, []);

  /* useEffect(() => {
    console.log(JSON.stringify(event));
  }, [event]); */

  return { event, isLoading };
};

export default useSocketOnEvent;
