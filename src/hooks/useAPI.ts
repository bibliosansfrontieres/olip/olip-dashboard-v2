import { DependencyList, useEffect, useRef, useState } from "react";
import { AxiosRequestConfig } from "axios";
import { BaseAPIResult } from "src/types";
import { ApiRequestResponse } from "src/types/apiRequestResponse.type";

const useAPI = <Type>(
  callAPI: (
    config?: AxiosRequestConfig,
  ) => Promise<Type & BaseAPIResult> | Promise<ApiRequestResponse<Type>>,
  dependencies?: DependencyList,
) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [result, setResult] = useState<Type | undefined>();
  const [error, setError] = useState<Error | null>(null);
  const abortControllerRef = useRef<AbortController | null>(null);

  useEffect(() => {
    launchCallAPI();
    return () => {
      if (abortControllerRef.current) {
        abortControllerRef.current.abort();
      }
    };
  }, dependencies || []);

  const launchCallAPI = async (): Promise<void> => {
    setIsLoading(true);
    if (abortControllerRef.current) {
      abortControllerRef.current.abort();
    }
    abortControllerRef.current = new AbortController();

    try {
      const resultAPI = await callAPI({
        signal: abortControllerRef.current.signal,
      });
      setResult(resultAPI);
      if (
        (resultAPI &&
          typeof resultAPI === "object" &&
          "errorMessage" in resultAPI &&
          resultAPI.errorMessage !== "CanceledError") ||
        (resultAPI !== null && typeof resultAPI === "object")
      ) {
        setIsLoading(false);
      }
    } catch (e) {
      setResult(undefined);
      setError(e as Error);
      setIsLoading(false);
    }
  };

  return { error, isLoading, refresh: launchCallAPI, result };
};

export default useAPI;
