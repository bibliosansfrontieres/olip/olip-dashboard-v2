import { useTranslation } from "react-i18next";
import { VisibilityTypeEnum } from "src/enums/visibilityType.enum";
import { VisibilityBase } from "src/types/visibility/visibilityBase.type";
import { getVisibilityType } from "src/utils/getVisibilityType";

export function useVisibilityDetails(
  visibility: VisibilityBase | undefined,
): string {
  const { t } = useTranslation();
  if (!visibility) return "";
  const visibilityType = getVisibilityType(visibility);
  switch (visibilityType) {
    case VisibilityTypeEnum.BY_USER:
      return visibility.users.map((user) => user.username).join(", ");
    case VisibilityTypeEnum.BY_ROLE:
      return visibility.roles.map((role) => t(`roles.${role.name}`)).join(", ");
    case VisibilityTypeEnum.PUBLIC:
      return "";
  }
}
