import { useEffect, useState } from "react";
import { getSSID as getSSIDByAPI } from "src/services/hardware.service";

const useGetSSID = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [ssid, setSsid] = useState<string | null | undefined>();

  useEffect(() => {
    getSSID();
  }, []);

  const getSSID = async (): Promise<void> => {
    setLoading(true);
    try {
      const { ssid } = await getSSIDByAPI();
      setSsid(ssid);
    } catch (_e) {
      setSsid(undefined);
    } finally {
      setLoading(false);
    }
  };

  const refreshSSID = (): void => {
    getSSID();
  };

  return { ssid, loading, refreshSSID } as const;
};

export default useGetSSID;
