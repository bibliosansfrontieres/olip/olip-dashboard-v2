import { useEffect, useState } from "react";
import { getRoles as getRolesByAPI } from "src/services/role.service";
import { Role } from "src/types/role.type";

type Params = { language?: string };

const useGetRoles = (params?: Params) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [roles, setRoles] = useState<Role[] | []>([]);

  useEffect(() => {
    getRoles();
  }, []);

  const getRoles = async (): Promise<void> => {
    setLoading(true);
    try {
      const { roles } = await getRolesByAPI({
        languageIdentifier: params?.language,
      });
      setRoles(roles || []);
    } catch (_e) {
      setRoles([]);
    } finally {
      setLoading(false);
    }
  };

  const refresh = (): void => {
    getRoles();
  };

  return { roles, loading, refresh } as const;
};

export default useGetRoles;
