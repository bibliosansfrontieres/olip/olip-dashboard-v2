import { useEffect, useState } from "react";
import { getLastHighlight } from "src/services/highlight.service";
import { getSetting } from "src/services/setting.service";
import { Highlight } from "src/types/highlight.type";

type Params = { checkIfAvailable?: boolean; id?: number; language?: string };

const useHighlight = (params?: Params) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [highlight, setHighlight] = useState<Highlight | null>(null);

  useEffect(() => {
    getHighlight(params || {});
  }, [params?.checkIfAvailable, params?.id, params?.language]);

  const getHighlight = async (
    getHighlightParam: Params = {},
  ): Promise<Highlight | null> => {
    setLoading(true);
    let highlightToReturn = null;
    let available = true;
    try {
      if (getHighlightParam.checkIfAvailable) {
        const { setting } = await getSetting("is_highlight_displayed");
        available = setting?.value === "true";
      }
      if (available) {
        if (getHighlightParam.id) {
          console.log("A implémenter");
        } else {
          const { highlight } = await getLastHighlight({
            languageIdentifier: getHighlightParam?.language,
          });
          setHighlight(highlight || null);
          highlightToReturn = highlight || null;
        }
      } else {
        setHighlight(null);
      }
      return highlightToReturn;
    } catch (e) {
      console.error(e);
      return null;
    } finally {
      setLoading(false);
    }
  };

  const refreshHighlight = (): void => {
    getHighlight(params || {});
  };

  return {
    getHighlight,
    highlight,
    loading,
    refreshHighlight,
    setHighlight,
  } as const;
};

export default useHighlight;
