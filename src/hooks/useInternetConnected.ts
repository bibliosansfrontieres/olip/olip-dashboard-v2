import { useEffect, useState } from "react";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import { InternetAvailableEvent } from "src/types/socket.type";

const useInternetConnected = () => {
  const { event: internetAvailableEvent } =
    useSocketOnEvent<InternetAvailableEvent>("internet-connection");
  const [isInternetConnected, setIsInternetConnected] = useState<
    boolean | undefined
  >(undefined);

  useEffect(() => {
    setIsInternetConnected(internetAvailableEvent?.isConnected);
  }, [internetAvailableEvent?.isConnected]);

  return isInternetConnected;
};

export default useInternetConnected;
