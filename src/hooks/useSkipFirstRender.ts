import { useEffect, useRef } from "react";

function useSkipFirstRender(
  callback: () => Promise<unknown> | unknown,
  dependencies: React.DependencyList,
) {
  const firstRenderRef = useRef(true);

  useEffect(() => {
    if (firstRenderRef.current) {
      firstRenderRef.current = false;
      return;
    }
    callback();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, dependencies);
}

export default useSkipFirstRender;
