import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";

export type SetParam = { name: string; value: string; defaultValue?: string };

const useHandleSearchParams = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [params, setParams] = useState<{ [key: string]: string }>();

  useEffect(() => {
    const actualParams: { [key: string]: string } = {};
    for (const [key, value] of searchParams.entries()) {
      actualParams[key] = value;
    }
    setParams(actualParams);
  }, [searchParams]);

  const handleParam = (setParams: SetParam | SetParam[]) => {
    const newParams = Array.isArray(setParams) ? setParams : [setParams];
    setSearchParams((actualParams) => {
      newParams.map(({ name, value, defaultValue }) => {
        if (value === defaultValue) {
          actualParams.delete(name);
        } else {
          actualParams.set(name, value);
        }
      });
      return actualParams;
    });
  };

  return [params, handleParam] as const;
};

export default useHandleSearchParams;
