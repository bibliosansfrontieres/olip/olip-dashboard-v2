import { FormikErrors, FormikProps } from "formik";
import { useEffect } from "react";

type ExtendedFormikProps<T> = FormikProps<T> & {
  values: {
    languages: string[];
  };
  isValid: boolean;
};

type Params<T> = {
  formik: ExtendedFormikProps<T>;
  actualLanguageIndex: number;
  changeDisplayLanguage: (lang: string) => void;
};

const useChangeLangFormWhenError = <T>(params: Params<T>) => {
  const { formik, changeDisplayLanguage, actualLanguageIndex } = params;

  /**
   * Permet de positionner l'utilisateur sur la langue qui contient une erreur
   */
  useEffect(() => {
    if (formik.isValid) return;

    const fieldErrorNames = getFieldErrorNames(formik.errors);
    if (fieldErrorNames.length <= 0 || formik.values.languages.length < 2)
      return;

    for (let index = 0; index < formik.values.languages.length; index++) {
      for (let i = 0; i < fieldErrorNames.length; i++) {
        if (
          fieldErrorNames[i].includes(`.${index}.`) &&
          actualLanguageIndex !== index
        ) {
          changeDisplayLanguage(formik.values.languages[index]);
          return;
        }
      }
    }
  }, [formik.submitCount]);

  const getFieldErrorNames = <FormValues>(
    formikErrors: FormikErrors<FormValues>,
  ) => {
    const transformObjectToDotNotation = (
      obj: FormikErrors<FormValues>,
      prefix = "",
      result: string[] = [],
    ) => {
      Object.keys(obj).forEach((key) => {
        const objectKey = key as keyof FormikErrors<FormValues>;
        const value = obj[objectKey];
        if (!value) return;

        const nextKey = prefix ? `${prefix}.${key}` : key;
        if (typeof value === "object") {
          transformObjectToDotNotation(value, nextKey, result);
        } else {
          result.push(nextKey);
        }
      });

      return result;
    };

    return transformObjectToDotNotation(formikErrors);
  };
};

export default useChangeLangFormWhenError;
