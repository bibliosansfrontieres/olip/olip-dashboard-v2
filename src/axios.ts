import axios from "axios";
import getEnv from "./utils/getEnv";

const instance = axios.create({
  baseURL: getEnv("VITE_AXIOS_BASEURL"),
  timeout: 30000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use((config) => {
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
});

export default instance;
