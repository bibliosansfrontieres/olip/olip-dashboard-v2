export enum CategorySortBy {
  ALPHABETICAL = "alphabetical",
  COUNT_PLAYLIST = "count_playlists",
  LAST_UPDATE = "last_update",
}
