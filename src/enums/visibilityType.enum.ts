export enum VisibilityTypeEnum {
  PUBLIC = "public",
  BY_USER = "byUser",
  BY_ROLE = "byRole",
}
