export enum ApplicationTabTypeEnum {
  ALL = "all",
  INSTALLED = "installed",
  NOT_INSTALLED = "notInstalled",
}
