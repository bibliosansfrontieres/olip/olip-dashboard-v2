export enum RoleEnum {
  ADMIN = "admin",
  ANIMATOR = "animator",
  USER = "user",
  USER_NO_PASSWORD = "user_no_password",
}
