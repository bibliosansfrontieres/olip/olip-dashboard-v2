import { blue, green, indigo, pink, purple } from "@mui/material/colors";
import { Color, Language, Page, WithProtection } from "../types";

export const ITEM_TYPE = {
  TEXT: { id: 1, name: "item.type.1" },
  VIDEO: { id: 2, name: "item.type.2" },
  AUDIO: { id: 3, name: "item.type.3" },
  IMAGE: { id: 4, name: "item.type.4" },
  OTHER: { id: 5, name: "item.type.5" },
};

export const menuListPages: Page[] = [
  { name: "header.pages.home", link: "/" },
  { name: "header.pages.catalog", link: "/catalog" },
  { name: "header.pages.applications", link: "/applications" },
];

export const ROOT = "admin";

export const SETTINGS_LIST_PAGES: Array<Page & WithProtection> = [
  {
    name: "settings.pages.dashboard.title",
    link: "/settings",
    permission: null,
  },
  {
    name: "settings.pages.users.titleForMenu",
    link: "/settings/users",
    permission: "read_users",
  },
  {
    name: "settings.pages.applications.title",
    link: "/settings/applications",
    permission: "manage_applications",
  },
  {
    name: "settings.pages.contents.title",
    link: "/settings/contents",
    permission: "update_category",
  },
  {
    name: "settings.pages.activity.title",
    link: "/settings/activity",
    permission: "read_activity",
  },
];

export const supportedLanguages: Language[] = [
  { id: "ara", name: "ٱلْعَرَبِيَّة" },
  { id: "ben", name: "বাংলা" },
  { id: "deu", name: "Deutsch" },
  { id: "ell", name: "Ελληνικά" },
  { id: "eng", name: "English" },
  { id: "fas", name: "فارسی" },
  { id: "fuc", name: "Pulaar" },
  { id: "fra", name: "Français" },
  { id: "hau", name: "Hausa" },
  { id: "hin", name: "हिन्दी" },
  { id: "ita", name: "Italiano" },
  { id: "nld", name: "Nederlands" },
  { id: "pol", name: "Polski" },
  //prs
  { id: "pus", name: "پښتو" },
  { id: "run", name: "Ikirundi" },
  { id: "ron", name: "Română" },
  { id: "rus", name: "Русский" },
  { id: "spa", name: "Español" },
  { id: "swa", name: "Kiswahili" },
  { id: "ukr", name: "Українська" },
  { id: "wol", name: "Wolof" },
];

export const APPLICATION_COLOR: Color[] = [
  { name: "blue", color: blue[500] },
  { name: "green", color: green[500] },
  { name: "orangeBSF", color: "#FF5B29" },
  { name: "yellowPrincipal", color: "#FDB851" },
  { name: "pink", color: pink[500] },
  { name: "purple", color: purple[500] },
  { name: "indigo", color: indigo[500] },
];
