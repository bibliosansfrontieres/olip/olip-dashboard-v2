import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import getEnv from "./utils/getEnv";
import translationARA from "src/locales/ar/ar.json";
import translationBEN from "src/locales/bn/bn.json";
import translationDEU from "src/locales/de/de.json";
import translationELL from "src/locales/el/el.json";
import translationENG from "src/locales/en/en.json";
import translationFAS from "src/locales/fa/fa.json";
import translationFRA from "src/locales/fr/fr.json";
import translationFUC from "src/locales/fuc/fuc.json";
import translationITA from "src/locales/it/it.json";
import translationHAU from "src/locales/ha/ha.json";
import translationHIN from "src/locales/hi/hi.json";
import translationNLD from "src/locales/nl/nl.json";
import translationPOL from "src/locales/pl/pl.json";
// PRS
import translationPUS from "src/locales/ps/ps.json";
import translationRON from "src/locales/ro/ro.json";
import translationRUN from "src/locales/rn/rn.json";
import translationRUS from "src/locales/ru/ru.json";
import translationSPA from "src/locales/es/es.json";
import translationSWA from "src/locales/sw/sw.json";
import translationUKR from "src/locales/uk/uk.json";
import translationWOL from "src/locales/wo/wo.json";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    lng: getEnv("VITE_DEFAULT_LANGUAGE") || "eng",
    resources: {
      ara: {
        translation: translationARA,
      },
      ben: {
        translation: translationBEN,
      },
      deu: {
        translation: translationDEU,
      },
      ell: {
        translation: translationELL,
      },
      eng: {
        translation: translationENG,
      },
      fas: {
        translation: translationFAS,
      },
      fra: {
        translation: translationFRA,
      },
      fuc: {
        translation: translationFUC,
      },
      hau: {
        translation: translationHAU,
      },
      hin: {
        translation: translationHIN,
      },
      ita: {
        translation: translationITA,
      },
      nld: {
        translation: translationNLD,
      },
      pol: {
        translation: translationPOL,
      },
      //prs
      pus: {
        translation: translationPUS,
      },
      ron: {
        translation: translationRON,
      },
      run: {
        translation: translationRUN,
      },
      rus: {
        translation: translationRUS,
      },
      spa: {
        translation: translationSPA,
      },
      swa: {
        translation: translationSWA,
      },
      ukr: {
        translation: translationUKR,
      },
      wol: {
        translation: translationWOL,
      },
    },
    /* keySeparator: false, */ // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
