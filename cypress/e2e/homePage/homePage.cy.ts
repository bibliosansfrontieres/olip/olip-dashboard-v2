describe("The Home Page", () => {
  beforeEach(() => {
    cy.visit("/"); // base Url in cypress.config.ts
    cy.intercept("api/applications*", {
      fixture: "homePage/applications",
    });
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    });
  });
  it("successfully loads", () => {
    cy.getByDataTestId("homeHeaderTitle").should("be.visible");
  });
  it("contains the different section", () => {
    cy.getByDataTestId("categoriesSection").should("be.visible");
    cy.getByDataTestId("categoriesSelectionSection").should("be.visible");
    cy.getByDataTestId("applicationsSection").should("be.visible");
  });
  it("test the categories section", () => {
    cy.getByDataTestId("categoriesSectionTitle").should("be.visible");
    cy.getByDataTestId("categoriesCard")
      .should("be.visible")
      .should("have.length.greaterThan", 0);
  });
  it("test the categories selection section", () => {
    cy.getByDataTestId("categoriesSelectionSectionTitle").should("be.visible");
    cy.getByDataTestId("categorySelection")
      .should("be.visible")
      .should("have.length.greaterThan", 0);
    cy.getByDataTestId("categorySelection")
      .first()
      .within(() => {
        cy.getByDataTestId("categorySelectionTitle").should("be.visible");
        cy.getByDataTestId("categorySelectionButton").should("be.visible");
        cy.getByDataTestId("itemsCard")
          .should("be.visible")
          .should("have.length.greaterThan", 0);
      });
  });
  it("test the applications section", () => {
    cy.getByDataTestId("applicationsCard").should("have.length", 2);
  });
  it("test the footer", () => {
    cy.get("footer").should("be.visible");
    cy.getByDataTestId("OLIPIcon").should("be.visible");
    cy.getByDataTestId("OLIPText")
      .should("be.visible")
      .should("contain.text", "is made with love by");
    cy.getByDataTestId("BSFLogo").should("be.visible");
    cy.getByDataTestId("year")
      .should("be.visible")
      .should("contain.text", "2025");
  });
});
