describe("The Category Selection Section", () => {
  beforeEach(() => {
    cy.intercept("api/categories/selection/1*", {
      fixture: "homePage/categorySelectionSection",
    }).as("categorySelectionSection");
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    }).as("categoryList");
    cy.visit("/");
    cy.wait("@categoryList");
    cy.wait("@categorySelectionSection");
  });
  it("test the title of selection", () => {
    cy.fixture("homePage/categoryList").then((categoriesJSON) => {
      categoriesJSON.categories.map((category, index) => {
        cy.getByDataTestId("categorySelectionTitle")
          .eq(index)
          .should("contain.text", category.categoryTranslations[0].title);
      });
    });
  });
  it("test the first selection", () => {
    cy.fixture("homePage/categorySelectionSection").then(
      (categorySelectionJSON) => {
        categorySelectionJSON.map(({ item }, index) => {
          cy.getByDataTestId("itemsCard")
            .eq(index)
            .within(() => {
              cy.get("span").should(
                "contain.text",
                item.dublinCoreItem.dublinCoreItemTranslations[0].title,
              );
            });
        });
      },
    );
  });
});
