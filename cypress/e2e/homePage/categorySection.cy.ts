describe("The Category Section", () => {
  beforeEach(() => {
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    }).as("categoryList");
    cy.visit("/");
    cy.wait("@categoryList");
  });
  it("test the categories section", () => {
    cy.getByDataTestId("categoriesSectionTitle")
      .should("be.visible")
      .and("contain.text", "4");
    cy.getByDataTestId("categoriesCard")
      .should("be.visible")
      .should("have.length", 4);
  });

  it("test the category card", () => {
    cy.fixture("homePage/categoryList").then((categoriesJSON) => {
      {
        categoriesJSON.categories.map((category, index) => {
          cy.getByDataTestId("categoriesCard")
            .eq(index)
            .within(() => {
              cy.get("img")
                .should("be.visible")
                .and(
                  "have.attr",
                  "alt",
                  category.categoryTranslations[0].title,
                );
              cy.get("span").should(
                "contain.text",
                category.categoryTranslations[0].title,
              );
            });
        });
      }
    });
  });
});
