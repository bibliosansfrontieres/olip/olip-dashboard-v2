import { v4 as uuidv4 } from "uuid";

describe("The Login", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.intercept("POST", "signin").as("signin");
    Cypress.env("users", []);
  });
  it("test the login modal", () => {
    cy.viewport(1280, 720);
    cy.getByDataTestId("loginButton").click();
    cy.getByDataTestId("loginTitle").should("be.visible");
    cy.getByDataTestId("usernameInput").should("be.visible");
    cy.getByDataTestId("passwordInput").should("be.visible");
    cy.getByDataTestId("noPasswordCheckbox").should("be.visible");
    cy.getByDataTestId("submitButton-login").should("be.visible");
  });
  it("test access to login button on smartphone", () => {
    cy.viewport("iphone-xr");
    //@TODO: do not use first and last
    // last because there is one element for sm screen and one for smartphone
    cy.getByDataTestId("menuButton").last().click();
    cy.getByDataTestId("loginButton").last().click();
    cy.getByDataTestId("loginTitle").should("be.visible");
  });
  it("test access to login button on tablet", () => {
    cy.viewport(820, 1080);
    //@TODO: do not use first and last
    // first because there is one element for sm screen and one for smartphone
    cy.getByDataTestId("menuButton").first().click();
    cy.getByDataTestId("loginButton").last().click();
    cy.getByDataTestId("loginTitle").should("be.visible");
  });

  it("test the login without password", () => {
    const uuid = uuidv4();
    const username = `cy_login_without_${uuid}`;
    cy.createUserAsAdmin({
      username: username,
    });
    cy.visit("/");
    cy.viewport(1280, 720);
    cy.getByDataTestId("loginButton").click();
    cy.getByDataTestId("noPasswordCheckbox", undefined, {
      timeout: 10000,
    }).click();
    cy.getByDataTestId("usernameInput").type(username);
    cy.getByDataTestId("submitButton-login").click();
    cy.wait("@signin").its("response.statusCode").should("eq", 200);
    cy.deleteLastUserAsAdmin();
  });
  it("test the login with password", () => {
    const uuid = uuidv4();
    const username = `cy_login_with_${uuid}`;
    cy.createUserAsAdmin({
      username: username,
      password: Cypress.env("userPassword"),
    });
    cy.visit("/");
    cy.viewport(1280, 720);
    cy.getByDataTestId("loginButton").click();
    cy.getByDataTestId("usernameInput").type(username);
    cy.getByDataTestId("passwordInput").type(Cypress.env("userPassword"), {
      log: false,
    });
    cy.getByDataTestId("submitButton-login").click();
    cy.wait("@signin").its("response.statusCode").should("eq", 200);
    cy.deleteLastUserAsAdmin();
  });
  it("test the login via POST request", () => {
    cy.request("POST", `${Cypress.env("apiUrl")}api/auth/signin`, {
      username: Cypress.env("adminUsername"),
      password: Cypress.env("adminPassword"),
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
  it("test the login with wrong password", () => {
    const uuid = uuidv4();
    const username = `cy_login_with_${uuid}`;
    cy.createUserAsAdmin({
      username: username,
      password: Cypress.env("userPassword"),
    });
    cy.visit("/");
    cy.viewport(1280, 720);
    cy.getByDataTestId("loginButton").click();
    cy.getByDataTestId("usernameInput").type(username);
    cy.getByDataTestId("passwordInput").type("userddfdd", { log: false });
    cy.getByDataTestId("submitButton-login").click();
    cy.wait("@signin").its("response.statusCode").should("eq", 401);
    cy.getByDataTestId("passwordError").should("be.visible");
    cy.deleteLastUserAsAdmin();
  });
  it("test the login with wrong username", () => {
    cy.viewport(1280, 720);
    cy.getByDataTestId("loginButton").click();
    cy.getByDataTestId("usernameInput").type("userdfdfd");
    cy.getByDataTestId("passwordInput").type("userddfdd");
    cy.getByDataTestId("submitButton-login").click();
    cy.wait("@signin").its("response.statusCode").should("eq", 404);
    cy.getByDataTestId("usernameError").should("be.visible");
    cy.getByDataTestId("noPasswordCheckbox").click();
    cy.getByDataTestId("submitButton-login").click();
    cy.wait("@signin").its("response.statusCode").should("eq", 404);
    cy.getByDataTestId("usernameError").should("be.visible");
  });
});
