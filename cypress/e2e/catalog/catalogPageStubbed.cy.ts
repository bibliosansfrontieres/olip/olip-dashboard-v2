describe("Test the catalog page with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/categories/catalog*", {
      fixture: "catalog/catalogResponse",
    });
    cy.visit("/catalog");
  });
  it("test the catalog page", () => {
    cy.fixture("catalog/catalogResponse").then((catalogJSON) => {
      cy.getByDataTestId("catalogPageCategorySection")
        .should("be.visible")
        .should("have.length", catalogJSON.categories.length);
      cy.getByDataTestId("catalogPageCategorySection").each(
        ($section, indexSection) => {
          cy.wrap($section).within(() => {
            // test the category title : should have the right title and number of playlists
            cy.getByDataTestId("catalogPageCategoryTitle")
              .should("be.visible")
              .should(
                "contain.text",
                catalogJSON.categories[indexSection].categoryTranslations[0]
                  .title,
              )
              .should(
                "contain.text",
                catalogJSON.categories[indexSection].categoryPlaylists.length,
              );
            // test each playlist card of the category, should have the right amount of card
            cy.getByDataTestId("playlistsCard")
              .should("be.visible")
              .should(
                "have.length",
                catalogJSON.categories[indexSection].categoryPlaylists.length,
              )
              // in each card, test the playlist documents number and the playlist title
              .each(($playlist, indexPlaylist) => {
                cy.wrap($playlist).within(() => {
                  cy.getByDataTestId("playlistCardDocumentCount").should(
                    "contain.text",
                    catalogJSON.categories[indexSection].categoryPlaylists[
                      indexPlaylist
                    ].categoryPlaylistItemsCount,
                  );
                  cy.getByDataTestId("playlistCardTitle").should(
                    "contain.text",
                    catalogJSON.categories[indexSection].categoryPlaylists[
                      indexPlaylist
                    ].playlist.playlistTranslations[0].title,
                  );
                });
              });
          });
        },
      );
    });
  });
});
