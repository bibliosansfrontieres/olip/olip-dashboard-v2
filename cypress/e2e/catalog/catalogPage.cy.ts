describe("Test the catalog page", () => {
  beforeEach(() => {
    cy.visit("/catalog");
  });
  it("test the catalog page", () => {
    cy.getByDataTestId("catalogPageTitle").should("be.visible");
    cy.getByDataTestId("catalogPageDescription").should("be.visible");
    cy.getByDataTestId("catalogPageCategorySection")
      .should("be.visible")
      .should("have.length.greaterThan", 0);
    cy.getByDataTestId("catalogPageCategorySection")
      .first()
      .within(() => {
        cy.getByDataTestId("catalogPageCategoryTitle").should("be.visible");
        cy.getByDataTestId("catalogPageCategoryButton").should("be.visible");
        cy.getByDataTestId("playlistsCard")
          .should("be.visible")
          .should("have.length.greaterThan", 0);
      });
  });
  it("test the filter category", () => {
    cy.filterCatalog(0, "categoryId", "categories");
  });
  it("test the filter application", () => {
    cy.filterCatalog(1, "applicationIds", "applications");
  });
  it("test the filter order", () => {
    cy.filterCatalog(2, "order", "order");
  });
});
