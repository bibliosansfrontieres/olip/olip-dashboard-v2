describe("Test the language", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.intercept("POST", "signin").as("signin");
    Cypress.env("users", []);
  });
  it("test the change language", () => {
    cy.viewport(1280, 720);
    // languages
    const supportedLanguages = [
      { id: "ara", name: "ٱلْعَرَبِيَّة" },
      { id: "ben", name: "বাংলা" },
      { id: "deu", name: "Deutsch" },
      { id: "ell", name: "Ελληνικά" },
      { id: "eng", name: "English" },
      { id: "fas", name: "فارسی" },
      { id: "fra", name: "Français" },
      { id: "fuc", name: "Pulaar" },
      { id: "hau", name: "Hausa" },
      { id: "hin", name: "हिन्दी" },
      { id: "ita", name: "Italiano" },
      { id: "nld", name: "Nederlands" },
      { id: "pol", name: "Polski" },
      //prs
      { id: "pus", name: "پښتو" },
      { id: "run", name: "Ikirundi" },
      { id: "ron", name: "Română" },
      { id: "rus", name: "Русский" },
      { id: "spa", name: "Español" },
      { id: "swa", name: "Kiswahili" },
      { id: "ukr", name: "Українська" },
      { id: "wol", name: "Wolof" },
    ];
    supportedLanguages.forEach((language) => {
      // first because there is one element for lg screen and one for sm
      cy.getByDataTestId("languageButtonMenu").first().click();
      cy.getByDataTestId(`languageButtonStack-${language.id}`)
        .click()
        .should(() => {
          expect(localStorage.getItem("language")).to.eq(language.id);
        });
    });
  });
  it("test the login => change language", () => {
    // set up : create user as admin with deu language and logout
    cy.createUserAsAdmin({
      username: "testLanguage",
      roleId: 4,
      language: "deu",
    });
    cy.clearAllLocalStorage();

    cy.visit("/");
    cy.viewport(1280, 720);
    // cy.getByDataTestId("loginButton").click();
    // cy.getByDataTestId("logoutButton").click();

    // set up : change language to fra

    cy.getByDataTestId("languageButtonMenu").first().click();
    cy.getByDataTestId(`languageButtonStack-fra`)
      .click()
      .should(() => {
        expect(localStorage.getItem("language")).to.eq("fra");
      });

    // login and assert language changed to deu
    cy.getByDataTestId("loginButton").click();
    cy.getByDataTestId("noPasswordCheckbox", undefined, {
      timeout: 10000,
    }).click();
    cy.getByDataTestId("usernameInput").type("testLanguage");
    cy.getByDataTestId("submitButton-login").click();
    cy.wait("@signin")
      .its("response.statusCode")
      .should("eq", 200)
      .should(() => {
        expect(localStorage.getItem("language")).to.eq("deu");
      });
    cy.deleteLastUserAsAdmin();
  });

  it("test the change user language", () => {
    // set up : create user as admin with deu language and logout
    cy.createUserAsAdmin({
      username: "testChangeLanguage",
      roleId: 4,
    });
    cy.clearAllLocalStorage();

    cy.loginWithoutPassword("testChangeLanguage");
    cy.visit("/");
    cy.viewport(1280, 720);
    cy.getByDataTestId("myAccountLink").click();
    cy.getByDataTestId("editProfileButton").first().click();
    cy.getByDataTestId("olipSelect").click();
    cy.get('[data-value="ita"]').click();
    cy.getByDataTestId("submitButton").click();
    cy.intercept("PATCH", "api/users/*", (req) => {
      expect(req.body.language).to.eq("ita");
      req.continue((res) => {
        expect(res.statusCode).to.eq(200);
        expect(localStorage.getItem("language")).to.eq("ita");
      });
    });
    cy.deleteLastUserAsAdmin();
  });
});
