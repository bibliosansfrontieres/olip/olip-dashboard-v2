describe("Test the category page plyalist component with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    });
    cy.intercept("api/categories/1*", {
      fixture: "category/category1Response",
    });
    cy.visit("/category/1");
  });
  it("test the filter by playlist component", () => {
    cy.fixture("category/category1Response").then((category1Response) => {
      cy.getByDataTestId("categoryPlaylistCount")
        .should("be.visible")
        .should(
          "contain.text",
          category1Response.category.categoryPlaylists.length,
        );
      cy.getByDataTestId("scrollBack").should("be.visible");
      cy.getByDataTestId("scrollForward").should("be.visible");
      cy.getByDataTestId("playlistStack")
        .children()
        .should(
          "have.length",
          category1Response.category.categoryPlaylists.length,
        );
      cy.getByDataTestId("playlistSelectCard")
        .should("be.visible")
        .should(
          "have.length",
          category1Response.category.categoryPlaylists.length,
        );
      cy.getByDataTestId("playlistSelectCard").each(($el, index) => {
        cy.wrap($el).should(
          "contain.text",
          category1Response.category.categoryPlaylists[index].playlist
            .playlistTranslations[0].title,
        );
      });
    });
  });
  it("test the playlists buttons", () => {
    cy.fixture("category/category1Response").then((category1Response) => {
      cy.getByDataTestId("playlistSelectCard").each(($el, index) => {
        cy.wrap($el).click();
        cy.url().should(
          "contain",
          category1Response.category.categoryPlaylists[index].playlist.id,
        );
      });
      cy.getByDataTestId("filterPlaylistChip").each(($el, index) => {
        cy.wrap($el).should(
          "contain.text",
          category1Response.category.categoryPlaylists[index].playlist
            .playlistTranslations[0].title,
        );
      });
    });
  });
});
