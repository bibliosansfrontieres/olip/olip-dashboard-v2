describe("Test the category page categories component with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    });
    cy.intercept("api/categories/1*", {
      fixture: "category/category1Response",
    });
    cy.visit("/category/1");
  });
  it("test the categories component", () => {
    cy.fixture("homePage/categoryList").then(({ categories }) => {
      cy.getByDataTestId("categoriesCard").should(
        "have.length",
        categories.length - 1,
      );
      cy.fixture("category/category1Response").then((category1Response) => {
        cy.getByDataTestId("categoriesCard").each((otherCategory, index) => {
          const categoriesFiltered = categories.filter(
            (categoryF) => categoryF.id !== category1Response.category.id,
          );
          cy.wrap(otherCategory).within(() => {
            cy.get("span").should(
              "contain.text",
              categoriesFiltered[index].categoryTranslations[0].title,
            );
          });
        });
      });
    });
  });
});
