describe("Test the category page with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    });
    cy.intercept("api/categories/1*", {
      fixture: "category/category1Response",
    });
    cy.visit("/category/1");
  });
  it("test the layout", () => {
    cy.fixture("category/category1Response").then((category1Response) => {
      cy.getByDataTestId("categoryPlaylistTitle")
        .should("be.visible")
        .should(
          "contain.text",
          category1Response.category.categoryTranslations[0].title,
        );
      cy.getByDataTestId("categoryPlaylistDescription")
        .should("be.visible")
        .should(
          "contain.text",
          category1Response.category.categoryTranslations[0].description,
        );
    });
  });
});
