describe("Test the category page plyalist component with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/categories/list?*", {
      fixture: "homePage/categoryList",
    });
    cy.intercept("api/categories/1*", {
      fixture: "category/category1Response",
    });
    cy.visit("/category/1");
  });
  it("test the items component", () => {
    cy.fixture("category/category1Response").then((category1Response) => {
      cy.getByDataTestId("categoryTotalItems").should(
        "contain.text",
        category1Response.meta.totalItemsCount,
      );
      cy.getByDataTestId("itemsCard").each((item, index) => {
        cy.wrap(item).within(() => {
          cy.getByDataTestId("itemCardTitle").should(
            "contain.text",
            category1Response.data[index].dublinCoreItem
              .dublinCoreItemTranslations[0].title,
          );
        });
      });
    });
  });
});
