describe("Test the search page with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/search*", { fixture: "search/searchResponse" }).as(
      "searchResponse",
    );
    cy.intercept("api/search/items*", {
      fixture: "search/itemsSearchResponse",
    }).as("itemsSearchResponse");
    cy.intercept("api/search/playlists*", {
      fixture: "search/playlistsSearchResponse",
    }).as("playlistsSearchResponse");
    cy.intercept("api/search/categories*", {
      fixture: "search/categoriesSearchResponse",
    }).as("categoriesSearchResponse");
    cy.intercept("api/search/applications*", {
      fixture: "search/applicationsSearchResponse",
    }).as("applicationsSearchResponse");

    cy.visit("/search?q=a");
    cy.wait("@searchResponse");
    cy.wait("@itemsSearchResponse");
    cy.wait("@playlistsSearchResponse");
    cy.wait("@categoriesSearchResponse");
    cy.wait("@applicationsSearchResponse");
  });
  it("test the search tabs", () => {
    const tabs = [
      { dataTest: "items", totalCount: 29 },
      { dataTest: "playlists", totalCount: 30 },
      { dataTest: "categories", totalCount: 1 },
      { dataTest: "applications", totalCount: 1 },
    ];
    tabs.forEach(({ dataTest, totalCount }) => {
      cy.getByDataTestId(`${dataTest}Tab`)
        .should("be.visible")
        .and("contain.text", totalCount.toString())
        .and("have.attr", "aria-selected", "false");
      cy.getByDataTestId(`${dataTest}Tab`).click();
      cy.getByDataTestId(`${dataTest}TabTitle`).should(
        "contain.text",
        totalCount.toString(),
      );
      cy.getByDataTestId("boxPaginationLayout").should("be.visible");
      cy.getByDataTestId(`${dataTest}Card`)
        .filter(":visible")
        .its("length")
        .should("be.eq", totalCount > 12 ? 12 : totalCount);
    });
  });
});
