describe("The Search Bar", () => {
  it("test the search bar on home page", () => {
    cy.visit("/");
    cy.getByDataTestId("searchForm").should("be.visible").type("test");
    cy.getByDataTestId("searchIcon").click();
    cy.url().should("contain", "search?q=test");
  });
  it("test the search bar on other page", () => {
    cy.visit("/catalog");
    cy.getByDataTestId("searchForm").should("be.visible");
    // @TODO: find a way to have only the one visible, and not using eq(1)
    cy.getByDataTestId("searchInput").eq(1).type("test{enter}");
    cy.url().should("contain", "search?q=test");
  });
  it("test the search page ", () => {
    cy.visit("/search?q=test");
    // call children because searchInput is on the div parent of the input
    cy.getByDataTestId("searchInput").children().should("have.value", "test");
    cy.getByDataTestId("searchPageQuery")
      .should("be.visible")
      .should("contain", "test");
    cy.getByDataTestId("searchPageCountResult").should("be.visible");
    // verify first all tab
    cy.getByDataTestId("allTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
  });
  it("test the autocomplete page", () => {
    cy.visit("/");
    cy.intercept("api/search/suggestions*", {
      fixture: "search/autoCompleteResponse",
    }).as("autoCompleteResponse");
    cy.getByDataTestId("searchForm").type("test");
    cy.wait("@autoCompleteResponse");
    cy.getByDataTestId("suggestionAutocomplete")
      .should("be.visible")
      .should("have.length", 3)
      .eq(1)
      .click();
    cy.getByDataTestId("searchInput")
      .children()
      .should("have.value", "Test OLIP");
  });
});
