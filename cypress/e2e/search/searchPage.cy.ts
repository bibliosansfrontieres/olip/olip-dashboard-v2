describe("Test the search page with stubbed response", () => {
  beforeEach(() => {
    cy.intercept("api/search*", { fixture: "search/searchResponse" }).as(
      "searchResponse",
    );
    cy.visit("/search?q=a");
    cy.wait("@searchResponse");
  });
  it("test the search page (All Tab)", () => {
    const category = [
      { dataTest: "items", totalCount: 29 },
      { dataTest: "playlists", totalCount: 30 },
      { dataTest: "categories", totalCount: 1 },
      { dataTest: "applications", totalCount: 1 },
    ];
    category.forEach(({ dataTest, totalCount }) => {
      cy.getByDataTestId(`${dataTest}AllTabTitle`)
        .should("be.visible")
        .and("contain.text", totalCount.toString());
      cy.getByDataTestId(`${dataTest}Card`)
        .filter(":visible")
        .its("length")
        .should("be.eq", totalCount > 4 ? 4 : totalCount);
      cy.getByDataTestId(`${dataTest}ChangeTabButton`)
        .should("be.visible")
        .click();
      cy.getByDataTestId(`${dataTest}Tab`)
        .should("be.visible")
        .and("contain.text", totalCount.toString())
        .and("have.attr", "aria-selected", "true");
      // to go back to all tab
      cy.getByDataTestId(`allTab`).click();
    });
  });
});
