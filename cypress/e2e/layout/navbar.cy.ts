describe("The Home Page", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("test on big screen", () => {
    cy.viewport(1280, 720);
    // logo
    cy.getByDataTestId("logo").should("be.visible");
    cy.getByDataTestId("logo").should("have.attr", "href", "/");
    // list pages
    const menuListPages = [
      { name: "header.pages.home", link: "/" },
      { name: "header.pages.catalog", link: "/catalog" },
      { name: "header.pages.applications", link: "/applications" },
    ];
    menuListPages.forEach((page) => {
      cy.getByDataTestId(page.name).should("be.visible");
      cy.getByDataTestId(page.name).should("have.attr", "href", page.link);
      cy.getByDataTestId(page.name).click();
      cy.url().should("contain", page.link);
    });
    // languages
    const supportedLanguages = [
      { id: "ara", name: "ٱلْعَرَبِيَّة" },
      { id: "ben", name: "বাংলা" },
      { id: "deu", name: "Deutsch" },
      { id: "ell", name: "Ελληνικά" },
      { id: "eng", name: "English" },
      { id: "fas", name: "فارسی" },
      { id: "fra", name: "Français" },
      { id: "fuc", name: "Pulaar" },
      { id: "hau", name: "Hausa" },
      { id: "hin", name: "हिन्दी" },
      { id: "ita", name: "Italiano" },
      { id: "nld", name: "Nederlands" },
      { id: "pol", name: "Polski" },
      //prs
      { id: "pus", name: "پښتو" },
      { id: "run", name: "Ikirundi" },
      { id: "ron", name: "Română" },
      { id: "rus", name: "Русский" },
      { id: "spa", name: "Español" },
      { id: "swa", name: "Kiswahili" },
      { id: "ukr", name: "Українська" },
      { id: "wol", name: "Wolof" },
    ];
    cy.getByDataTestId("languageButtonMenu").should("be.visible");
    // first because there is one element for lg screen and one for sm
    cy.getByDataTestId("languageButtonMenu").first().click();
    supportedLanguages.forEach((language) => {
      cy.getByDataTestId(`languageButtonStack-${language.id}`)
        .scrollIntoView()
        .should("be.visible");
      cy.getByDataTestId(`languageButtonStack-${language.id}`).should(
        "have.text",
        language.name,
      );
    });
  });
  it("test on tablet", () => {
    cy.viewport(768, 1024);
    cy.getByDataTestId("logo").should("be.visible");
    cy.getByDataTestId("logo").should("have.attr", "href", "/");
    cy.getByDataTestId("header.pages.home").should("not.be.visible");
    cy.getByDataTestId("languageButtonMenu").should("be.visible");
    cy.getByDataTestId("menuButton").should("be.visible");
    // first because there is one element for sm screen and one for smartphone
    cy.getByDataTestId("menuButton").first().click();
    cy.getByDataTestId("drawer-header.pages.home").should("be.visible");
  });
  it("test on smartphone", () => {
    cy.viewport("iphone-xr");
    cy.getByDataTestId("logo").should("be.visible");
    cy.getByDataTestId("logo").should("have.attr", "href", "/");
    cy.getByDataTestId("header.pages.home").should("not.be.visible");
    cy.getByDataTestId("menuButton").should("be.visible");
    // last because there is one element for sm screen and one for smartphone
    cy.getByDataTestId("menuButton").last().click();
    cy.getByDataTestId("drawer-header.pages.home").should("be.visible");
    cy.getByDataTestId("languageButtonMenu").should("be.visible");
  });
});
