describe("The Activity Monitoring", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.visit("settings/activity");
    cy.intercept("api/hardware/ssid*", { fixture: "settings/ssidResponse" });
  });
  it("verify the activity monitoring page", () => {
    cy.getByDataTestId("activityTitle").should("be.visible");

    cy.getByDataTestId("ssidGrid").should("be.visible");
    cy.getByDataTestId("ssidName").should("be.visible");
    cy.getByDataTestId("ssidTitle").should("be.visible");

    cy.getByDataTestId("installedContentGrid").should("be.visible");
    cy.getByDataTestId("installedContentsNumber").should("be.visible");
    cy.getByDataTestId("installedContentsTitle").should("be.visible");

    cy.getByDataTestId("actualTotalUsersGrid").should("be.visible");
    cy.getByDataTestId("actualTotalUsersNumber").should("be.visible");
    cy.getByDataTestId("actualTotalUsersTitle").should("be.visible");

    cy.getByDataTestId("actualConnectedUsersGrid").should("be.visible");
    cy.getByDataTestId("actualConnectedUsersNumber").should("be.visible");
    cy.getByDataTestId("actualConnectedUsersTitle").should("be.visible");

    cy.getByDataTestId("doughnutContentGrid").should("be.visible");
    cy.getByDataTestId("doughnutContentTitle").should("be.visible");
    //@TODO: add test for verification donut, when its fixed
  });
});
