describe("The Logo and Color", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.visit("/settings");
    //@TODO: remove first
    cy.getByDataTestId("editPersonalizationButton").first().click();
    cy.intercept("PATCH", "logo").as("logo");
    cy.intercept("PATCH", "principal_color").as("principal_color");
  });
  it("test the color change", () => {
    //click on the first radio not checked
    cy.get("input[type='radio']").each(($radio) => {
      cy.log("radio", $radio);
      cy.wrap($radio);
      if ($radio.is(":checked") === false) {
        cy.wrap($radio).as("colorValue");
        cy.wrap($radio).click();
        return false;
      }
    });
    cy.get("@colorValue").then((colorValue) => {
      cy.getByDataTestId("submitButton-editPersonalizationModal").click();
      cy.wait("@principal_color")
        .its("response.body.value")
        .should("eq", colorValue.val());
      cy.wait("@logo").then((interception) => {
        assert.isEmpty(interception.request.body.value);
        assert.strictEqual(interception.response.statusCode, 200);
      });
    });
  });
  it.skip("test the logo change", () => {
    //@TODO: find why its flaky
    cy.getByDataTestId("inputFile").selectFile(
      "cypress/fixtures/bsf-logo.png",
      { force: true },
    );
    cy.getByDataTestId("submitButton-editPersonalizationModal").click();
    cy.wait("@logo").then((interception) => {
      assert.isNotEmpty(interception.request.body.value);
      assert.strictEqual(interception.response.statusCode, 200);
    });
  });
  it("test both logo and color change", () => {
    cy.getByDataTestId("inputFile").selectFile(
      "cypress/fixtures/bsf-logo.png",
      { force: true },
    );
    //click on the first radio not checked
    cy.get("input[type='radio']").each(($radio) => {
      cy.log("radio", $radio);
      cy.wrap($radio);
      if ($radio.is(":checked") === false) {
        cy.wrap($radio).as("colorValue");
        cy.wrap($radio).click();
        return false;
      }
    });
    cy.get("@colorValue").then((colorValue) => {
      cy.getByDataTestId("submitButton-editPersonalizationModal").click();
      cy.wait("@principal_color")
        .its("response.body.value")
        .should("eq", colorValue.val());
      cy.wait("@logo").then((interception) => {
        assert.isNotEmpty(interception.request.body.value);
        assert.strictEqual(interception.response.statusCode, 200);
      });
    });
  });
});
