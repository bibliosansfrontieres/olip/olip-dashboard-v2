describe("The Dashboard", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.visit("/settings");
  });
  it("test the accountInformation", () => {
    cy.getByDataTestId("DashboardTitle").should("be.visible");
    cy.getByDataTestId("settingCardLayout").first().should("be.visible");
    cy.getByDataTestId("accountInfoCardStack").within(() => {
      cy.getByDataTestId("displayValueValue")
        .first()
        .should("have.text", Cypress.env("adminUsername"));
      cy.getByDataTestId("displayValueValue").should("have.length", 4);
    });
    cy.getByDataTestId("accountInfoCardAvatar").should("be.visible");
  });

  it("test the personalization", () => {
    cy.getByDataTestId("PersonalizeTitle").should("be.visible");
    cy.getByDataTestId("settingCardLayout").eq(1).should("be.visible");
    cy.getByDataTestId("logoLabel").should("be.visible");
    cy.getByDataTestId("logoAvatar").should("be.visible");
    cy.getByDataTestId("colorLabel").should("be.visible");
    cy.getByDataTestId("colorPalette").should("be.visible");
  });

  it("test the editoCard", () => {
    cy.intercept("api/editos/last*", {
      fixture: "settings/lastEditoResponse",
    });
    cy.getByDataTestId("settingCardLayout")
      .eq(2)
      .should("be.visible")
      .within(() => {
        cy.getByDataTestId("displayInHomePageLabel").should("be.visible");
        cy.getByDataTestId("displayInHomePageToggle").should("be.visible");
        cy.getByDataTestId("languagesAvailable").should("be.visible");
      });
    cy.getByDataTestId("editoImage").should("be.visible");
    cy.getByDataTestId("editoStackContent").should("be.visible");
  });

  it("test the highlightCard", () => {
    cy.getByDataTestId("settingCardLayout")
      .eq(3)
      .should("be.visible")
      .within(() => {
        cy.getByDataTestId("displayInHomePageLabel").should("be.visible");
        cy.getByDataTestId("displayInHomePageToggle").should("be.visible");
        cy.getByDataTestId("languagesAvailable").should("be.visible");
      });
    cy.getByDataTestId("highlightStackContent").should("be.visible");
  });
});
