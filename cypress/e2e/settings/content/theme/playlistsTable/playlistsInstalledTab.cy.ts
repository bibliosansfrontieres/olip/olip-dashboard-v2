describe("Settings theme page - playlists installed tab", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("api/categories/back-office/1*", {
      fixture: "settings/thematics/thematicResponse",
    });
    cy.intercept("api/playlists/installed*", {
      fixture: "settings/thematics/installedTab",
    });
    cy.intercept("DELETE", "api/playlists*", {
      fixture: "settings/thematics/linkedPlaylistsDeleteResponse",
    }).as("deletePlaylists");
    cy.intercept("POST", "api/categories/1/playlists*").as("addToSelection");

    cy.visit("/settings/contents/theme/1");
    cy.getByDataTestId("editCategoryPlaylistsButton").eq(0).click();
    cy.getByDataTestId("tabInstalled").click();
  });

  it('should verify tab "installed"', () => {
    cy.getByDataTestId("uninstallButton")
      .should("be.visible")
      .should("be.disabled");
    cy.getByDataTestId("addToSelectionButton")
      .should("be.visible")
      .should("be.disabled");
  });

  it("should verified uninstalled function", () => {
    cy.fixture("settings/thematics/installedTab").then(({ data }) => {
      cy.getByDataTestId(`${data[0].id}Checkbox`).click();
      cy.getByDataTestId(`${data[1].id}Checkbox`).click();
      cy.getByDataTestId("uninstallButton").should("not.be.disabled").click();
      cy.wait("@deletePlaylists")
        .its("request.body")
        .should("eql", {
          languageIdentifier: "fra",
          playlistIds: [data[0].id, data[1].id],
        });
      cy.getByDataTestId("uninstallPlaylistTitle").should("be.visible");
    });

    cy.fixture("settings/thematics/linkedPlaylistsDeleteResponse").then(
      (playlistCategories) => {
        cy.getByDataTestId("uninstallPlaylistWarning").should("be.visible");
        cy.getByDataTestId("uninstallPlaylistPlaylistTitle")
          .should("be.visible")
          .and(
            "contain.text",
            playlistCategories[0].playlist.playlistTranslations[0].title,
          );
        cy.getByDataTestId("uninstallPlaylistCategoryTitle")
          .should("be.visible")
          .and(
            "contain.text",
            playlistCategories[0].categories[0].categoryTranslations[0].title,
          );
      },
    );
    cy.getByDataTestId("askConfirm").should("be.visible");
    cy.fixture("settings/thematics/installedTab").then(({ data }) => {
      cy.getByDataTestId("uninstallUnlinkPlaylistTitle")
        .should("be.visible")
        .and("contain.text", data[0].playlistTranslations[0].title);
    });

    cy.fixture("settings/thematics/installedTab").then(({ data }) => {
      cy.getByDataTestId("confirmButton").click();
      cy.wait("@deletePlaylists")
        .its("request.body")
        .should("eql", {
          languageIdentifier: "fra",
          playlistIds: [data[0].id, data[1].id],
          confirm: true,
        });
    });
  });

  it("should verify add to selection", () => {
    cy.fixture("settings/thematics/installedTab").then(({ data }) => {
      cy.getByDataTestId(`${data[0].id}Checkbox`).click();
      cy.getByDataTestId(`${data[1].id}Checkbox`).click();
      cy.getByDataTestId("addToSelectionButton")
        .should("not.be.disabled")
        .click();
      cy.wait("@addToSelection")
        .its("request.body")
        .should("eql", {
          playlistIds: [data[0].id, data[1].id],
        });
    });
  });
});
