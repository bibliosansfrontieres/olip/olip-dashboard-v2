describe("Settings theme page - category card", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("api/categories/back-office/1/category-playlists?*", {
      fixture: "settings/thematics/categoryPlaylistsResponse",
    });
    cy.visit("/settings/contents/theme/1");
    cy.intercept(
      "api/category-playlists/back-office/132/category-playlist-items?*",
      {
        fixture: "settings/thematics/categoryPlaylistItemsResponse",
      },
    ).as("categoryPlaylistItemsResponse");
  });
  it("verify the first collapsed row layout", () => {
    const header = [
      "settings.pages.contents.theme.playlistsCard.table.itemTable.header.title",
      "settings.pages.contents.theme.playlistsCard.table.itemTable.header.type",
      "settings.pages.contents.theme.playlistsCard.table.itemTable.header.application",
    ];
    cy.getByDataTestId("Chevron-downIcon").first().click();
    cy.wait("@categoryPlaylistItemsResponse");
    cy.getByDataTestId("descriptionTitleCollapsed").should("be.visible");
    cy.fixture("settings/thematics/categoryPlaylistsResponse").then(
      (playlistJSON) => {
        cy.getByDataTestId("descriptionInformationCollapsed").should(
          "contain.text",
          playlistJSON.categoryPlaylists[0].playlist.playlistTranslations[0]
            .description,
        );
      },
    );
    header.forEach((head) => {
      cy.getByDataTestId(`tableHeader${head}`).should("be.visible");
    });
    cy.fixture("settings/thematics/categoryPlaylistItemsResponse").then(
      (categoryPlaylistsJSON) => {
        cy.getByDataTestId("itemRow")
          .should(
            "have.length",
            categoryPlaylistsJSON.categoryPlaylistItems.length,
          )
          .each(($row, index) => {
            cy.wrap($row).within(() => {
              //@TODO: test image cell
              cy.getByDataTestId("itemTitleCell").should(
                "have.text",
                categoryPlaylistsJSON.categoryPlaylistItems[index].item
                  .dublinCoreItem.dublinCoreItemTranslations[0].title,
              );
              //@TODO: test type cell
              //@TODO: test application cell
            });
          });
      },
    );
  });
});
