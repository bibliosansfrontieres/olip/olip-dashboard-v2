const header = [
  "settings.pages.contents.theme.playlistsCard.table.header.name",
  "settings.pages.contents.theme.playlistsCard.table.header.application",
  "settings.pages.contents.theme.playlistsCard.table.header.size",
];

describe("Settings theme page - category card", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("api/categories/back-office/1/category-playlists?*", {
      fixture: "settings/thematics/categoryPlaylistsResponse",
    });
    cy.visit("/settings/contents/theme/1");
  });
  it("verify the playlist card layout", () => {
    cy.getByDataTestId("settingCardLayout")
      .eq(1)
      .should("be.visible")
      .within(() => {
        cy.getByDataTestId("settingCardLayoutTitle").should("be.visible");
      });
    header.forEach((head) => {
      cy.getByDataTestId(`tableHeader${head}`).should("be.visible");
    });
    cy.getByDataTestId("deleteCategoryButton").should("be.visible");
    cy.fixture("settings/thematics/categoryPlaylistsResponse").then(
      (playlistJSON) => {
        cy.getByDataTestId("categoryPlaylistRow")
          .should("have.length", playlistJSON.categoryPlaylists.length)
          .each(($row, index) => {
            cy.wrap($row).within(() => {
              //@TODO: test image cell
              cy.getByDataTestId("titleCell").should(
                "have.text",
                playlistJSON.categoryPlaylists[index].playlist
                  .playlistTranslations[0].title,
              );
              //@TODO: test application cell
              cy.getByDataTestId("sizeCell").should(
                "contain.text",
                (
                  playlistJSON.categoryPlaylists[index].playlist.size /
                  1024 /
                  1024
                ).toFixed(2),
              );
            });
          });
      },
    );
  });
});
