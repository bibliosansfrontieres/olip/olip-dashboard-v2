describe("Settings theme page - category card", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("api/categories/back-office/1*", {
      fixture: "settings/thematics/thematicResponse",
    });
    cy.visit("/settings/contents/theme/1");
  });
  it("verify the layout", () => {
    cy.getByDataTestId("categoryBackButton").should("be.visible");
    cy.getByDataTestId("deleteCategoryButton").should("be.visible");
    cy.fixture("settings/thematics/thematicResponse").then((categoryJSON) => {
      cy.getByDataTestId("categoryTitle")
        .should("be.visible")
        .and("contain.text", categoryJSON.categoryTranslations[0].title);
    });
    cy.getByDataTestId("settingCardLayout")
      .should("be.visible")
      .should("have.length", 2);
  });
  it("verify the category main card layout", () => {
    cy.getByDataTestId("settingCardLayout")
      .eq(0)
      .should("be.visible")
      .within(() => {
        cy.getByDataTestId("settingCardLayoutTitle").should("be.visible");
      });
    cy.getByDataTestId("editCategoryMainButton").should("be.visible");
    cy.getByDataTestId("categoryToggleDisplay").should("be.visible");
    cy.getByDataTestId("categoryToggleDisplaySwitch")
      .should("be.visible")
      .within(() => cy.get("input").should("be.checked"));
    cy.getByDataTestId("languagesStack").should("be.visible");
    cy.fixture("settings/thematics/thematicResponse").then((categoryJSON) => {
      cy.getByDataTestId("languageChip")
        .should("be.visible")
        .should("have.length", categoryJSON.categoryTranslations.length);
    });
    cy.getByDataTestId("previewImage").should("be.visible");
    cy.getByDataTestId("displayValueLabel")
      .should("be.visible")
      .should("have.length", 3);
  });
  it("verifiy the change language in category main card", () => {
    cy.getByDataTestId("languageChip").each(($el, index) => {
      cy.wrap($el).click();
      cy.fixture("settings/thematics/thematicResponse").then((categoryJSON) => {
        cy.getByDataTestId("displayValueValue").each(
          ($displayValue, indexDisplayValue) => {
            if (indexDisplayValue <= 1) {
              cy.wrap($displayValue).should(
                "contain.text",
                categoryJSON.categoryTranslations[index][
                  indexDisplayValue === 0 ? "title" : "description"
                ],
              );
            }
          },
        );
        //@TODO: test the visibility
      });
    });
  });
});
