describe("Settings content - create content", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.visit("/settings/contents");
    cy.getByDataTestId("createCategoryButton").click();
  });
  it("verify the creation modal", () => {
    cy.getByDataTestId("contentTitle").should("be.visible");
    cy.getByDataTestId("cancelCreateCategoryButton").should("be.visible");
    cy.getByDataTestId("submitCreateCategoryButton").should("be.visible");
  });
});
