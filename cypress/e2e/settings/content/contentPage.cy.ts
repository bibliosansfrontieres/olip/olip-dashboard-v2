describe("Settings content", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("api/categories/back-office/list*", {
      fixture: "settings/thematics/contentsResponse",
    });
    cy.visit("/settings/contents");
  });
  it("verify the content page", () => {
    cy.getByDataTestId("contentTitle").should("be.visible");
    cy.getByDataTestId("createCategoryButton").should("be.visible");
    cy.fixture("settings/thematics/contentsResponse").then((categoriesJSON) => {
      cy.getByDataTestId("allCategoryTab")
        .should("be.visible")
        .and("contain.text", categoriesJSON.categories.length);
    });
    cy.getByDataTestId("bsfCategoryTab")
      .should("be.visible")
      .and("contain.text", "2");
    cy.getByDataTestId("localCategoryTab")
      .should("be.visible")
      .and("contain.text", "1");
  });
  it("verify the list with the stubbed response", () => {
    cy.fixture("settings/thematics/contentsResponse").then((categoriesJSON) => {
      cy.getByDataTestId("categoryListItem")
        // *2 because we also see them in the other tabs : bsf and local
        .should("have.length", categoriesJSON.categories.length * 2)
        .each(($el, index) => {
          cy.wrap($el).within(() => {
            if (index < categoriesJSON.categories.length) {
              cy.getByDataTestId("categoryListItemIcon").should("be.visible");
              cy.getByDataTestId("categoryListItemAvatar").should("be.visible");
              cy.getByDataTestId("categoryTitle").should("be.visible");
              cy.getByDataTestId("categoryOrigin").should("be.visible");
              cy.getByDataTestId("categoryNumberPlaylist")
                .should("be.visible")
                .should(
                  "contain.text",
                  categoriesJSON.categories[index].categoryPlaylistsCount,
                );
              cy.getByDataTestId("categoryHomeDisplay").should("be.visible");
              cy.getByDataTestId("categoryVisibility").should("be.visible");
              cy.getByDataTestId("categoryListItemIcon").should("be.visible");
            }
            if (index === 0) {
              cy.getByDataTestId("categoryUpdateChip").should("be.visible");
            }
          });
        });
    });
  });
  it("verify the tabs", () => {
    cy.getByDataTestId("allCategoryTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
    cy.getByDataTestId("bsfCategoryTab").click();
    cy.getByDataTestId("bsfCategoryTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
    cy.getByDataTestId("localCategoryTab").click();
    cy.getByDataTestId("localCategoryTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
  });
});
