describe("Settings users", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("api/users*", { fixture: "usersResponse" }).as(
      "usersResponse",
    );
    cy.visit("/settings/users");
    cy.wait("@usersResponse");
  });
  it("verify the user page", () => {
    cy.getByDataTestId("userTitle").should("be.visible");
    cy.getByDataTestId("profileCount").should("be.visible");
    cy.getByDataTestId("createUserButton").should("be.visible");
  });
  it("verify the user list", () => {
    cy.getByDataTestId("searchFilter").should("be.visible");
    cy.getByDataTestId("boxPaginationLayout").should("be.visible");
    cy.getByDataTestId("usersList").should("be.visible");
    cy.getByDataTestId("userLine").should("be.visible").and("have.length", 4);
  });
  it("verify the list with the stubbed response", () => {
    cy.getByDataTestId("userLine")
      .eq(0)
      .within(() => {
        cy.getByDataTestId("listItemAvatar").should("be.visible");
        cy.getByDataTestId("userPhoto").should("be.visible");
        cy.getByDataTestId("listItemUsername").should("be.visible");
        cy.getByDataTestId("chipRootUser").should("be.visible");
        cy.getByDataTestId("listItemRole").should("be.visible");
        cy.getByDataTestId("chipYou").should("be.visible");
      });
    cy.getByDataTestId("userLine").eq(0).children().should("have.length", 3);

    cy.getByDataTestId("userLine")
      .eq(1)
      .within(() => {
        cy.getByDataTestId("listItemAvatar").should("be.visible");
        cy.getByDataTestId("userPhoto").should("be.visible");
        cy.getByDataTestId("listItemUsername").should("be.visible");
        cy.getByDataTestId("listItemRole").should("be.visible");
        cy.getByDataTestId("editUserButton").should("be.visible");
        cy.getByDataTestId("listItemDivider").should("be.visible");
        cy.getByDataTestId("deleteUserButton").should("be.visible");
      });
    cy.getByDataTestId("userLine").eq(1).children().should("have.length", 4);
  });
});
