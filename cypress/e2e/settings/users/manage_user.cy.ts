import { v4 as uuidv4 } from "uuid";

//TODO: unskip test when no bug anymore when creating
describe.skip("Edit and delete user", () => {
  beforeEach(() => {
    const uuid = uuidv4();
    cy.createUserAsAdmin({ username: `cy_manage_user_${uuid}` });
    cy.visit("/settings/users");
    cy.intercept("DELETE", "api/users/*").as("deleteUser");
    Cypress.env("users", []);
  });
  it("edit the user", () => {
    // last user because last created
    cy.getByDataTestId("userLine")
      .last()
      .within(() => {
        // force true because of snackbar
        cy.getByDataTestId("editUserButton").click();
      });
    cy.getByDataTestId("roleAccordion").eq(3).click();
    cy.getByDataTestId("createUserInputUsername").type("_modified");
    cy.getByDataTestId("formControlLayout")
      .last()
      .within(() => {
        cy.getByDataTestId("olipSelect").click();
      });
    cy.get('[data-value="deu"]').click();
    cy.getByDataTestId("submitEditUserButton").click();
    cy.intercept("PATCH", "api/users/*", (req) => {
      expect(req.body.username).to.eq(
        `${Cypress.env("users")[0].name}_modified`,
      );
      expect(req.body.roleId).to.eq(4);
      expect(req.body.language).to.eq("deu");
      req.continue((res) => {
        expect(res.statusCode).to.eq(200);
      });
    });
    cy.getByDataTestId("OLIPSnackbar").should("be.visible");
    cy.deleteLastUser();
    cy.assertUserNotFound();
  });
  it("delete the user", () => {
    cy.getByDataTestId("userLine")
      .last()
      .within(() => {
        cy.getByDataTestId("deleteUserButton").click();
      });
    cy.getByDataTestId("deleteUserModalWarningText").should("be.visible");
    cy.getByDataTestId("cancelDeleteUserButton").should("be.visible");
    cy.getByDataTestId("submitDeleteUserButton").should("be.visible").click();
    cy.wait("@deleteUser").its("response.statusCode").should("eq", 200);
    cy.getByDataTestId("OLIPSnackbar").should("be.visible");
  });
});
