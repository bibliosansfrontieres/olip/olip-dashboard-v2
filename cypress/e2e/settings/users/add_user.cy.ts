import { v4 as uuidv4 } from "uuid";

//TODO: unskip test when no bug anymore when creating
describe.skip("Settings users : create and delete user", () => {
  const uuid = uuidv4();
  const username = `cy_add_user_${uuid}`;
  beforeEach(() => {
    cy.loginAdmin();
    cy.visit("/settings/users");
    cy.intercept("POST", "users").as("createUser");
    Cypress.env("users", []);
    cy.getByDataTestId("createUserButton").click();
  });
  it("verify the create user modal", () => {
    cy.getByDataTestId("createUserModalTitle").should("be.visible");
    cy.getByDataTestId("cancelCreateUserButton").should("be.visible");
    cy.getByDataTestId("submitCreateUserButton").should("be.visible");
  });
  it("verify the accordion role list", () => {
    cy.getByDataTestId("formControlLayout").should("be.visible");
    const roles: Array<{ name: string; value: number; permissions: string[] }> =
      [
        {
          name: "settings.pages.users.roles.admin",
          value: 1,
          permissions: [
            "form.user.permissions.accessContent",
            "form.user.permissions.updateMyProfile",
            "form.user.permissions.manageContent",
            "form.user.permissions.manageUser",
            "form.user.permissions.manageApps",
            "form.user.permissions.accessActivity",
          ],
        },
        {
          name: "settings.pages.users.roles.animator",
          value: 2,
          permissions: [
            "form.user.permissions.accessContent",
            "form.user.permissions.updateMyProfile",
            "form.user.permissions.manageContent",
          ],
        },
        {
          name: "settings.pages.users.roles.user",
          value: 3,
          permissions: [
            "form.user.permissions.accessContent",
            "form.user.permissions.updateMyProfile",
          ],
        },
        {
          name: "settings.pages.users.roles.user_no_password",
          value: 4,
          permissions: [
            "form.user.permissions.accessContent",
            "form.user.permissions.updateMyProfile",
          ],
        },
      ];
    cy.getByDataTestId("roleAccordion").should("have.length", roles.length);
    cy.getByDataTestId("roleAccordionSummary")
      .should("have.length", roles.length)
      .first()
      .click();
    cy.getByDataTestId("roleAccordionDetails")
      .first()
      .within(() => {
        cy.getByDataTestId("permissionItem").should(
          "have.length",
          roles[0].permissions.length,
        );
      });
  });

  it('verify the "create user" form', () => {
    cy.getByDataTestId("createUserInputUsername")
      .scrollIntoView()
      .should("be.visible");
    cy.getByDataTestId("createUserInputPassword").should("be.visible");
    cy.getByDataTestId("olipSelect").should("be.visible");
    cy.getByDataTestId("submitCreateUserButton").click();
    cy.getByDataTestId("formHelperText")
      .should("be.visible")
      .should("have.length", 3);
  });

  it("submit the form and delete the user", () => {
    cy.getByDataTestId("roleAccordion").eq(2).click();
    cy.getByDataTestId("createUserInputUsername").type(username);
    cy.getByDataTestId("createUserInputPassword").type(username + "password", {
      log: false,
    });
    cy.getByDataTestId("formControlLayout")
      .last()
      .within(() => {
        cy.getByDataTestId("olipSelect").click();
      });
    cy.get('[data-value="deu"]').click();

    cy.getByDataTestId("submitCreateUserButton").click();
    cy.wait("@createUser").then((interception) => {
      Cypress.env("users").push(interception.response.body);
      expect(interception.request.body.username).to.eq(username);
      expect(interception.request.body.roleId).to.eq(3);
      expect(interception.request.body.language).to.eq("deu");
      expect(interception.response.statusCode).to.eq(201);
    });
    cy.getByDataTestId("OLIPSnackbar").should("be.visible");
    cy.deleteLastUser();
    cy.assertUserNotFound();
  });
});
