describe("Force check update button", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.visit("/settings/applications");
    cy.intercept("GET", "/api/applications/force-check-update").as(
      "forceCheckUpdate",
    );
    cy.intercept("GET", "/api/applications*").as("getApplications");
  });
  it.skip("verify the content page", () => {
    //TODO: find a way to send info via websocket to have button available
    cy.getByDataTestId("forceCheckUpdateButton")
      .should("be.visible")
      .should("not.be.disabled");
    cy.getByDataTestId("forceCheckUpdateIcon").should("be.visible");
    cy.getByDataTestId("forceCheckUpdateButton").click();
    cy.getByDataTestId("forceCheckUpdateButton").should("be.disabled");
    cy.wait("@forceCheckUpdate").its("response.statusCode").should("eq", 200);
    cy.wait("@getApplications").its("response.statusCode").should("eq", 200);
    cy.getByDataTestId("forceCheckUpdateButton").should("not.be.disabled");
  });
});
