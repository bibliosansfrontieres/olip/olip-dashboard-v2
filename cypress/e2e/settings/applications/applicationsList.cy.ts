describe("Verify applications settings list", () => {
  beforeEach(() => {
    cy.loginAdmin();
    cy.intercept("/api/applications*", {
      fixture: "settings/applications/applicationsList",
    });
    cy.visit("/settings/applications");
  });
  it("verify the content page", () => {
    cy.getByDataTestId("ApplicationsSettingstitle").should("be.visible");
    cy.fixture("settings/applications/applicationsList").then(({ data }) => {
      cy.getByDataTestId("allApplicationTab")
        .should("be.visible")
        .and("contain.text", data.length);
      cy.getByDataTestId("installedApplicationTab")
        .should("be.visible")
        .and(
          "contain.text",
          data.filter((application) => application.isInstalled).length,
        );
      cy.getByDataTestId("notInstalledApplicationTab")
        .should("be.visible")
        .and(
          "contain.text",
          data.filter((application) => !application.isInstalled).length,
        );
    });
  });
  it("verify the list with the stubbed response", () => {
    cy.fixture("settings/applications/applicationsList").then(({ data }) => {
      cy.getByDataTestId("applicationCard")
        // *2 because we also see them in the other tabs : bsf and local
        .should("have.length", data.length * 2)
        .each(($el, index) => {
          cy.wrap($el).within(() => {
            if (index < data.length) {
              cy.getByDataTestId("applicationAvatar").should("be.visible");
              cy.getByDataTestId("applicationName")
                .should("be.visible")
                .should("contain.text", data[index].displayName);
              cy.getByDataTestId("applicationType")
                .should("be.visible")
                .should(
                  "contain.text",
                  data[index].applicationType.applicationTypeTranslations[0]
                    .label,
                );
              cy.getByDataTestId("applicationDescription")
                .should("be.visible")
                .should(
                  "contain.text",
                  data[index].applicationTranslations[0].shortDescription,
                );
              cy.getByDataTestId("applicationVisibility").should("be.visible");
              cy.getByDataTestId("applicationSize")
                .should("be.visible")
                .should(
                  "contain.text",
                  (Number(data[index].size) / 1024).toFixed(2),
                );

              if (index === 0) {
                // verify application olip
                cy.getByDataTestId("applicationOlip").should("be.visible");
              }
              if (index === 1) {
                // verify uninstalled application
                cy.getByDataTestId("installButton")
                  .should("be.visible")
                  .and("not.be.disabled");
              }
              if (index === 2) {
                // verify installed application
                cy.getByDataTestId("visibilityButton")
                  .should("be.visible")
                  .and("not.be.disabled");
                cy.getByDataTestId("uninstallButton")
                  .should("be.visible")
                  .and("not.be.disabled");
              }
            }
          });
        });
    });
  });
  it("verify the tabs", () => {
    cy.getByDataTestId("allApplicationTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
    cy.getByDataTestId("installedApplicationTab").click();
    cy.getByDataTestId("installedApplicationTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
    cy.getByDataTestId("notInstalledApplicationTab").click();
    cy.getByDataTestId("notInstalledApplicationTab")
      .should("be.visible")
      .and("have.attr", "aria-selected", "true");
  });
  describe("Verify the installed application ", () => {
    beforeEach(() => {
      cy.loginAdmin();
      cy.intercept("/api/applications*", {
        fixture: "settings/applications/applicationInstalled",
      }).as("getApplications");
      cy.fixture("settings/applications/applicationInstalled").then(
        ({ data }) => {
          cy.intercept(`api/applications/uninstall/${data[0].id}*`, {
            statusCode: 200,
          }).as("uninstallApplication");
        },
      );
      cy.visit("/settings/applications");
    });
    it("open the visibility modal", () => {
      // take only first because there is same element in the tab installed
      cy.getByDataTestId("applicationCard")
        .eq(0)
        .within(() => {
          cy.getByDataTestId("visibilityButton").click();
        });
      cy.getByDataTestId("visibilityModalForm").should("be.visible");
      cy.getByDataTestId("secondaryButtonOlipDialog").should("be.visible");
      cy.getByDataTestId("submitButton").should("be.visible");
    });
    it("uninstall the application", () => {
      // take only first because there is same element in the tab installed
      cy.getByDataTestId("applicationCard")
        .eq(0)
        .within(() => {
          // verify the modal layout
          cy.getByDataTestId("uninstallButton").click();
        });
      cy.getByDataTestId("applicationLogo").should("be.visible");
      cy.fixture("settings/applications/applicationInstalled").then(
        ({ data }) => {
          cy.getByDataTestId("applicationName")
            .should("be.visible")
            .and("contain.text", data[0].displayName);
        },
      );
      cy.getByDataTestId("uninstallModalQuestion").should("be.visible");
      cy.getByDataTestId("uninstallModalSubQuestion").should("be.visible");
      cy.getByDataTestId("uninstallModalCancel").should("be.visible");
      cy.getByDataTestId("uninstallModalConfirm")
        .should("be.visible")
        .and("not.be.disabled");

      // uninstall the application
      cy.getByDataTestId("uninstallModalConfirm").click();
      cy.wait("@uninstallApplication");
      cy.getByDataTestId("OLIPSnackbar").should("be.visible");
      cy.wait("@getApplications").its("response.statusCode").should("eq", 200);
    });
  });
  describe("Verify the uninstalled application ", () => {
    beforeEach(() => {
      cy.loginAdmin();
      cy.intercept("/api/applications*", {
        fixture: "settings/applications/applicationUninstalled",
      }).as("getApplications");
      cy.fixture("settings/applications/applicationUninstalled").then(
        ({ data }) => {
          cy.intercept(`api/applications/install/${data[0].id}*`, {
            statusCode: 200,
          }).as("installApplication");
        },
      );
      cy.visit("/settings/applications");
    });
    it("install the application", () => {
      // take only first because there is same element in the tab installed
      cy.getByDataTestId("applicationCard")
        .eq(0)
        .within(() => {
          // verify the modal layout
          cy.getByDataTestId("installButton").click();
        });
      cy.wait("@installApplication");
      cy.getByDataTestId("OLIPSnackbar").should("be.visible");
      cy.wait("@getApplications").its("response.statusCode").should("eq", 200);
    });
  });
});
