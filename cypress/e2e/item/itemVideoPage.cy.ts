describe("item (video) page", () => {
  beforeEach(() => {
    cy.intercept("api/items/1?*", {
      fixture: "items/itemVideo",
    });
    cy.intercept("api/items/suggestions/1?*", {
      fixture: "items/itemsSuggestion",
    });
    cy.visit("/item/1");
  });
  it("test the page", () => {
    cy.getByDataTestId("videoIcon").should("be.visible");
    cy.getByDataTestId("launchVideoButton").should("be.visible");
    cy.get("video")
      .should("be.visible")
      .invoke("attr", "src")
      .then((source) => {
        cy.fixture("items/itemVideo").then((video) => {
          expect(source).to.contain(video.file.path);
        });
      });
  });
});
