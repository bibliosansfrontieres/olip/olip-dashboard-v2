describe("Test the item page", () => {
  beforeEach(() => {
    cy.intercept("api/items/1?*", {
      fixture: "items/itemVideo",
    });
    cy.intercept("api/items/suggestions/1?*", {
      fixture: "items/itemsSuggestion",
    });
    cy.visit("/item/1");
  });
  it("test the item page layout", () => {
    //@TODO: test the type chip
    cy.fixture("items/itemVideo").then((item) => {
      cy.getByDataTestId("itemHeaderTitle")
        .should("be.visible")
        .should(
          "contain.text",
          item.dublinCoreItem.dublinCoreItemTranslations[0].title,
        );
      cy.getByDataTestId("itemHeaderDescription")
        .should("be.visible")
        .should(
          "contain.text",
          item.dublinCoreItem.dublinCoreItemTranslations[0].description,
        );
      cy.getByDataTestId("downloadButton")
        .should("be.visible")
        .should("contain.text", (item.file.size / 1024 / 1024).toFixed(2));
    });
  });
  it("test the discover more section", () => {
    cy.getByDataTestId("seeMoreItems").should("be.visible");
    cy.getByDataTestId("seeMoreItemsTitle").should("be.visible");
    cy.getByDataTestId("seeMoreItemsScrollLeft").should("be.visible");
    cy.getByDataTestId("seeMoreItemsScrollRight").should("be.visible");
    cy.fixture("items/itemsSuggestion").then((itemsSuggestion) => {
      cy.getByDataTestId("itemsCard")
        .should("be.visible")
        .should("have.length", itemsSuggestion.data.length);
      cy.getByDataTestId("itemsCard").each((itemCard, index) => {
        cy.wrap(itemCard).within(() => {
          cy.getByDataTestId("itemCardTitle").should(
            "contain.text",
            itemsSuggestion.data[index].dublinCoreItem
              .dublinCoreItemTranslations[0].title,
          );
        });
      });
    });
  });
});
