Cypress.Commands.add(
  "getByDataTestId",
  (dataTestId: string, otherSelector?: string, options?: any) => {
    return cy.get(
      `[data-testid="${dataTestId}"] ${otherSelector ?? ""}`,
      options,
    );
  },
);

Cypress.Commands.add(
  "filterCatalog",
  (index: number, filterUrl: string, filterRequest: string) => {
    cy.getByDataTestId("olipSelect").eq(index).click();
    cy.getByDataTestId("menuItem")
      .should("be.visible")
      .first()
      .invoke("attr", "data-value")
      .then((val) => {
        cy.intercept(`categories?${filterUrl}=${val}*`).as(
          `categories${index}`,
        );
      });
    cy.getByDataTestId("menuItem")
      .first()
      .click()
      .invoke("attr", "data-value")
      .then((val) => {
        cy.url().should("contain", `?${filterRequest}=${val}`);
        cy.wait(`@categories${index}`);
      });
  },
);

Cypress.Commands.add("login", (username, password) => {
  cy.session(username, () => {
    cy.request("POST", `${Cypress.env("apiUrl")}api/auth/signin`, {
      username: username,
      password: password,
    }).then((response) => {
      window.localStorage.setItem("accessToken", response.body.accessToken);
    });
  });
});

Cypress.Commands.add("loginWithoutPassword", (username) => {
  cy.session(username, () => {
    cy.request("POST", `${Cypress.env("apiUrl")}api/auth/signin`, {
      username: username,
    }).then((response) => {
      window.localStorage.setItem("accessToken", response.body.accessToken);
    });
  });
});

Cypress.Commands.add("loginAdmin", () => {
  cy.login(Cypress.env("adminUsername"), Cypress.env("adminPassword"));
});

Cypress.Commands.add("deleteLastUser", () => {
  const token = window.localStorage.getItem("accessToken");
  cy.request({
    method: "DELETE",
    url: `${Cypress.env("apiUrl")}api/users/${Cypress.env("users")[0].id}`,
    auth: { bearer: token },
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});

Cypress.Commands.add("assertUserNotFound", () => {
  const token = window.localStorage.getItem("accessToken");
  cy.request({
    method: "GET",
    url: `${Cypress.env("apiUrl")}api/users/${Cypress.env("users")[0].id}`,
    auth: { bearer: token },
    failOnStatusCode: false,
  }).then((response) => {
    expect(response.status).to.eq(404);
  });
});

Cypress.Commands.add("createUserAsAdmin", (user) => {
  cy.loginAdmin();
  cy.createUser(user);
});

Cypress.Commands.add("deleteLastUserAsAdmin", () => {
  cy.loginAdmin();
  cy.deleteLastUser();
  cy.assertUserNotFound();
});
Cypress.Commands.add("createUser", (user) => {
  const body = {
    username: user?.username ?? "username",
    language: user.language ?? "fra",
  };
  if (user.password) {
    body["roleId"] = user?.roleId ?? 3;
  } else if (!user?.password) {
    body["roleId"] = user?.roleId ?? 4;
  }
  if ((!user?.password && user?.roleId !== 4) || user?.password) {
    body["password"] = user?.password ?? "password";
  }
  const token = window.localStorage.getItem("accessToken");
  cy.request({
    method: "POST",
    url: `${Cypress.env("apiUrl")}api/users`,
    auth: { bearer: token },
    body: body,
  }).then((response) => {
    Cypress.env("users").push(response.body);
  });
});
