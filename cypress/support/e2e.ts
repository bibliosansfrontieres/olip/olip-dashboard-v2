// ***********************************************************
// This example support/e2e.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";

// Alternatively you can use CommonJS syntax:
// require('./commands')
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface createUserObject {
      username?: string;
      password?: string;
      language?: string;
      roleId?: number;
    }
    interface Chainable {
      getByDataTestId(
        dataTestId: string,
        otherSelector?: string,
        options?: any,
      ): Chainable<JQuery<HTMLElement>>;
      filterCatalog(
        index: number,
        filterUrl: string,
        filterRequest: string,
      ): Chainable<JQuery<HTMLElement>>;
      // loginCached(): Chainable<void>;
      login(username: string, password: string): Chainable<void>;
      loginWithoutPassword(username: string): Chainable<void>;
      deleteLastUser(): Chainable<void>;
      assertUserNotFound(): Chainable<void>;
      createUser(user: createUserObject): Chainable<void>;
      createUserAsAdmin(user: createUserObject): Chainable<void>;
      loginAdmin(): Chainable<void>;
      deleteLastUserAsAdmin(): Chainable<void>;
    }
  }
}
